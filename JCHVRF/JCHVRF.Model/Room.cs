﻿//********************************************************************
// 文件名: Room.cs
// 描述: 定义 VRF 项目中的房间类
// 作者: clh
// 创建时间: 2012-04-01
// 修改历史: 
// 2016-1-29 迁入JCHVRF
//********************************************************************

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace JCHVRF.Model
{
    [Serializable]
    public class Room : INotifyPropertyChanged
    {
        public Room() { }
        public Room(int number)
        {
            this._no = number;
            this._area = 30;
            this._loadIndexCool = 170;
            this._rqCapacityCool = 5.1;
            this._loadIndexHeat = 150;
            this._rqCapacityHeat = 4.5;            
            this._peopleNumber = 6;
            this._freshAirIndex = 0.5;    // m3/h时为30，m3/min时为0.5，默认为m3/min
            this._freshAir = 3;               // _peopleNumber * _freshAirIndex  m3/h时为180
        }

        private string _id;
        /// <summary>
        /// 唯一编号
        /// </summary>
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _no;
        /// <summary>
        /// 递增编号，自动生成
        /// </summary>
        public int NO
        {
            get { return _no; }
            set { _no = value; }
        }

        private string _name;
        /// <summary>
        /// 房间自定义名称
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _location;
        /// <summary>
        /// 房间所在的城市，add on 20130812 clh
        /// </summary>
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        private string _type;
        /// <summary>
        /// 房间类型
        /// </summary>
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private double _area;
        /// <summary>
        /// 房间面积m^2 (默认值30)
        /// </summary>
        public double Area
        {
            get { return _area; }
            set { _area = value; }
        }

        private double _loadIndexCool;
        /// <summary>
        /// 冷指标w/m^2
        /// </summary>
        public double LoadIndexCool
        {
            get { return _loadIndexCool; }
            set { _loadIndexCool = value; }
        }

        private double _loadIndexHeat;
        /// <summary>
        /// 热指标w/m^2
        /// </summary>
        public double LoadIndexHeat
        {
            get { return _loadIndexHeat; }
            set { _loadIndexHeat = value; }
        }

        private double _rqCapacityCool;
        /// <summary>
        /// 冷负荷需求kw
        /// </summary>
        public double RqCapacityCool
        {
            get { return _rqCapacityCool; }
            set { _rqCapacityCool = value; }
        }

        private double _rqCapacityHeat;
        /// <summary>
        /// 冷负荷需求kw
        /// </summary>
        public double RqCapacityHeat
        {
            get { return _rqCapacityHeat; }
            set { _rqCapacityHeat = value; }
        }

        private double _sensibleHeat;
        /// <summary>
        /// 显热 Sensible Heat (kW)
        /// </summary>
        public double SensibleHeat
        {
            get { return _sensibleHeat; }
            set { _sensibleHeat = value; }
        }

        private double _airFlow;
        /// <summary>
        /// 风量 Air Flow (m3/h)
        /// </summary>
        public double AirFlow
        {
            get { return _airFlow; }
            set { _airFlow = value; }
        }

        private double _staticPressure;
        /// <summary>
        /// 静压 Static Pressure (Pa)
        /// </summary>
        public double StaticPressure
        {
            get { return _staticPressure; }
            set { _staticPressure = value; }
        }

        private int _peopleNumber;
        /// <summary>
        /// 房间人数
        /// </summary>
        public int PeopleNumber
        {
            get { return _peopleNumber; }
            set { _peopleNumber = value; }
        }

        private bool _isSensibleHeatEnable;
        /// <summary>
        /// 显热值是否有效
        /// </summary>
        public bool IsSensibleHeatEnable
        {
            get { return _isSensibleHeatEnable; }
            set { _isSensibleHeatEnable = value; }
        }

        private bool _isAirFlowEnable;
        /// <summary>
        /// 风量值是否有效
        /// </summary>
        public bool IsAirFlowEnable
        {
            get { return _isAirFlowEnable; }
            set { _isAirFlowEnable = value; }
        }

        private bool _isStaticPressureEnable;
        /// <summary>
        /// 静压值是否有效
        /// </summary>
        public bool IsStaticPressureEnable
        {
            get { return _isStaticPressureEnable; }
            set { _isStaticPressureEnable = value; }
        }

        private double _freshAirIndex;
        /// <summary>
        /// 新风指标
        /// </summary>
        public double FreshAirIndex
        {
            get { return _freshAirIndex; }
            set { _freshAirIndex = value; }
        }

        private double _freshAir;
        /// <summary>
        /// 新风风量需求
        /// </summary>
        public double FreshAir
        {
            get { return _freshAir; }
            set { _freshAir = value; }
        }
        //end 20120313 

        private string _freshAirAreaId;
        /// <summary>
        /// 所属的新风区域的ID
        /// </summary>
        public string FreshAirAreaId
        {
            get { return _freshAirAreaId; }
            set { _freshAirAreaId = value; }
        }

        //New Properties added for Add/Edit Room start
        private double _CoolingDryBulb;

        public double CoolingDryBulb
        {
            get { return _CoolingDryBulb; }
            set { _CoolingDryBulb = value; }
        }

        private double _CoolingWetBulb;

        public double CoolingWetBulb
        {
            get { return _CoolingWetBulb; }
            set { _CoolingWetBulb = value; }
        }

        private double _CoolingRelativeHumidity;

        public double CoolingRelativeHumidity
        {
            get { return _CoolingRelativeHumidity; }
            set { _CoolingRelativeHumidity = value; }
        }


        private double _HeatingDryBulb;

        public double HeatingDryBulb
        {
            get { return _HeatingDryBulb; }
            set { _HeatingDryBulb = value; }
        }

        private bool _IsRoomChecked = false;
        public bool IsRoomChecked
        {
            get
            { return _IsRoomChecked; }
            set
            {
                _IsRoomChecked = value;
                OnChanged("IsRoomChecked");
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnChanged(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        //New Properties added for Add/Edit Room end
    }

}
