﻿//********************************************************************
// 文件名: Accessory.cs
// 描述: 定义 VRF 项目中的附件类，室内机的可选附件
// 作者: clh
// 创建时间: 2016-2-3
// 修改历史: 
//********************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JCHVRF.Model
{
    [Serializable]
    public class Accessory
    {
        string _factoryCode;
        /// <summary>
        /// 工厂代码, 此处的代码与室内机的工厂代码不全一致
        /// </summary>
        public string FactoryCode
        {
            get { return _factoryCode; }
            set { _factoryCode = value; }
        }

        string _brandCode;
        /// <summary>
        /// 品牌代码
        /// </summary>
        public string BrandCode
        {
            get { return _brandCode; }
            set { _brandCode = value; }
        }

        string _unitType;
        /// <summary>
        /// 室内机产品类型
        /// </summary>
        public string UnitType
        {
            get { return _unitType; }
            set { _unitType = value; }
        }

        double _minCapacity;
        /// <summary>
        /// 最小制冷量数值 kW
        /// </summary>
        public double MinCapacity
        {
            get { return _minCapacity; }
            set { _minCapacity = value; }
        }

        double _maxCapacity;
        /// <summary>
        /// 最大制冷量数值 kW
        /// </summary>
        public double MaxCapacity
        {
            get { return _maxCapacity; }
            set { _maxCapacity = value; }
        }

        string _type;
        /// <summary>
        /// 附件类型
        /// </summary>
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        string _model_York;
        /// <summary>
        /// 附件型号名称(York)
        /// </summary>
        public string Model_York
        {
            get { return _model_York; }
            set { _model_York = value; }
        }

        string _model_Hitachi;
        /// <summary>
        /// 附件型号名称(Hitachi)
        /// </summary>
        public string Model_Hitachi
        {
            get { return _model_Hitachi; }
            set { _model_Hitachi = value; }
        }

        int _maxNumber;
        /// <summary>
        /// 最大可选数量
        /// </summary>
        public int MaxNumber
        {
            get { return _maxNumber; }
            set { _maxNumber = value; }
        }

        bool _isDefault;
        /// <summary>
        /// 是否默认自带
        /// </summary>
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }


        bool _isShared;
        /// <summary>
        /// Remote Control Switch 是否共享 on 2017-11-16 by xyj
        /// </summary>
        public bool IsShared
        {
            get { return _isShared; }
            set { _isShared = value; }
        }
    }
}
