﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JCHVRF.Model.New
{
    [Serializable]
    public class Creator
    {
        private int _id;
        public int Id
        {
            get
            {

                return _id;
            }
            set
            {
                _id = value;
            }
        }

        private string _companyName;
        public string CompanyName
        {
            get
            {

                return _companyName;
            }
            set
            {
                _companyName = value;
            }
        }

        private string _streetAddress;
        public string StreetAddress
        {
            get
            {
                return _streetAddress;
            }
            set
            {
                _streetAddress = value;
            }
        }
        private string _suburb;
        public string Suburb
        {
            get
            {
                return _suburb;
            }
            set
            {
                _suburb = value;
            }
        }

        private string _townCity;
        public string TownCity
        {
            get
            {
                return _townCity;
            }
            set
            {
                _townCity = value;
            }
        }
        //mandetory
        private string _country;
        public string Country
        {
            get
            {
                return _country;
            }
            set
            {
                _country = value;
            }
        }
        private string _gpsPosition;
        public string GpsPosition
        {
            get
            {
                return _gpsPosition;
            }
            set
            {
                _gpsPosition = value;
            }
        }
        //mandetory
        private string _contactName;
        public string ContactName
        {
            get
            {
                return _contactName;
            }
            set
            {
                _contactName = value;
            }
        }
        //mandetory
        private string _phone;
        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
            }
        }
        //mandetory
        private string _contactEmail;
        public string ContactEmail
        {
            get
            {
                return _contactEmail;
            }
            set
            {
                _contactEmail = value;
            }
        }

        private string _idNumber;
        public string IdNumber
        {
            get
            {
                return _idNumber;
            }
            set
            {
                _idNumber = value;
            }
        }
    }
}
