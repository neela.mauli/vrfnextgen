﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JCHVRF.Model
{
    [Serializable]
    public class Outdoor
    {
        #region 构造函数
        public Outdoor() { }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="model"> 完整型号 </param>
        public Outdoor(string model)
        {
            this._model = model;
        }
        #endregion

        #region 字段

        private string _regionCode;
        /// <summary>
        /// 区域
        /// </summary>
        public string RegionCode
        {
            get { return _regionCode; }
            set { _regionCode = value; }
        }

        private string _productType;
        /// <summary>
        /// 产品类型
        /// </summary>
        public string ProductType
        {
            get { return _productType; }
            set { _productType = value; }
        }


        private int _maxRatio;
        /// <summary>
        /// 产品类型
        /// </summary>
        public int MaxRatio
        {
            get { return _maxRatio; }
            set { _maxRatio = value; }
        }


        private string _model;
        /// <summary>
        /// 型号 --ShortName
        /// </summary>
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private string _modelFull;
        /// <summary>
        /// 型号名 Full Name
        /// </summary>
        public string ModelFull
        {
            get { return _modelFull; }
            set { _modelFull = value; }
        }

        private string _model_York;
        /// <summary>
        /// 型号 --ShortName
        /// </summary>
        public string Model_York
        {
            get { return _model_York; }
            set { _model_York = value; }
        }

        private string _model_Hitachi;
        /// <summary>
        /// 型号 --ShortName
        /// </summary>
        public string Model_Hitachi
        {
            get { return _model_Hitachi; }
            set { _model_Hitachi = value; }
        }
        
        private string _auxModelName;
        /// <summary>
        /// 辅助型号名 —— 新增 20130910
        /// </summary>
        public string AuxModelName
        {
            get { return _auxModelName; }
            set { _auxModelName = value; }
        }

        private double _coolingCapacity;    //kw
        /// <summary>
        /// 制冷量
        /// </summary>
        public double CoolingCapacity
        {
            get { return _coolingCapacity; }
            set { _coolingCapacity = value; }
        }

        private double _heatingCapacity;    //kw
        /// <summary>
        /// 制热量
        /// </summary>
        public double HeatingCapacity
        {
            get { return _heatingCapacity; }
            set { _heatingCapacity = value; }
        }

        private string _length;             //mm
        public string Length
        {
            get { return _length; }
            set { _length = value; }
        }

        private string _width;              //mm
        public string Width
        {
            get { return _width; }
            set { _width = value; }
        }

        private string _height;             //mm
        public string Height
        {
            get { return _height; }
            set { _height = value; }
        }

        private string _gasPipe_Hi;            //mm
        /// <summary>
        /// 气管管长（高压或默认）
        /// </summary>
        public string GasPipe_Hi
        {
            get { return _gasPipe_Hi; }
            set { _gasPipe_Hi = value; }
        }

        private string _gasPipe_Lo;            //mm
        /// <summary>
        /// 气管管长(低压)
        /// </summary>
        public string GasPipe_Lo
        {
            get { return _gasPipe_Lo; }
            set { _gasPipe_Lo = value; }
        }

        private string _liquidPipe;         //mm
        /// <summary>
        /// 液管管长
        /// </summary>
        public string LiquidPipe
        {
            get { return _liquidPipe; }
            set { _liquidPipe = value; }
        }

        private double _airFlow;            //m3/h
        /// <summary>
        /// 风量
        /// </summary>
        public double AirFlow
        {
            get { return _airFlow; }
            set { _airFlow = value; }
        }

        private double _power_Cooling;         //kw
        /// <summary>
        /// 输入功率（PI）
        /// </summary>
        public double Power_Cooling
        {
            get { return _power_Cooling; }
            set { _power_Cooling = value; }
        }

        private double _power_Heating;         //kw
        /// <summary>
        /// 输入功率（PI）
        /// </summary>
        public double Power_Heating
        {
            get { return _power_Heating; }
            set { _power_Heating = value; }
        }

        private double _maxCurrent;
        /// <summary>
        /// 最大电流值，替换原来的RatedCurrent
        /// </summary>
        public double MaxCurrent
        {
            get { return _maxCurrent; }
            set { _maxCurrent = value; }
        }

        private double _MCCB;               //A
        /// <summary>
        /// 塑料外壳式断路器(MCCB) 电流
        /// </summary>
        public double MCCB
        {
            get { return _MCCB; }
            set { _MCCB = value; }
        }

        private double _weight;             //kg
        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private double _noiseLevel;         //dBA
        /// <summary>
        /// 噪音级别
        /// </summary>
        public double NoiseLevel
        {
            get { return _noiseLevel; }
            set { _noiseLevel = value; }
        }

        private double _maxRefrigerantCharge;
        /// <summary>
        /// 最大冷媒充注量
        /// </summary>
        public double MaxRefrigerantCharge
        {
            get { return _maxRefrigerantCharge; }
            set { _maxRefrigerantCharge = value; }
        }

        private double _refrigerantCharge;   //kg
        /// <summary>
        /// 冷媒充注量
        /// </summary>
        public double RefrigerantCharge
        {
            get { return _refrigerantCharge; }
            set { _refrigerantCharge = value; }
        }

        private int _maxIU;
        /// <summary>
        /// 最大室内机连接数量
        /// </summary>
        public int MaxIU
        {
            get { return _maxIU; }
            set { _maxIU = value; }
        }

        private int _recommendedIU;
        /// <summary>
        /// 推荐室内机连接数量
        /// </summary>
        public int RecommendedIU
        {
            get { return _recommendedIU; }
            set { _recommendedIU = value; }
        }

        //private double _ESP;                //Pa
        ///// <summary>
        ///// 机外静压
        ///// </summary>
        //public double ESP
        //{
        //    get { return _ESP; }
        //    set { _ESP = value; }
        //}

        private string _type;
        /// <summary>
        /// 室外机类型，YVOH(Horizontal.../Top...)
        /// </summary>
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private double _price;
        /// <summary>
        /// 价格
        /// </summary>
        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private string _typeImage;
        /// <summary>
        /// 类型图片文件名（不带路径）
        /// </summary>
        public string TypeImage
        {
            get { return _typeImage; }
            set { _typeImage = value; }
        }

        //private string _powerSupply;
        ///// <summary>
        ///// 电源功率需求
        ///// </summary>
        //public string PowerSupply
        //{
        //    get { return _powerSupply; }
        //    set { _powerSupply = value; }
        //}

        private string _fullModuleName;
        /// <summary>
        /// 室外机组合机组
        /// </summary>
        public string FullModuleName
        {
            get { return _fullModuleName; }
            set { _fullModuleName = value; }
        }


        // add on 20140429 clh

        private double _MaxPipeLength;
        /// <summary>
        /// 系统允许的最大的配管实际长度，从标准室外机表直接获取
        /// </summary>
        public double MaxPipeLength
        {
            get { return _MaxPipeLength; }
            set { _MaxPipeLength = value; }
        }

        private double _MaxEqPipeLength;
        /// <summary>
        /// 系统允许的最大的配管等效管长度，从标准室外机表直接获取
        /// </summary>
        public double MaxEqPipeLength
        {
            get { return _MaxEqPipeLength; }
            set { _MaxEqPipeLength = value; }
        }


        private double _MaxOutdoorAboveHeight;
        /// <summary>
        /// 室外机在室内机上方时，系统允许的最大高度差，从标准室外机表直接获取
        /// </summary>
        public double MaxOutdoorAboveHeight
        {
            get { return _MaxOutdoorAboveHeight; }
            set { _MaxOutdoorAboveHeight = value; }
        }

        private double _MaxOutdoorBelowHeight;
        /// <summary>
        /// 室外机在室内机下方时，系统允许的最大高度差，从标准室外机表直接获取
        /// </summary>
        public double MaxOutdoorBelowHeight
        {
            get { return _MaxOutdoorBelowHeight; }
            set { _MaxOutdoorBelowHeight = value; }
        }

        private double _MaxDiffIndoorHeight;
        /// <summary>
        /// 系统允许的室内机最大高度差，从标准室外机表直接获取
        /// </summary>
        public double MaxDiffIndoorHeight
        {
            get { return _MaxDiffIndoorHeight; }
            set { _MaxDiffIndoorHeight = value; }
        }

        private double _MaxIndoorLength;
        /// <summary>
        /// 系统允许的从第一分歧管到最远端室外机的最长距离，从标准室外机表直接获取
        /// </summary>
        public double MaxIndoorLength
        {
            get { return _MaxIndoorLength; }
            set { _MaxIndoorLength = value; }
        }

        private double _maxIndoorLength_MaxIU;
        /// <summary>
        /// 系统允许的从第一分歧管到最远端室外机的最长距离，当连接的室内机数量大于推荐数量时的取值
        /// </summary>
        public double MaxIndoorLength_MaxIU
        {
            get { return _maxIndoorLength_MaxIU; }
            set { _maxIndoorLength_MaxIU = value; }
        }

        private double _MaxPipeLengthwithFA;
        /// <summary>
        /// 一对一新风机组时，最大的等效果长度，从标准室外机表直接获取
        /// </summary>
        public double MaxPipeLengthwithFA
        {
            get { return _MaxPipeLengthwithFA; }
            set { _MaxPipeLengthwithFA = value; }
        }

        private double _MaxDiffIndoorLength;
        /// <summary>
        /// 系统允许的从第一分歧管到最远端室内机与第一分歧管到最近室内机的距离差，从标准室外机表直接获取
        /// </summary>
        public double MaxDiffIndoorLength
        {
            get { return _MaxDiffIndoorLength; }
            set { _MaxDiffIndoorLength = value; }
        }

        private double _maxDiffIndoorLength_MaxIU;
        /// <summary>
        /// 系统允许的从第一分歧管到最远端室内机与第一分歧管到最近室内机的距离差，当连接的室内机数量大于推荐数量时的取值
        /// </summary>
        public double MaxDiffIndoorLength_MaxIU
        {
            get { return _maxDiffIndoorLength_MaxIU; }
            set { _maxDiffIndoorLength_MaxIU = value; }
        }

        // add on 20150128 clh 组合室外机内部分歧管数据
        private string _jointKitModelL;
        /// <summary>
        /// 分歧管型号，液管（若有多个，则用斜杠分隔）
        /// </summary>
        public string JointKitModelL
        {
            get { return _jointKitModelL; }
            set { _jointKitModelL = value; }
        }

        private string _jointKitPriceL;
        /// <summary>
        /// 分歧管价格，液管（若有多个，则为价格之和）
        /// </summary>
        public string JointKitPriceL
        {
            get { return _jointKitPriceL; }
            set { _jointKitPriceL = value; }
        }

        private string _jointKitModelG;
        /// <summary>
        /// 分歧管型号，气管（若有多个，则用斜杠分隔）
        /// </summary>
        public string JointKitModelG
        {
            get { return _jointKitModelG; }
            set { _jointKitModelG = value; }
        }

        private string _jointKitPriceG;
        /// <summary>
        /// 分歧管价格，气管（若有多个，则为价格之和）
        /// </summary>
        public string JointKitPriceG
        {
            get { return _jointKitPriceG; }
            set { _jointKitPriceG = value; }
        }
        
        private string _maxOperationPI_Cooling;
        /// <summary>
        /// 最大输入功率--制冷工况
        /// </summary>
        public string MaxOperationPI_Cooling
        {
            get { return _maxOperationPI_Cooling; }
            set { _maxOperationPI_Cooling = value; }
        }

        private string _maxOperationPI_Heating;
        /// <summary>
        /// 最大输入功率--制热工况
        /// </summary>
        public string MaxOperationPI_Heating
        {
            get { return _maxOperationPI_Heating; }
            set { _maxOperationPI_Heating = value; }
        }


        private string _partLoadTableName;
        /// <summary>
        /// 对应的容量表的表名
        /// </summary>
        public string PartLoadTableName
        {
            get { return _partLoadTableName; }
            set { _partLoadTableName = value; }
        }

        #region 液管最大长度计算及L3验证所需属性 add on 20160515 by Yunxiao Lin
        private double _maxTotalPipeLength;
        /// <summary>
        /// 内机数量小于等于推荐值时的液管长度和上限
        /// </summary>
        public double MaxTotalPipeLength
        {
            get { return _maxTotalPipeLength; }
            set { _maxTotalPipeLength = value; }
        }

        private double _maxTotalPipeLength_MaxIU;
        /// <summary>
        /// 内机数量大于推荐值，小于等于允许最大值时的液管长度和上限
        /// </summary>
        public double MaxTotalPipeLength_MaxIU
        {
            get { return _maxTotalPipeLength_MaxIU; }
            set { _maxTotalPipeLength_MaxIU = value; }
        }

        private double _maxMKIndoorPipeLength;
        /// <summary>
        /// 内机数量小于等于推荐值时，每个Multi_kit到每个IDU的最大长度上限
        /// </summary>
        public double MaxMKIndoorPipeLength
        {
            get { return _maxMKIndoorPipeLength; }
            set { _maxMKIndoorPipeLength = value; }
        }

        private double _maxMKIndoorPipeLength_MaxIU;
        /// <summary>
        /// 内机数量大于推荐值小于等于允许最大值时，每个Multi_kit到每个IDU的最大长度上限
        /// </summary>
        public double MaxMKIndoorPipeLength_MaxIU
        {
            get { return _maxMKIndoorPipeLength_MaxIU; }
            set { _maxMKIndoorPipeLength_MaxIU = value; }
        }

        private string _series;
        /// <summary>
        /// 系列              add on 20161027
        /// </summary>
        public string Series
        {
            get { return _series; }
            set { _series = value; }
        }

        private double _horsepower;
        /// 匹数 add on 20161109 by Yunxiao Lin
        /// <summary>
        /// 匹数
        /// </summary>
        public double Horsepower
        {
            get { return _horsepower; }
            set { _horsepower = value; }
        }

        #endregion

        #endregion

        private double _minRefrigerantCharge;
        /// <summary>
        /// 最小冷媒充注量
        /// </summary>
        public double MinRefrigerantCharge
        {
            get { return _minRefrigerantCharge; }
            set { _minRefrigerantCharge = value; }
        }

        #region 新增能效比参数 add by axj 20180504

        private double _eer;
        /// <summary>
        /// EER
        /// </summary>
        public double EER
        {
            get { return _eer; }
            set { _eer = value; }
        }

        private double _cop;
        /// <summary>
        /// COP
        /// </summary>
        public double COP
        {
            get { return _cop; }
            set { _cop = value; }
        }

        private double _seer;
        /// <summary>
        /// SEER
        /// </summary>
        public double SEER
        {
            get { return _seer; }
            set { _seer = value; }
        }
        private double _scop;
        /// <summary>
        /// SCOP
        /// </summary>
        public double SCOP
        {
            get { return _scop; }
            set { _scop = value; }
        }
        private double _soudPower;
        /// <summary>
        /// SoundPower
        /// </summary>
        public double SoundPower
        {
            get { return _soudPower; }
            set { _soudPower = value; }
        }
        #endregion

    }
}
