﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JCBase.UI;
using JCHVRF.Model;
using JCBase.Utility;

namespace JCHVRF.BLL
{
    public class CommonBLL
    {
        #region 判断当前长度单位是否为公英制

        //// 判断当前长度单位是否为英制
        ///// <summary>
        ///// 判断当前长度单位是否为英制,
        ///// </summary>
        ///// <returns></returns>
        //public static bool IsLength_ft()
        //{
        //    return SystemSetting.UserSetting.unitsSetting.settingLENGTH == Unit.ut_Size_ft;
        //}

        /// 判断当前管径单位是否为英制
        /// <summary>
        /// 判断当前管径单位是否为英制
        /// </summary>
        /// <returns></returns>
        public static bool IsDimension_inch()
        {
            return SystemSetting.UserSetting.unitsSetting.settingDimension == Unit.ut_Dimension_inch;
        }
        #endregion

        #region 报告相关

        /// <summary>
        /// 报告中是否显示价格信息，ver1.20版本暂放开出口区域的价格
        /// </summary>
        /// <returns></returns>
        public static bool ShowPrice()
        {
            CDL.SVRFreg sv = new CDL.SVRFreg();
            sv = CDL.Sec.GetSVRF();
            //if ((!IsDearler && IsPriceValid) || IsSuperUser)
            if (sv.PriceValid)
                return true;
            return false;
        }

        #endregion


        #region 判断当前界面语言
        // 判断当前界面的语言是否是中文
        /// <summary>
        /// 判断当前界面的语言是否是中文
        /// </summary>
        /// <returns></returns>
        public static bool IsChinese()
        {
            return LangType.CurrentLanguage == LangType.CHINESE;
        }

        // 判断当前界面的语言是否是英文
        /// <summary>
        /// 判断当前界面的语言是否是英文
        /// </summary>
        /// <returns></returns>
        public static bool IsEnglish()
        {
            return LangType.CurrentLanguage == LangType.ENGLISH;
        }


        // 判断当前界面的语言是否是法文
        /// <summary>
        /// 判断当前界面的语言是否是法文
        /// </summary>
        /// <returns></returns>
        public static bool IsFrance()
        {
            return LangType.CurrentLanguage == LangType.FRENCH;
        }


        // 判断当前界面的语言是否是西班牙
        /// <summary>
        /// 判断当前界面的语言是否是西班牙
        /// </summary>
        /// <returns></returns>
        public static bool IsSpain()
        {
            return LangType.CurrentLanguage == LangType.TURKISH;
        }


        // 判断当前界面的语言是否是土耳其
        /// <summary>
        /// 判断当前界面的语言是否是土耳其
        /// </summary>
        /// <returns></returns>
        public static bool IsTurkey()
        {
            return LangType.CurrentLanguage == LangType.TURKISH;
        }


        #endregion


        /// <summary>
        /// 检验房间内的室内机是否完全满足房间需求 --室外机选型后调用
        /// </summary>
        /// <param name="ri"></param>
        /// <param name="thisProject"></param>
        /// <returns></returns>
        public static bool FullMeetRoomRequired(RoomIndoor ri, Project thisProject)
        {
            string wType;
            return MeetRoomRequired(ri, thisProject, 0, thisProject.RoomIndoorList, out wType);
        }

        /// <summary>
        /// 检验房间内的室内机是否满足有公差的房间需求 --室外机选型后调用
        /// </summary>
        /// <param name="ri"></param>
        /// <param name="thisProject"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool MeetRoomRequired(RoomIndoor ri, Project thisProject, double tolerance, List<RoomIndoor> RoomIndoorList, out string wType)
        {
            wType = "";
            if (ri.IndoorItem == null || thisProject == null)
                return false;
            if (!ri.IsFreshAirArea) //新风区域只有新风量需求，没有制冷，制热和显热需求，所以新风区域不需要比较
            {
                Room r = null;
                if (thisProject.FloorList != null)
                {
                    foreach (Floor floor in thisProject.FloorList)
                    {
                        if (floor.RoomList != null)
                        {
                            foreach (Room r_temp in floor.RoomList)
                            {
                                if (r_temp.Id == ri.RoomID)
                                {
                                    r = r_temp;
                                    break;
                                }
                            }
                        }
                        if (r != null)
                            break;
                    }
                }

                if (r == null)
                {
                    if (!ri.IsAuto) //手动非房间选型不需要检验需求
                        return true;
                }
                double coolcap = 0;
                double heatcap = 0;
                double sh = 0;
                double rqcoolcap = ri.RqCoolingCapacity;
                double rqheatcap = ri.RqHeatingCapacity;
                double rqsh = ri.RqSensibleHeat;
                foreach (RoomIndoor ri_temp in RoomIndoorList)
                {
                    if (ri_temp.IsFreshAirArea == ri.IsFreshAirArea && ri_temp.RoomID == ri.RoomID)
                    {
                        coolcap += ri_temp.ActualCoolingCapacity;
                        heatcap += ri_temp.ActualHeatingCapacity;
                        sh += ri_temp.ActualSensibleHeat;
                    }
                }
                //校验制冷容量
                if (thisProject.IsCoolingModeEffective)
                {
                    if (coolcap < rqcoolcap * (1 - tolerance))
                    {
                        wType = "reqCool";
                        return false;
                    }
                }
                //校验制热容量
                if (thisProject.IsHeatingModeEffective && !ri.IndoorItem.ProductType.Contains(", CO"))
                {
                    if (heatcap < rqheatcap * (1 - tolerance))
                    {
                        wType = "reqHeat";
                        return false;
                    }
                }
                //校验显热
                if (r != null && !r.IsSensibleHeatEnable)
                    return true;
                if (sh < rqsh * (1 - tolerance))
                {
                    wType = "sensible";
                    return false;
                }
            }
            return true;
        }
    }
}
