﻿using JCHVRF.BLL;
using JCHVRF.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JCHVRF_New
{
    /// <summary>
    /// Interaction logic for Region.xaml
    /// </summary>
    public partial class Region : Window
    {
        
        RegionDAL objRegionDal = new RegionDAL();
        RegionBLL objRegionBll = new RegionBLL();
        public Region()
        {
            InitializeComponent();
            BindRegion();
        }
        //
        public void BindRegion()
        {

            DataTable objtbl = objRegionDal.GetParentRegionTable();
            CmbRegion.ItemsSource = objtbl.DefaultView;
            CmbRegion.DisplayMemberPath = objtbl.Columns["Region"].ToString();
            CmbRegion.SelectedValuePath = objtbl.Columns["Code"].ToString();

        }

        private void CmbRegion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            DataTable dt = objRegionBll.GetSubRegionList(this.CmbRegion.SelectedValue.ToString());
            CmbSubRegion.ItemsSource = dt.DefaultView;
            CmbSubRegion.DisplayMemberPath = dt.Columns["Region"].ToString();
            CmbSubRegion.SelectedValuePath = dt.Columns["Code"].ToString();

        }
    }
}
