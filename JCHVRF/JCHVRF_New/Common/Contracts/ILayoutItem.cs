﻿/****************************** File Header ******************************\
File Name:	ILayoutItem.cs
Date Created:	2/25/2019
Description:	
\*************************************************************************/

namespace JCHVRF_New.Common.Contracts
{
    #region Interfaces

    public interface ILayoutItem
    {
        #region Properties

        //
        // Summary:
        //     Gets or sets the height that will be initially used when the content is dragged
        //     and then displayed in a floating window.
        /// <summary>
        /// Gets or sets the FloatingHeight
        /// </summary>
        double FloatingHeight { get; set; }

        //
        // Summary:
        //     Gets or sets the left edge of a floating window that will contain this content.
        /// <summary>
        /// Gets or sets the FloatingLeft
        /// </summary>
        double FloatingLeft { get; set; }

        //
        // Summary:
        //     Gets or sets the top edge of a floating window that will contain this content.
        /// <summary>
        /// Gets or sets the FloatingTop
        /// </summary>
        double FloatingTop { get; set; }

        //
        // Summary:
        //     Gets or sets the width that will be initially used when the content is dragged
        //     and then displayed in a floating window.
        /// <summary>
        /// Gets or sets the FloatingWidth
        /// </summary>
        double FloatingWidth { get; set; }

        /// <summary>
        /// Gets or sets the Title
        /// </summary>
        string Title { get; set; }

        #endregion
    }

    #endregion
}
