﻿/****************************** File Header ******************************\
File Name:	ObservableModel.cs
Date Created:	2/4/2019
Description:	Base Class for All models in the application. 
               Supports Property Change Notifications and Validation Handling.
\*************************************************************************/

namespace JCHVRF_New.Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;



    public class ModelBase : INotifyPropertyChanged, IDataErrorInfo
    {
        private readonly Dictionary<string, Func<ModelBase, object>> propertyGetters;
        private readonly Dictionary<string, ValidationAttribute[]> validators;
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the error message for the property with the given name.
        /// </summary>
        /// <param name="propertyName"> Name of the property </param>
        public string this[string propertyName]
        {
            get
            {
                if (this.propertyGetters.ContainsKey(propertyName))
                {
                    var propertyValue = this.propertyGetters[propertyName](this);
                    var errorMessages = this.validators[propertyName]
                        .Where(v => !v.IsValid(propertyValue))
                        .Select(v => v.ErrorMessage).ToArray();

                    return string.Join(Environment.NewLine, errorMessages);
                }

                return string.Empty;
            }
        }

        public ModelBase()
        {
            this.validators = this.GetType()
                .GetProperties()
                .Where(p => this.GetValidations(p).Length != 0)
                .ToDictionary(p => p.Name, p => this.GetValidations(p));

            this.propertyGetters = this.GetType()
                .GetProperties()
                .Where(p => this.GetValidations(p).Length != 0)
                .ToDictionary(p => p.Name, p => this.GetValueGetter(p));
        }

        private ValidationAttribute[] GetValidations(PropertyInfo property)
        {
            return (ValidationAttribute[])property.GetCustomAttributes(typeof(ValidationAttribute), true);
        }

        private Func<ModelBase, object> GetValueGetter(PropertyInfo property)
        {
            return new Func<ModelBase, object>(viewmodel => property.GetValue(viewmodel, null));
        }

        public void SetValue<TField>(ref TField field, TField newValue, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<TField>.Default.Equals(field, newValue)) return;
            field = newValue;
            RaisePropertyChanged(propertyName);
        }

        /// <summary>
        /// Gets All the validation errors in the viewModel.
        /// </summary>
        public string Error
        {
            get
            {
                var errors = from validator in this.validators
                             from attribute in validator.Value
                             where !attribute.IsValid(this.propertyGetters[validator.Key](this))
                             select attribute.ErrorMessage;

                return string.Join(Environment.NewLine, errors.ToArray());
            }
        }

        /// <summary>
        /// Gets the number of properties which have a validation attribute and are currently valid
        /// </summary>
        public int ValidPropertiesCount
        {
            get
            {
                return this.validators.Where(validator => validator.Value.All(attribute => attribute.IsValid(this.propertyGetters[validator.Key](this)))).Count();
            }
        }

        /// <summary>
        /// Gets the number of properties which have a validation attribute
        /// </summary>
        public int TotalPropertiesWithValidationCount
        {
            get
            {
                return this.validators.Count();
            }
        }

        /// <summary>
        /// Returns true if All validations are passed.
        /// </summary>
        public bool IsValid { get { return this.ValidPropertiesCount == this.TotalPropertiesWithValidationCount; } }

        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            if (this.propertyGetters.ContainsKey(propertyName))
                RaisePropertyChanged("IsValid");
        }
    }
}
