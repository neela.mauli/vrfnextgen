﻿using JCHVRF.Model;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace JCHVRF_New.Common.Helpers
{
    class StringPayLoad1 : PubSubEvent<string>
    { }
    class RefreshDashboard : PubSubEvent
    { }

    class OduIduVisibility : PubSubEvent<Visibility>
    { }

    class AddCreatorPayLoad : PubSubEvent
    { }
    class ProjectInfoSubscriber : PubSubEvent<bool>
    { }
    class DesignerTabSubscriber : PubSubEvent<bool>
    { }
    class TypeTabSubscriber : PubSubEvent<bool>
    { }
    class FloorListSaveSubscriber : PubSubEvent
    { }
    class RoomListSaveSubscriber : PubSubEvent
    { }
    class FloorListAddSubscriber : PubSubEvent
    { }
    class FloorButtonEnableSubscriber : PubSubEvent<bool>
    { }
    class FloorTabSubscriber : PubSubEvent<bool>
    { }

    //ACC - RAG
    class TheuInfoVisibility : PubSubEvent<Visibility>
    { }

    class cntrlTexthiding : PubSubEvent<Visibility>
    { }

    class OduTabSubscriber : PubSubEvent<bool>
    { }
    class IduTabSubscriber : PubSubEvent<bool>
    { }
    class ProjectTypeInfoTabNext : PubSubEvent
    { }
    class DesignConditionTabNext : PubSubEvent
    { }
    class TypeInfoTabNext : PubSubEvent
    { }
    class FloorTabNext : PubSubEvent
    { }
    class ODUTypeTabNext : PubSubEvent
    { }
    class IDUTypeTabNext : PubSubEvent
    { }

    //ACC - RAG
    class CreateButtonVisibility : PubSubEvent<Visibility>
    { }

    //ACC - RAG
    class NextButtonVisibility : PubSubEvent<Visibility>
    { }


    public class TabValidation
    {
        public bool IsProjectTypeTabValid { get; set; }

    }

}
