﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JCHVRF_New.Model;
using JCBase.Utility;

namespace JCHVRF_New.Common.Helpers
{
    public static class ExtensionMethods
    {
        public static double ValueAsPerUnit(this double Value, string ToUnit)
        {
            //if (string.Equals(FromUnit, ToUnit, StringComparison.OrdinalIgnoreCase))
            //    return Value;

            switch (ToUnit)
            {
                case Unit.ut_Capacity_ton:
                    return Value * 0.28434517;
                case Unit.ut_Capacity_btu:
                    return Value * 3412.142;

                case Unit.ut_Airflow_m3h:
                    return Value * 0.0167;
                case Unit.ut_Airflow_cfm:
                    return Value * 0.589;
                case Unit.ut_Airflow_ls:
                    return Value * 0.278;

                case Unit.ut_Area_ft2:
                    return Value * 10.764;

                case Unit.ut_Weight_lbs:
                    return Value * 2.20462262185;

                case Unit.ut_Dimension_inch:
                    return Value * 39.37007874;
                    
                case Unit.ut_Temperature_f:
                    return (Value * (9 / 5)) + 32;

                case Unit.ut_Size_ft:
                    return Value * 3.281;

                case Unit.ut_LoadIndex_MBH:
                    return Value * 0.317;
                    
                case Unit.ut_WaterFlowRate_lmin:
                    return Value * 16.667;
            }



            //switch (SelectedAirflowUnit)
            //{
            //    case AirflowUnit.ls:
            //        SystemSetting.UserSetting.unitsSetting.settingAIRFLOW = Unit.ut_Airflow_ls;
            //        break;
            //    case AirflowUnit.m3h: //It means m3/min in legacy
            //        SystemSetting.UserSetting.unitsSetting.settingAIRFLOW = Unit.ut_Airflow_m3h;
            //        break;
            //    case AirflowUnit.m3hr:// This is not in new application, it meant m3/h in legacy
            //        SystemSetting.UserSetting.unitsSetting.settingAIRFLOW = Unit.ut_Airflow_m3hr;
            //        break;
            //    case AirflowUnit.cfm:
            //        SystemSetting.UserSetting.unitsSetting.settingAIRFLOW = Unit.ut_Airflow_cfm;
            //        break;
            //}

            //switch (SelectedTemperatureUnit)
            //{
            //    case TemperatureUnit.F:
            //        SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE = Unit.ut_Temperature_f;
            //        break;
            //    case TemperatureUnit.C:
            //        SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE = Unit.ut_Temperature_c;
            //        break;
            //}

            //switch (SelectedLengthUnit)
            //{
            //    case LengthUnit.m:
            //        SystemSetting.UserSetting.unitsSetting.settingLENGTH = Unit.ut_Size_m;
            //        break;
            //    case LengthUnit.ft:
            //        SystemSetting.UserSetting.unitsSetting.settingLENGTH = Unit.ut_Size_ft;
            //        break;
            //}

            //switch (SelectedDimensionsUnit)
            //{
            //    case DimensionsUnit.mm:
            //        SystemSetting.UserSetting.unitsSetting.settingDimension = Unit.ut_Dimension_mm;
            //        break;
            //    case DimensionsUnit.inch:
            //        SystemSetting.UserSetting.unitsSetting.settingDimension = Unit.ut_Dimension_inch;
            //        break;
            //}

            //// This was in legacy app for Unit Dimensions and above one for Piping Dimensions
            ////if (rbDemensionsmmUnit.Checked == true)
            ////{
            ////    SystemSetting.UserSetting.unitsSetting.settingDimensionUnit = Unit.ut_Dimension_mm;
            ////}
            ////else
            ////{
            ////    SystemSetting.UserSetting.unitsSetting.settingDimensionUnit = Unit.ut_Dimension_inch;
            ////}

            //switch (SelectedWeightUnit)
            //{
            //    case WeightUnit.kg:
            //        SystemSetting.UserSetting.unitsSetting.settingWEIGHT = Unit.ut_Weight_kg;
            //        break;
            //    case WeightUnit.lbs:
            //        SystemSetting.UserSetting.unitsSetting.settingWEIGHT = Unit.ut_Weight_lbs;
            //        break;
            //}

            //switch (SelectedAreaUnit)
            //{
            //    case AreaUnit.m2:
            //        SystemSetting.UserSetting.unitsSetting.settingAREA = Unit.ut_Area_m2;
            //        break;
            //    case AreaUnit.ft2:
            //        SystemSetting.UserSetting.unitsSetting.settingAREA = Unit.ut_Area_ft2;
            //        break;
            //}

            //switch (SelectedLoadIndexUnit)
            //{
            //    case LoadIndexUnit.Wm2:
            //        SystemSetting.UserSetting.unitsSetting.settingLOADINDEX = Unit.ut_LoadIndex_w;
            //        break;
            //    case LoadIndexUnit.MBH:
            //        SystemSetting.UserSetting.unitsSetting.settingLOADINDEX = Unit.ut_LoadIndex_MBH;
            //        break;
            //}

            //switch (SelectedWaterFlowRateUnit)
            //{
            //    case WaterFlowRateUnit.m3h:
            //        SystemSetting.UserSetting.unitsSetting.settingWaterFlowRate = Unit.ut_WaterFlowRate_m3h;
            //        break;
            //    case WaterFlowRateUnit.lmin:
            //        SystemSetting.UserSetting.unitsSetting.settingWaterFlowRate = Unit.ut_WaterFlowRate_lmin;
            //        break;
            //}


            return Value;
        }
    }
}
