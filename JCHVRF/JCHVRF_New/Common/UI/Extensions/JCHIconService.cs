﻿/****************************** File Header ******************************\
File Name:	JCHUIService.cs
Date Created:	2/14/2019
Description:	
\*************************************************************************/

namespace JCHVRF_New.Common.UI.Extensions
{
    using FontAwesome.WPF;
    using System.Windows;
    using System.Windows.Media;

    public class JCHUIService : DependencyObject
    {
        // Using a DependencyProperty as the backing store for IconColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconColorProperty =
            DependencyProperty.Register("IconColor", typeof(SolidColorBrush), typeof(JCHUIService), new PropertyMetadata(Brushes.Black));

        // Using a DependencyProperty as the backing store for IconHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconHeightProperty =
            DependencyProperty.Register("IconHeight", typeof(double), typeof(JCHUIService), new PropertyMetadata(15.0));

        // Using a DependencyProperty as the backing store for Icon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconProperty =
            DependencyProperty.RegisterAttached("Icon", typeof(FontAwesomeIcon), typeof(JCHUIService), new PropertyMetadata(FontAwesomeIcon.None));

        public static readonly DependencyProperty IconWidthProperty =
    DependencyProperty.Register("IconWidth", typeof(double), typeof(JCHUIService), new PropertyMetadata(15.0));

        // Using a DependencyProperty as the backing store for NotificationCount.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NotificationCountProperty =
            DependencyProperty.RegisterAttached("NotificationCount", typeof(int), typeof(JCHUIService), new PropertyMetadata(0));

        /// <summary>
        /// The GetIcon
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <returns>The <see cref="FontAwesomeIcon"/></returns>
        public static FontAwesomeIcon GetIcon(UIElement element)
        {
            return (FontAwesomeIcon)element.GetValue(IconProperty);
        }

        /// <summary>
        /// The GetIconColor
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <returns>The <see cref="SolidColorBrush"/></returns>
        public static SolidColorBrush GetIconColor(UIElement element)
        {
            return (SolidColorBrush)element.GetValue(IconColorProperty);
        }

        /// <summary>
        /// The GetIconHeight
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <returns>The <see cref="double"/></returns>
        public static double GetIconHeight(UIElement element)
        {
            return (double)element.GetValue(IconHeightProperty);
        }

        /// <summary>
        /// The GetIconWidth
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <returns>The <see cref="double"/></returns>
        public static double GetIconWidth(UIElement element)
        {
            return (double)element.GetValue(IconWidthProperty);
        }

        /// <summary>
        /// The GetNotificationCount
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <returns>The <see cref="int"/></returns>
        public static int GetNotificationCount(UIElement element)
        {
            return (int)element.GetValue(NotificationCountProperty);
        }

        /// <summary>
        /// The SetIcon
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <param name="value">The value<see cref="FontAwesomeIcon"/></param>
        public static void SetIcon(UIElement element, FontAwesomeIcon value)
        {
            element.SetValue(IconProperty, value);
        }

        /// <summary>
        /// The SetIconColor
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <param name="value">The value<see cref="SolidColorBrush"/></param>
        public static void SetIconColor(UIElement element, SolidColorBrush value)
        {
            element.SetValue(IconColorProperty, value);
        }

        /// <summary>
        /// The SetIconHeight
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <param name="value">The value<see cref="double"/></param>
        public static void SetIconHeight(UIElement element, double value)
        {
            element.SetValue(IconProperty, value);
        }

        /// <summary>
        /// The SetIconWidth
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <param name="value">The value<see cref="double"/></param>
        public static void SetIconWidth(UIElement element, double value)
        {
            element.SetValue(IconProperty, value);
        }

        /// <summary>
        /// The SetNotificationCount
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <param name="value">The value<see cref="int"/></param>
        public static void SetNotificationCount(UIElement element, int value)
        {
            element.SetValue(NotificationCountProperty, value);
        }

        /// <summary>
        /// The GetGlyphVisibility
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <returns>The <see cref="int"/></returns>
        public static Visibility GetGlyphVisibility(UIElement element)
        {
            return (Visibility)element.GetValue(GlyphVisibilityProperty);
        }

        /// <summary>
        /// The SetGlyphVisibility
        /// </summary>
        /// <param name="element">The element<see cref="UIElement"/></param>
        /// <param name="value">The value<see cref="int"/></param>
        public static void SetGlyphVisibility(UIElement element, Visibility value)
        {
            element.SetValue(GlyphVisibilityProperty, value);
        }

        // Using a DependencyProperty as the backing store for GlyphVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GlyphVisibilityProperty =
            DependencyProperty.Register("GlyphVisibility", typeof(Visibility), typeof(JCHUIService), new PropertyMetadata(Visibility.Collapsed));
    }
}
