﻿/****************************** File Header ******************************\
File Name:	RegionNames.cs
Date Created:	2/13/2019
Description:	
\*************************************************************************/

namespace JCHVRF_New.Common.Constants
{
    public static class RegionNames
    {
        #region Fields

        public static readonly string ContentRegion = "ContentRegion";

        public static readonly string MasterDesignerLeftArea = "MasterDesignerLeftArea";

        public static readonly string MasterDesignerRightBottomArea = "MasterDesignerRightBottomArea";

        public static readonly string MasterDesignerRightTopArea = "MasterDesignerRightTopArea";

        public static readonly string ToolbarRegion = "ToolbarRegion";
        public static readonly string MainAppRegion = "MainAppRegion";

        #endregion
    }
}
