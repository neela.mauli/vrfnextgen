﻿#pragma checksum "..\..\..\Views\DesignerCondition.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "54F4CB777703C906FA07CEE86945957A03E2E53A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CustomControls;
using JCHVRF_New.Views;
using Prism.Interactivity;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Regions.Behaviors;
using Prism.Unity;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace JCHVRF_New.Views {
    
    
    /// <summary>
    /// DesignerCondition
    /// </summary>
    public partial class DesignerCondition : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 61 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Country;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox City;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumericAltitude;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAltitude;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnChangeTemp;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumericOutdoorDB;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblOutdoorDb;
        
        #line default
        #line hidden
        
        
        #line 156 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblNumericOutDB;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumeroutdoorHDWBT;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbloutdoorHeatingWB;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblNumeroutdoorHDWBT;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumeroutdoorHDRH;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblOutdoorHDRH;
        
        #line default
        #line hidden
        
        
        #line 194 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblNumeroutdoorHDRH;
        
        #line default
        #line hidden
        
        
        #line 203 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumericOutdoorIntelWater;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblOutdoorIw;
        
        #line default
        #line hidden
        
        
        #line 222 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumeroutdoorIntelWaterTemp;
        
        #line default
        #line hidden
        
        
        #line 229 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblOutdoorHDIW;
        
        #line default
        #line hidden
        
        
        #line 232 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblNumeroutdoorHDIW;
        
        #line default
        #line hidden
        
        
        #line 244 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumeroutdoorHDDBT;
        
        #line default
        #line hidden
        
        
        #line 251 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblOutdoorHDDBT;
        
        #line default
        #line hidden
        
        
        #line 254 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblNumeroutdoorHDDBT;
        
        #line default
        #line hidden
        
        
        #line 268 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumericCoolDryBulb;
        
        #line default
        #line hidden
        
        
        #line 275 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblValCoolDryBulb;
        
        #line default
        #line hidden
        
        
        #line 278 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblCoolDryBulb;
        
        #line default
        #line hidden
        
        
        #line 287 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumericCoolWetBulb;
        
        #line default
        #line hidden
        
        
        #line 295 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblValCoolWetBulb;
        
        #line default
        #line hidden
        
        
        #line 298 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblCoolWetBulb;
        
        #line default
        #line hidden
        
        
        #line 306 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumericInternalRH;
        
        #line default
        #line hidden
        
        
        #line 313 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblRelativeHumidity;
        
        #line default
        #line hidden
        
        
        #line 316 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblInternalRH;
        
        #line default
        #line hidden
        
        
        #line 329 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DecimalUpDown NumericHeatingDryBulb;
        
        #line default
        #line hidden
        
        
        #line 336 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblHeatingDryBulb;
        
        #line default
        #line hidden
        
        
        #line 339 "..\..\..\Views\DesignerCondition.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblHeatingDB;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/JCHVRF_New;component/views/designercondition.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\DesignerCondition.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Country = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 2:
            this.City = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.NumericAltitude = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 4:
            this.lblAltitude = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.btnChangeTemp = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.NumericOutdoorDB = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 7:
            this.lblOutdoorDb = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.lblNumericOutDB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.NumeroutdoorHDWBT = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 10:
            this.lbloutdoorHeatingWB = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.lblNumeroutdoorHDWBT = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.NumeroutdoorHDRH = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 13:
            this.lblOutdoorHDRH = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.lblNumeroutdoorHDRH = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.NumericOutdoorIntelWater = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 16:
            this.lblOutdoorIw = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.NumeroutdoorIntelWaterTemp = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 18:
            this.lblOutdoorHDIW = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.lblNumeroutdoorHDIW = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 20:
            this.NumeroutdoorHDDBT = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 21:
            this.lblOutdoorHDDBT = ((System.Windows.Controls.Label)(target));
            return;
            case 22:
            this.lblNumeroutdoorHDDBT = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 23:
            this.NumericCoolDryBulb = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 24:
            this.lblValCoolDryBulb = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.lblCoolDryBulb = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 26:
            this.NumericCoolWetBulb = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 27:
            this.lblValCoolWetBulb = ((System.Windows.Controls.Label)(target));
            return;
            case 28:
            this.lblCoolWetBulb = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 29:
            this.NumericInternalRH = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 30:
            this.lblRelativeHumidity = ((System.Windows.Controls.Label)(target));
            return;
            case 31:
            this.lblInternalRH = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 32:
            this.NumericHeatingDryBulb = ((Xceed.Wpf.Toolkit.DecimalUpDown)(target));
            return;
            case 33:
            this.lblHeatingDryBulb = ((System.Windows.Controls.Label)(target));
            return;
            case 34:
            this.lblHeatingDB = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

