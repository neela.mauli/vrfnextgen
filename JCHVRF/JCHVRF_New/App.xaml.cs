﻿using System;
using System.Globalization;
using System.Windows;

namespace JCHVRF_New
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            JCBase.Utility.MyConfig.AppPath = AppDomain.CurrentDomain.BaseDirectory;
            JCBase.Util.CultureHelper.SetCurrentCultrue(new CultureInfo("en-US"));

            Bootstrapper bootstrapper = new Bootstrapper();
            bootstrapper.Run();
        }
    }
}
