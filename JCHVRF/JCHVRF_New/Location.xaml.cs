﻿using JCHVRF.BLL.New;
using JCHVRF.Model;
using JCHVRF.Model.New;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace JCHVRF_New
{
    /// <summary>
    /// Interaction logic for Location.xaml
    /// </summary>
    public partial class Location : Window
    {
        ProjectLocation projectModelObj = new ProjectLocation();
        ProjectBLLLocation objProjectLocation = new ProjectBLLLocation();
        public Location()
        {
            InitializeComponent();
            BindComboBox();
        }

        //private void Save_OnClick(object sender, RoutedEventArgs e)
        //{
            


        //}

        public void BindComboBox()
        {
            try
            {
                DataTable obj = objProjectLocation.GetData();
                CmbRegion.ItemsSource= obj.DefaultView;
                CmbRegion.DisplayMemberPath = obj.Columns["Region"].ToString();
                CmbRegion.SelectedValuePath = obj.Columns["Code"].ToString();

            }
            catch (System.Exception)
            {

                throw;
            }


        }

        private void Button_Cancel(object sender, RoutedEventArgs e)
        {
            this.Close();

        }

        private void CmbSubRegion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataTable obj = objProjectLocation.GetDataSubRegion(this.CmbRegion.SelectedValue.ToString());
                //CmbSubRegion.ItemsSource = obj.DefaultView;
                //CmbSubRegion.DisplayMemberPath = obj.Columns["Region"].ToString();
                //CmbSubRegion.SelectedValuePath = obj.Columns["Code"].ToString();

            }
            catch (System.Exception)
            {

                throw;
            }


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //projectModelObj.Region = CmbRegion.Text.ToString();
                //projectModelObj.SubRegion = CmbSubRegion.Text.ToString();
                projectModelObj.GpsCoordinate = txtGpsCoordinate.Text;
                objProjectLocation.InsertLocationDetails(projectModelObj);
            }
            catch (System.Exception)
            {

                throw;
            }

        }
    }
}
