﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Text.RegularExpressions;
using System.Windows.Forms.VisualStyles;
using JCHVRF.Model.New;
using JCHVRF.BLL.New;

namespace JCHVRF_New
{
    /// <summary>
    /// Interaction logic for NewClient.xaml
    /// </summary>
    public partial class NewClient : Window
    {
      
        ProjectInfoBLL objBll = new ProjectInfoBLL();        
        private bool isValid = true;

        List<string> validationError = new List<string>();
        public NewClient()
        {
            InitializeComponent();
        }

        #region validtion events
        private void txtCompanyName_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidatetxtCompanyName();
        }

        private void txtExistingClient_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidatetxtExistingClient();
        }

        private void txtStreetAddress_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidatetxtStreetAddress();
        }

        private void txtPhone_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidatetxtPhone();
        }

        private void txtSuburb_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidatetxtSuburb();
        }

        private void txtContactEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidatetxtContactEmail();
        }

        private void txtTownCity_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidatetxtTownCity();
        }

        private void txtIdNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidatetxtIdNumber();
        }

        private void txtCountry_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidatetxtCountry();
        }
        #endregion


        #region validtion events from bool value
        private bool ValidatetxtCompanyName()
        {
            String st = @"!|@|#|\$|%|\?|\>|\<|\*";
            if (string.IsNullOrEmpty(txtCompanyName.Text.Trim()))
            {
                lblValidCompayName.Text = "Company name can not be blank.";
                return false;
            }
            else if (Regex.IsMatch(txtCompanyName.Text.Trim(), st))
            {
                lblValidCompayName.Text = "Special characters not allowed.";
                return false;
            }
            else
            {
                lblValidCompayName.Text = string.Empty;
                //isValid = true;
                return true;
            }            
        }

        private bool ValidatetxtExistingClient()
        {
            String st = @"!|@|#|\$|%|\?|\>|\<|\*";
            if (string.IsNullOrEmpty(txtExistingClient.Text.Trim()))
            {
                lblValidContactName.Text = "Contact name can not be blank.";
                return false;
            }
            else if (Regex.IsMatch(txtExistingClient.Text.Trim(), st))
            {
                lblValidContactName.Text = "Special characters not allowed.";
                return false;
            }
            else
            {
                lblValidContactName.Text = string.Empty;
                return true;
            }
        }

        private bool ValidatetxtStreetAddress()
        {
            String st = @"!|@|#|\$|%|\?|\>|\<|\*";
            if (Regex.IsMatch(txtStreetAddress.Text.Trim(), st))
            {
                lblValidStreetAddress.Text = "Special characters not allowed.";
                return false;
            }
            else
            {
                lblValidStreetAddress.Text = string.Empty;
                return true;
            }
        }

        private bool ValidatetxtPhone()
        {
            String st = @"!|@|#|\$|%|\?|\>|\<|\*";
            if (string.IsNullOrEmpty(txtPhone.Text.Trim()))
            {
                lblValidPhone.Text = "Phone can not be blank.";
                return false;
            }
            else if (Regex.IsMatch(txtPhone.Text.Trim(), st))
            {
                lblValidPhone.Text = "Special characters not allowed.";
                return false;
            }
            else
            {
                lblValidPhone.Text = string.Empty;
                return true;
            }
        }

        private bool ValidatetxtSuburb()
        {
            String st = @"!|@|#|\$|%|\?|\>|\<|\*";
            if (Regex.IsMatch(txtSuburb.Text.Trim(), st))
            {
                lblValidSuburb.Text = "Special characters not allowed.";
                return false;
            }
            else
            {
                lblValidSuburb.Text = string.Empty;
                return true;
            }
        }

        private bool ValidatetxtContactEmail()
                {
            string email = txtContactEmail.Text;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);

            if (string.IsNullOrEmpty(txtContactEmail.Text.Trim()))
            {
                lblValidContactEmail.Text = "Contact email can not be blank.";
                return false;
            }
            else if (match.Success)
            {
                lblValidContactEmail.Text = string.Empty;
                return true;
            }
            else
            {
                lblValidContactEmail.Text = "Please enter valid mail id.";
                return false;
            }
        }

        private bool ValidatetxtTownCity()
        {
            String st = @"!|@|#|\$|%|\?|\>|\<|\*";
            if (Regex.IsMatch(txtTownCity.Text.Trim(), st))
            {
                lblValidCIty.Text = "Special characters not allowed.";
                return false;
            }
            else
            {
                lblValidCIty.Text = string.Empty;
                return true;
            }
        }

        private bool ValidatetxtIdNumber()
        {
            String st = @"!|@|#|\$|%|\?|\>|\<|\*";
            if (Regex.IsMatch(txtIdNumber.Text.Trim(), st))
            {
                lblValidIdNumber.Text = "Special characters not allowed.";
                return false;
            }
            else
            {
                lblValidIdNumber.Text = string.Empty;
                return true;
            }
        }

        private bool ValidatetxtCountry()
        {
            String st = @"!|@|#|\$|%|\?|\>|\<|\*";
            if (string.IsNullOrEmpty(txtCountry.Text.Trim()))
            {
                lblValidCountry.Text = "Country name can not be blank.";
                return false;
            }
            else if (Regex.IsMatch(txtCountry.Text.Trim(), st))
            {
                lblValidCountry.Text = "Special characters not allowed.";
                return false;
            }
            else
            {
                lblValidCountry.Text = string.Empty;
                return true;
            }
        }
        #endregion
        #region reset details
        private void cancel()
        {
            txtCompanyName.Text = string.Empty;
            txtExistingClient.Text = string.Empty;
            txtStreetAddress.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtSuburb.Text = string.Empty;
            txtContactEmail.Text = string.Empty;
            txtTownCity.Text = string.Empty;
            txtIdNumber.Text = string.Empty;
            txtCountry.Text = string.Empty;
        }
        #endregion

        private void checkValidation()
        {
            txtCompanyName.RaiseEvent(new RoutedEventArgs(LostFocusEvent, txtCompanyName));
            txtExistingClient.RaiseEvent(new RoutedEventArgs(LostFocusEvent, txtExistingClient));
            txtStreetAddress.RaiseEvent(new RoutedEventArgs(LostFocusEvent, txtStreetAddress));
            txtPhone.RaiseEvent(new RoutedEventArgs(LostFocusEvent, txtPhone));
            txtSuburb.RaiseEvent(new RoutedEventArgs(LostFocusEvent, txtSuburb));
            txtContactEmail.RaiseEvent(new RoutedEventArgs(LostFocusEvent, txtContactEmail));
            txtTownCity.RaiseEvent(new RoutedEventArgs(LostFocusEvent, txtTownCity));
            txtIdNumber.RaiseEvent(new RoutedEventArgs(LostFocusEvent, txtIdNumber));
            txtCountry.RaiseEvent(new RoutedEventArgs(LostFocusEvent, txtCountry));
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
           this.Close();
        }

     
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            
            ProjectInfo obj = new ProjectInfo();
            Client objClient = new Client();
            //checkValidation();
            if ( (ValidatetxtCompanyName() == false) || (ValidatetxtContactEmail()== false) || (ValidatetxtCountry() == false) ||
                (ValidatetxtExistingClient() == false) || (ValidatetxtIdNumber() == false) ||
                (ValidatetxtPhone() == false) || (ValidatetxtStreetAddress() == false) || (ValidatetxtSuburb()== false) || (ValidatetxtTownCity() == false))
            {
                MessageBox.Show("Please fill all mandetory fields.");
            }
            else
            {
                objClient.CompanyName = txtCompanyName.Text.Trim();
                objClient.ContactName = txtExistingClient.Text.Trim();
                objClient.StreetAddress = txtStreetAddress.Text.Trim();
                objClient.Phone = txtPhone.Text.Trim();
                objClient.Suburb = txtSuburb.Text.Trim();
                objClient.ContactEmail = txtContactEmail.Text.Trim();
                objClient.TownCity = txtTownCity.Text.Trim();
                objClient.IdNumber = txtIdNumber.Text.Trim();
                objClient.Country = txtCountry.Text.Trim();
                Application.Current.Properties["ClientInfo"] = objClient;
                Application.Current.Properties["ClientCompanyName"] = objClient.CompanyName;
                MessageBox.Show("Details saved successfull.");
                this.Close();
            }
        }

    }
}
