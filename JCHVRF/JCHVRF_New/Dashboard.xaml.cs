﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JCHVRF_New.ViewModels;
using JCHVRF.BLL;
using System.Collections.ObjectModel;
using JCHVRF_New.Model;
using Prism.Regions;
using Prism.Events;
using JCHVRF_New.Common.Helpers;

namespace JCHVRF_New
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : UserControl
    {
        private IRegionManager _regionManager;
        private IEventAggregator _eventAggregator;

        //This is to handle exception on createCacheTable 
        //which comes when we traverse to Dashboard from masterdesigner by clicking on Home in left Navigation pane

        //public ProjectViewModel project { get; set; }
        public Dashboard(IRegionManager regionManager, IEventAggregator eventAggregator)
        {
            InitializeComponent();
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            CachTableBLL CommonBll = new CachTableBLL();
            CommonBll.CreateCachTable();
            ProjectTemplateViewModel vm = new ProjectTemplateViewModel(_regionManager);
            vm.LoadProjects("All");
            GridAllProject.DataContext = vm;
            _eventAggregator.GetEvent<RefreshDashboard>().Subscribe(RefreshDashboard);
           
        }

        private void RefreshDashboard()
        {
            ProjectTemplateViewModel vm = new ProjectTemplateViewModel(_regionManager);
            vm.LoadProjects(String.Empty);
            GridAllProject.DataContext = vm;
            ProjectListTabControl.SelectedIndex = 0;
        }

        private void btnNewProject_Click(object sender, RoutedEventArgs e)
        {
            //var wizard = new MainWindow();
            JCHVRF.Model.Project.CurrentProject = new JCHVRF.Model.Project();
            JCHVRF.Model.Project.CurrentProject.RegionCode = string.IsNullOrWhiteSpace(JCHVRF.Model.SystemSetting.UserSetting.locationSetting.region) ? "EU_W" : JCHVRF.Model.SystemSetting.UserSetting.locationSetting.region;
            JCHVRF.Model.Project.CurrentProject.SubRegionCode = string.IsNullOrWhiteSpace(JCHVRF.Model.SystemSetting.UserSetting.locationSetting.subRegion) ? "GBR" : JCHVRF.Model.SystemSetting.UserSetting.locationSetting.subRegion;
            var wizard = new Views.CreateProjectWizard();
            wizard.ShowDialog();
        }
        private void txtProjectSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            //To Update ProjectViewModel on the basis of text changed in the search box
        }
        private void txtSearch_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtProjectSearch.Text == "Search")
                txtProjectSearch.Text = "";
        }
        private void txtSearch_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtProjectSearch.Text.Trim() == "")
                txtProjectSearch.Text = "Search";
        }
        private void SearchProject()
        {
            if (txtProjectSearch.Text.Trim().Length > 3)
            {
                ProjectTemplateViewModel vm = new ProjectTemplateViewModel(_regionManager);
                vm.LoadProjects("All");
                TabAllProject.IsSelected = true;
                var res = vm.Projects.Where(x => x.ProjectName.IndexOf(txtProjectSearch.Text, StringComparison.InvariantCultureIgnoreCase) >= 0).ToList();
                vm.Projects = new ObservableCollection<Project>(res);
                GridAllProject.DataContext = vm;
            }
            else
            {
                ProjectTemplateViewModel vm = new ProjectTemplateViewModel(_regionManager);
                vm.LoadProjects("All");
                GridAllProject.DataContext = vm;
                TabAllProject.IsSelected = true;
            }
        }
        private void txtProjectSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SearchProject();
            }
        }
        private void lnkBtnSearch_OnClick(object sender, RoutedEventArgs e)
        {
            SearchProject();
        }
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string searchType = "all";
            if (TabWeekProject != null && GridWeekProject != null && TabWeekProject.IsSelected)
            {
                searchType = "week";
                ProjectTemplateViewModel vm = new ProjectTemplateViewModel(_regionManager);
                vm.LoadProjects(searchType);
                GridWeekProject.DataContext = vm;
            }
            else if (TabMonthProject != null && GridMonthProject != null && TabMonthProject.IsSelected)
            {
                searchType = "month";
                ProjectTemplateViewModel vm = new ProjectTemplateViewModel(_regionManager);
                vm.LoadProjects(searchType);
                GridMonthProject.DataContext = vm;
            }
            else if (TabAllProject != null && GridAllProject != null && TabAllProject.IsSelected)
            {
                ProjectTemplateViewModel vm = new ProjectTemplateViewModel(_regionManager);
                vm.LoadProjects(searchType);
                GridAllProject.DataContext = vm;
            }
        }
    }
}
