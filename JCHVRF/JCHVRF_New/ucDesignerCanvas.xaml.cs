﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lassalle.WPF.Flow;
using JCHVRF.Model;
using JCHVRF.MyPipingBLL;
using JCBase.Utility;
using JCHVRF.BLL;
using System.Data;
using System.Configuration;
using NextGenModel = JCHVRF.Model.NextGen;
using NextGenBLL = JCHVRF.MyPipingBLL.NextGen;
using JCHVRF.Const;
using JCHVRF_New.ViewModels;
using JCHVRF.VRFMessage;
using ng = JCHVRF.Model.NextGen;
using Point = System.Windows.Point;
//using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing;
using drawing = System.Drawing;
using JCHVRF_New.Utility;
using System.Collections;
using WL = Lassalle.WPF.Flow;
using NextGenPipingBLL = JCHVRF.MyPipingBLL.NextGen;

namespace JCHVRF_New
{
    /// <summary>
    /// Interaction logic for ucDesignerCanvas.xaml
    /// </summary>
    public partial class ucDesignerCanvas : UserControl
    {
        List<string> ERRList = null;
        List<string> MSGList = null;
        List<PointF[]> ptArrayList = new List<PointF[]>();
        List<PointF[]> ptArrayList_power = new List<PointF[]>();
        List<PointF[]> ptArrayList_ground = new List<PointF[]>();
        List<PointF[]> ptArrayList_mainpower = new List<PointF[]>();


        double ZoomScaleAddFlow = 1;
        Node nodeWiring, nodePiping;
        NextGenModel.ImageData btnImagePiping, btnImageWiring;
        bool bToggle;
        bool bPanning;

        // Zooming object
        Point location;
        System.Windows.Size size;
        double dZoomCounter;
        double iWidth;
        double yHeight;
        //public ImageData imgData { get; set; }
        NextGenBLL.UtilPiping utilPiping = new NextGenBLL.UtilPiping();
        string ut_length;
        string ut_power;
        string ut_temperature;
        string ut_airflow;
        string ut_weight;
        AddFlow addWiringflow;
        public UtilityWiring utilWiring = new UtilityWiring();

        //#region NodeProperty
        //static readonly DependencyProperty NodeProp =
        //    DependencyProperty.RegisterAttached("NodeProp", typeof(object), typeof(object),
        //        new FrameworkPropertyMetadata(false));
        //static object GetNodeProperty(Node node)
        //{
        //    if (node == null)
        //        return false;
        //    return (object)node.GetValue(NodeProp);
        //}
        //static void SetNodeProperty(Node node, object value)
        //{
        //    if (node != null)
        //        node.SetValue(NodeProp, value);
        //}
        //#endregion

        public event EventHandler<Object> SelectEquipment;

        protected virtual void OnSelectEquipment(Object data)
        {
            if (SelectEquipment != null) SelectEquipment(this, data);
        }

        public event EventHandler<NextGenModel.ImageData> AddEquipment;

        protected virtual void OnAddEquipment(NextGenModel.ImageData data)
        {
            if (SelectEquipment != null) AddEquipment(this, data);
        }

        public JCHVRF.Model.Project projectLegacy { get; set; }
        public JCHVRF.Model.NextGen.SystemVRF currentSystem { get; set; }

        //start for context menu canvas
        public ContextMenu AddflowContextMenu = new ContextMenu();
        Node DeleteNode = null;
        //end for context menu canvas

        private void InitializeWiringAddFlow()
        {
            this.addWiringflow = new AddFlow();
            this.addWiringflow.AllowDrop = false;
            this.addWiringflow.CanDrawNode = false;
            addWiringflow.MouseLeftButtonDown += AddWiringflow_MouseLeftButtonDown;
        }

        private void AddWiringflow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var AddflowToPanning = (AddFlow)ScrollViewer.Content;
            if (bPanning == true)
            {
                AddflowToPanning.IsPanning = true;
                bPanning = false;
            }
            else
            {
                AddflowToPanning.IsPanning = false;
            }
        }

        public ucDesignerCanvas(JCHVRF.Model.Project project)
        {
            InitializeComponent();
            InitializeWiringAddFlow();
            this.addflow.MouseLeftButtonDown += Addflow_MouseLeftButtonDown;
            this.projectLegacy = project;
            // this.projectLagecy.IsCoolingModeEffective = false;
            this.addflow.CanDrawNode = false; //added to disable node drawing on canvas

            location = this.addflow.Origin;
            size = this.addflow.Extent;
            bToggle = false;
            bPanning = false;
            this.dZoomCounter = 5;
            double iWidth = size.Width;
            double yHeight = size.Height;
            DrawPreSelectedNodesOnEdit();
            //createDynamicDiagramWithImages(this.addflow);

            //createDynamicDiagramWithImages(this.addflow);

            //start for context menu canvas
            IntializeContextMenu();
            //end for context menu canvas

        }

        public ucDesignerCanvas()
        {
            InitializeComponent();
            InitializeWiringAddFlow();
        }


        public void IntializeContextMenu()
        {
            MenuItem DeleteMenuItem = new MenuItem();
            DeleteMenuItem.Header = "Delete Node";
            DeleteMenuItem.Click += DeleteMenuItem_Click;
            AddflowContextMenu.Items.Add(DeleteMenuItem);
        }
        //end for context menu canvas
        protected override void OnDrop(DragEventArgs e)
        {
            base.OnDrop(e);
        }

        private void DrawPreSelectedNodesOnEdit()
        {
            if (this.projectLegacy.SystemListNextGen != null && this.projectLegacy.SystemListNextGen.Count > 0 && this.projectLegacy.SystemListNextGen[0].MyPipingNodeOutTemp != null && this.projectLegacy.SystemListNextGen[0].IsPipingOK == true)
                return;
            double left = 100;
            double top = 100;
            if (this.projectLegacy.RoomIndoorList != null)
            {
                if (this.projectLegacy.RoomIndoorList.Count > 0)
                {
                    foreach (var node in this.projectLegacy.RoomIndoorList)
                    {
                        if (node.IndoorNO > 0)
                        {
                            NextGenModel.ImageData data = new NextGenModel.ImageData();
                            data.imagePath = node.DisplayImagePath;
                            data.imageName = node.DisplayImageName;
                            data.equipmentType = "Indoor";
                            data.NodeNo = node.IndoorNO;
                            data.UniqName = "Ind" + Convert.ToString(node.IndoorNO);
                            try
                            {
                                BitmapImage bitmap = new BitmapImage(new Uri(data.imagePath));
                                ImageSource img = bitmap;

                                Node imageNode = new Node(left, top, 61, 65, data.NodeNo.ToString(), addflow);
                                imageNode.Image = img;
                                setNodeStyle(imageNode, data);
                                NextGenBLL.PipingBLL.SetNodeProperty(imageNode, data);
                                this.addflow.AddNode(imageNode);
                                left = left + 100;
                            }
                            catch { }
                        }
                    }
                }
            }
            if (this.projectLegacy.CanvasODUList != null)
            {
                top = top + 200;
                left = 100;
                if (this.projectLegacy.CanvasODUList.Count > 0)
                {
                    foreach (var SysVRF in this.projectLegacy.CanvasODUList)
                    {
                        if (SysVRF.OutdoorItem != null)
                        {
                            NextGenModel.ImageData data = new NextGenModel.ImageData();
                            data.imagePath = SysVRF.DisplayImageName;
                            data.imageName = SysVRF.Series;
                            data.equipmentType = "Outdoor";
                            data.NodeNo = SysVRF.NO;
                            data.UniqName = "Out" + Convert.ToString(SysVRF.NO);
                            try
                            {
                                BitmapImage bitmap = new BitmapImage(new Uri(data.imagePath));
                                ImageSource img = bitmap;

                                Node imageNode = new Node(left, top, 61, 65, data.NodeNo.ToString(), addflow);
                                imageNode.Image = img;
                                setNodeStyle(imageNode, data);
                                NextGenBLL.PipingBLL.SetNodeProperty(imageNode, data);
                                this.addflow.AddNode(imageNode);
                                left = left + 100;
                            }
                            catch { }
                        }
                    }
                }
            }

        }

        private void addflow_Drop(object sender, DragEventArgs e)
        {
            //Start Bug#1646
            lblCanvasError.BorderBrush = new SolidColorBrush(Colors.Transparent);
            lblCanvasError.BorderThickness = new Thickness(0);
            lblCanvasError.Content = string.Empty;
            //End Bug#1646

            int InNodeCount = 0;
            int OutNodeCount = 0;
            string UniName = "";
            NextGenModel.ImageData data = new NextGenModel.ImageData();
            var SenderData = (NextGenModel.ImageData)e.Data.GetData(typeof(NextGenModel.ImageData));

            var AllNodeList = this.addflow.Items.OfType<Node>().ToArray();

            foreach (Node a in AllNodeList)
            {
                if (NextGenBLL.PipingBLL.GetNodeProperty(a).ToString() != "False")
                {
                    if (((NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(a)).equipmentType == "Outdoor")
                    {
                        if (this.projectLegacy.CanvasODUList != null)
                            foreach (NextGenModel.SystemVRF SVRF in this.projectLegacy.CanvasODUList)
                            {
                                OutNodeCount = OutNodeCount > SVRF.NO ? OutNodeCount : SVRF.NO;
                            }
                    }
                    else if (((NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(a)).equipmentType == "Indoor")
                    {
                        foreach (RoomIndoor ri in this.projectLegacy.RoomIndoorList)
                        {
                            InNodeCount = InNodeCount > ri.IndoorNO ? InNodeCount : ri.IndoorNO;
                        }
                    }
                }
            }
            //if (SenderData.equipmentType == "Outdoor")   // Allow only 1 outdoor unit later we will delete it.
            //    if (OutNodeCount > 0) // Allow only 1 outdoor unit later we will delete it.
            //    {
            //        MessageBox.Show("You can drag only 1 outdoor unit");
            //        return;
            //    }

            if (SenderData.equipmentType == "Outdoor")
            {
                UniName = "Out" + Convert.ToString(OutNodeCount + 1);
            }
            else if (SenderData.equipmentType == "Indoor")
            {
                UniName = "Ind" + Convert.ToString(InNodeCount + 1);
            }
            data.imagePath = SenderData.imagePath;
            data.imageName = SenderData.imageName;
            data.equipmentType = SenderData.equipmentType;
            data.NodeNo = AllNodeList.Count() + 1;
            data.UniqName = UniName;

            BitmapImage bitmap = new BitmapImage(new Uri(SenderData.imagePath));
            ImageSource img = bitmap;
            double left = e.GetPosition(this.addflow).X;
            double top = e.GetPosition(this.addflow).Y;
            Node imageNode = new Node(left, top, 61, 65, data.NodeNo.ToString(), addflow);


            imageNode.Image = img;
            setNodeStyle(imageNode, data);
            // imageNode.SetCurrentValue(imageNode, data);

            NextGenBLL.PipingBLL.SetNodeProperty(imageNode, data);
            this.addflow.AddNode(imageNode);
            this.addflow.MouseLeftButtonDown += Addflow_MouseLeftButtonDown;
            //To Call Method of ParentPage

            data.NodeNo = Convert.ToInt32(UniName.Remove(0, 3));
            OnAddEquipment(data);

        }

        public void UpdateEquipmentOnCanvas(NextGenModel.ImageData imgData)
        {
            try
            {
                var allNodeList = this.addflow.Items.OfType<Node>().ToArray();
                foreach (var node in allNodeList)
                {
                    var selectedNode = (NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(node);
                    if (selectedNode.UniqName == imgData.UniqName)
                    {
                        BitmapImage bitmap = new BitmapImage(new Uri(imgData.imagePath));
                        ImageSource img = bitmap;
                        node.Image = img;
                        setNodeStyle(node, imgData);
                        NextGenBLL.PipingBLL.SetNodeProperty(node, imgData);
                    }

                }
            }
            catch { }
        }

        private void Addflow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            var selectedNodelist = this.addflow.Items.OfType<Node>().ToArray();
            //var equipmentProperties = selectedNodelist[0];
            //foreach (Node a in selectedNodelist)
            //{
            //    var equipmentProperties = NextGenBLL.PipingBLL.GetNodeProperty(a);
            //}

            double left = e.GetPosition(this.addflow).X;
            double top = e.GetPosition(this.addflow).Y;

            var selectedNode = this.addflow.GetItemAt(new Point(left, top)) as Node;
            if (selectedNode != null)
            {
                if ((NextGenBLL.PipingBLL.GetNodeProperty(selectedNode).ToString()) != "False")
                {

                    NextGenModel.ImageData equipmentProperties = (NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(selectedNode);
                    if (equipmentProperties.equipmentType == "Indoor" || equipmentProperties.equipmentType == "Outdoor")
                    {
                        equipmentProperties.NodeNo = Convert.ToInt32(equipmentProperties.UniqName.Remove(0, 3));
                        OnSelectEquipment(equipmentProperties);
                    }
                    else
                        CreatePipingWiringToggleButton(equipmentProperties.imageName);
                }
            }


            //Do panning true here
            var AddflowToPanning = (AddFlow)ScrollViewer.Content;
            if (bPanning == true)
            {

                AddflowToPanning.IsPanning = true;
                bPanning = false;
            }
            else
            {
                AddflowToPanning.IsPanning = false;
            }

        }

        private void BindProductType()
        {
            MyProductTypeBLL productTypeBll = new MyProductTypeBLL();
            DataTable dt = productTypeBll.GetProductTypeData(projectLegacy.BrandCode, projectLegacy.SubRegionCode);
            if (dt != null && dt.Rows.Count > 0)
            {
                projectLegacy.ProductType = Convert.ToString(dt.Rows[0]["ProductType"]);
            }
        }

        private NextGenBLL.PipingBLL GetPipingBLLInstance()
        {
            string ut_length = SystemSetting.UserSetting.unitsSetting.settingLENGTH;
            string ut_power = SystemSetting.UserSetting.unitsSetting.settingPOWER;
            string ut_temperature = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            string ut_airflow = SystemSetting.UserSetting.unitsSetting.settingAIRFLOW;
            string ut_weight = SystemSetting.UserSetting.unitsSetting.settingWEIGHT;
            string ut_dimension = SystemSetting.UserSetting.unitsSetting.settingDimension;

            bool isInch = CommonBLL.IsDimension_inch();
            NextGenBLL.UtilPiping utilPiping = new NextGenBLL.UtilPiping();
            return new NextGenBLL.PipingBLL(this.projectLegacy, utilPiping, null, isInch, ut_weight, ut_length, ut_power);
        }

        private void setNodeStyle(Node node, NextGenModel.ImageData data)
        {
            node.Geometry = new RectangleGeometry(new Rect(0, 0, 61, 65), 3, 3);
            node.Stroke = System.Windows.Media.Brushes.RoyalBlue;
            node.Fill = System.Windows.Media.Brushes.White;
            node.Text = data.imageName;
            node.ImageMargin = new Thickness(3);
            node.TextMargin = new Thickness(3);
            node.ImagePosition = ImagePosition.CenterTop;
            node.TextPosition = TextPosition.CenterBottom;
            this.addflow.CanDrawNode = false;
            this.addflow.CanDrawLink = false;
            node.FontSize = 9;
            node.Tooltip = data.equipmentType + " : " + data.imageName;
            node.IsSelectable = true;
            node.IsEditable = false; //issue fix 1653

        }

        #region add new methods take  after latest code

        public bool ExportVictorGraph(string filePath, bool isBackWhite = false)
        {
            try
            {
                AddFlow addFlowItem = this.addflow;
                NextGenModel.SystemVRF sysItem = currentSystem;
                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(900, 900);
               
                System.Drawing.Graphics gs = System.Drawing.Graphics.FromImage(bmp);
                System.Drawing.Imaging.Metafile mf = new System.Drawing.Imaging.Metafile(filePath, gs.GetHdc());
                
                System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(mf);
                
                string PipingImageDir = GetImagePathPiping();
                
                DrawEMF_PipingNextGen(g, addFlowItem, sysItem, PipingImageDir, this.projectLegacy);
                g.ResetTransform();
                g.Save();
                g.Flush();
                g.Dispose();
                gs.Dispose();
                mf.Dispose();
                // emf.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool ExportVictorGraph_wiring(string filePath, bool isBackWhite = false)
        {
            AddFlow addFlowItem = this.addflow;
            NextGenModel.SystemVRF sysItem = currentSystem;
            try
            {

                string imageDir = GetImagePathWiring();
                Bitmap bmp = new Bitmap(875, 348);
                Graphics gs = Graphics.FromImage(bmp);
                Metafile mf = new Metafile(filePath, gs.GetHdc());
                Graphics g = Graphics.FromImage(mf);
                if (isBackWhite)
                {
                    g.Clear(System.Drawing.Color.White);
                }
                DrawEMF_wiringNextGen(g, addFlowItem, sysItem, imageDir);
                g.Save();
                g.Dispose();
                gs.Dispose();
                mf.Dispose();
                //emf.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private string GetImagePathWiring()
        {
            string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
            // string navigateToFolder = defaultFolder+ "NVRF\\NodeImageWiring\\";
            string navigateToFolder = "..\\..\\Report\\NodeImageWiring\\";
            //string navigateToFolder= "..\\..\\Image\\NodeImageWiring\\";
            string sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            return sourceDir;
        }


        private string GetImagePathPiping()
        {
            //string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
            //string navigateToFolder = defaultFolder+ "NVRF\\NodeImagePiping\\";
            //string sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            //return sourceDir;

            string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
            //string navigateToFolder = "..\\..\\Report\\NodeImagePiping\\";
            string navigateToFolder = "..\\..\\Image\\TypeImages\\";
            string sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            return sourceDir;
        }
        //public void DrawEMF_Piping(Graphics g, AddFlow addFlowPiping, NextGenModel.SystemVRF curSystemItem, string PipingImageDir, JCHVRF.Model.Project thisProject)
        //{
        //    NextGenBLL.PipingBLL pipBll = new NextGenBLL.PipingBLL(thisProject);
        //    bool isHitachi = thisProject.BrandCode == "H";
        //    bool isInch = CommonBLL.IsDimension_inch();
        //    NextGenBLL.UtilEMF utilEMF = new NextGenBLL.UtilEMF();
        //    // bool isVertical = this.uc_CheckBox_PipingVertical.Checked;
        //    utilEMF.utLength = SystemSetting.UserSetting.unitsSetting.settingLENGTH;//设置长度单位 add by axj 20161229
        //    utilEMF.isManualLength = curSystemItem.IsInputLengthManually;//设置是否输入管长 add by axj 20161229
        //    string title = "Additional Refrigerant Charge";//添加追加冷媒title add by axj 20161229
        //    foreach (Item item in addFlowPiping.Items)
        //    {
        //        if (item is Node)
        //        {
        //            Node nd = item as Node;
        //            if (nd is NextGenModel.MyNodeYP)
        //            {
        //                if (!(nd as NextGenModel.MyNodeYP).IsCP)
        //                {
        //                    utilEMF.DrawYP(g, nd, false);
        //                }
        //                else
        //                {
        //                    utilEMF.DrawYP(g, nd, true);
        //                }
        //            }
        //            else if (nd is NextGenModel.MyNodeOut)
        //            {
        //                // Draw the YP model inside the outdoor unit combination and the pipe diameter data
        //                NextGenModel.MyNodeOut ndOut = nd as NextGenModel.MyNodeOut;
        //                //string outModel = curSystemItem.OutdoorItem.Model;
        //                string outModel = curSystemItem.OutdoorItem.Model_York; //According to the PM requirements, the outdoor unit and the extension will display Model_York or Model_Hitachi 20180214 by Yunxiao Lin.
        //                                                                        //JTWH 室外机需要做SizeUp计算 add on 20160615 by Yunxiao Lin
        //                NextGenModel.NodeElement_Piping itemOut = pipBll.GetPipingNodeOutElement(curSystemItem, isHitachi);
        //                string nodeImageFile = PipingImageDir + itemOut.Name + ".txt";

        //                if (itemOut.UnitCount == 1)
        //                {
        //                    if (isHitachi)
        //                        outModel = curSystemItem.OutdoorItem.Model_Hitachi;
        //                    //uncomment that code also
        //                    utilEMF.DrawNode(g, nd, nodeImageFile);
        //                }
        //                else
        //                {
        //                    outModel = curSystemItem.OutdoorItem.AuxModelName;
        //                    //uncomment that code also
        //                    utilEMF.DrawNode_OutdoorGroup(g, nd, nodeImageFile, outModel, curSystemItem.Name, itemOut, isInch);
        //                }
        //            }
        //            else if (nd is NextGenModel.MyNodeIn)
        //            {
        //                NextGenModel.MyNodeIn ndIn = nd as NextGenModel.MyNodeIn;

        //                NextGenModel.NodeElement_Piping itemInd = pipBll.GetPipingNodeIndElement(ndIn.RoomIndooItem.IndoorItem.Type, isHitachi);
        //                string nodeImageFile = PipingImageDir + itemInd.Name + ".txt";
        //                utilEMF.DrawNode(g, nd, nodeImageFile);
        //            }
        //            else if (nd is NextGenModel.MyNodeCH)
        //            {
        //                NextGenModel.MyNodeCH ndCH = nd as NextGenModel.MyNodeCH;
        //                string nodeImageFile = PipingImageDir + "CHbox.txt";
        //                utilEMF.DrawNode(g, nd, nodeImageFile);
        //            }
        //            else if (nd is NextGenModel.MyNodeMultiCH)
        //            {
        //                NextGenModel.MyNodeMultiCH ndMCH = nd as NextGenModel.MyNodeMultiCH;
        //                string nodeImageFile = PipingImageDir + "MultiCHbox.txt";
        //                utilEMF.DrawNode(g, nd, nodeImageFile);
        //            }
        //            else if (nd is NextGenModel.MyNodeLegend)
        //            {
        //                //绘制图例文字
        //                //uncomment also
        //                utilEMF.DrawText(g, nd);
        //            }
        //            else if (nd.Tooltip == title)//添加追加冷媒文字 add by axj 20161229
        //            {
        //                utilEMF.DrawText(g, nd);
        //            }
        //            else if (nd.Text != null && nd.Text.Contains("Cooling") && nd.Text.Contains("Heating"))//添加实际容量文字 add by axj 20161229
        //            {
        //                //uncomment code
        //                utilEMF.DrawActualCapacityText(g, nd);
        //            }
        //            else
        //            {
        //                //uncomment code
        //                utilEMF.DrawLabelText(g, nd);
        //            }
        //        }
        //        else if (item is Link)
        //        {
        //            //uncomment code
        //            utilEMF.DrawLine(g, (Link)item);
        //        }
        //    }
        //}


        #region add new methods take  after latest code 


        private void DrawEMF_wiringNextGen(Graphics g, AddFlow addFlowWiring, NextGenModel.SystemVRF curSystemItem, string WiringImageDir)
        {
            try
            {
                Wiring WiringPage = new Wiring(this.projectLegacy, this.projectLegacy.SystemListNextGen[0], addWiringflow);
                addFlowWiring = WiringPage.GetAddFlowInstance();


                JCHVRF.Model.Project thisProject = this.projectLegacy;
                PointF ptf1 = new PointF(0, 0);
                PointF ptf2 = new PointF(0, 0);
                PointF ptf3 = new PointF(0, 0);
                NextGenBLL.UtilEMF utilEMF = new NextGenBLL.UtilEMF();

                //try
                //{   
                foreach (Item item in addFlowWiring.Items)
                {
                    if (item is Node)
                    {
                        Node nd = item as Node;
                        if (nd is NextGenModel.WiringNodeOut)
                        {
                            NextGenModel.WiringNodeOut ndOut = nd as NextGenModel.WiringNodeOut;
                            NodeElement_Wiring item_wiring = utilPiping.GetNodeElement_Wiring_ODUNextGen(curSystemItem.OutdoorItem, thisProject.BrandCode);
                            string nodePointsFile = System.IO.Path.Combine(WiringImageDir, item_wiring.KeyName + ".txt");
                            //apply per new nextgen class
                            utilEMF.DrawNode_wiringNextGen(g, ndOut, nodePointsFile, curSystemItem.Name, item_wiring);
                        }
                        else if (nd is NextGenModel.WiringNodeIn)
                        {

                            NextGenModel.WiringNodeIn ndIn = nd as NextGenModel.WiringNodeIn;
                            List<string> strArrayList_powerType = new List<string>();
                            List<string> strArrayList_powerVoltage = new List<string>();
                            List<double> dArrayList_powerCurrent = new List<double>();

                            int powerIndex = 0;
                            bool isNewPower = false;
                            NodeElement_Wiring item_wiring = utilPiping.GetNodeElement_Wiring_IDUNextGen(ndIn.RoomIndoorItem.IndoorItem, thisProject.BrandCode, curSystemItem.OutdoorItem.Type, ref strArrayList_powerType, ref strArrayList_powerVoltage, ref dArrayList_powerCurrent, ref powerIndex, ref isNewPower);
                            //string nodePointsFile = FileLocal.GetNodeImageTextWiring(item_wiring.KeyName);
                            string nodePointsFile = System.IO.Path.Combine(WiringImageDir, item_wiring.KeyName + ".txt");
                            //apply as per new nextgen class
                            utilEMF.DrawNode_wiringNextGen(g, ndIn, nodePointsFile, ndIn.RoomIndoorItem.IndoorName, item_wiring);
                        }
                        else if (nd is NextGenModel.WiringNodeCH)
                        {
                            //WiringNodeCH ndCH = nd as WiringNodeCH;
                            //NodeElement_Wiring item_wiring = utilEMF.GetNodeElement_Wiring_CH(ndCH);
                            //string nodePointsFile = Path.Combine(WiringImageDir, item_wiring.KeyName + ".txt");
                            //utilEMF.DrawNode_wiring(g, ndCH, nodePointsFile, "CH Unit", item_wiring);
                            //apply as per new next gen class
                            utilEMF.DrawNodeTextWiringCHbox(g, nd);
                        }
                        else if (nd is NextGenModel.MyNodeGround_Wiring)
                        {
                            NextGenModel.MyNodeGround_Wiring ndGnd = nd as NextGenModel.MyNodeGround_Wiring;
                            string nodePointsFile = System.IO.Path.Combine(WiringImageDir, "Ground.txt");
                            //aaply as per new next gen class
                            utilEMF.DrawNode_wiringNextGen(g, ndGnd, nodePointsFile, "", null);

                        }
                        else if (nd is NextGenModel.MyNodeRemoteControler_Wiring)
                        {

                            NextGenModel.MyNodeRemoteControler_Wiring ndRC = nd as NextGenModel.MyNodeRemoteControler_Wiring;
                            string nodePointsFile = System.IO.Path.Combine(WiringImageDir, "RemoteControler.txt");
                            utilEMF.DrawNode_wiringNextGen(g, ndRC, nodePointsFile, "", null);
                        }
                        else
                        {

                            if (!string.IsNullOrEmpty(nd.Text))
                            {

                                PointF pf = new PointF(Convert.ToInt64(nd.Location.X), Convert.ToInt64(nd.Location.Y));

                                //g.DrawString(nd.Text, utilEMF.textFont_wiring, utilEMF.textBrush_wiring, pf);
                                if (nd.Text != "//" && nd.Text != "///" && nd.Text != "////")
                                    g.DrawString(nd.Text, utilEMF.textFont_wiring, utilEMF.textBrush_wiring, pf);
                                else
                                {
                                    System.Drawing.Brush brush = new System.Drawing.SolidBrush(drawing.Color.Red);
                                    pf.Y += 2.5f;
                                    g.DrawString(nd.Text, utilEMF.textFont_wiring, brush, pf);
                                }
                            }
                        }
                    }
                }
                //}
                //catch (Exception ex)
                //{
                //    LogHelp.WriteLog(n.ToString()+"  "+ex.Message, ex);
                //}

                var links = addFlowWiring.Items.OfType<Link>().ToArray();
                foreach (var item in links)
                { 
                  utilEMF.DrawLine(g, (Link)item);
                }

                //foreach (PointF[] pt in ptArrayList)
                //{
                //    //drawing.Pen pen = new drawing.Pen(drawing.Color.Black, 0.1f);
                //    //g.DrawLines(pen, pt);
                //    //g.DrawLines(utilEMF.pen_wiring, pt);
                //}
                List<PointF[]> ptArrayList_power = new List<PointF[]>();

                List<PointF[]> ptArrayList_ground = new List<PointF[]>();
                /// <summary>
                /// 室内机总电源线坐标列表
                /// </summary>
                List<PointF[]> ptArrayList_mainpower = new List<PointF[]>();
                foreach (PointF[] pt in ptArrayList_power)
                {
                    drawing.Pen pen = new drawing.Pen(drawing.Color.Red, 0.3f);
                    g.DrawLines(pen, pt);
                    //g.DrawLines(utilEMF.pen_wiring_power, pt); 
                }

                foreach (PointF[] pt in ptArrayList_ground)
                {
                    drawing.Pen pen = new drawing.Pen(drawing.Color.Yellow, 0.1f);
                    PointF[] pt1 = pt;
                    if (pt.Length > 2)
                    {
                        pt1 = new PointF[] { pt[0], pt[1] };
                    }
                    g.DrawLines(pen, pt1);
                    //g.DrawLines(utilEMF.pen_wiring_dash, pt); 
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// 绘制 EMF 图片(Piping)
        /// <summary>
        /// 绘制 EMF 图片(Piping)
        /// </summary>
        /// <param name="addFlow1"></param>
        /// <param name="g"></param>
        public void DrawEMF_PipingNextGen(Graphics g, WL.AddFlow addFlowPiping, NextGenModel.SystemVRF curSystemItem, string PipingImageDir, JCHVRF.Model.Project thisProject)
        {
            NextGenBLL.PipingBLL pipBll = new NextGenBLL.PipingBLL(thisProject);
            bool isHitachi = thisProject.BrandCode == "H";
            bool isInch = CommonBLL.IsDimension_inch();
            // bool isVertical = this.uc_CheckBox_PipingVertical.Checked;
            NextGenBLL.UtilEMF utilEMF = new NextGenBLL.UtilEMF();
            utilEMF.utLength = SystemSetting.UserSetting.unitsSetting.settingLENGTH;//设置长度单位 add by axj 20161229
            utilEMF.isManualLength = curSystemItem.IsInputLengthManually;//设置是否输入管长 add by axj 20161229
            string title = "Additional Refrigerant Charge";//添加追加冷媒title add by axj 20161229

            var nodes = addFlowPiping.Items.OfType<Node>().ToArray();
            var links = addFlowPiping.Items.OfType<Link>().ToArray();

            foreach (var item in nodes)
            {
                if (item is NextGenModel.MyNodeYP)
                {
                    if (!(item as NextGenModel.MyNodeYP).IsCP)
                    {
                        utilEMF.DrawYPNextGen(g, item, false);
                    }
                    else
                    {
                        utilEMF.DrawYPNextGen(g, item, true);
                    }
                }
                else if (item is NextGenModel.MyNodeOut)
                {
                    //Draw the YP model inside the outdoor unit combination and the pipe diameter data
                    NextGenModel.MyNodeOut ndOut = item as NextGenModel.MyNodeOut;
                    //string outModel = curSystemItem.OutdoorItem.Model;
                    string outModel = curSystemItem.OutdoorItem.Model_York;
                    string nodeImageFile = ((System.Windows.Media.Imaging.BitmapImage)ndOut.Image).UriSource.OriginalString;

                    if (isHitachi)
                        outModel = curSystemItem.OutdoorItem.Model_Hitachi;
                    // outModel = "RAS-14-18FSNSE";
                    utilEMF.DrawNodeNextGenPiping(g, item, nodeImageFile);

                }
                else if (item is NextGenModel.MyNodeIn)
                {
                    NextGenModel.MyNodeIn ndIn = item as NextGenModel.MyNodeIn;

                    string nodeImageFile = ((System.Windows.Media.Imaging.BitmapImage)ndIn.Image).UriSource.OriginalString;
                    utilEMF.DrawNodeNextGenPiping(g, item, nodeImageFile);
                }
                else if (item is NextGenModel.MyNodeCH)
                {
                    NextGenModel.MyNodeCH ndCH = item as NextGenModel.MyNodeCH;
                    string nodeImageFile = ((System.Windows.Media.Imaging.BitmapImage)ndCH.Image).UriSource.OriginalString;
                    utilEMF.DrawNodeNextGenPiping(g, item, nodeImageFile);
                }
                else if (item is NextGenModel.MyNodeMultiCH)
                {
                    NextGenModel.MyNodeMultiCH ndMCH = item as NextGenModel.MyNodeMultiCH;
                    string nodeImageFile = PipingImageDir + "MultiCHbox.png";
                    utilEMF.DrawNodeNextGenPiping(g, item, nodeImageFile);
                }
                else if (item is NextGenModel.MyNodeLegend)
                {
                    //绘制图例文字
                    utilEMF.DrawText(g, item);
                }
                else if (item.Tooltip == title)
                {
                    utilEMF.DrawText(g, item);
                }
                else if (item.Text != null && item.Text.Contains("Cooling") && item.Text.Contains("Heating"))
                {
                    utilEMF.DrawActualCapacityText(g, item);
                }
                else
                {
                   utilEMF.DrawLabelText(g, item);
                }
            }

            foreach (var item in links)
            {
                utilEMF.DrawLine(g, (Link)item);
            }
        }




        #endregion new methods 

        #endregion new methods add after latest

        /// <summary>
        /// Check Outdoor Equipment count in Canvas
        /// </summary>
        /// <returns></returns>
        private bool CheckOutdoorCount()
        {
            var AllNodeList = this.addflow.Items.OfType<Node>().ToArray();
            int OutNodeCount = 0;
            foreach (Node a in AllNodeList)
            {
                if (((NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(a)).equipmentType == "Outdoor")
                    OutNodeCount = OutNodeCount + 1;
            }
            if (OutNodeCount > 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SelectedNode"></param>
        /// 
        public void DoDrawingPiping(bool reset)
        {
            NextGenBLL.UtilPiping utilPiping = new NextGenBLL.UtilPiping();
            NextGenBLL.PipingBLL pipBll = GetPipingBLLInstance();
            //utilPiping
            currentSystem = projectLegacy.SystemListNextGen.FirstOrDefault();

            bool isHitachi = projectLegacy.BrandCode == "H";
            bool isHR = NextGenBLL.PipingBLL.IsHeatRecovery(projectLegacy.SystemListNextGen.FirstOrDefault());

            string dir = GetBinDirectoryPath(ConfigurationManager.AppSettings["PipingNodeImageDirectory"].ToString());

            NextGenModel.MyNodeOut pipingNodeOut = currentSystem.MyPipingNodeOut;
            if (pipingNodeOut == null || currentSystem.OutdoorItem == null) return;
            if (pipingNodeOut.ChildNode == null) return;
            if (isHR)
            {

                //SetAllNodesIsCoolingonlyFrom();
                pipBll.SetIsCoolingOnly(currentSystem.MyPipingNodeOut);
            }
            if (reset)
            {
                currentSystem.IsManualPiping = false;
                utilPiping.ResetColors();
                InitAndRemovePipingNodes();
                pipBll.DrawPipingNodes(currentSystem, dir, ref this.addflow);
                pipBll.DrawPipingLinks(currentSystem, this.addflow);
                pipBll.DrawLegendText(currentSystem, ref this.addflow);
            }

            //Start: Bug#1646
            lblCanvasError.BorderBrush = new SolidColorBrush(Colors.Transparent);
            lblCanvasError.BorderThickness = new Thickness(0);
            lblCanvasError.Content =string.Empty;
            //End: Bug#1646
        }

        private void InitAndRemovePipingNodes()
        {
            //     this.addflow = new AddFlow();
            var Nodes = this.addflow.Items.OfType<Node>().ToList();
            var Captions = this.addflow.Items.OfType<Caption>().ToList();
            var Links = this.addflow.Items.OfType<Link>().ToList();
            Nodes.ForEach((item) =>
            {
                this.addflow.RemoveNode(item);
            });
            Captions.ForEach((item) =>
            {
                this.addflow.RemoveCaption(item);
            });
            Links.ForEach((item) =>
            {
                this.addflow.RemoveLink(item);
            });
        }

       
        /// <summary>
        /// Get bin directory path for debug folder 
        /// </summary>
        /// <param name="AppSettingPath"></param>
        /// <returns></returns>
        private string GetBinDirectoryPath(string AppSettingPath)
        {
            string binDirectory = AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\').ToString();
            binDirectory += AppSettingPath;
            return binDirectory;
        }


        private void DeleteOutdoorEquipment(NextGenModel.ImageData SelectedNode)
        {
            if (SelectedNode.equipmentType == "Outdoor")
            {
                var AllNodeList = this.addflow.Items.OfType<Node>().ToArray();
                foreach (Node a in AllNodeList)
                {
                    if (((NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(a)).UniqName == SelectedNode.UniqName)
                    {
                        MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                        if (messageBoxResult == MessageBoxResult.Yes)
                        {

                            this.addflow.RemoveNode(a);
                            break;
                        }
                    }
                }
            }
        }

        private void CreatePipingWiringToggleImage()
        {
            nodeWiring = new Node(800, 5, 50, 50, "", addflow);
            nodePiping = new Node(800, 5, 50, 50, "", addflow);

            nodeWiring.Stroke = System.Windows.Media.Brushes.Transparent;
            nodeWiring.IsXMoveable = false;
            nodeWiring.IsYMoveable = false;

            nodePiping.Stroke = System.Windows.Media.Brushes.Transparent;
            nodePiping.IsXMoveable = false;
            nodePiping.IsYMoveable = false;

            addflow.CanDrawLink = false;

            btnImagePiping = new NextGenModel.ImageData();
            btnImageWiring = new NextGenModel.ImageData();


            btnImagePiping.imageName = "Piping";
            nodePiping.Image = new BitmapImage(new Uri("pack://application:,,,/Image/TypeImages/Piping.png"));

            btnImageWiring.imageName = "WireChange";
            nodeWiring.Image = new BitmapImage(new Uri("pack://application:,,,/Image/TypeImages/Wiring.png"));

            //Add default iamges to the node in the canvas.
            NextGenBLL.PipingBLL.SetNodeProperty(nodePiping, btnImagePiping);
            addflow.AddNode(nodePiping);
        }

        private void UcDesignerCanvasZoomIn(object sender, MouseButtonEventArgs e)
        {
            var AddFlowToZoom = (AddFlow)ScrollViewer.Content;
            if (AddFlowToZoom.Zoom < 3)
            {
                AddFlowToZoom.Zoom = AddFlowToZoom.Zoom + 0.3;
            }
        }

        private void UcDesignerCanvasZoomOut(object sender, MouseButtonEventArgs e)
        {
            var AddFlowToZoom = (AddFlow)ScrollViewer.Content;
            if (AddFlowToZoom.Zoom > 0.3)
            {
                AddFlowToZoom.Zoom = AddFlowToZoom.Zoom - 0.3;
            }
        }

        private void UcDesignerCanvasPipingWiringToggle(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (this.projectLegacy.SystemListNextGen != null && this.projectLegacy.SystemListNextGen[0].MyPipingNodeOut != null)
                {
                    if (bToggle == false)
                    {
                        if (this.projectLegacy.SystemListNextGen[0].IsPipingOK == true)
                        {
                            ToggleImage.Source = new BitmapImage(new Uri("pack://application:,,,/Image/TypeImages/Piping.png"));
                            bToggle = true;
                            Wiring WiringPage = new Wiring(this.projectLegacy, this.projectLegacy.SystemListNextGen[0], addWiringflow);
                            addWiringflow = WiringPage.GetAddFlowInstance();
                            addWiringflow.Zoom = 1;
                            ScrollViewer.Content = addWiringflow;
                            //Wiring.Visibility = Visibility.Visible;
                        }
                    }
                    else
                    {
                        ToggleImage.Source = new BitmapImage(new Uri("pack://application:,,,/Image/TypeImages/Wiring.png"));
                        bToggle = false;
                        addflow.Zoom = 1;
                        ScrollViewer.Content = addflow;
                        //Wiring.Visibility = Visibility.Hidden;
                    }
                }
            }
            catch { }
        }

        private void UcDesignerCanvasPanning(object sender, MouseButtonEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Hand;
            bPanning = true;
        }



        private void UcDesignerCanvasReAlign(object sender, MouseButtonEventArgs e)
        {
            //addflow.ScrollIncrement;
            Mouse.OverrideCursor = null;
            addflow.ResetChangedFlag();//.HorizontalAlignment = 0;/
            addflow.IsPanning = false;
            //addflow.VerticalAlignment = 0;
        }

        private void CreatePipingWiringToggleButton(string ImageType)
        {

            if (ImageType == "WireChange")
            {
                addflow.RemoveNode(nodePiping);
                btnImagePiping.imageName = "Piping";
                NextGenBLL.PipingBLL.SetNodeProperty(nodePiping, btnImagePiping);
                addflow.AddNode(nodePiping);
            }
            else
            {
                addflow.RemoveNode(nodeWiring);
                btnImageWiring.imageName = "WireChange";
                NextGenBLL.PipingBLL.SetNodeProperty(nodeWiring, btnImageWiring);
                addflow.AddNode(nodeWiring);
            }
        }

        private void Addflow_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var AddflowToPanning = (AddFlow)ScrollViewer.Content;
            AddflowToPanning.IsPanning = false;
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        //Please keep this code. Do not delete it please.
        /*
        public void createDynamicDiagramWithImages(AddFlow addFlow)
        {
            double incrementFactorLeft = 50;
            double incrementFactorTop = 30;
            double incrementFactorWidth = 55;
            double incrementFactorheight = 30;
            Image imageIndoor = new Image();
            Image imageIndoor1 = new Image();
            imageIndoor.Source = new BitmapImage(new Uri("pack://application:,,,/Image/4_Way_Cassette.png"));
            imageIndoor1.Source = new BitmapImage(new Uri("pack://application:,,,/Image/IDU_Image.png"));
            Image imageOutdoor = new Image();
            imageOutdoor.Source = new BitmapImage(new Uri("pack://application:,,,/Image/ODU_Image.png"));

            addflow.PinSize = 5;
            addflow.PinFill = Brushes.Aqua;
            addflow.PinStroke = Brushes.AliceBlue;

            addflow.NodeModel.PinsLayout =
                new PointCollection { new Point(0, 50), new Point(50, 0), new Point(100, 50), new Point(50, 100) };
            addflow.LinkModel.LineStyle = LineStyle.VH;

            for (int i = 0; i < 30; i++)
            {
                double left = incrementFactorLeft;
                double top = incrementFactorTop;
                double width = incrementFactorWidth;
                double height = incrementFactorheight;

                if (i == 0)
                {
                    Node outNode = new Node(left, top, 60, 60, "Outdoor Unit", addFlow);
                    outNode.Stroke = Brushes.Transparent;
                    outNode.Fill = Brushes.Transparent;
                    outNode.Image = imageOutdoor.Source;
                    this.addflow.AddNode(outNode);
                    incrementFactorLeft = incrementFactorLeft + 5;
                    incrementFactorTop = incrementFactorTop + 30;
                }
                else
                {
                    Node node = new Node(left+50, top+70, width+50, height+70, null, addFlow);
                    node.Stroke = Brushes.Transparent;
                    node.Fill = Brushes.Transparent;
                    if (i % 2 == 0)
                        node.Image = imageIndoor.Source;
                    else
                        node.Image = imageIndoor1.Source;

                    this.addflow.AddNode(node);
                    incrementFactorLeft = incrementFactorLeft + 80;

                    if (i % 5 == 0)
                    {
                        incrementFactorLeft = 50;
                        incrementFactorTop = incrementFactorTop + 70;
                    }
                }
            }
            var nodes = addflow.Items.OfType<Node>().ToArray();

            createLinksDynamically(nodes, addFlow);           

        }
        public void createLinksDynamically(Node[] nodeCollection, AddFlow addFlow)
        {
            for (var i = 0; i < nodeCollection.Length - 1; i++)
            {
                Link ConnectingLink = new Link(nodeCollection[0], nodeCollection[nodeCollection.Length - (i + 1)], 3, 3, "", addFlow);

                addFlow.AddLink(ConnectingLink);
            }
        }
        */

        public bool DrawAutoPipingValidation(out string ErrorMessage)
        {
            bool isValidData = true;
            ErrorMessage = string.Empty;
            var Nodes = this.addflow.Items.OfType<Node>().ToArray();
            int OutDoorCount = 0;
            bool isOutdoorNotFound = false;
            MyNodeOut myNodeOut = new MyNodeOut();
            if (Nodes != null && Nodes.Length > 0) // idu and odu on canvas
            {
                if (Nodes.Length == 1)
                {

                    var selectedNode = Nodes[0];
                    var equipmentProperties = (NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(selectedNode);
                    if (equipmentProperties.equipmentType.Equals("Indoor", StringComparison.OrdinalIgnoreCase))//only one idu on the canvas
                    {
                        isValidData = false;
                        ErrorMessage = string.Format(ValidationMessage.AtleastOneODU);
                    }
                    else if (equipmentProperties.equipmentType.Equals("outdoor", StringComparison.OrdinalIgnoreCase))// only one odu on the canvas
                    {
                        isValidData = false;
                        ErrorMessage = string.Format(ValidationMessage.AtleastOneIDU);
                    }
                }
                else// more than one nodes on canvas
                {
                    foreach (var item in Nodes.Distinct())
                    {
                        var selectedNode = item;
                        if (selectedNode.GetType().FullName == "JCHVRF.Model.NextGen.MyNodeOut" || selectedNode.GetType().FullName == "JCHVRF.Model.NextGen.MyNodeIn")
                        {
                            var equipmentProperties = (NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(selectedNode);

                            if (equipmentProperties.equipmentType.Equals("Indoor", StringComparison.OrdinalIgnoreCase))
                            {
                                isValidData = true;
                                continue;
                            }
                            else if (equipmentProperties.equipmentType.Equals("outdoor", StringComparison.OrdinalIgnoreCase))
                            {
                                isOutdoorNotFound = true;
                                OutDoorCount += 1;
                                if (OutDoorCount > 1)// More than one odu placed.
                                {
                                    ErrorMessage = string.Format(ValidationMessage.MorethanOneODU);
                                    isValidData = false;
                                    break;
                                }
                            }
                        }
                        else
                        {

                            if (selectedNode.GetType().FullName.Equals("Lassalle.WPF.Flow.Node", StringComparison.OrdinalIgnoreCase))
                            {
                                var equipmentProperties = (NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(selectedNode);

                                if (equipmentProperties != null && equipmentProperties.equipmentType != null)
                                {
                                    if (equipmentProperties.equipmentType.Equals("Indoor", StringComparison.OrdinalIgnoreCase))
                                    {
                                        isValidData = true;
                                        continue;
                                    }
                                    else if (equipmentProperties.equipmentType.Equals("outdoor", StringComparison.OrdinalIgnoreCase))
                                    {
                                        isOutdoorNotFound = true;
                                        OutDoorCount += 1;
                                        if (OutDoorCount > 1)// More than one odu placed.
                                        {
                                            ErrorMessage = string.Format(ValidationMessage.MorethanOneODU);
                                            isValidData = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!isOutdoorNotFound)//if only more than idu are placed.
                    {
                        isValidData = false;
                        ErrorMessage = string.Format(ValidationMessage.AtleastOneODU);
                    }
                }
            }
            else // empty canvas
            {
                isValidData = false;
                ErrorMessage = string.Format(ValidationMessage.AtleastOneODUAndOneIDU);
            }
            return isValidData;
        }

        private void DoPipingCalculation(NextGenBLL.PipingBLL pipBll, JCHVRF.Model.NextGen.MyNodeOut nodeOut, out NextGenBLL.PipingErrors errorType)
        {
            errorType = NextGenBLL.PipingErrors.OK;
            if (nodeOut.ChildNode == null) return;
            //分歧管型号和管径改为如果后面的大于前面的，则后面的替换为前面的型号和管径  by Shen Junjie on 20170409
            //getSumCalculation(ref firstDstNode, factoryCode, type, unitType);

            pipBll.GetSumCapacity(nodeOut.ChildNode);

            pipBll.IsBranchKitNeedSizeUp(currentSystem);

            PipingBranchKit firstBranchKit = null;
            if (nodeOut.ChildNode is JCHVRF.Model.NextGen.MyNodeYP)
            {
                JCHVRF.Model.NextGen.MyNodeYP nodeYP = nodeOut.ChildNode as JCHVRF.Model.NextGen.MyNodeYP;
                if (nodeYP.IsCP)
                {
                    //第一分歧管可能是梳形管 20170711 by Yunxiao Lin
                    firstBranchKit = pipBll.getFirstHeaderBranchPipeCalculation(nodeYP, currentSystem, out errorType);
                }
                else
                {
                    // 第一分歧管放大一号计算
                    firstBranchKit = pipBll.getFirstPipeCalculation(nodeYP, currentSystem, out errorType);
                }
                if (errorType != NextGenBLL.PipingErrors.OK)
                {
                    SetSystemPipingOK(currentSystem, false);
                    return;
                }
            }

            //分歧管型号和管径改为如果后面的大于前面的，则后面的替换为前面的型号和管径  by Shen Junjie on 20170409
            pipBll.getSumCalculationInversion(firstBranchKit, nodeOut, nodeOut.ChildNode, currentSystem, false, out errorType);
            if (errorType != NextGenBLL.PipingErrors.OK)
            {
                SetSystemPipingOK(currentSystem, false);
                return;
            }

            pipBll.CheckIndoorNumberConnectedCHBox(nodeOut);
        }

        private void SetSystemPipingOK(JCHVRF.Model.NextGen.SystemVRF sysItem, bool isPipingOK)
        {
            if (sysItem.IsPipingOK != isPipingOK)
            {
                sysItem.IsPipingOK = isPipingOK;
                //SetTabControlImageKey();
            }
        }

        private NextGenBLL.PipingBLL GetPipingBLLInstanceValidation()
        {
            bool isInch = CommonBLL.IsDimension_inch();
            return new NextGenBLL.PipingBLL(this.projectLegacy, utilPiping, this.addflow, isInch, ut_weight, ut_length, ut_power);
        }

        private void DoPipingFinalVerification()
        {
            NextGenBLL.PipingErrors errorType = NextGenBLL.PipingErrors.OK;
            if (currentSystem.OutdoorItem == null)
                return;

            if (currentSystem.IsManualPiping && currentSystem.IsUpdated)
            {

                return;
            }
            //this.Cursor = Cursors.WaitCursor;
            UtilityValidation ObjPipValidation = new UtilityValidation(this.projectLegacy, ref addflow);
            JCHVRF.MyPipingBLL.NextGen.PipingBLL pipBll = GetPipingBLLInstanceValidation();
            bool isHR = NextGenBLL.PipingBLL.IsHeatRecovery(currentSystem);
            pipBll.SetPipingLimitation(currentSystem);

            errorType = pipBll.ValidateSystemHighDifference(currentSystem);

            if (errorType == NextGenBLL.PipingErrors.OK)
            {
                errorType = pipBll.ValidatePipeLength(currentSystem, addflow);
            }
            #region
            if (errorType == NextGenBLL.PipingErrors.OK)
            {
                if (!currentSystem.IsPipingOK)
                {
                    pipBll.SetDefaultColor(addflow, isHR);
                }
                if (errorType == NextGenBLL.PipingErrors.OK)
                {
                    if (currentSystem.PipeEquivalentLength < currentSystem.HeightDiff)
                    {
                        errorType = NextGenBLL.PipingErrors.PIPING_LENGTH_HEIGHT_DIFF; //-32;
                    }
                }

                if (currentSystem.IsInputLengthManually)
                {

                    errorType = pipBll.ValMainBranch(currentSystem, addflow);
                }
                if (errorType == NextGenBLL.PipingErrors.OK)
                {
                    if (NextGenBLL.PipingBLL.IsHeatRecovery(currentSystem) && !pipBll.ValCoolingOnlyIndoorCapacityRate(currentSystem, addflow))
                    {
                        errorType = NextGenBLL.PipingErrors.COOLINGONLYCAPACITY; //-12;
                    }
                }

                if (errorType == NextGenBLL.PipingErrors.OK)
                {
                    errorType = pipBll.ValidateIDUOfMultiCHBox(currentSystem);
                }

                if (errorType == NextGenBLL.PipingErrors.OK)
                {
                    SetSystemPipingOK(currentSystem, true);
                    DoPipingCalculation(pipBll, currentSystem.MyPipingNodeOut, out errorType);
                    if (currentSystem.IsPipingOK)
                    {
                        if (currentSystem.IsInputLengthManually && !pipBll.ValCHToIndoorMaxTotalLength(currentSystem, addflow))
                        {
                            errorType = NextGenBLL.PipingErrors.MKTOINDOORLENGTH1; //-8;
                        }
                        else if (!pipBll.ValMaxIndoorNumberConnectToCH(currentSystem, addflow))
                        {
                            errorType = NextGenBLL.PipingErrors.INDOORNUMBERTOCH; //-13;
                        }
                        else
                        {
                            SetSystemPipingOK(currentSystem, true);

                            if (currentSystem.IsInputLengthManually)
                            {
                                double d1 = pipBll.GetAddRefrigeration(currentSystem, ref addflow);
                                currentSystem.AddRefrigeration = d1;

                                pipBll.DrawAddRefrigerationText(currentSystem);
                            }
                            else
                                currentSystem.AddRefrigeration = 0;
                        }
                    }
                    ObjPipValidation.DrawTextToAllNodes(currentSystem.MyPipingNodeOut, null, currentSystem);
                    //UtilTrace.SaveHistoryTraces();
                }
            }
            #endregion
            if (errorType != NextGenBLL.PipingErrors.OK)
            {
                SetSystemPipingOK(currentSystem, false);
            }
            ShowWarningMsg(errorType);
            //UtilTrace.SaveHistoryTraces();                        
            //this.Cursor = Cursors.Default;
        }

        private void ShowWarningMsg(NextGenBLL.PipingErrors errorType)
        {
            double len = 0;
            int count = 0;
            string rate = "";
            string msg = "";
            string templen = "";
            string temphei = "";
            switch (errorType)
            {
                case NextGenBLL.PipingErrors.LINK_LENGTH://-1:
                    msg = Msg.PIPING_LINK_LENGTH;
                    break;
                case NextGenBLL.PipingErrors.WARN_ACTLENGTH://-2:
                    len = Unit.ConvertToControl(currentSystem.MaxPipeLength, UnitType.LENGTH_M, ut_length);
                    msg = Msg.PIPING_WARN_ACTLENGTH(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.EQLENGTH://-3:
                    len = Unit.ConvertToControl(currentSystem.MaxEqPipeLength, UnitType.LENGTH_M, ut_length);
                    msg = Msg.PIPING_EQLENGTH(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.FIRSTLENGTH://-4:
                    len = Unit.ConvertToControl(currentSystem.MaxIndoorLength, UnitType.LENGTH_M, ut_length);
                    msg = Msg.PIPING_FIRSTLENGTH(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.LENGTHFACTOR://-5:
                    len = Unit.ConvertToControl(currentSystem.PipeEquivalentLength, UnitType.LENGTH_M, ut_length);
                    double diff = Unit.ConvertToControl(currentSystem.HeightDiff, UnitType.LENGTH_M, ut_length);
                    msg = Msg.PIPING_LENGTHFACTOR(currentSystem.Name, len.ToString("n2") + ut_length, Math.Abs(diff).ToString("n2") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.TOTALLIQUIDLENGTH://-6:
                    len = Unit.ConvertToControl(currentSystem.MaxTotalPipeLength, UnitType.LENGTH_M, ut_length);
                    msg = Msg.PIPING_TOTALLIQUIDLENGTH(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.MKTOINDOORLENGTH://-7:
                    len = Unit.ConvertToControl(currentSystem.MaxMKIndoorPipeLength, UnitType.LENGTH_M, ut_length);
                    msg = Msg.PIPING_MKTOINDOORLENGTH(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.MKTOINDOORLENGTH1://-8:
                    len = Unit.ConvertToControl(PipingBLL.MaxCHToIndoorTotalLength, UnitType.LENGTH_M, ut_length);
                    msg = Msg.PIPING_MKTOINDOORLENGTH(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.MAINBRANCHCOUNT://-9:
                    count = PipingBLL.MaxMainBranchCount;
                    msg = Msg.PIPING_MAINBRANCHCOUNT(count.ToString());
                    break;
                case NextGenBLL.PipingErrors.COOLINGCAPACITYRATE://-10:
                    rate = PipingBLL.MinMainBranchCoolingCapacityRate;
                    msg = Msg.PIPING_COOLINGCAPACITYRATE(rate);
                    break;
                //case -11:
                //    rate = PipingBLL.MinMainBranchHeatingCapacityRate;
                //    msg = Msg.PIPING_HEATINGCAPACITYRATE(rate);
                //    break;
                case NextGenBLL.PipingErrors.COOLINGONLYCAPACITY://-12:
                    msg = Msg.PIPING_COOLINGONLYCAPACITY();
                    break;
                case NextGenBLL.PipingErrors.INDOORNUMBERTOCH://-13:
                    count = PipingBLL.MaxIndoorNumberConnectToCH;
                    msg = Msg.PIPING_INDOORNUMBERTOCH(count.ToString());
                    break;
                // 多台室外机组成机组时，校验第一分歧管到第一Piping Connection kit之间的管长不能小于0.5m add on 20170720 by Shen Junjie
                case NextGenBLL.PipingErrors.FIRST_CONNECTION_KIT_TO_FIRST_BRANCH_MIN_LENGTH://-14:
                    double betweenConnectionKits_Min = Unit.ConvertToControl(0.5, UnitType.LENGTH_M, ut_length);
                    string betweenConnectionKits_Msg = betweenConnectionKits_Min.ToString("n2") + ut_length;
                    msg = Msg.PIPING_FIRST_CONNECTION_KIT_TO_FIRST_BRANCH_MIN_LENGTH(betweenConnectionKits_Msg);
                    break;
                case NextGenBLL.PipingErrors._3RD_MAIN_BRANCH://-15:
                    //不能有第三层主分支。
                    msg = Msg.GetResourceString("ERROR_PIPING_3RD_MAIN_BRANCH");
                    break;
                case NextGenBLL.PipingErrors._4TH_BRANCH_NOT_MAIN_BRANCH://-16:
                    //第4(或更远的)分支不能是一个主分支。
                    msg = Msg.GetResourceString("ERROR_PIPING_4TH_BRANCH_NOT_MAIN_BRANCH");
                    break;
                case NextGenBLL.PipingErrors.DIFF_LEN_FURTHEST_CLOSESST_IU: //-17
                    msg = Msg.GetResourceString("ERROR_PIPING_DIFF_LEN_FURTHEST_CLOSESST_IU");
                    break;
                case NextGenBLL.PipingErrors.NO_MATCHED_BRANCH_KIT://-18
                    msg = Msg.GetResourceString("ERROR_PIPING_NO_MATCHED_BRANCH_KIT");
                    break;
                case NextGenBLL.PipingErrors.NO_MATCHED_CHBOX://-19
                    msg = Msg.GetResourceString("ERROR_PIPING_NO_MATCHED_CHBOX");
                    break;
                case NextGenBLL.PipingErrors.NO_MATCHED_MULTI_CHBOX: //-20
                    msg = Msg.GetResourceString("ERROR_PIPING_NO_MATCHED_MULTI_CHBOX");
                    break;
                case NextGenBLL.PipingErrors.NO_MATCHED_SIZE_UP_IU: //-21
                    msg = Msg.WARNING_DATA_EXCEED;
                    break;
                case NextGenBLL.PipingErrors.MAX_HIGHDIFF_UPPER://-22:
                    len = Unit.ConvertToControl(currentSystem.MaxOutdoorAboveHeight, UnitType.LENGTH_M, ut_length);
                    msg = Msg.Piping_HeightDiffH(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.MAX_HIGHDIFF_LOWER://-23:
                    len = Unit.ConvertToControl(currentSystem.MaxOutdoorBelowHeight, UnitType.LENGTH_M, ut_length);
                    msg = Msg.Piping_HeightDiffL(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.MAX_HIGHDIFF_INDOOR://-24:
                    len = Unit.ConvertToControl(currentSystem.MaxDiffIndoorHeight, UnitType.LENGTH_M, ut_length);
                    msg = Msg.Piping_Indoor_HeightDiff(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.MAX_CHBOXHIGHDIFF://-25:
                    len = Unit.ConvertToControl(currentSystem.NormalCHBoxHighDiffLength, UnitType.LENGTH_M, ut_length);
                    msg = Msg.DiffCHBoxHeightValue(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.MAX_MULTICHBOXHIGHDIFF://-26:
                    len = Unit.ConvertToControl(currentSystem.NormalSameCHBoxHighDiffLength, UnitType.LENGTH_M, ut_length);
                    msg = Msg.DiffMulitBoxHeightValue(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.MAX_CHBOX_INDOORHIGHDIFF://-27:
                    len = Unit.ConvertToControl(currentSystem.NormalCHBox_IndoorHighDiffLength, UnitType.LENGTH_M, ut_length);
                    msg = Msg.DiffCHBox_IndoorHeightValue(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.INDOORLENGTH_HIGHDIFF://-28
                    templen = Unit.ConvertToControl(PipingBLL.TempActualLength, UnitType.LENGTH_M, ut_length).ToString("n2") + ut_length;
                    temphei = Unit.ConvertToControl(PipingBLL.TempMaxLength, UnitType.LENGTH_M, ut_length).ToString("n2") + ut_length;
                    msg = Msg.INDOORLENGTH_HIGHDIFF_MSG(templen, temphei);
                    break;
                case NextGenBLL.PipingErrors.CHBOXLENGTH_HIGHDIFF://-29
                    templen = Unit.ConvertToControl(PipingBLL.TempActualLength, UnitType.LENGTH_M, ut_length).ToString("n2") + ut_length;
                    temphei = Unit.ConvertToControl(PipingBLL.TempMaxLength, UnitType.LENGTH_M, ut_length).ToString("n2") + ut_length;
                    msg = Msg.CHBOXLENGTH_HIGHDIFF_MSG(templen, temphei);
                    break;
                case NextGenBLL.PipingErrors.CHBOX_INDOORLENGTH_HIGHDIFF://-30
                    templen = Unit.ConvertToControl(PipingBLL.TempActualLength, UnitType.LENGTH_M, ut_length).ToString("n2") + ut_length;
                    temphei = Unit.ConvertToControl(PipingBLL.TempMaxLength, UnitType.LENGTH_M, ut_length).ToString("n2") + ut_length;
                    msg = Msg.CHBOX_INDOORLENGTH_HIGHDIFF_MSG(templen, temphei);
                    break;
                case NextGenBLL.PipingErrors.PIPING_CHTOINDOORTOTALLENGTH: //-31
                    len = Unit.ConvertToControl(40, UnitType.LENGTH_M, ut_length);
                    msg = Msg.PIPING_CHTOINDOORTOTALLENGTH(len.ToString("n0") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.PIPING_LENGTH_HEIGHT_DIFF: //-32
                    len = Unit.ConvertToControl(currentSystem.PipeEquivalentLength, UnitType.LENGTH_M, ut_length);
                    double diffs = Unit.ConvertToControl(currentSystem.HeightDiff, UnitType.LENGTH_M, ut_length);
                    msg = Msg.PIPING_LENGTHFACTOR(currentSystem.Name, len.ToString("n2") + ut_length, Math.Abs(diffs).ToString("n2") + ut_length);
                    break;
                case NextGenBLL.PipingErrors.MIN_DISTANCE_BETWEEN_MULTI_KITS://-33
                    msg = Msg.PIPING_MIN_LEN_BETWEEN_MULTI_KITS(Unit.ConvertToControl(0.5, UnitType.LENGTH_M, ut_length).ToString() + ut_length);
                    break;
                case NextGenBLL.PipingErrors.ODU_PIPE_LENGTH_LIMITS://-34
                    msg = Msg.GetResourceString("PIPING_ODU_PIPE_LENGTH_LIMITS");
                    break;
                case NextGenBLL.PipingErrors.ANZ_MAX_BIG_IDU_OF_MUlTI_CHBOX: //-35                    
                    msg = Msg.GetResourceString("ANZ_MAX_BIG_IDU_OF_MUlTI_CHBOX");
                    break;
                case NextGenBLL.PipingErrors.OK: //0
                default:
                    msg = "";
                    break;
            }
            ShowWarningMsg(msg);
        }

        private void ShowWarningMsg(string msg)
        {
            ShowWarningMsg(msg, System.Drawing.Color.Red);
        }

        private void ShowWarningMsg(string msg, System.Drawing.Color msgColor)
        {
            //tlblStatus.Text = msg;
            //tlblStatus.ForeColor = msgColor;

            //JCMsg.ShowInfoOK(msg);
        }

        public void Validate()
        {
            if (projectLegacy.SystemListNextGen[0] == null)
                return;
            else if (projectLegacy.SystemListNextGen[0].IsPipingOK == true)
                return;
            if (currentSystem == null && projectLegacy.SystemListNextGen[0] != null)
            {
                if (projectLegacy.SystemListNextGen.Count == 0)
                    return;
                // curSystemItem = ProjectLegacy.SystemListNextGen[0];
            }

            utilPiping = new NextGenBLL.UtilPiping();
            ut_length = SystemSetting.UserSetting.unitsSetting.settingLENGTH;
            ut_power = SystemSetting.UserSetting.unitsSetting.settingPOWER;
            ut_temperature = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            ut_airflow = SystemSetting.UserSetting.unitsSetting.settingAIRFLOW;
            ut_weight = SystemSetting.UserSetting.unitsSetting.settingWEIGHT;

            DoPipingFinalVerification();

            //ValidateViewModel vm = new ValidateViewModel(this.projectLegacy, ref this.addflow);


        }

        //start for context menu canvas
        private void addflow_rightClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                addflow.ContextMenu = null;
                double left = e.GetPosition(this.addflow).X;
                double top = e.GetPosition(this.addflow).Y;
                var selectedItem = this.addflow.GetItemAt(new Point(left, top));
                //var selectedNodelist = this.addflow.Items.OfType<Node>().ToArray();
                var selectedNode = selectedItem as Node;
                if (selectedNode != null)
                {
                    DeleteNode = selectedNode;
                    addflow.ContextMenu = AddflowContextMenu;
                }
            }
            catch { }
        }
        private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure you want to delete selected node?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);

                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    addflow.RemoveNode(DeleteNode);

                    var NodeType = DeleteNode.GetType().Name;
                    //Start: Bug#1646
                    if (NodeType == "MyNodeIn" || NodeType == "MyNodeOut")
                    {
                        lblCanvasError.BorderBrush = new SolidColorBrush(Colors.Red);
                        lblCanvasError.BorderThickness = new Thickness(2);
                        lblCanvasError.Content = "The diagram is not complete or there are some excess units, please amend it";
                        try
                        {
                            this.projectLegacy.SystemListNextGen.FirstOrDefault().MyPipingNodeOut = null;
                            this.projectLegacy.SystemListNextGen.FirstOrDefault().MyPipingNodeOutTemp = null;
                            this.projectLegacy.SystemListNextGen.FirstOrDefault().MyPipingNodeOut = null;
                            this.projectLegacy.SystemListNextGen.FirstOrDefault().IsPipingOK = false;

                            
                        }
                        catch (Exception ex)

                        {


                        }

                    }
                    //Start: Bug#1646
                    // Node selectedNode = null;
                    // bool blnIsLastNode = false;
                    // MasterDesigner mdObj = new SelectNextNodeOnDeletionMasterDesigner(projectLegacy);
                    // var selectedNodelist = this.addflow.Items.OfType<Node>().ToArray();
                    //foreach (Node a in selectedNodelist)
                    //{
                    //    var NodeType = a.GetType().Name;
                    //    if (NodeType == "MyNodeIn" || NodeType == "MyNodeOut" || NodeType == "Node")
                    //    {
                    //        var equipmentProperties = NextGenBLL.PipingBLL.GetNodeProperty(a);
                    //        selectedNode = a;
                    //    }
                    //}
                    //if (selectedNode != null)
                    //{
                    //    if ((NextGenBLL.PipingBLL.GetNodeProperty(selectedNode).ToString()) != "False")
                    //    {
                    //        NextGenModel.ImageData equipmentProperties = (NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(selectedNode);
                    //        if (equipmentProperties.equipmentType == "Indoor" || equipmentProperties.equipmentType == "Outdoor")
                    //        {
                    //            equipmentProperties.NodeNo = Convert.ToInt32(equipmentProperties.UniqName.Remove(0, 3));
                    //            //MasterDesigner.blnIsCallfromDelete = true;
                    //            OnSelectEquipment(equipmentProperties);
                    //        }
                    //    }
                    //}

                    //if (selectedNode != null)
                    //{

                    //    NextGenModel.ImageData NodeDataNext = (NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(selectedNode);
                    //    mdObj.SelectNextNodeOnDeletion(NodeDataNext, blnIsLastNode);
                    //    selectedNode.IsSelected = true;
                    //}
                    //else
                    //{
                    //    blnIsLastNode = true;
                    //    mdObj.SelectNextNodeOnDeletion(null, blnIsLastNode);
                    //}
                    if (NextGenBLL.PipingBLL.GetNodeProperty(DeleteNode).ToString() != "False")
                    {
                        NextGenModel.ImageData NodeData = (NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(DeleteNode);
                        if (((NextGenModel.ImageData)NextGenBLL.PipingBLL.GetNodeProperty(DeleteNode)).equipmentType == "Indoor")
                        {
                            foreach (RoomIndoor r in projectLegacy.RoomIndoorList)
                            {
                                if (r.IndoorNO == Convert.ToInt32(NodeData.UniqName.Remove(0, 3)))
                                {
                                    projectLegacy.RoomIndoorList.Remove(r);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (projectLegacy.CanvasODUList != null && projectLegacy.CanvasODUList.Count > 0)
                                foreach (JCHVRF.Model.NextGen.SystemVRF r in projectLegacy.CanvasODUList)
                                {
                                    if (r.NO == Convert.ToInt32(NodeData.UniqName.Remove(0, 3)))
                                    {
                                        projectLegacy.CanvasODUList.Remove(r);
                                        break;
                                    }
                                }
                        }
                        OnSelectEquipment(null);
                    }
                }
              
                
            }
            catch { }

        }

        //end for context menu canvas

    }
}
