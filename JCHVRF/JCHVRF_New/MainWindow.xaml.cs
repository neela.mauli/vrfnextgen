﻿
using JCHVRF.BLL.New;
using JCHVRF.Model.New;
using JCBase.UI;
using JCHVRF.VRFMessage;
using System;
using System.Windows;
using System.Windows.Controls;
using JCBase.Utility;
using System.Linq;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using JCHVRF.BLL;

using System.Drawing;
using System.Windows.Media;
using System.Text.RegularExpressions;
using System.Threading;
using JCHVRF_New.Views;

namespace JCHVRF_New
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        JCHVRF.Model.Project projectLegacy = new JCHVRF.Model.Project();
        JCHVRF.Model.DefaultSettingModel designConditionsLegacy = new JCHVRF.Model.DefaultSettingModel();


        ProjectInfoBLL objProjectInfoBll = new ProjectInfoBLL();
        JCHVRF.DAL.RegionDAL objRegionDal = new JCHVRF.DAL.RegionDAL();
        JCHVRF.BLL.RegionBLL objRegionBll = new JCHVRF.BLL.RegionBLL();

        Floor objflr = new Floor();
        ProjectBlobInfo objprojblob = new ProjectBlobInfo();

        private bool isValid = true;
        bool isFirstLoad = false;
        bool isDesignTabFirstLoad = true;
        public int floorcount;

        ObservableCollection<Floor> listFloor = new ObservableCollection<Floor>();
        ObservableCollection<DesignCondition> designconditions = new ObservableCollection<DesignCondition>();


        public MainWindow()
        {
            InitializeComponent();
            BindRegion();
            BindSubRegion();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            this.MaxHeight = 750;
            this.MaxWidth = 1000;
            this.ResizeMode = ResizeMode.NoResize;
            DeliveryDate.SelectedDate = DateTime.Now;
        }

        #region BindRegion & SubRegion

        /// <summary>
        /// Function To Bind List of Region on UI
        /// </summary>
        public void BindRegion()
        {
            DataTable objtbl = objRegionDal.GetParentRegionTable();
            CmbRegion.ItemsSource = objtbl.DefaultView;
            CmbRegion.DisplayMemberPath = objtbl.Columns["Region"].ToString();
            CmbRegion.SelectedValuePath = objtbl.Columns["Code"].ToString();

            //Default Selection of Western Europe Region
            CmbRegion.SelectedItem = CmbRegion.Items.GetItemAt(3);
            CmbRegion.IsEnabled = false;

            //Set ProjectLegacy Data
            projectLegacy.RegionCode = Convert.ToString(CmbRegion.SelectedValue);
        }

        /// <summary>
        /// Function To Bind List of SubRegion on UI
        /// </summary>
        public void BindSubRegion()
        {
            DataTable dt = objRegionBll.GetSubRegionList(this.CmbRegion.SelectedValue.ToString());
            CmbSubRegion.ItemsSource = dt.DefaultView;
            CmbSubRegion.DisplayMemberPath = dt.Columns["Region"].ToString();
            CmbSubRegion.SelectedValuePath = dt.Columns["Code"].ToString();

            //Default Selection of Western Europe Region
            CmbSubRegion.SelectedItem = CmbSubRegion.Items.GetItemAt(CmbSubRegion.Items.Count - 1);
            CmbSubRegion.IsEnabled = false;

            //Set ProjectLegacy Data
            projectLegacy.SubRegionCode = Convert.ToString(CmbSubRegion.SelectedValue);
        }

        /// <summary>
        /// Function To Bind List of SubRegions on the basis of Selection of Region on UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbRegion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BindSubRegion();
        }
        #endregion BindRegion

        #region UIEvents
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            NewClient frmNewClient = new NewClient();
            frmNewClient.ShowDialog();
            string sCompnayName = string.Empty;
            if (Application.Current.Properties["ClientCompanyName"] != null)
                sCompnayName = Application.Current.Properties["ClientCompanyName"].ToString();
            txtExistingClient.Text = sCompnayName;
        }
        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            // ... Get DatePicker reference.
            var picker = sender as DatePicker;

            // ... Get nullable DateTime from SelectedDate.
            DateTime? date = picker.SelectedDate;
            if (date == null)
            {
                // ... A null object.
                this.Title = "No date";
            }
            //else if (date != null)
            //{
            //    if (picker.SelectedDate < DateTime.Now.Date)
            //    {
            //        MessageBox.Show("Delivery Date cannot be before than Current date");
            //        DeliveryDate.SelectedDate = DateTime.Now;
            //        // DeliveryDate.SelectedDate = Convert.ToDateTime(date.Value.ToString("dd-MM-yyyy"));
            //    }

            //}
            else
            {
                // ... No need to display the time.
                //this.Title = date.Value.ToShortDateString();
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }
        private void txtExistingClient_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtExistingClient.Text.Trim()))
            {
                lblExistingClient.Content = "Client Name can not be blank.";

            }
            else
            {
                lblExistingClient.Content = string.Empty;
            }
        }
        private void txtProjectName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtProjectName.Text.Trim()))
            {
                lblProjectName.Text = "Project Name can not be blank.";

            }
            else
            {
                lblProjectName.Text = string.Empty;
            }
        }
        private void DoCancel()
        {
            //To Do:
            //Dashboard objFrmDashboard = new Dashboard();
            //objFrmDashboard.ShowDialog();
        }
        private void btnProjectInfoNext_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!(ValidateInputFeilds()))
                {
                    ValidateProjectData();
                    if (DeliveryDate.SelectedDate < DateTime.Now.Date)
                    {
                        MessageBox.Show("Delivery Date cannot be less than Current date");
                        DeliveryDate.SelectedDate = DateTime.Now;
                    }
                    else if (isValid == false)
                    {
                        MessageBox.Show("Please fill  mandatory fields.");
                        txtProjectName.Focus();
                    }
                    else
                    {
                        int index = Tab.SelectedIndex + 1;
                        if (index >= Tab.Items.Count)
                            index = 0;
                        Tab.SelectedIndex = index;
                        SetDesignCondition();
                        #region SetProjectLegacyData for ProjectInfo
                        projectLegacy.Name = txtProjectName.Text;
                        projectLegacy.clientName = txtExistingClient.Text;
                        projectLegacy.Location = txtProjectLocation.Text;
                        projectLegacy.DeliveryRequiredDate = Convert.ToDateTime(DeliveryDate.Text);
                        projectLegacy.Remarks = txtNotes.Text;
                        if (rbbh.IsChecked == true)
                        {
                            projectLegacy.BrandCode = "H";
                        }

                        // Start - Removing it for now as per bug #1546
                        //if (rbby.IsChecked == true)
                        //{
                        //    projectLegacy.BrandCode = "Y";
                        //}
                        // End - Removing it for now as per bug #1546
                        //ToDo
                        projectLegacy.FactoryCode = "S";
                        #endregion
                    }
                }

            }
            catch (System.Exception)
            {
                throw;
            }
        }
        private void btnDesignConditionsPrevious_Click(object sender, RoutedEventArgs e)
        {
            //prev
            int index = Tab.SelectedIndex - 1;
            if (index < 0)
                index = Tab.Items.Count - 1;
            Tab.SelectedIndex = index;
        }
        private void btnDesignConditionsNext_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateCoolDryBulb() == false || ValidateCoolWetBulb() == false || ValidateRH() == false || ValidateHDDBT() == false || ValidateOdb() == false || ValidateOIw() == false || ValidateOHDIW() == false || ValidateOHDWBT() == false || ValidateOutdoorHDRH() == false || ValidateOHDIW() == false || ValidateHDB() == false)
            {
                // MessageBox.Show("Please Enter Valid Range");
                return;
            }

            populateDesigncondition(); // Start Bug# 1537
            int index = Tab.SelectedIndex + 1;
            if (index >= Tab.Items.Count)
                index = 0;
            Tab.SelectedIndex = index;

            //ToDo
            //Set DesignConditions Data in Project
            projectLegacy.Altitude = Convert.ToInt32(NumericAltitude.Value);
            var designCond = designConditionsLegacy;

        }
        private void rbtMultiFloor_Click(object sender, RoutedEventArgs e)
        {
            if (rbtMultiFloor.IsChecked == true)
                btnBulkAdd.IsEnabled = true;
            stkMultiFloor.Visibility = Visibility.Visible;
            tbFloor.Visibility = Visibility.Visible;
            btnTypeNext.Visibility = Visibility.Visible;
            txtMultiFloor.Text = "2";
            isFirstLoad = true;


        }
        private void rbtSingleFloor_Click(object sender, RoutedEventArgs e)
        {
            if (rbtSingleFloor.IsChecked == true)
                btnBulkAdd.IsEnabled = false;
            stkMultiFloor.Visibility = Visibility.Hidden;
            // commented as per sushil
            //tbFloor.Visibility = Visibility.Hidden;
            //btnTypeNext.Visibility = Visibility.Hidden;
            objflr.MultiFloorCount = 1;
            //btnTypeNext.Content = "Create";
            isFirstLoad = true;
        }
        private void btnProjectInfoCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnTypeCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void DesignConditionCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnFloorCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnTypePre_Click(object sender, RoutedEventArgs e)
        {
            int index = Tab.SelectedIndex - 1;
            if (index < 0)
                index = Tab.Items.Count - 1;
            Tab.SelectedIndex = index;
            populateDesigncondition(); // bug#1537
        }
        private void btnTypeNext_Click(object sender, RoutedEventArgs e)
        {
            EnableDisableControl();

            if (rbtMultiFloor.IsChecked == true)
            {
                if (txtMultiFloor.Text == "")
                {
                    MessageBox.Show("Floor number ranges between 1 and 1000!");
                    txtMultiFloor.Focus();
                    return;
                }
                int iFloorValue = (Int32.Parse(txtMultiFloor.Text));
                if (iFloorValue > 999 || iFloorValue == 0)
                {
                    txtMultiFloor.Focus();
                    MessageBox.Show("Floor number ranges between 1 and 1000!");
                    txtMultiFloor.Text = iFloorValue.ToString();
                    goto exitfun;
                }

                if (txtMultiFloor.Text != "" && txtMultiFloor.Text != "0" && txtMultiFloor.Text != "1")
                {
                    txtMultiFloor.Text = iFloorValue.ToString();

                }
                else if (txtMultiFloor.Text == "1")
                {

                    objflr.MultiFloorCount = 1;
                }
                else
                {
                    objflr.MultiFloorCount = 1;
                    MessageBox.Show("Please enter number of floors or select single floor.");
                    Tab.SelectedIndex = Tab.SelectedIndex - 1;
                }
            }
            else
                btnBulkAdd.IsEnabled = false;

            //next
            int index = Tab.SelectedIndex + 1;
            if (index >= Tab.Items.Count)
                index = 0;
            Tab.SelectedIndex = index;

            //Set ProjectData for Piping Type and Floors
            //If 2 Pipe: Heating Mode is ineffective
            //If 3 Pipe: Heating Mode is effective
            if (rbtBrand2Pipe.IsEnabled)
            {
                projectLegacy.IsCoolingModeEffective = true;
                projectLegacy.IsHeatingModeEffective = false;
            }

            if (rbtBrand3Pipe.IsEnabled)
            {
                projectLegacy.IsCoolingModeEffective = true;
                projectLegacy.IsHeatingModeEffective = true;
            }
            exitfun:
            ;// do nothing
        }
        private void btnChangeTemp_Click(object sender, RoutedEventArgs e)
        {
            if (btnChangeTemp.Content.ToString().Trim() == "Change To °F".Trim())
            {

                NumericCoolDryBulb.Value = CelsiusToFahrenheit(NumericCoolDryBulb.Value);
                NumericCoolWetBulb.Value = CelsiusToFahrenheit(NumericCoolWetBulb.Value);
                NumericHeatingDryBulb.Value = CelsiusToFahrenheit(NumericHeatingDryBulb.Value);
                NumericOutdoorDB.Value = CelsiusToFahrenheit(NumericOutdoorDB.Value);
                NumericOutdoorIntelWater.Value = CelsiusToFahrenheit(NumericOutdoorIntelWater.Value);
                NumeroutdoorHDDBT.Value = CelsiusToFahrenheit(NumeroutdoorHDDBT.Value);
                NumeroutdoorHDWBT.Value = CelsiusToFahrenheit(NumeroutdoorHDWBT.Value);
                NumeroutdoorIntelWaterTemp.Value = CelsiusToFahrenheit(NumeroutdoorIntelWaterTemp.Value);
                btnChangeTemp.Content = "Change To °C";
                lblCoolDryBulb.Text = "°F";
                lblCoolWetBulb.Text = "°F";
                lblNumericOutDB.Text = "°F";
                lblOutIntelWater.Text = "°F";
                lblNumericOutTempIntelWater.Text = "°F";
                lblNumeroutdoorHDDBT.Text = "°F";
                lblNumeroutdoorHDWBT.Text = "°F";
                lblOutIntelWater.Text = "°F";
                lblHeatingDB.Text = "°F";
                lblNumericOutTempIntelWater.Text = "°F";

            }
            else if (btnChangeTemp.Content.ToString() == "Change To °C")
            {

                NumericCoolDryBulb.Value = FahrenheitToCelsius(NumericCoolDryBulb.Value);
                NumericCoolWetBulb.Value = FahrenheitToCelsius(NumericCoolWetBulb.Value);
                NumericHeatingDryBulb.Value = FahrenheitToCelsius(NumericHeatingDryBulb.Value);
                NumericOutdoorIntelWater.Value = FahrenheitToCelsius(NumericOutdoorIntelWater.Value);
                NumericOutdoorDB.Value = FahrenheitToCelsius(NumericOutdoorDB.Value);
                NumeroutdoorHDDBT.Value = FahrenheitToCelsius(NumeroutdoorHDDBT.Value);
                NumeroutdoorHDWBT.Value = FahrenheitToCelsius(NumeroutdoorHDWBT.Value);
                //NumeroutdoorHDRH.Value = FahrenheitToCelsius(NumeroutdoorHDRH.Value); // Correction done - This is a percentage value
                NumeroutdoorIntelWaterTemp.Value = FahrenheitToCelsius(NumeroutdoorIntelWaterTemp.Value);// Added this to handle conversion for this value
                lblCoolDryBulb.Text = "°C";
                lblCoolWetBulb.Text = "°C";
                lblNumericOutDB.Text = "°C";
                lblNumeroutdoorHDDBT.Text = "°C";
                lblNumeroutdoorHDWBT.Text = "°C";
                lblOutIntelWater.Text = "°C";
                lblHeatingDB.Text = "°C";
                lblNumericOutTempIntelWater.Text = "°C";
                btnChangeTemp.Content = "Change To °F";
            }

        }


        private void btnFloorPre_Click(object sender, RoutedEventArgs e)
        {
            int index = Tab.SelectedIndex - 1;
            if (index < 0)
                index = Tab.Items.Count - 1;
            Tab.SelectedIndex = index;
            // added to handle txtMultiFloor Text value as per the change in floor tab
            txtMultiFloor.Text = dtFloor.Items.Count.ToString();
        }

        #region TabFunctions

        private void tbFloor_Selected(object sender, RoutedEventArgs e)
        {
            EnableDisableControl();

            if (rbtMultiFloor.IsChecked == true && txtMultiFloor.Text != "0" && txtMultiFloor.Text != "")
            {

                objflr.MultiFloorCount = Convert.ToInt32(txtMultiFloor.Text);

            }

            else if (rbtMultiFloor.IsChecked == true)
            {


                if (Convert.ToInt32(txtMultiFloor.Text) > 999 || Convert.ToInt32(txtMultiFloor.Text) == 0)
                {
                    txtMultiFloor.Focus();
                    MessageBox.Show("Floor number ranges between 1 and 1000!");

                    goto exitfun;
                }

            }
            else
            {
                btnBulkAdd.IsEnabled = false;
                isFirstLoad = true;//when radioButton selected is SingleFloor
                objflr.MultiFloorCount = 1; //when radioButton selected is SingleFloor
            }

            if (isFirstLoad || floorcount != objflr.MultiFloorCount)// updated to handle scenario - go back to 'Type' tab and modify floorcount
            {

                AddFloorForFirstLoad(objflr.MultiFloorCount);

                isFirstLoad = false;
            }

            dtFloor.ItemsSource = listFloor;
            txtMultiFloor.Text = Convert.ToString(dtFloor.Items.Count);
            floorcount = objflr.MultiFloorCount;

            exitfun:
            ;
        }
        private void DesignTab_Selected(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtProjectName.Text) || string.IsNullOrEmpty(txtExistingClient.Text)
                                                          || string.IsNullOrEmpty(txtProjectLocation.Text))
            {
                MessageBox.Show("Please fill  the mandatory fields.");
            }
            else
                SetDesignCondition();
        }
        private void TypeTab_Selected(object sender, RoutedEventArgs e)
        {
            if (isFirstLoad)
                txtMultiFloor.Text = Convert.ToString(dtFloor.Items.Count);
            isFirstLoad = false;

        }
        #endregion TabFunctions

        private void txtMultiFloor_TextChanged(object sender, TextChangedEventArgs e)
        {
            isFirstLoad = true;

        }
        private void btnProjectInfoCancel_OnClick(object sender, RoutedEventArgs e)
        {
            DoCancel();
        }
        private void btnDesignConditionCancel_OnClick(object sender, RoutedEventArgs e)
        {
            DoCancel();
        }
        private void BtnBulkAdd_OnClick(object sender, RoutedEventArgs e)
        {
            AddBulkFloor objAssBulkFloor = new AddBulkFloor();
            objAssBulkFloor.ObMainWindow = this;
            objAssBulkFloor.ShowDialog();
        }
        private void btnFlooAdd_Click(object sender, RoutedEventArgs e)
        {

            if (listFloor.Count < 1000)
                AddFloor(1);
            else
                MessageBox.Show("Floor limit is 1000 !");

            if (rbtSingleFloor.IsChecked == true && listFloor.Count == 1)
            {
                btnFlooAdd.IsEnabled = false;
            }
        }
        private void btnFloorRemove_Click(object sender, RoutedEventArgs e)
        {
            EnableDisableControl();
            if (listFloor != null && listFloor.Count > 0)
            {
                int lastIndex = listFloor.Count - 1;
                listFloor.RemoveAt(lastIndex);
            }
            if (listFloor != null && rbtSingleFloor.IsChecked == true && listFloor.Count == 0)
            {
                btnFlooAdd.IsEnabled = true;
            }
            dtFloor.ItemsSource = listFloor;
            txtMultiFloor.Text = Convert.ToString(dtFloor.Items.Count);
        }
        private void chkSelectAll_Checked(object sender, RoutedEventArgs e)
        {
            if (listFloor != null && listFloor.Count > 0)
            {
                foreach (var item in listFloor)
                {
                    item.IsFloorChecked = true;
                }
                dtFloor.ItemsSource = listFloor;
            }
        }
        private void chkSelectAll_Unchecked(object sender, RoutedEventArgs e)
        {

            if (listFloor != null && listFloor.Count > 0)
            {
                foreach (var item in listFloor)
                {
                    item.IsFloorChecked = false;
                }
                dtFloor.ItemsSource = listFloor;
            }
        }
        private void BtnRemove_OnClick(object sender, RoutedEventArgs e)
        {
            ObservableCollection<Floor> newListFloor = new ObservableCollection<Floor>();
            if (listFloor != null && listFloor.Count > 0)
            {
                foreach (var item in listFloor)
                {
                    if (item.IsFloorChecked != true)
                    {
                        newListFloor.Add(item);
                    }

                }
                listFloor.Clear();
                listFloor = newListFloor;
                dtFloor.ItemsSource = newListFloor;
            }
        }
        #endregion

        #region Validations
        private void ValidateProjectData()
        {
            if (string.IsNullOrEmpty(txtProjectName.Text.Trim()) || string.IsNullOrEmpty(txtExistingClient.Text.Trim()))
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }



        }
        #region DesignConditionValidations
        private bool ValidateCoolDryBulb()
        {
            double nCDBVal = Convert.ToDouble(NumericCoolDryBulb.Value);
            if ((nCDBVal >= 16) && (nCDBVal <= 30))
            {

                lblValCoolDryBulb.Content = string.Empty;
                return true;

            }
            else
            {
                lblValCoolDryBulb.Content = "Range[16, 30]";

                if (Convert.ToDecimal(NumericCoolDryBulb.Value) < Convert.ToDecimal(NumericCoolWetBulb.Value) && !(NumericCoolDryBulb.Value == 0))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }

                return false;
            }

        }
        private void EnableDisableControl()
        {
            if (rbtSingleFloor.IsChecked == true)
            {
                btnFlooAdd.IsEnabled = false;
            }
            else
            {
                btnFlooAdd.IsEnabled = true;
            }
        }
        private bool ValidateCoolDryBulbF()
        {
            double nCDBVal = Convert.ToDouble(NumericCoolDryBulb.Value);
            if ((nCDBVal > 60.8) && (nCDBVal < 86))
            {

                lblValCoolDryBulb.Content = string.Empty;
                return true;

            }
            else
            {
                lblValCoolDryBulb.Content = "Range[60.8, 86]";

                if (Convert.ToDecimal(NumericCoolDryBulb.Value) < Convert.ToDecimal(NumericCoolWetBulb.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }

                return false;
            }

        }
        private bool ValidateCoolWetBulbF()
        {
            double nCWBVal = Convert.ToDouble(NumericCoolWetBulb.Value);

            if ((nCWBVal > 57.2) && (nCWBVal < 75.2))
            {

                lblValCoolWetBulb.Content = string.Empty;
                if (Convert.ToDecimal(NumericCoolWetBulb.Value) > Convert.ToDecimal(NumericCoolDryBulb.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }
                return true;

            }
            else
            {
                lblValCoolWetBulb.Content = "Range[57.2,75.2]";

                if (Convert.ToDecimal(NumericCoolWetBulb.Value) > Convert.ToDecimal(NumericCoolDryBulb.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }
                return false;
            }


        }
        private bool ValidateCoolWetBulb()
        {
            double nCWBVal = Convert.ToDouble(NumericCoolWetBulb.Value);

            if ((nCWBVal >= 14) && (nCWBVal <= 24))
            {

                lblValCoolWetBulb.Content = string.Empty;
                if (Convert.ToDecimal(NumericCoolWetBulb.Value) > Convert.ToDecimal(NumericCoolDryBulb.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));
                    return false;
                }

                return true;

            }
            else
            {
                lblValCoolWetBulb.Content = "Range[14, 24]";

                if (Convert.ToDecimal(NumericCoolWetBulb.Value) > Convert.ToDecimal(NumericCoolDryBulb.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }
                return false;
            }


        }
        private bool ValidateRH()
        {
            double nRHVal = Convert.ToDouble(NumericInternalRH.Value);

            if ((nRHVal >= 13) && (nRHVal <= 100))
            {

                lblRelativeHumidity.Content = string.Empty;
                return true;

            }
            else
            {
                lblRelativeHumidity.Content = "Range[13, 100]";
                return false;
            }

        }
        private bool ValidateHDB()
        {
            double nHDBVal = Convert.ToDouble(NumericHeatingDryBulb.Value);

            if ((nHDBVal >= 16) && (nHDBVal <= 24))
            {

                lblHeatingDryBulb.Content = string.Empty;
                return true;

            }
            else
            {
                lblHeatingDryBulb.Content = "Range[16, 24]";
                return false;
            }

        }
        private bool ValidateHDBF()
        {
            double nHDBVal = Convert.ToDouble(NumericHeatingDryBulb.Value);

            if ((nHDBVal > 60.8) && (nHDBVal < 75.2))
            {

                lblHeatingDryBulb.Content = string.Empty;
                return true;

            }
            else
            {
                lblHeatingDryBulb.Content = "Range[60.8, 75.2]";
                return false;
            }

        }
        private bool ValidateOdb()
        {
            double nOdb = Convert.ToDouble(NumericOutdoorDB.Value);

            if ((nOdb >= 10) && (nOdb <= 43))
            {

                lblOutdoorDb.Content = string.Empty;
                return true;

            }
            else
            {
                lblOutdoorDb.Content = "Range[10, 43]";
                return false;
            }

        }
        private bool ValidateOdbF()
        {
            double nOdb = Convert.ToDouble(NumericOutdoorDB.Value);

            if ((nOdb > 50) && (nOdb < 109.4))
            {

                lblOutdoorDb.Content = string.Empty;
                return true;

            }
            else
            {
                lblOutdoorDb.Content = "Range[50, 109.4]";
                return false;
            }

        }
        private bool ValidateOIw()
        {
            double nOdb = Convert.ToDouble(NumericOutdoorIntelWater.Value);

            if ((nOdb >= 10) && (nOdb <= 45))
            {

                lblOutdoorIw.Content = string.Empty;
                return true;

            }
            else
            {
                lblOutdoorIw.Content = "Range[10, 45]";
                return false;
            }

        }
        private bool ValidateOIwF()
        {
            double nOdb = Convert.ToDouble(NumericOutdoorIntelWater.Value);

            if ((nOdb > 50) && (nOdb < 113))
            {

                lblOutdoorIw.Content = string.Empty;
                return true;

            }
            else
            {
                lblOutdoorIw.Content = "Range[50, 113]";
                return false;
            }

        }
        private bool ValidateHDDBT()
        {
            double nOHDDBT = Convert.ToDouble(NumeroutdoorHDDBT.Value);

            if ((nOHDDBT >= -18) && (nOHDDBT <= 33))
            {

                lblOutdoorHDDBT.Content = string.Empty;
                if (Convert.ToDecimal(NumeroutdoorHDDBT.Value) < Convert.ToDecimal(NumeroutdoorHDWBT.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));
                    return false;
                }
                return true;

            }
            else
            {

                lblOutdoorHDDBT.Content = "Range[-18, 33]";

                if (Convert.ToDecimal(NumeroutdoorHDDBT.Value) < Convert.ToDecimal(NumeroutdoorHDWBT.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }
                return false;
            }
        }
        private bool ValidateHDDBTF()
        {
            double nOHDDBT = Convert.ToDouble(NumeroutdoorHDDBT.Value);

            if ((nOHDDBT > -0.4) && (nOHDDBT < 91.4))
            {

                lblOutdoorHDDBT.Content = string.Empty;
                if (Convert.ToDecimal(NumeroutdoorHDDBT.Value) < Convert.ToDecimal(NumeroutdoorHDWBT.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }
                return true;

            }
            else
            {
                lblOutdoorHDDBT.Content = "Range[-0.4, 91.4]";

                if (Convert.ToDecimal(NumeroutdoorHDDBT.Value) < Convert.ToDecimal(NumeroutdoorHDWBT.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }
                return false;
            }
        }
        private bool ValidateOHDWBT()
        {
            double nOHDWBT = Convert.ToDouble(NumeroutdoorHDWBT.Value);

            if ((nOHDWBT >= -20) && (nOHDWBT <= 15))
            {

                lblOutdoorHDWBT.Content = string.Empty;
                if (Convert.ToDecimal(NumeroutdoorHDDBT.Value) < Convert.ToDecimal(NumeroutdoorHDWBT.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));
                    return false;
                }
                return true;

            }
            else
            {

                lblOutdoorHDWBT.Content = "Range[-20, 15]";

                if (Convert.ToDecimal(NumeroutdoorHDDBT.Value) < Convert.ToDecimal(NumeroutdoorHDWBT.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }
                return false;
            }
        }
        private bool ValidateOHDWBTF()
        {
            double nOHDWBT = Convert.ToDouble(NumeroutdoorHDWBT.Value);

            if ((nOHDWBT > -4) && (nOHDWBT < 59))
            {

                lblOutdoorHDWBT.Content = string.Empty;
                if (Convert.ToDecimal(NumeroutdoorHDDBT.Value) < Convert.ToDecimal(NumeroutdoorHDWBT.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }
                return true;

            }
            else
            {

                lblOutdoorHDWBT.Content = "Range[-4, 59]";

                if (Convert.ToDecimal(NumeroutdoorHDDBT.Value) < Convert.ToDecimal(NumeroutdoorHDWBT.Value))
                {

                    JCMsg.ShowWarningOK(Msg.WARNING_TXT_LESSTHAN(UnitTemperature.WB.ToString(), UnitTemperature.DB.ToString()));

                }
                return false;
            }
        }
        private bool ValidateOutdoorHDRH()
        {
            double nOdb = Convert.ToDouble(NumeroutdoorHDRH.Value);
            if ((nOdb >= 13) && (nOdb <= 100))
            {
                lblOutdoorHDRH.Content = string.Empty;
                return true;
            }
            else
            {
                lblOutdoorHDRH.Content = "Range[13, 100]";
                return false;
            }
        }
        private bool ValidateOHDIW()
        {
            double nOHDIW = Convert.ToDouble(NumeroutdoorIntelWaterTemp.Value);

            if ((nOHDIW >= 10) && (nOHDIW <= 45))
            {

                lblOutdoorHDIW.Content = string.Empty;
                return true;

            }
            else
            {
                lblOutdoorHDIW.Content = "Range[10, 45]";
                return false;
            }

        }
        private bool ValidateOHDIWF()
        {
            double nOHDIW = Convert.ToDouble(NumeroutdoorIntelWaterTemp.Value);

            if ((nOHDIW > 50) && (nOHDIW < 113))
            {

                lblOutdoorHDIW.Content = string.Empty;
                return true;

            }
            else
            {
                lblOutdoorHDIW.Content = "Range[50, 113]";
                return false;
            }

        }

        #endregion DesignConditionValidations

        private bool ValidateInputFeilds()
        {
            string specialCharacters = @"\*?/:'<>|" + "\"";
            char[] specialCharactersArray = specialCharacters.ToCharArray();
            if (txtProjectName.Text.IndexOfAny(specialCharactersArray) > -1)
            {
                MessageBox.Show("[Project Name] cannot contain any of the following characters: " + specialCharacters, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                txtProjectName.Focus();
                return true;
            }
            else if (txtProjectLocation.Text.IndexOfAny(specialCharactersArray) > -1)
            {
                MessageBox.Show("[Project Location] cannot contain any of the following characters: " + specialCharacters, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                txtProjectLocation.Focus();
                return true;
            }
            return false;
        }


        #endregion Validations

        #region DesignConditions
        void SetDefaultDesignConditions()
        {
            NumericCoolDryBulb.Value = Convert.ToDecimal(designConditionsLegacy.IndoorCoolingDB);
            NumericCoolWetBulb.Value = Convert.ToDecimal(designConditionsLegacy.IndoorCoolingWB);
            NumericInternalRH.Value = Convert.ToDecimal(designConditionsLegacy.IndoorCoolingRH);
            NumericHeatingDryBulb.Value = Convert.ToDecimal(designConditionsLegacy.IndoorHeatingDB);
            NumericOutdoorDB.Value = Convert.ToDecimal(designConditionsLegacy.OutdoorCoolingDB);
            NumericOutdoorIntelWater.Value = Convert.ToDecimal(designConditionsLegacy.OutdoorCoolingIW);
            NumeroutdoorHDDBT.Value = Convert.ToDecimal(designConditionsLegacy.OutdoorHeatingDB);
            NumeroutdoorHDWBT.Value = Convert.ToDecimal(designConditionsLegacy.OutdoorHeatingWB);
            NumeroutdoorHDRH.Value = Convert.ToDecimal(designConditionsLegacy.OutdoorHeatingRH);
            //  NumeroutdoorHDRH.Value = 50;
            calRH((decimal)designConditionsLegacy.IndoorCoolingDB, (decimal)designConditionsLegacy.IndoorCoolingWB, true);
            calRH((decimal)designConditionsLegacy.OutdoorCoolingDB, (decimal)designConditionsLegacy.OutdoorHeatingWB, false);
            NumeroutdoorIntelWaterTemp.Value = Convert.ToDecimal(designConditionsLegacy.OutdoorHeatingIW);
        }

        private void SetDesignCondition()
        {
            if (isDesignTabFirstLoad)
            {
                //designconditions.Add(new DesignCondition { indoorCoolingDB = 27.0m, indoorCoolingWB = 19.6m, indoorCoolingRH = 0.0m, NumericOutdoorIntelWater = 35.0m, indoorCoolingHDB = 20.0m, outdoorHeatingDB = 35.0m, outdoorCoolingDB = 7.0m, outdoorHeatingWB = 3.1m, outdoorHeatingRH = 87.00m, outdoorCoolingIW = 35, outdoorHeatingIW = 15 });
                //foreach (var item in designconditions)
                //{
                //    NumericCoolDryBulb.Value = item.indoorCoolingDB;
                //    NumericCoolWetBulb.Value = item.indoorCoolingWB;
                //    NumericInternalRH.Value = item.indoorCoolingRH;
                //    NumericHeatingDryBulb.Value = item.indoorCoolingHDB;
                //    NumericOutdoorDB.Value = item.outdoorHeatingDB;
                //    NumericOutdoorIntelWater.Value = item.NumericOutdoorIntelWater;
                //    NumeroutdoorHDDBT.Value = item.outdoorCoolingDB;
                //    NumeroutdoorHDWBT.Value = item.outdoorHeatingWB;
                //    calRH((decimal)item.indoorCoolingDB, item.indoorCoolingWB, true);
                //    calRH((decimal)item.outdoorCoolingDB, item.outdoorHeatingWB, false);
                //    NumeroutdoorIntelWaterTemp.Value = item.outdoorHeatingIW;
                //    break;

                //}
                SetDefaultDesignConditions();
                isDesignTabFirstLoad = false;
            }
            else
            {
                populateDesigncondition();
                // SetDefaultDesignConditions();
                //foreach (var item in designconditions)
                //{
                //    NumericCoolDryBulb.Value = item.indoorCoolingDB;
                //    NumericCoolWetBulb.Value = item.indoorCoolingWB;
                //    NumericInternalRH.Value = item.indoorCoolingRH;
                //    NumericHeatingDryBulb.Value = item.indoorCoolingHDB;
                //    NumericOutdoorDB.Value = item.outdoorHeatingDB;
                //    NumericOutdoorIntelWater.Value = item.NumericOutdoorIntelWater;
                //    NumeroutdoorHDDBT.Value = item.outdoorCoolingDB;
                //    NumeroutdoorHDWBT.Value = item.outdoorHeatingWB;
                //    NumeroutdoorHDRH.Value = item.outdoorHeatingRH;
                //    NumeroutdoorIntelWaterTemp.Value = item.outdoorHeatingIW;
                //    break;
                //}
            }

        }
        void calRH(decimal dt, decimal wt, bool isIn)
        {
            FormulaCalculate fcal = new FormulaCalculate();
            decimal p = fcal.GetPressure(0);
            decimal rh = fcal.GetRH(dt, wt, p);

            if (isIn)
            {
                NumericInternalRH.Value = Convert.ToDecimal((rh * 100).ToString("n0"));
            }
            else
            {
                //NumeroutdoorHDRH.Value = Convert.ToDecimal((rh * 100).ToString("n0")); // This sets RH to zero everytime. commenting it for now.
            }

        }
        public void DoCalculateByOptionInd(string Opt)
        {
            string ut_temperature;
            if (btnChangeTemp.Content.ToString() == "Change To °C")
            {
                ut_temperature = "°F";

            }
            else
            {
                ut_temperature = "°C";
            }

            double dbcool = Unit.ConvertToSource(Convert.ToDouble(NumericCoolDryBulb.Value.ToString()), UnitType.TEMPERATURE, ut_temperature);
            double wbcool = Unit.ConvertToSource(Convert.ToDouble(NumericCoolWetBulb.Value.ToString()), UnitType.TEMPERATURE, ut_temperature);
            double rhcool = Convert.ToDouble(NumericInternalRH.Value);
            FormulaCalculate fc = new FormulaCalculate();
            decimal pressure = fc.GetPressure(Convert.ToDecimal(0));
            if (Opt == UnitTemperature.WB.ToString())
            {
                double rh = Convert.ToDouble(fc.GetRH(Convert.ToDecimal(dbcool), Convert.ToDecimal(wbcool), pressure));

                if (this.NumericInternalRH.Value.ToString() != (rh * 100).ToString("n0"))
                {
                    this.NumericInternalRH.Value = Convert.ToDecimal((rh * 100).ToString("n0"));
                }
            }
            else if (Opt == UnitTemperature.DB.ToString())
            {
                double wb = Convert.ToDouble(fc.GetWTByDT(Convert.ToDecimal(dbcool), Convert.ToDecimal(rhcool / 100), pressure));

                if (NumericCoolDryBulb.Value.ToString() != wb.ToString("n1"))
                {
                    if (rhcool != 0)
                    {

                        NumericCoolWetBulb.Value = Convert.ToDecimal(wb.ToString("n1"));

                    }
                }

            }
            else if (Opt == UnitTemperature.RH.ToString())
            {
                double wb = Convert.ToDouble(fc.GetWTByDT(Convert.ToDecimal(dbcool), Convert.ToDecimal(rhcool / 100), pressure));

                if (this.NumericCoolWetBulb.Value.ToString() != wb.ToString("n1"))
                {
                    if (rhcool != 0)
                    {
                        this.NumericCoolWetBulb.Value = (Decimal)wb;
                    }

                }
            }


        }
        private void DoCalculateByOptionOut(string Opt)
        {
            string ut_temperature;
            if (btnChangeTemp.Content.ToString() == "Change To °C")
            {
                ut_temperature = "°F";
            }
            else
            {
                ut_temperature = "°C";
            }

            if (!string.IsNullOrEmpty(NumeroutdoorHDDBT.Value.ToString()) && !string.IsNullOrEmpty(NumeroutdoorHDWBT.Value.ToString()) && !string.IsNullOrEmpty(this.NumeroutdoorHDRH.Value.ToString()))
            {
                double dbcool = Unit.ConvertToSource(Convert.ToDouble(NumeroutdoorHDDBT.Value.ToString()), UnitType.TEMPERATURE, ut_temperature);
                double wbcool = Unit.ConvertToSource(Convert.ToDouble(this.NumeroutdoorHDWBT.Value.ToString()), UnitType.TEMPERATURE, ut_temperature);
                double rhcool = Convert.ToDouble(this.NumeroutdoorHDRH.Value);

                FormulaCalculate fc = new FormulaCalculate();
                decimal pressure = fc.GetPressure(Convert.ToDecimal(0));
                if (Opt == UnitTemperature.WB.ToString())
                {
                    double rh = Convert.ToDouble(fc.GetRH(Convert.ToDecimal(dbcool), Convert.ToDecimal(wbcool), pressure));

                    if (this.NumeroutdoorHDRH.Value.ToString() != (rh * 100).ToString("n0"))
                    {
                        this.NumeroutdoorHDRH.Value = (decimal)(rh * 100);
                    }
                }
                else if (Opt == UnitTemperature.DB.ToString())
                {
                    double wb = Convert.ToDouble(fc.GetWTByDT(Convert.ToDecimal(dbcool), Convert.ToDecimal(rhcool / 100), pressure));
                    if (this.NumeroutdoorHDWBT.Value.ToString() != wb.ToString("n1"))
                    {
                        if (rhcool != 0)
                        {
                            this.NumeroutdoorHDWBT.Value = (decimal)wb;
                        }
                    }
                }
                else if (Opt == UnitTemperature.RH.ToString())
                {
                    double wb = Convert.ToDouble(fc.GetWTByDT(Convert.ToDecimal(dbcool), Convert.ToDecimal(rhcool / 100), pressure));

                    if (this.NumeroutdoorHDWBT.Value.ToString() != wb.ToString("n1"))
                    {
                        if (rhcool != 0)
                        {
                            this.NumeroutdoorHDWBT.Value = (decimal)wb;
                        }

                    }
                }

            }

        }
        public decimal CelsiusToFahrenheit(decimal Value)
        {
            decimal Fahrenheit, Celsius;
            Celsius = Value;
            Fahrenheit = (Celsius * 9 / 5) + 32;
            return Fahrenheit;
        }
        public decimal FahrenheitToCelsius(decimal Value)
        {
            decimal Fahrenheit, Celsius;
            Fahrenheit = Value;
            Celsius = (Fahrenheit - 32) * 5 / 9;
            return Celsius;
        }
        public void NumericCoolDryBulb_LostFocus(object sender, RoutedEventArgs e)
        {
            if (btnChangeTemp.Content.ToString().Trim() == "Change To °C".Trim())
            {
                if (ValidateCoolDryBulbF() == false)
                {

                }
                else
                {
                    DoCalculateByOptionInd("DB");
                }
            }

            else if (ValidateCoolDryBulb() == false)
            {

            }

            else
            {
                DoCalculateByOptionInd("DB");
            }



        }
        private void NumericCoolWetBulb_LostFocus(object sender, RoutedEventArgs e)
        {


            if (btnChangeTemp.Content.ToString().Trim() == "Change To °C".Trim())
            {
                if (ValidateCoolWetBulbF() == false)
                {

                }
                else
                {
                    DoCalculateByOptionInd("WB");
                }
            }

            else if (ValidateCoolWetBulb() == false)
            {

            }


            else
            {
                DoCalculateByOptionInd("WB");
            }

        }
        private void NumericInternalRH_LostFocus(object sender, RoutedEventArgs e)
        {

            if (ValidateRH() == false)
            {

            }
            else
            {
                DoCalculateByOptionInd(UnitTemperature.RH.ToString()); //issue fix 1647
                                                                       // DoCalculateByOptionInd(NumericInternalRH.Value.ToString());
            }
        }
        private void NumericHeatingDryBulb_LostFocus(object sender, RoutedEventArgs e)
        {
            if (btnChangeTemp.Content.ToString().Trim() == "Change To °C".Trim())
            {
                if (ValidateHDBF() == false)
                {

                }
                else
                {
                    DoCalculateByOptionInd("DB");
                }
            }

            else if (ValidateHDB() == false)
            {

            }
            else
            {
                DoCalculateByOptionInd(UnitTemperature.RH.ToString()); //issue fix 1647
                //  DoCalculateByOptionInd(NumericInternalRH.Value.ToString());
            }
        }
        private void NumericOutdoorIntelWater_LostFocus(object sender, RoutedEventArgs e)
        {
            if (btnChangeTemp.Content.ToString().Trim() == "Change To °C".Trim())
            {
                if (ValidateOIwF() == false)
                {

                }
            }


            else if (ValidateOIw() == false)
            {

            }
            else
            {

            }
        }
        private void NumeroutdoorHDWBT_LostFocus(object sender, RoutedEventArgs e)
        {

            if (btnChangeTemp.Content.ToString().Trim() == "Change To °C".Trim())
            {
                if (ValidateOHDWBTF() == false)
                {

                }
                else
                {
                    DoCalculateByOptionOut(UnitTemperature.WB.ToString());
                }
            }


            else if (ValidateOHDWBT() == false)
            {

            }
            else
            {
                DoCalculateByOptionOut(UnitTemperature.WB.ToString());

            }

        }
        private void NumeroutdoorHDDBT_LostFocus(object sender, RoutedEventArgs e)
        {

            if (btnChangeTemp.Content.ToString().Trim() == "Change To °C".Trim())
            {

                if (ValidateHDDBTF() == false)
                {
                    // JCBase.UI.JCForm.JCValidateGroup((sender as TextBox).Parent as Panel);
                }

                else
                {
                    DoCalculateByOptionOut(UnitTemperature.DB.ToString());

                }

            }

            else if (ValidateHDDBT() == false)
            {

            }

            else
            {
                DoCalculateByOptionOut(UnitTemperature.DB.ToString());

            }


        }
        private void NumeroutdoorHDRH_LostFocus(object sender, RoutedEventArgs e)
        {
            if (ValidateOutdoorHDRH() == false)
            {

            }
            else
            {
                DoCalculateByOptionOut(UnitTemperature.RH.ToString());
            }

        }
        private void NumericOutdoorDB_LostFocus(object sender, RoutedEventArgs e)
        {

            if (btnChangeTemp.Content.ToString().Trim() == "Change To °C".Trim())
            {

                if (ValidateOdbF() == false)
                {
                    // JCBase.UI.JCForm.JCValidateGroup((sender as TextBox).Parent as Panel);
                }


            }

            else if (ValidateOdb() == false)
            {

            }

        }
        private void NumeroutdoorIntelWaterTemp_LostFocus(object sender, RoutedEventArgs e)
        {
            if (btnChangeTemp.Content.ToString().Trim() == "Change To °C".Trim())
            {
                if (ValidateOHDIWF() == false)
                {

                }

            }

            else if (ValidateOHDIW() == false)
            {

            }

        }
        public void populateDesigncondition()
        {
            designconditions = new ObservableCollection<DesignCondition>();
            designconditions.Add(new DesignCondition
            {
                indoorCoolingDB = NumericCoolDryBulb.Value,
                indoorCoolingWB = NumericCoolWetBulb.Value,
                indoorCoolingRH = NumericInternalRH.Value,
                indoorCoolingHDB = NumericHeatingDryBulb.Value,
                outdoorHeatingDB = NumericOutdoorDB.Value,
                NumericOutdoorIntelWater = NumericOutdoorIntelWater.Value,
                outdoorCoolingDB = NumeroutdoorHDDBT.Value,
                outdoorHeatingWB = NumeroutdoorHDWBT.Value,
                outdoorHeatingRH = NumeroutdoorHDRH.Value,
                outdoorHeatingIW = NumeroutdoorIntelWaterTemp.Value,

            });
        }
        #endregion

        #region Type

        #endregion

        #region Floor
        public void AddFloorForFirstLoad(int floorCount)
        {
            EnableDisableControl();


            dtFloor.ItemsSource = null;
            if (rbtSingleFloor.IsChecked == true)
            {
                listFloor = new ObservableCollection<Floor>();
                listFloor.Add(new Floor { floorName = "Floor 0", elevationFromGround = 0, IsFloorChecked = false });
                btnFlooAdd.IsEnabled = false;

            }
            // updated - handles scenario when user reduces floorcount
            if (rbtMultiFloor.IsChecked == true && (listFloor.Count == 0 || listFloor.Count > objflr.MultiFloorCount))
            {
                listFloor = new ObservableCollection<Floor>();
                int dtCount = dtFloor.Items.Count;
                for (int i = 0; i < floorCount; i++)
                {
                    string lblFloor = string.Empty;

                    lblFloor = "Floor " + (dtCount + i);

                    listFloor.Add(new Floor { floorName = lblFloor, elevationFromGround = 0, IsFloorChecked = false });

                }

            }
            //added to handle scenario to keep the listfloor object and add more floors to the same object when floorcount is increased
            else
            {
                for (int i = listFloor.Count; i < objflr.MultiFloorCount; i++)
                {
                    string lblFloor = string.Empty;

                    lblFloor = "Floor " + (listFloor.Count);

                    listFloor.Add(new Floor { floorName = lblFloor, elevationFromGround = 0, IsFloorChecked = false });

                }
            }
        }
        public void AddFloor(int floorCount = 0)
        {
            int dtCount = dtFloor.Items.Count;

            if (projectLegacy.IsCoolingModeEffective == true && projectLegacy.IsHeatingModeEffective == false)
                rbtBrand2Pipe.IsChecked = true;
            else
                rbtBrand3Pipe.IsChecked = true;

            if (floorCount > 0)
                rbtMultiFloor.IsChecked = true;
            else
                rbtSingleFloor.IsChecked = true;

            if ((dtCount + floorCount) > 999)
            {
                MessageBox.Show("Floor limit is 1000 !");
                return;
            }

            for (int i = 0; i < floorCount; i++)
            {
                string lblFloor = string.Empty;
                lblFloor = "Floor " + (dtCount + i);
                listFloor.Add(new Floor { floorName = lblFloor, elevationFromGround = 0, IsFloorChecked = false });
                // Start Added to update txtMultipleFloor textbox
                objflr.MultiFloorCount = objflr.MultiFloorCount + 1;
                floorcount = objflr.MultiFloorCount;
                // End Added to update txtMultipleFloor textbox
            }

        }
        #endregion

        #region SaveProjectData
        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            #region CreatePojectObjectNew
            JCHVRF.Model.New.Project objProjectInfoBlob = new JCHVRF.Model.New.Project();
            objProjectInfoBlob.objProjectInfo.SystemID = "999";
            //ToRemove: Remove HardCoding of ProjectID
            objProjectInfoBlob.objProjectInfo.ProjectID = 10;
            objProjectInfoBlob.objProjectInfo.ProjectName = txtProjectName.Text;
            objProjectInfoBlob.objProjectInfo.ActiveFlag = 1;
            objProjectInfoBlob.objProjectInfo.LastUpdateDate = DateTime.Now.Date;
            objProjectInfoBlob.objProjectInfo.Version = MyConfig.Version;
            objProjectInfoBlob.objProjectInfo.DBVersion = ConfigurationManager.AppSettings["DBVersion"].ToString();
            objProjectInfoBlob.objProjectInfo.Measure = 0;
            objProjectInfoBlob.objProjectInfo.Location = txtProjectLocation.Text;
            objProjectInfoBlob.objProjectInfo.SoldTo = "";
            objProjectInfoBlob.objProjectInfo.ShipTo = "";
            objProjectInfoBlob.objProjectInfo.OrderNo = "";
            objProjectInfoBlob.objProjectInfo.ContractNo = "";
            objProjectInfoBlob.objProjectInfo.Region = "India";
            objProjectInfoBlob.objProjectInfo.Office = "";
            objProjectInfoBlob.objProjectInfo.Engineer = "";
            objProjectInfoBlob.objProjectInfo.YINo = "";
            objProjectInfoBlob.objProjectInfo.DeliveryDate = Convert.ToDateTime(DeliveryDate.Text);
            objProjectInfoBlob.objProjectInfo.OrderDate = DateTime.Now.Date;
            objProjectInfoBlob.objProjectInfo.Remarks = txtNotes.Text;
            objProjectInfoBlob.objProjectInfo.ProjectType = "";
            objProjectInfoBlob.objProjectInfo.Vendor = "";
            #endregion CreatePojectObjectNew

            #region CreateProjectLegacy
            JCHVRF.Model.Project projectLegacy = new JCHVRF.Model.Project();
            projectLegacy.Name = txtProjectName.Text;
            //projectLegacy.ActiveFlag = 1;
            projectLegacy.UpdateDate = DateTime.Now.Date;
            projectLegacy.Version = MyConfig.Version;
            //projectLegacy.DBVersion = ConfigurationManager.AppSettings["DBVersion"].ToString();
            //projectLegacy.Measure = 0;
            projectLegacy.Location = txtProjectLocation.Text;
            projectLegacy.SoldTo = "";
            projectLegacy.ShipTo = "";
            projectLegacy.PurchaseOrderNO = "";
            projectLegacy.ContractNO = "";
            projectLegacy.RegionCode = Convert.ToString(CmbRegion.SelectedValue);
            projectLegacy.SalesOffice = "";
            projectLegacy.SalesEngineer = "";
            projectLegacy.SalesYINO = "";
            projectLegacy.DeliveryRequiredDate = Convert.ToDateTime(DeliveryDate.Text);
            projectLegacy.OrderDate = DateTime.Now.Date;
            projectLegacy.Remarks = txtNotes.Text;
            projectLegacy.salesCompany = "";
            projectLegacy.ProductType = "Comm. Tier 2, HP";
            #endregion

            #region SaveBolbDataInBlob
            objProjectInfoBlob.objProjectBlobInfo.ExistingClient = txtExistingClient.Text;
            //update legacy object
            projectLegacy.clientName = txtExistingClient.Text;

            if (rbbh.IsChecked == true)
            {
                objProjectInfoBlob.objProjectBlobInfo.Brand = "H";

                //Update Project Legacy 
                projectLegacy.BrandCode = "H";
            }
            // Start - Removing it for now as per bug #1546
            //else if (rbby.IsChecked == true)
            //{
            //    objProjectInfoBlob.objProjectBlobInfo.Brand = "Y";

            //    //Update Project Legacy 
            //    projectLegacy.BrandCode = "Y";
            //}
            // ENd - Removing it for now as per bug #1546
            if (rbjch.IsChecked == true)
            {
                objProjectInfoBlob.objProjectBlobInfo.SelectedSource = "JCH";
                //Update Project Legacy 
                projectLegacy.FactoryCode = "JCH";
            }
            else if (ygf.IsChecked == true)
            {
                objProjectInfoBlob.objProjectBlobInfo.SelectedSource = "YGF";
                //Update Project Legacy 
                projectLegacy.FactoryCode = "YGF";
            }


            Client obj = (Client)Application.Current.Properties["ClientInfo"];
            if (obj != null)
            {
                objProjectInfoBlob.objClient = obj;
            }


            objProjectInfoBlob.objDesignCondition.NumericAltitude = NumericAltitude.Value;
            objProjectInfoBlob.objDesignCondition.indoorCoolingDB = NumericCoolDryBulb.Value;
            objProjectInfoBlob.objDesignCondition.indoorCoolingWB = NumericCoolWetBulb.Value;
            objProjectInfoBlob.objDesignCondition.indoorCoolingRH = NumericInternalRH.Value;
            objProjectInfoBlob.objDesignCondition.indoorCoolingHDB = NumericHeatingDryBulb.Value;
            objProjectInfoBlob.objDesignCondition.outdoorHeatingDB = NumericOutdoorDB.Value;
            objProjectInfoBlob.objDesignCondition.NumericOutdoorIntelWater = NumericOutdoorIntelWater.Value;
            objProjectInfoBlob.objDesignCondition.outdoorCoolingDB = NumeroutdoorHDDBT.Value;
            objProjectInfoBlob.objDesignCondition.outdoorHeatingWB = NumeroutdoorHDWBT.Value;
            objProjectInfoBlob.objDesignCondition.outdoorHeatingRH = NumeroutdoorHDRH.Value;
            objProjectInfoBlob.objDesignCondition.outdoorHeatingIW = NumeroutdoorIntelWaterTemp.Value;
            //objProjectInfoBlob.objDesignCondition.outdoorCoolingDB = NumericOutdoorDB.Value;

            if (rbtBrand2Pipe.IsChecked == true)
            {
                objProjectInfoBlob.objFloor.Cooling = "2Pipe";

                //Update Legacy Project Object
                projectLegacy.IsHeatingModeEffective = false;
            }
            if (rbtBrand3Pipe.IsChecked == true)
            {
                objProjectInfoBlob.objFloor.CollingAndHeating = "3Pipe";
                //Update Legacy Project Object
                projectLegacy.IsHeatingModeEffective = true;
            }

            if (rbtSingleFloor.IsChecked == true)
            {
                objProjectInfoBlob.objFloor.SingleFloor = objflr.MultiFloorCount.ToString();


            }

            if (rbtMultiFloor.IsChecked == true)
            {
                objProjectInfoBlob.objFloor.MultipleFloor = "MultipleFloor";
                //updateLegacy

            }
            objProjectInfoBlob.objFloor.FloorDetails = listFloor.ToList();

            //Update Legacy Project Object
            List<JCHVRF.Model.Floor> floorLegacy = new List<JCHVRF.Model.Floor>();

            #region CreateFloorList
            int floorID = 0;
            foreach (var floor in listFloor)
            {
                floorLegacy.Add(new JCHVRF.Model.Floor
                {
                    Name = floor.floorName,
                    Height = floor.elevationFromGround,
                    Id = Convert.ToString(floorID)
                });
                floorID++;
            }
            #endregion CreateFloorList

            projectLegacy.FloorList = floorLegacy;
            #endregion SaveBolbDataInBlob

            //ToRemove
            //To Save New Project Object
            //objProjectInfoBll.InsertProjectInfoDetails(objProjectInfoBlob);

            //To Save ProjectNextGen
            projectLegacy.RegionCode = "EU_W";
            projectLegacy.SubRegionCode = "GBR";

            //To Add Room

            JCHVRF.Model.RoomIndoor roomIndoorLegacy = new JCHVRF.Model.RoomIndoor
            {
                RoomID = "1",
                RoomName = "DummyRoom",
                IndoorItem = new JCHVRF.Model.Indoor { }//Adding dummy indoor.
            };
            projectLegacy.RoomIndoorList.Add(roomIndoorLegacy);

            //check Floor Grid
            if (dtFloor.Items.Count == 0)
            {
                MessageBox.Show("Please add atleast one floor to create this project.");

            }
            else
            {
                //To Add Room
                bool result = objProjectInfoBll.CreateProject(projectLegacy);
                if (result)
                {
                    MessageBox.Show("Project details saved successfully!");
                }
                else
                {
                    Environment.Exit(0);
                }

                int ProjectID = objProjectInfoBll.GetMaxProjectId();
                projectLegacy.projectID = ProjectID;

                //To Do
                //MasterDesigner objDesigner = new MasterDesigner(projectLegacy);
                //objDesigner.Show();

                // Start bug 1554
                this.Close();

                Application.Current.MainWindow.Close();
                // System.Windows.Application..Windows.Equals
                // end bug 1554
                //objDesigner.WindowState = WindowState.Maximized;
            }
        }
        #endregion SaveProjectData

        //US- 5.1.1.1 update projectInfo on save button click
        #region updateProject 
        //US 5.1.1.1
        public void updateProjectData(JCHVRF.Model.Project project)
        {
            if (dtFloor.Items.Count == 0)
            {
                MessageBox.Show("Please add atleast one floor to create this project.");

            }
            else
            {
                bool status = objProjectInfoBll.UpdateProject(project);
                MessageBox.Show("Successfully saved!");
            }
        }
        #endregion

        //US- 5.1.1.1 Display Main window data for the specific project currently opened.
        #region DisplayWindowData
        public void DisplaySystemProperties(JCHVRF.Model.Project project)
        {

            isDesignTabFirstLoad = false;

            //Hiding previous, next and create buttons. Showing Cancel and Save buttons.
            stkProjectInfoButton.Visibility = Visibility.Collapsed;
            gridDesignConditionButton.Visibility = Visibility.Collapsed;
            txtButtonTypeBox.Visibility = Visibility.Collapsed;
            txtButtonFloorBox.Visibility = Visibility.Collapsed;
            stkSysPropertiesFloorButton.Visibility = Visibility.Visible;
            stkSysPropertiesTypeButton.Visibility = Visibility.Visible;
            stkSysPropertiesDesignConditionButton.Visibility = Visibility.Visible;
            stkSysPropertiesProjectInfoButton.Visibility = Visibility.Visible;

            // enable all tabs
            tabProjectInfo.IsEnabled = true;
            tabDesignConditions.IsEnabled = true;
            tabType.IsEnabled = true;
            tbFloor.IsEnabled = true;

            //General Tab
            txtProjectName.Text = project.Name.ToString();
            txtProjectLocation.Text = project.Location.ToString();

            txtExistingClient.Text = project.clientName == null ? "" : project.clientName;
            DeliveryDate.Text = project.DeliveryRequiredDate.ToString();
            txtNotes.Text = project.Remarks == null ? "" : project.Remarks;

            //Design Conditions
            SetDefaultDesignConditions(); //Harcoded values being picked up for now as the region is also hardcoded at the moment.
            //NumericAltitude.Value = Convert.ToInt16(project.Altitude);
            //NumericCoolDryBulb.Value = Convert.ToDecimal(designConditionsLegacy.indoorCoolingDB);
            //NumericCoolWetBulb.Value = Convert.ToDecimal(designConditionsLegacy.indoorCoolingWB);
            //NumericInternalRH.Value = Convert.ToDecimal(designConditionsLegacy.indoorCoolingRH);

            //NumericHeatingDryBulb.Value = Convert.ToDecimal(designConditionsLegacy.indoorHeatingDB);

            //NumericOutdoorDB.Value = Convert.ToDecimal(designConditionsLegacy.outdoorCoolingDB);
            //NumericOutdoorIntelWater.Value = Convert.ToDecimal(designConditionsLegacy.outdoorCoolingIW);
            //NumeroutdoorHDDBT.Value = Convert.ToDecimal(designConditionsLegacy.outdoorHeatingDB);
            //NumeroutdoorHDWBT.Value = Convert.ToDecimal(designConditionsLegacy.outdoorHeatingWB);
            //NumeroutdoorIntelWaterTemp.Value = Convert.ToDecimal(designConditionsLegacy.outdoorHeatingIW);
            //NumeroutdoorHDRH.Value = Convert.ToDecimal(designConditionsLegacy.outdoorHeatingRH);

            //Type
            if (project.IsHeatingModeEffective)
            {
                rbtBrand2Pipe.IsChecked = true;
                rbtBrand3Pipe.IsChecked = false;
            }
            else
            {
                rbtBrand2Pipe.IsChecked = false;
                rbtBrand3Pipe.IsChecked = true;
            }

            if (project.FloorList.Count > 0)
            {
                rbtSingleFloor.IsChecked = false;
                rbtMultiFloor.IsChecked = true;
                stkMultiFloor.Visibility = Visibility.Visible;
                txtMultiFloor.Text = project.FloorList.Count.ToString();
                int dtCount = dtFloor.Items.Count;

                for (int i = 0; i < project.FloorList.Count; i++)
                {
                    listFloor.Add(new Floor { floorName = "Floor" + project.FloorList[i].Id, elevationFromGround = project.FloorList[i].Height, IsFloorChecked = true });

                }
                isFirstLoad = false;
                dtFloor.ItemsSource = listFloor;
            }
            else
            {
                rbtSingleFloor.IsChecked = true;
                rbtMultiFloor.IsChecked = false;
            }

            if (project.BrandCode == "H")
            {
                rbbh.IsChecked = true;
                // Start - Removing it for now as per bug #1546
                // rbby.IsChecked = false;
                // End - Removing it for now as per bug #1546
            }
            else
            {
                rbbh.IsChecked = false;
                // Start - Removing it for now as per bug #1546
                //rbby.IsChecked = true;
                // End - Removing it for now as per bug #1546
            }

            if (project.FactoryCode == "JCH")
            {
                rbjch.IsChecked = true;
                ygf.IsChecked = false;
            }
            else
            {
                rbjch.IsChecked = false;
                ygf.IsChecked = true;
            }

            //Floors
            //dtFloor.ItemsSource = project.FloorList;

            //assigning project object to public instance projectLegacy
            projectLegacy = project;
        }
        #endregion DisplayWindowData

        private void btnSPCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            // disable all tabs
            tabProjectInfo.IsEnabled = false;
            tabDesignConditions.IsEnabled = false;
            tabType.IsEnabled = false;
            tbFloor.IsEnabled = false;

            //Enabling previous, next and create buttons. Hiding Cancel and Save buttons.
            stkProjectInfoButton.Visibility = Visibility.Visible;
            gridDesignConditionButton.Visibility = Visibility.Visible;
            txtButtonTypeBox.Visibility = Visibility.Visible;
            txtButtonFloorBox.Visibility = Visibility.Visible;
            stkSysPropertiesFloorButton.Visibility = Visibility.Collapsed;
            stkSysPropertiesTypeButton.Visibility = Visibility.Collapsed;
            stkSysPropertiesDesignConditionButton.Visibility = Visibility.Collapsed;
            stkSysPropertiesProjectInfoButton.Visibility = Visibility.Collapsed;
        }

        private void btnSPSave_Click(object sender, RoutedEventArgs e)
        {
            #region general project info
            projectLegacy.Name = txtProjectName.Text;
            projectLegacy.Location = txtProjectLocation.Text;
            projectLegacy.DeliveryRequiredDate = Convert.ToDateTime(DeliveryDate.Text);
            projectLegacy.Remarks = txtNotes.Text;
            projectLegacy.clientName = txtExistingClient.Text;
            projectLegacy.UpdateDate = DateTime.Now.Date;
            projectLegacy.Location = txtProjectLocation.Text;
            #endregion

            if (rbbh.IsChecked == true)
            {
                projectLegacy.BrandCode = "H";
            }
            //else if (rbby.IsChecked == true)
            //{
            //   projectLegacy.BrandCode = "Y";
            //}
            if (rbjch.IsChecked == true)
            {
                projectLegacy.FactoryCode = "JCH";
            }
            else if (ygf.IsChecked == true)
            {
                projectLegacy.FactoryCode = "YGF";
            }


            if (rbtBrand2Pipe.IsChecked == true)
            {
                projectLegacy.IsHeatingModeEffective = false;
            }
            if (rbtBrand3Pipe.IsChecked == true)
            {
                projectLegacy.IsHeatingModeEffective = true;
            }


            List<JCHVRF.Model.Floor> floorLegacy = new List<JCHVRF.Model.Floor>();

            #region CreateFloorList
            int floorID = 0;
            foreach (var floor in listFloor)
            {
                floorLegacy.Add(new JCHVRF.Model.Floor
                {
                    Name = floor.floorName,
                    Height = floor.elevationFromGround,
                    Id = Convert.ToString(floorID)
                });
                floorID++;
            }
            #endregion CreateFloorList

            projectLegacy.FloorList = floorLegacy;

            updateProjectData(projectLegacy);

            this.Close();

            // disable all tabs
            tabProjectInfo.IsEnabled = false;
            tabDesignConditions.IsEnabled = false;
            tabType.IsEnabled = false;
            tbFloor.IsEnabled = false;

            //Enabling previous, next and create buttons. Hiding Cancel and Save buttons.
            stkProjectInfoButton.Visibility = Visibility.Visible;
            gridDesignConditionButton.Visibility = Visibility.Visible;
            txtButtonTypeBox.Visibility = Visibility.Visible;
            txtButtonFloorBox.Visibility = Visibility.Visible;
            stkSysPropertiesFloorButton.Visibility = Visibility.Collapsed;
            stkSysPropertiesTypeButton.Visibility = Visibility.Collapsed;
            stkSysPropertiesDesignConditionButton.Visibility = Visibility.Collapsed;
            stkSysPropertiesProjectInfoButton.Visibility = Visibility.Collapsed;
        }

        private void Tab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (string.IsNullOrEmpty(txtProjectName.Text) || string.IsNullOrEmpty(txtExistingClient.Text)
                                                         || string.IsNullOrEmpty(txtProjectLocation.Text))
            {
                Tab.SelectedIndex = 0;
            }

            // This has been handled in btnTypeNext_Click
            //if (rbtMultiFloor.IsChecked == true)
            //{

            //    int iFloorValue = Int32.Parse(txtMultiFloor.Text == "" ? "1" : txtMultiFloor.Text);

            //    if (iFloorValue > 999 || iFloorValue == 0)
            //    {
            //        MessageBox.Show("Floor number range between 1 and 1000!");

            //        txtMultiFloor.Focus();
            //        Tab.SelectedIndex = 2;

            //    }
            //}

        }


        #region existingclient 
        /// <summary>
        ///currently binding Project Names. This needs to be updated to show client names.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtExistingClient_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {

            bool found = false;

            var data = objProjectInfoBll.GetAllProjectsList();

            string query = (sender as TextBox).Text;

            if (query.Length == 0)
            {
                // Clear   
                resultStack.Children.Clear();
                borderResulStack.Visibility = System.Windows.Visibility.Collapsed;
                txtOrBlock.Visibility = Visibility.Visible;
                btnclient.Visibility = Visibility.Visible;
                txtBlockSubRegion.Visibility = Visibility.Visible;
                CmbSubRegion.Visibility = Visibility.Visible;
            }
            else
            {
                borderResulStack.Visibility = System.Windows.Visibility.Visible;
                txtOrBlock.Visibility = Visibility.Hidden;
                btnclient.Visibility = Visibility.Hidden;
                txtBlockSubRegion.Visibility = Visibility.Hidden;
                CmbSubRegion.Visibility = Visibility.Hidden;
            }

            // Clear the list   
            resultStack.Children.Clear();

            //Add the result

            foreach (var obj in data)
            {
                try
                {
                    // start bug #1561
                    var blob = JCHVRF.DAL.New.Utility.Deserialize<JCHVRF.Model.Project>(obj.ProjectBlob);
                    // end bug #1561

                    if (blob.clientName != null)
                    {
                        int count = blob.clientName.IndexOf(query, StringComparison.OrdinalIgnoreCase);
                        if (count >= 0)
                        {
                            // The word starts with this... Autocomplete must work   
                            addItem(blob.clientName);
                            found = true;
                        }
                    }
                }
                catch { }

            }


            if (!found)
            {
                resultStack.Children.Add(new TextBlock() { Text = "No results found." });

                borderResulStack.Visibility = System.Windows.Visibility.Collapsed;
                txtOrBlock.Visibility = Visibility.Visible;
                btnclient.Visibility = Visibility.Visible;
                txtBlockSubRegion.Visibility = Visibility.Visible;
                CmbSubRegion.Visibility = Visibility.Visible;

                txtExistingClient.Text = string.Empty;
            }
        }

        private void addItem(string text)
        {
            TextBlock block = new TextBlock();

            // Add the text   
            block.Text = text;


            // Mouse events   
            block.MouseLeftButtonUp += (sender, e) =>
            {
                txtExistingClient.Text = (sender as TextBlock).Text;
                resultStack.Children.Clear();
                borderResulStack.Visibility = System.Windows.Visibility.Collapsed;
                txtOrBlock.Visibility = Visibility.Visible;
                btnclient.Visibility = Visibility.Visible;
                txtBlockSubRegion.Visibility = Visibility.Visible;
                CmbSubRegion.Visibility = Visibility.Visible;

            };

            block.MouseEnter += (sender, e) =>
            {
                TextBlock b = sender as TextBlock;
                b.Background = new SolidColorBrush(Colors.LightBlue);
            };

            block.MouseLeave += (sender, e) =>
            {
                TextBlock b = sender as TextBlock;
                b.Background = new SolidColorBrush(Colors.Transparent);
            };

            // Add to the panel   
            resultStack.Children.Add(block);

        }


        #endregion existingclient

        // start for issue fix 1541
        private void txtProjectInfo_GotFocus(object sender, RoutedEventArgs e)
        {
            var textboxFocus = sender as TextBox;
            textboxFocus.Text = Convert.ToString(textboxFocus.Text);
            textboxFocus.CaretIndex = textboxFocus.Text.Length;
        }


        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox TB = sender as TextBox;
            if (TB != null && TB.Text.Trim() == "")
            {
                TB.Text = "0";

                TB.Focus();
            }
            else
                btnCreate.IsEnabled = false;
        }


        // end for issue fix 1541
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            // disable all tabs
            tabProjectInfo.IsEnabled = false;
            tabDesignConditions.IsEnabled = false;
            tabType.IsEnabled = false;
            tbFloor.IsEnabled = false;

            //Enabling previous, next and create buttons. Hiding Cancel and Save buttons.
            stkProjectInfoButton.Visibility = Visibility.Visible;
            gridDesignConditionButton.Visibility = Visibility.Visible;
            txtButtonTypeBox.Visibility = Visibility.Visible;
            txtButtonFloorBox.Visibility = Visibility.Visible;
            stkSysPropertiesFloorButton.Visibility = Visibility.Collapsed;
            stkSysPropertiesTypeButton.Visibility = Visibility.Collapsed;
            stkSysPropertiesDesignConditionButton.Visibility = Visibility.Collapsed;
            stkSysPropertiesProjectInfoButton.Visibility = Visibility.Collapsed;
        }

        // private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //     //var TextBox_Floor = sender as TextBox;
        //     //TextBox_Floor.Text = TextBox_Floor.Text.Trim() == "" ? "0" : TextBox_Floor.Text;
        //     //e.Handled = new System.Text.RegularExpressions.Regex("[^0-9]+").IsMatch(TextBox_Floor.Text);
        //     Regex regex = new Regex("^[.][0-9]+$|^[0-9]*[.]{0,1}[0-9]*$");
        //     e.Handled = !regex.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.ToString()));

        //}

        private void TextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[.][0-9]+$|^[0-9]*[.]{0,1}[0-9]*$");
            e.Handled = !regex.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text.Trim()));
            if (e.Handled)
                btnCreate.IsEnabled = false;
            else
                btnCreate.IsEnabled = true;
        }
    }
}






