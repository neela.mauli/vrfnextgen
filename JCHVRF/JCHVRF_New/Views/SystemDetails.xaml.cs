﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JCHVRF_New.Views
{
    /// <summary>
    /// Interaction logic for SystemDetails.xaml
    /// </summary>
    public partial class SystemDetails : UserControl
    {
        public SystemDetails()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (stkoutdordetails.IsVisible == true)
            {
                stkoutdordetails.Visibility = Visibility.Collapsed;

            }
            else { stkoutdordetails.Visibility = Visibility.Visible; }
        }

        private void Button_Click_Indoor(object sender, RoutedEventArgs e)
        {
            if (stkindordetails.IsVisible == true)
            {
                stkindordetails.Visibility = Visibility.Collapsed;
            }
            else { stkindordetails.Visibility = Visibility.Visible; }
        }

        private void BtnEditSystem_Click(object sender, RoutedEventArgs e)
        {
           //new UCEditSystem().ShowDialog();
        }
    }
}
