﻿using JCHVRF.Model;
using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using System.Data;
using JCHVRF.DALFactory;
using System.Windows.Media;
using System.Windows.Controls;
using JCHVRF_New.ViewModels;
using System.ComponentModel;
using JCHVRF_New.Views;
using Lassalle.WPF.Flow;
using System.Windows.Media.Imaging;
using System.Linq;
using JCHVRF.Entity;
using JCHVRF.BLL;
using JCHVRF.MyPipingBLL;
using JCBase.Utility;
using JCHVRF;
using System.Collections.Generic;
using NextGenModel = JCHVRF.Model.NextGen;
using NextGenBLL = JCHVRF.MyPipingBLL.NextGen;
using JCHVRF.BLL.New;
using JCHVRF.Report;
using JCHVRF_New.Model;
using JCHVRF_New.Utility;

namespace JCHVRF_New.Views
{
    /// <summary>
    /// Interaction logic for MasterDesigner.xaml
    /// </summary>
    public partial class MasterDesigner : UserControl, INotifyPropertyChanged
    {
        ucDesignerCanvas designerCanvas = null;
        IndoorBLL indoorBLL;
        List<string> ERRList = null;
        List<string> MSGList = null;
        string utTemperature;
        double dbCool = SystemSetting.UserSetting.defaultSetting.IndoorCoolingDB;
        double wbCool = SystemSetting.UserSetting.defaultSetting.IndoorCoolingWB;
        double dbHeat = SystemSetting.UserSetting.defaultSetting.IndoorHeatingDB;
        double rhCool = SystemSetting.UserSetting.defaultSetting.IndoorCoolingRH;
        string namePrefix = string.Empty;

        //public static bool blnIsCallfromDelete = false;

        //#region NodeProperty
        //static readonly DependencyProperty NodeProperty =
        //    DependencyProperty.RegisterAttached("NodeProperty", typeof(object), typeof(object),
        //        new FrameworkPropertyMetadata(false));
        //static object GetNodeProperty(Node node)
        //{
        //    if (node == null)
        //        return false;
        //    return (object)node.GetValue(NodeProperty);
        //}
        //static void SetNodeProperty(Node node, object value)
        //{
        //    if (node != null)
        //        node.SetValue(NodeProperty, value);
        //}
        //#endregion
        //As of now we are using custom type within the scope. However Need to move it to NodeItem class to achieve seperation of concerns.
        //public class ImageData
        //{
        //    public string imagePath { get; set; }
        //    public string imageName { get; set; }
        //    public string equipmentType { get; set; }
        //    public int NodeNo { get; set; }   // Added to uniquely identify the node
        //    public string UniqName { get; set; }
        //}
        IDataAccessObject _dao;
        string _region;
        string _brandCode;
        DataTable dtFillImageType;
        DataTable dtFullPath;
        DataTable dtFullImagePath;
        NextGenModel.ImageData SelectedNode;
        public enum equipmentTypeButtonClick
        {
            All = 0,
            IDU = 1,
            ODU = 2,
            Pipe = 3,
            CC = 4

        }
        public equipmentTypeButtonClick enumEQSrch;
        public JCHVRF.Model.Project projectLegacy
        {
            get; set;
        }

        public IDUEquipmentPropertiesViewModel IDUEquipmentPropertiesViewModel
        {
            get
            {
                return iduEquipmentPropertiesViewModel;
            }
            set
            {
                iduEquipmentPropertiesViewModel = value;
                OnPropertyChanged("IDUEquipmentPropertiesViewModel");
            }
        }

        private IDUEquipmentPropertiesViewModel iduEquipmentPropertiesViewModel;

        private ODUEquipmentPropertiesViewModel oduEquipmentPropertiesViewModel;

        public ODUEquipmentPropertiesViewModel OduEquipmentPropertiesViewModel
        {
            get { return oduEquipmentPropertiesViewModel; }

            set
            {


                oduEquipmentPropertiesViewModel = value;
                OnPropertyChanged("ODUEquipmentPropertiesViewModel");
            }
        }


        /// <summary>
        /// Gets or Sets Values for System properties.
        /// </summary>
        public SystemPropertiesViewmodel SystemPropertiesViewModel
        {
            get
            {
                return systemPropertiesViewModel;
            }
            set
            {
                systemPropertiesViewModel = value;
                OnPropertyChanged("SystemPropertiesViewModel");
            }
        }
        private SystemPropertiesViewmodel systemPropertiesViewModel;

        public MasterDesigner()
        {
            InitializeComponent();
            this.Loaded += Main_Loaded;
        }

        /// <summary>
        /// Method to show equipment properties when selected on canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="imgData"></param>
        protected void SelectEquipmentOnCanvas(object sender, Object imgData)
        {
            if (imgData != null)
            {
                NextGenModel.SystemVRF Outitem = new NextGenModel.SystemVRF();
                this.SelectedNode = (NextGenModel.ImageData)imgData;
                //ToGet Data for Selected Equipment from Project
                ProjectBLL pBLL = new ProjectBLL(this.projectLegacy);
                //Selected First Indoor Unit
                var roomIndoor = pBLL.GetIndoor(this.SelectedNode.NodeNo);
                if (this.projectLegacy.CanvasODUList != null && this.projectLegacy.CanvasODUList.Count > 0)
                {
                    foreach (NextGenModel.SystemVRF item in this.projectLegacy.CanvasODUList)
                    {
                        if (item.NO == this.SelectedNode.NodeNo)
                            Outitem = item;
                    }
                }
                ShowEquipmentProperties(roomIndoor, this.SelectedNode.equipmentType, Outitem);
            }
            else
            {
                this.SelectedNode = null;
                brdMain.Child = null;
            }
        }

        /// <summary>
        /// Method to show equipment Properties when Added on Canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="imgData"></param>
        protected void AddEquipmentOnCanvas(object sender, NextGenModel.ImageData imgData)
        {
            //ToAdd Data for Selected Equipment from Project
            ProjectBLL pBLL = new ProjectBLL(this.projectLegacy);
            if (imgData.equipmentType == "Indoor")
            {
                var itemIndex = this.projectLegacy.RoomIndoorList.FindIndex(x => x.IndoorNO == 0);
                if (itemIndex >= 0)
                    this.projectLegacy.RoomIndoorList.RemoveAt(itemIndex);
                Indoor indoor = new Indoor
                {
                    //ImgName contains Type for IDU and Series for ODU
                    Type = imgData.imageName
                };
                //ToDo  
                // Adding Node Property 
                //Adding default Name prefix using the xml.
                namePrefix = SystemSetting.UserSetting.defaultSetting.IndoorName;

                var NodeName = namePrefix + Convert.ToString(imgData.NodeNo);//Using Name Prefixes
                if (!(this.projectLegacy.RoomIndoorList.Any(x => x.IndoorNO == imgData.NodeNo)))
                {
                    double wbCool = SystemSetting.UserSetting.defaultSetting.IndoorCoolingWB;
                    double dbHeat = SystemSetting.UserSetting.defaultSetting.IndoorHeatingDB;
                    //RoomID:1 :: As there is no rooms available as per FER
                    var roomIndoor = pBLL.AddIndoor("1", indoor);  // This line is commented because it add a indoor node to obj
                    roomIndoor.IndoorName = NodeName;
                    roomIndoor.DisplayImagePath = imgData.imagePath;
                    roomIndoor.IndoorNO = imgData.NodeNo;
                    roomIndoor.WBCooling = Unit.ConvertToSource(wbCool, UnitType.TEMPERATURE, utTemperature);
                    roomIndoor.WBHeating = Unit.ConvertToSource(dbHeat, UnitType.TEMPERATURE, utTemperature);
                    roomIndoor.DisplayImageName = imgData.imageName;
                    //roomIndoor.DisplayImagePath = imgData.imagePath;
                    //End Adding Node Property

                    //Add Indoor Unit to ProjectLegacy Object
                    // projectLegacy.RoomIndoorList.Add(roomIndoor);
                    this.SelectedNode = null;
                    ShowEquipmentProperties(roomIndoor, imgData.equipmentType, null);
                }
            }


            if (imgData.equipmentType == "Outdoor")
            {
                Outdoor outDooor = new Outdoor
                {
                    //ImgName contains Type for IDU and Series for ODU
                    Series = imgData.imageName,
                };

                if (this.projectLegacy.SystemListNextGen == null || this.projectLegacy.CanvasODUList == null)
                {
                    this.projectLegacy.SystemListNextGen = new List<NextGenModel.SystemVRF>();
                    this.projectLegacy.CanvasODUList = new List<NextGenModel.SystemVRF>();
                }
                //ToDo
                //RoomID:1 :: As there is no rooms available as per FER              

                //if (this.projectLegacy.SystemListNextGen.Count == 0)
                //{
                NextGenModel.SystemVRF ODUItem = new NextGenModel.SystemVRF();

                //Adding default Name prefix using the xml.
                namePrefix = SystemSetting.UserSetting.defaultSetting.OutdoorName;
                //ODUItem.Name = "Sys " + Convert.ToString(imgData.NodeNo); FER
                ODUItem.Name = namePrefix + Convert.ToString(imgData.NodeNo); //NamePrefix is used instead of hardcoded string "Sys" in SER
                ODUItem.NO = imgData.NodeNo;
                ODUItem.IsAuto = true;
                ODUItem.SysType = SystemType.OnlyIndoor;
                ODUItem.OutdoorItem = outDooor;
                ODUItem.Series = imgData.imageName;
                ODUItem.DisplayImageName = imgData.imagePath;
                //SetNodeProperty(nodePiping, imgData);

                ODUItem.DBCooling = SystemSetting.UserSetting.defaultSetting.OutdoorCoolingDB;
                ODUItem.DBHeating = SystemSetting.UserSetting.defaultSetting.OutdoorHeatingDB;
                ODUItem.WBHeating = SystemSetting.UserSetting.defaultSetting.OutdoorHeatingWB;
                ODUItem.RHHeating = SystemSetting.UserSetting.defaultSetting.OutdoorHeatingRH;
                ODUItem.PipeEquivalentLength = SystemSetting.UserSetting.pipingSetting.pipingEqLength;
                ODUItem.PipeEquivalentLengthbuff = SystemSetting.UserSetting.pipingSetting.pipingEqLength;
                ODUItem.FirstPipeLength = SystemSetting.UserSetting.pipingSetting.firstBranchLength;
                ODUItem.FirstPipeLengthbuff = SystemSetting.UserSetting.pipingSetting.firstBranchLength;
                ODUItem.HeightDiff = SystemSetting.UserSetting.pipingSetting.pipingHighDifference;
                ODUItem.PipingLengthFactor = SystemSetting.UserSetting.pipingSetting.pipingCorrectionFactor;
                ODUItem.PipingPositionType = SystemSetting.UserSetting.pipingSetting.pipingPositionType;
                ODUItem.IWCooling = SystemSetting.UserSetting.defaultSetting.OutdoorCoolingIW;
                ODUItem.IWHeating = SystemSetting.UserSetting.defaultSetting.OutdoorHeatingIW;
                projectLegacy.CanvasODUList.Add(ODUItem);
                this.SelectedNode = null;
                //  }


                //var roomIndoor = pBLL.AddOutdoorToControlGroup(system, outDooor);
                //Add Indoor Unit to ProjectLegacy Object
                //projectLegacy.RoomIndoorList.Add(roomIndoor);
                ShowEquipmentProperties(null, imgData.equipmentType, ODUItem);
            }
        }



        /// <summary>
        /// Method to update equipment Properties when selected on Canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="imgData"></param>
        protected void UpdateSelectedEquipmentValues(object sender, Object ControlName)
        {
            if (this.SelectedNode != null)
            {
                if (ControlName != null)
                {
                    if (this.projectLegacy.RoomIndoorList != null && this.projectLegacy.RoomIndoorList.Count > 0)
                    {
                        ProjectBLL pBLL = new ProjectBLL(this.projectLegacy);
                        var itemIndex = this.projectLegacy.RoomIndoorList.FindIndex(x => x.IndoorNO == this.SelectedNode.NodeNo);
                        if (itemIndex >= 0)
                        {
                            var item = this.projectLegacy.RoomIndoorList.ElementAt(itemIndex);
                            this.projectLegacy.RoomIndoorList.RemoveAt(itemIndex);

                            if ((string)ControlName == "Type")
                            {
                                item.IndoorItem.Type = IDUEquipmentPropertiesViewModel.SelectedType;
                                NextGenModel.ImageData imgData = new NextGenModel.ImageData();
                                imgData.NodeNo = this.SelectedNode.NodeNo;
                                imgData.equipmentType = "Indoor";
                                imgData.imageName = IDUEquipmentPropertiesViewModel.SelectedType;
                                imgData.imagePath = GetImagePath(IDUEquipmentPropertiesViewModel.SelectedType);
                                imgData.UniqName = this.SelectedNode.UniqName;
                                if (IDUEquipmentPropertiesViewModel.SelectedType != null)
                                {
                                    if (!string.IsNullOrEmpty(imgData.imagePath))
                                    {
                                        designerCanvas.UpdateEquipmentOnCanvas(imgData);
                                        item.DisplayImageName = imgData.imageName;
                                        item.DisplayImagePath = imgData.imagePath;
                                        item.IndoorNO = imgData.NodeNo;
                                    }
                                }

                            }
                            else if ((string)ControlName == "FanSpeed")
                                item.FanSpeedLevel = IDUEquipmentPropertiesViewModel.SelectedFanSpeed;
                            else if ((string)ControlName == "Floor")
                                item.RoomName = IDUEquipmentPropertiesViewModel.SelectedFloor;
                            else if ((string)ControlName == "Model")
                            {
                                //item.IndoorItem.Model = IDUEquipmentPropertiesViewModel.SelectedModel;
                                item.DBCooling = IDUEquipmentPropertiesViewModel.LblDbCool;
                                item.DBHeating = IDUEquipmentPropertiesViewModel.LblDBHeat;
                                item.CoolingCapacity = IDUEquipmentPropertiesViewModel.LblCoolCapacity;
                                item.HeatingCapacity = IDUEquipmentPropertiesViewModel.LblHeatCapacity;
                                item.SensibleHeat = IDUEquipmentPropertiesViewModel.LblSensibleHeating;
                                item.RqAirflow = IDUEquipmentPropertiesViewModel.LblAirFlow;
                                if (IDUEquipmentPropertiesViewModel.SelectedModel != null)
                                {
                                    var ModelFullName = IDUEquipmentPropertiesViewModel.IDUEquipmentList.FirstOrDefault(x => x.Model == IDUEquipmentPropertiesViewModel.SelectedModel);
                                    if (ModelFullName != null)
                                    {
                                        var ListAccessory = item.IndoorItem.ListAccessory;
                                        Indoor inItem = indoorBLL.GetItem(ModelFullName.ModelFull, null, "Universal IDU", null);
                                        inItem.ListAccessory = ListAccessory;
                                        item.IndoorItem = inItem;
                                        item.RoomID = "0";
                                        item.IsExchanger = false;
                                        item.ListAccessory = ListAccessory;
                                        item.IsAuto = false;
                                        item.IsMainIndoor = false;
                                    }
                                }
                                //  var sitem = pBLL.AddIndoor("", inItem);
                            }
                            else if ((string)ControlName == "Equipment")
                                item.IndoorName = IDUEquipmentPropertiesViewModel.EquipmentName;

                            else
                            {

                                item.DBCooling = IDUEquipmentPropertiesViewModel.LblDbCool;
                                item.DBHeating = IDUEquipmentPropertiesViewModel.LblDBHeat;
                                item.CoolingCapacity = IDUEquipmentPropertiesViewModel.LblCoolCapacity;
                                item.HeatingCapacity = IDUEquipmentPropertiesViewModel.LblHeatCapacity;
                                item.SensibleHeat = IDUEquipmentPropertiesViewModel.LblSensibleHeating;
                                item.RqAirflow = IDUEquipmentPropertiesViewModel.LblAirFlow;
                            }
                            this.projectLegacy.RoomIndoorList.Insert(itemIndex, item);
                        }
                    }
                }
            }
        }

        private string GetImagePath(string equipName)
        {
            string imgPath = string.Empty;
            foreach (DataRow dataRow in dtFullImagePath.Rows)
            {
                string imgName = dataRow.ItemArray[1].ToString();
                if (imgName == equipName)
                {
                    imgPath = dataRow.ItemArray[0].ToString();
                    break;

                }
            }
            return imgPath;
        }
        protected void UpdateSelectedOutdoorValues(object sender, Object ControlName)
        {
            if (this.SelectedNode != null)
            {
                if (ControlName != null)
                {
                    if (this.projectLegacy.CanvasODUList != null && this.projectLegacy.CanvasODUList.Count > 0)
                    {
                        var itemIndex = this.projectLegacy.CanvasODUList.FindIndex(x => x.NO == this.SelectedNode.NodeNo);
                        if (itemIndex >= 0)
                        {
                            var item = this.projectLegacy.CanvasODUList.ElementAt(itemIndex);
                            this.projectLegacy.CanvasODUList.RemoveAt(itemIndex);
                            if ((string)ControlName == "Type")
                            {
                                item.SelOutdoorType = OduEquipmentPropertiesViewModel.SelectedType;
                                if (item.OutdoorItem != null)
                                    item.OutdoorItem.Type = OduEquipmentPropertiesViewModel.SelectedType;
                            }
                            else if ((string)ControlName == "Power")
                            {
                                item.Power = OduEquipmentPropertiesViewModel.SelectedPower;
                            }
                            else if ((string)ControlName == "Outdoor")
                            {
                                if (item.OutdoorItem != null)
                                {
                                    item.OutdoorItem.ModelFull = OduEquipmentPropertiesViewModel.SelectedOutdoor;
                                    //item.OutdoorItem.FullModuleName = OduEquipmentPropertiesViewModel.SelectedOutdoor;
                                }
                                else
                                {
                                    Outdoor OTD = new Outdoor();
                                    OTD.Series = item.Series;
                                    OTD.ModelFull = OduEquipmentPropertiesViewModel.SelectedOutdoor;
                                    //OTD.FullModuleName = OduEquipmentPropertiesViewModel.SelectedOutdoor;
                                    item.OutdoorItem = OTD;
                                }
                            }
                            else if ((string)ControlName == "Equipment")
                            {
                                item.Name = OduEquipmentPropertiesViewModel.OduName;
                            }
                            else if ((string)ControlName == "AutoManual")
                            {
                                item.IsAuto = OduEquipmentPropertiesViewModel.IsAuto;
                                if (OduEquipmentPropertiesViewModel.IsAuto == true)
                                {
                                    //item.DBCooling = SystemSetting.UserSetting.defaultSetting.outdoorCoolingDB;
                                    //item.DBHeating = SystemSetting.UserSetting.defaultSetting.outdoorHeatingDB;
                                    //item.WBHeating = SystemSetting.UserSetting.defaultSetting.outdoorHeatingWB;
                                    //item.RHHeating = SystemSetting.UserSetting.defaultSetting.outdoorHeatingRH;
                                    //item.PipeEquivalentLength = SystemSetting.UserSetting.pipingSetting.pipingEqLength;
                                    //item.PipeEquivalentLengthbuff = SystemSetting.UserSetting.pipingSetting.pipingEqLength;
                                    //item.FirstPipeLength = SystemSetting.UserSetting.pipingSetting.firstBranchLength;
                                    //item.FirstPipeLengthbuff = SystemSetting.UserSetting.pipingSetting.firstBranchLength;
                                    //item.HeightDiff = SystemSetting.UserSetting.pipingSetting.pipingHighDifference;
                                    //item.PipingLengthFactor = SystemSetting.UserSetting.pipingSetting.pipingCorrectionFactor;
                                    //item.PipingPositionType = SystemSetting.UserSetting.pipingSetting.pipingPositionType;
                                    //item.IWCooling = SystemSetting.UserSetting.defaultSetting.outdoorCoolingIW;
                                    //item.IWHeating = SystemSetting.UserSetting.defaultSetting.outdoorHeatingIW;
                                }
                            }
                            else if ((string)ControlName == "IndoreFreshAir")
                            {
                                if (OduEquipmentPropertiesViewModel.IsBothIndoreFreshAir == true)
                                {
                                    item.SysType = SystemType.CompositeMode;
                                }
                                else item.SysType = SystemType.OnlyIndoor;
                            }
                            else if ((string)ControlName == "MaxRatio")
                            {
                                if (OduEquipmentPropertiesViewModel.SelectedMaxRatio > 0)
                                {
                                    //if (item.SysType == SystemType.OnlyFreshAir)
                                    //    return;
                                    //if (OduEquipmentPropertiesViewModel.SelectedMaxRatio == Convert.ToInt32(item.MaxRatio * 100))
                                    //    return;
                                    //item.MaxRatio = Convert.ToDouble(OduEquipmentPropertiesViewModel.SelectedMaxRatio) / 100;

                                    item.MaxRatio = Math.Round((Convert.ToDouble(OduEquipmentPropertiesViewModel.SelectedMaxRatio) / 100), 2);
                                }
                            }

                            this.projectLegacy.CanvasODUList.Insert(itemIndex, item);
                        }
                    }
                }
            }
        }
        protected void ShowEquipmentProperties(RoomIndoor roomIndoor, string equipmentType, NextGenModel.SystemVRF ODUItem)
        {
            switch (equipmentType)
            {
                case "Indoor":
                    IDUEquipmentPropertiesViewModel = new IDUEquipmentPropertiesViewModel(JCHVRF.Model.Project.CurrentProject, roomIndoor);
                    IDUEquipmentProperties IDUEObj = new IDUEquipmentProperties(IDUEquipmentPropertiesViewModel);
                    IDUEObj.UpdateSelectedEquipmentProperty += new EventHandler<Object>(UpdateSelectedEquipmentValues);
                    brdMain.Child = IDUEObj;
                    break;

                case "Outdoor":
                    OduEquipmentPropertiesViewModel = new ODUEquipmentPropertiesViewModel(JCHVRF.Model.Project.CurrentProject, ODUItem);
                    ODUEquipmentProperties ODUEObj = new ODUEquipmentProperties(oduEquipmentPropertiesViewModel);
                    ODUEObj.UpdateSelectedOutdoorProperty += new EventHandler<Object>(UpdateSelectedOutdoorValues);
                    brdMain.Child = ODUEObj;
                    break;
                default:
                    IDUEquipmentPropertiesViewModel = new IDUEquipmentPropertiesViewModel(JCHVRF.Model.Project.CurrentProject, roomIndoor);
                    brdMain.Child = new IDUEquipmentProperties(IDUEquipmentPropertiesViewModel);
                    break;
            }
        }

        protected void ShowSystemProperties()
        {
            NextGenModel.SystemVRF currentSystem = projectLegacy.SystemListNextGen.FirstOrDefault();
            NextGenModel.MyNodeOut pipingNodeOut = currentSystem.MyPipingNodeOut;
            if (pipingNodeOut == null || currentSystem.OutdoorItem == null) return;
            if (pipingNodeOut.ChildNode == null) return;
            this.SystemPropertiesViewModel = new SystemPropertiesViewmodel(this.projectLegacy);
            //this.brdSystemProperties.Child = new SystemProperties(SystemPropertiesViewModel);
        }

        private void Main_Loaded(object sender, RoutedEventArgs e)
        {
            //To DO, below line will ultimately be removed, when changing to MVVM
            projectLegacy = (DataContext as MasterDesignerViewModel)?.Project;

            utTemperature = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            //Clear Error log control
            ErrorLog.RemoveErrorFromList(Model.ErrorType.Error, Model.ErrorCategory.PipingErrors);
            ErrorLog.RemoveErrorFromList(Model.ErrorType.Error, Model.ErrorCategory.WiringError);
            ErrorLog.RemoveErrorFromList(Model.ErrorType.Warning, Model.ErrorCategory.PipingErrors);
            ErrorLog.RemoveErrorFromList(Model.ErrorType.Warning, Model.ErrorCategory.WiringError);

            if (this.projectLegacy.CanvasODUList == null || this.projectLegacy.CanvasODUList.Count == 0)
            {
                this.projectLegacy.CanvasODUList = new List<NextGenModel.SystemVRF>();
                if (this.projectLegacy.SystemListNextGen != null && this.projectLegacy.SystemListNextGen.Count > 0)
                {
                    this.projectLegacy.CanvasODUList.Add(this.projectLegacy.SystemListNextGen.FirstOrDefault());
                }
            }
            //IDUEquipmentPropertiesViewModel = new IDUEquipmentPropertiesViewModel(projectLegacy);
            //brdMain.Child = new IDUEquipmentProperties(IDUEquipmentPropertiesViewModel);
            //ToDo: This needs to get called on Selection of Equipment
            //ImageData data = new ImageData {
            //     equipmentType="Indoor"
            //};

            // SelectEquipmentOnCanvas("",data);


            CheckReportVisibility();
            designerCanvas = new ucDesignerCanvas(this.projectLegacy);
            ReDrawPipingNodeAndWiringNode();
            designerCanvas.SelectEquipment += new EventHandler<Object>(SelectEquipmentOnCanvas);
            designerCanvas.AddEquipment += new EventHandler<NextGenModel.ImageData>(AddEquipmentOnCanvas);
            Frame.Content = designerCanvas.Content;
            JCHVRF.Model.Project.CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            this.indoorBLL = new IndoorBLL(JCHVRF.Model.Project.CurrentProject.SubRegionCode, JCHVRF.Model.Project.CurrentProject.RegionCode, JCHVRF.Model.Project.CurrentProject.BrandCode);
            //GetDisplayName();
            GetIduDisplayName();
            PopulateImage(dtFillImageType);
            fillImages(dtFillImageType);
        }

        public void GetIduDisplayName()
        {
            DataTable dtIndoor = indoorBLL.GetIduDisplayName();
            dtFillImageType = dtIndoor.DefaultView.ToTable(true, new string[] { "TypeImage", "UnitType", "SelectionType" });
        }
        private void DragImage(object sender, MouseButtonEventArgs e)
        {
            object data;
            Image image = e.Source as Image;
            TextBlock tb = e.Source as TextBlock;
            if (image != null)
            {
                data = (NextGenModel.ImageData)image.DataContext;
                DragDrop.DoDragDrop(image, data, DragDropEffects.Copy);

            }
            else
            {
                data = (NextGenModel.ImageData)tb.DataContext;
                DragDrop.DoDragDrop(tb, data, DragDropEffects.Copy);
            }
        }
        //Please use this method "addflow_Drop" to remove dependency of UC
        /*
        private void addflow_Drop(object sender, DragEventArgs e)
        {
            var data = (ImageData)e.Data.GetData(typeof(ImageData));
            BitmapImage bitmap = new BitmapImage(new Uri(data.imagePath));            
            ImageSource img = bitmap;
            Image imageControl = new Image() { Width = img.Width, Height = img.Height, Source = img };
            double left = e.GetPosition(this.addflow).X;
            double top = e.GetPosition(this.addflow).Y;
            Node imageNode = new Node(left, top, img.Width, img.Height, "", addflow);
            imageNode.Stroke = Brushes.Transparent;
            imageNode.Fill = Brushes.Transparent;
            imageNode.Image = img;
            imageNode.Tooltip = data.equipmentType + " : " + data.imageName;
            imageNode.IsSelectable = true;            
            SetNodeProperty(imageNode, data);
            this.addflow.AddNode(imageNode);
            this.addflow.MouseLeftButtonDown += Addflow_MouseLeftButtonDown;
        }
        
        private void Addflow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var selectedNode = this.addflow.SelectedItems.OfType<Node>().ToArray();
            if(selectedNode.Length>0)
            {
                var equipmentProperties = GetNodeProperty(selectedNode[0]);                
            }
        }
        */

        private void fillImages(DataTable data)
        {
            if (data == null)
            {
                imageThumbnails.ItemsSource = null;
                return;
            }
            if (data.Rows.Count > 0)
            {
                string imageFullPath;
                string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
                string navigateToFolder = "..\\..\\Image\\TypeImages";
                string sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);

                dtFullPath = new DataTable();
                dtFullPath.Columns.Add("imagePath");
                dtFullPath.Columns.Add("imageName");
                dtFullPath.Columns.Add("equipmentType");

                foreach (DataRow dr in data.Rows)
                {
                    imageFullPath = sourceDir + "\\" + dr.ItemArray[0];
                    if (File.Exists(imageFullPath))
                    {
                        DataRow row = dtFullPath.NewRow();
                        row[0] = imageFullPath;
                        row[1] = dr.ItemArray[1];
                        row[2] = dr.ItemArray[2];
                        dtFullPath.Rows.Add(row);
                    }
                }
                var EnumerableData = dtFullPath.AsEnumerable().Select(r => new NextGenModel.ImageData
                {
                    imagePath = r.Field<string>("imagePath"),
                    imageName = r.Field<string>("imageName"),
                    equipmentType = r.Field<string>("equipmentType")
                });

                imageThumbnails.ItemsSource = EnumerableData;
            }
            else
                imageThumbnails.ItemsSource = null;
        }

        /// <summary>
        ///  Populate all the Images 
        /// </summary>
        /// <param name="dtEquipment"></param>
        private void PopulateImage(DataTable dtEquipment)
        {
            if (dtEquipment != null && dtEquipment.Rows.Count > 0)
            {
                string imageFullPath;
                string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
                string navigateToFolder = "..\\..\\Image\\TypeImages";
                string sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
                dtFullImagePath = new DataTable();
                dtFullImagePath.Columns.Add("imagePath");
                dtFullImagePath.Columns.Add("imageName");
                dtFullImagePath.Columns.Add("equipmentType");

                foreach (DataRow dr in dtEquipment.Rows)
                {
                    imageFullPath = sourceDir + "\\" + dr.ItemArray[0];
                    if (File.Exists(imageFullPath))
                    {
                        DataRow row = dtFullImagePath.NewRow();
                        row[0] = imageFullPath;
                        row[1] = dr.ItemArray[1];
                        row[2] = dr.ItemArray[2];
                        dtFullImagePath.Rows.Add(row);
                    }
                }
            }

        }
        public void GetDisplayName()
        {
            _dao = (new GetDatabase()).GetDataAccessObject();

            _region = "GBR";
            _brandCode = "H";
            dtFillImageType = new DataTable();
            string query;
            //**** Indoor ****//
            // query = "Select Distinct UnitType, TypeImage, SelectionType from IDU_Std_UNIVERSAL where RegionCode like '%/" + _region + "/%' and SelectionType = 'Indoor' UNION " +

            query = "Select t2.Display_Name as UnitType, t1.TypeImage, 'Indoor' as SelectionType from IDU_Std_UNIVERSAL t1 left join JCHVRF_Universal_Display t2 on t1.UnitType = t2.Universal_Name"
                    + " Where t1.RegionCode like '%/" + _region + "/%' and t2.RegionCode like '%/" + _region + "/%' and t1.DeleteFlag = 1 Order by t2.Display_Name UNION" +


                //**** Outdoor ****// Need to make it generic. Will cover it after FER. SOC and moving to appropriate layer to be done.
                " Select distinct t1.ODU_Series as UnitType, t2.TypeImage, 'Outdoor' as SelectionType from JCHVRF_Relation_Universal t1 right join ODU_Std_UNIVERSAL t2 on t2.Series = t1.ODU_series"
                + " Where t1.RegionCode like '%/" + _region + "/%' and t2.RegionCode like '%/" + _region + "/%' and t1.brandCode = '" + _brandCode + "'";
            //" Select Distinct Series as UnitType, TypeImage, 'Outdoor' as SelectionType from ODU_Std_UNIVERSAL where RegionCode like '%/" + _region + "/%'";


            DataTable dtSql = _dao.GetDataTable(query);
            // dtSql=getUniqueEqupmentTypeImage(dtSql);

            dtFillImageType = dtSql.DefaultView.ToTable(true, new string[] { "TypeImage", "UnitType", "SelectionType" });


            ////DataTable dt = JCBase.Utility.Util.InsertDsCachForLoad("JCHVRF_Universal_Display");
            //DataView dv = dtSql.DefaultView;

            ////DO NOT DELETE COMMENTS. IT IS REQUIRED WHEN REGION WILL BE IMPLEMENTED

            //string strFilter = "RegionCode like '*/" + _region + "/*' and DeleteFlag = 1";
            //dv.RowFilter = strFilter;
            ////dv.Sort = "Display_Name";
            //return dv.ToTable(true, new string[] { "Display_Name" });
        }

        //private DataTable getUniqueEqupmentTypeImage(DataTable table)
        //{
        //    DataTable EqupmentTypeDT = new DataTable();
        //    EqupmentTypeDT.Columns.Add("UnitType");
        //    EqupmentTypeDT.Columns.Add("TypeImage");
        //    EqupmentTypeDT.Columns.Add("SelectionType");
        //    var FilteredRecord=table.DefaultView.ToTable(true, "UnitType","SelectionType");
        //    if(FilteredRecord != null && FilteredRecord.Rows.Count>0)
        //    {                
        //        foreach(DataRow row in FilteredRecord.Rows)
        //        {
        //            if(row["UnitType"]!=null)
        //            {
        //                var ImageRow=  table.Select("UnitType='" + Convert.ToString(row["UnitType"])+"'");
        //                if (ImageRow != null)
        //                {
        //                    DataRow dtrow = EqupmentTypeDT.NewRow();
        //                    dtrow[0] = row["UnitType"];
        //                    dtrow[1] = ImageRow[0]["TypeImage"];
        //                    dtrow[2] = row["SelectionType"];
        //                    EqupmentTypeDT.Rows.Add(dtrow);
        //                }
        //            }
        //        }
        //    }           
        //    return EqupmentTypeDT;
        //}
        private void lnkBtnSearch_OnClick(object sender, RoutedEventArgs e)
        {

            string b = txtSearch.Text;

            if (b.Length > 2)
            {
                DataRow[] filteredRows;
                switch (enumEQSrch)
                {

                    case equipmentTypeButtonClick.All:
                        filteredRows = dtFillImageType.Rows.Cast<DataRow>().Where(r => r.ItemArray.Any(
                              c => c.ToString().IndexOf(b, StringComparison.OrdinalIgnoreCase) >= 0)).ToArray();

                        if (filteredRows.Length > 0)
                        {
                            fillImages(filteredRows.CopyToDataTable());
                        }
                        else fillImages(null);
                        return;
                    case equipmentTypeButtonClick.IDU:

                        filteredRows = dtFillImageType.Rows.Cast<DataRow>().Where(z => z.Field<string>("SelectionType") == "Indoor").Where(r => r.ItemArray.Any(
                          c => c.ToString().IndexOf(b, StringComparison.OrdinalIgnoreCase) >= 0)).ToArray();

                        if (filteredRows.Length > 0)
                        {
                            fillImages(filteredRows.CopyToDataTable());
                        }
                        else fillImages(null);
                        return;

                    case equipmentTypeButtonClick.ODU:

                        filteredRows = dtFillImageType.Rows.Cast<DataRow>().Where(z => z.Field<string>("SelectionType") == "Outdoor").Where(r => r.ItemArray.Any(
                              c => c.ToString().IndexOf(b, StringComparison.OrdinalIgnoreCase) >= 0)).ToArray();

                        if (filteredRows.Length > 0)
                        {
                            fillImages(filteredRows.CopyToDataTable());
                        }
                        else fillImages(null);
                        return;
                    case equipmentTypeButtonClick.Pipe:


                        filteredRows = dtFillImageType.Rows.Cast<DataRow>().Where(r => r.ItemArray.Any(
                       c => c.ToString().IndexOf(b, StringComparison.OrdinalIgnoreCase) >= 0)).ToArray();

                        if (filteredRows.Length > 0)
                        {
                            fillImages(filteredRows.CopyToDataTable());
                        }
                        else fillImages(null);
                        return;

                }
            }
            else return;
        }


        private string filePath = string.Empty;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Open Dialog and Select the .VRF File 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnkBtnOpenIcon_OnClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            if (fileDialog.ShowDialog() == true)
            {
                filePath = fileDialog.FileName;
                string fileExtention = System.IO.Path.GetExtension(filePath);
                if (fileExtention.ToUpper() == ".VRF")
                {
                }
                else
                {
                    MessageBox.Show("File is not Valid", "", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void txtSearch_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtSearch.Text == "Search")
                txtSearch.Text = "";
        }

        private void txtSearch_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtSearch.Text.Trim() == "")
                txtSearch.Text = "Search";
        }
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            enumEQSrch = equipmentTypeButtonClick.All;
            var result = dtFillImageType.AsEnumerable();

            if (result.AsDataView<DataRow>().Count > 0)
            {
                var res = result.CopyToDataTable<DataRow>();
                fillImages(res);
            }
            else
                fillImages(null);
        }

        private void btnIDU_Click(object sender, RoutedEventArgs e)
        {
            enumEQSrch = equipmentTypeButtonClick.IDU;
            var result = dtFillImageType.AsEnumerable().Where(myRow => myRow.Field<string>("SelectionType") == "Indoor");

            if (result.AsDataView<DataRow>().Count > 0)
            {
                var res = result.CopyToDataTable<DataRow>();
                fillImages(res);
            }
            else
                fillImages(null);
        }

        private void btnODU_Click(object sender, RoutedEventArgs e)
        {
            enumEQSrch = equipmentTypeButtonClick.ODU;
            var result = dtFillImageType.AsEnumerable().Where(myRow => myRow.Field<string>("SelectionType") == "Outdoor");

            if (result.AsDataView<DataRow>().Count > 0)
            {
                var res = result.CopyToDataTable<DataRow>();
                fillImages(res);
            }
            else
                fillImages(null);

            // ErrorLog.LogError(ErrorLog.errorType.error, ErrorLog.errorMsg.PipeLength);
        }

        //Added by Shivraj - 7/3/2019
        //For showing Central Controller Equipment List
        private void btnCC_Click(object sender, RoutedEventArgs e)
        {
            enumEQSrch = equipmentTypeButtonClick.CC;
            // ToDo : Add Central Controller list
            var result = dtFillImageType.AsEnumerable().Where(myRow => myRow.Field<string>("SelectionType") == "Indoor");

            if (result.AsDataView<DataRow>().Count > 0)
            {
                var res = result.CopyToDataTable<DataRow>();
                fillImages(res);
            }
            else
                fillImages(null);

            // ErrorLog.LogError(ErrorLog.errorType.error, ErrorLog.errorMsg.PipeLength);
        }

       

        //private void btnPipe_Click(object sender, RoutedEventArgs e)
        //{
        //    enumEQSrch = equipmentTypeButtonClick.Pipe;
        //    var result = dtFillImageType.AsEnumerable().Where(myRow => myRow.Field<string>("SelectionType") == "Pipe");

        //    if (result.AsDataView<DataRow>().Count > 0)
        //    {
        //        var res = result.CopyToDataTable<DataRow>();
        //        fillImages(res);
        //    }
        //    else
        //        fillImages(null);
        //}

        private void lnkBtnAutoPipingIcon_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string errorMessage = string.Empty;
                bool IsValidDraw = designerCanvas.DrawAutoPipingValidation(out errorMessage);
                if (IsValidDraw)
                {
                    var IsValid = ReselectOutdoor();
                    if (IsValid == true)
                    {
                        ErrorLog.RemoveErrorFromList(Model.ErrorType.Error, Model.ErrorCategory.PipingErrors);
                        designerCanvas.DoDrawingPiping(true);
                        this.projectLegacy.SystemListNextGen[0].IsPipingOK = false;
                        ShowSystemProperties();
                    }
                    else
                        return;
                }
                else
                {
                    MessageBox.Show(string.Format(errorMessage));
                    ErrorLog.LogError(Model.ErrorType.Error, Model.ErrorCategory.PipingErrors, errorMessage);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
                return;
            }
        }

        #region GenerateReport
        private void btnGenerateReport_Click(object sender, RoutedEventArgs e)
        {

            string outputDir = "";
            string filePath_pic = FileLocal.GetNamePathPipingPicture("1", outputDir = null);
            designerCanvas.ExportVictorGraph(filePath_pic, true);
            string filePath_picwiring = FileLocal.GetNamePathWiringPicture("1", outputDir);
            designerCanvas.ExportVictorGraph_wiring(filePath_picwiring, true);
            if ((projectLegacy.SystemListNextGen.Count > 0) && (projectLegacy.projectID != 0))
            {
                if (projectLegacy.SystemListNextGen[0].IsPipingOK == true)
                {
                    var projectID = Convert.ToInt32(Application.Current.Properties["ProjectId"]);
                    if (projectID != 0)
                    {
                        ProjectInfoBLL bll = new ProjectInfoBLL();
                        JCHVRF.Entity.ProjectInfo projectNextGen = bll.GetProjectInfo(projectID);
                        if (ProjectBLL.IsSupportedNewReport(projectNextGen.ProjectLegacy))
                        {
                            NewReport rpt = new NewReport(projectNextGen.ProjectLegacy);
                            string reportTempalte = GetReportTemplate();
                            string writeReport = WriteReport();
                            if (!rpt.ExportReportWord(reportTempalte, writeReport))
                            {
                                return;
                            }
                        }
                        else
                        {
                            string reportTempalte = GetReportTemplateRegionWise();
                            string writeReport = WriteReportTemplateRegionWise();
                            ReportForAspose rpt = new ReportForAspose(this.projectLegacy);
                            //NewReport rpt = new NewReport(this.projectLegacy);
                            if (!rpt.ExportReportWord(reportTempalte, writeReport))
                                return;
                        }

                    }
                    else
                    {
                        MessageBox.Show("System doesn’t allow user to generate report unless AutoPiping save not complete!");
                    }
                }
                else
                {
                    MessageBox.Show("System doesn’t allow user to generate report unless validation is not complete");
                }

            }
            else
            {
                MessageBox.Show("System doesn’t allow user to generate report unless AutoPiping not complete!");
            }
        }
        #endregion GenerateReport

        //get path from files
        #region 
        public string GetReportTemplate()
        {

            string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
            string navigateToFolder = "..\\..\\Report\\Template\\NewReport\\NewReport.doc";
            string sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            return sourceDir;
        }
        public string GetReportTemplateRegionWise()
        {

            string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
            string navigateToFolder = "..\\..\\Report\\Template\\VRF_Report.doc";
            string sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            return sourceDir;
        }
        public string WriteReportTemplateRegionWise()
        {

            string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
            string navigateToFolder = "..\\..\\Report\\Template\\VRF_Report.doc";
            string sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            return sourceDir;
        }
        #endregion

        #region 
        public string WriteReport()
        {

            string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
            string navigateToFolder = "..\\..\\Report\\Template\\Report.doc";
            string sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            return sourceDir;
        }
        #endregion 
        private void BindProductType()
        {
            MyProductTypeBLL productTypeBll = new MyProductTypeBLL();
            DataTable dt = productTypeBll.GetProductTypeData(this.projectLegacy.BrandCode, this.projectLegacy.SubRegionCode);
            if (dt != null && dt.Rows.Count > 0)
            {
                this.projectLegacy.ProductType = Convert.ToString(dt.Rows[0]["ProductType"]);
            }
        }


        private bool ReselectOutdoor()
        {
            string ErrMsg = string.Empty;
            MSGList = new List<string>();
            ERRList = new List<string>();
            bool IsValidDraw = Utility.Validation.IsValidatedSystemVRF(this.projectLegacy, out ErrMsg);
            try
            {
                ErrorLog.RemoveErrorFromList(Model.ErrorType.Error, Model.ErrorCategory.PipingErrors);
                SelectOutdoorResult result;
                BindProductType();
                NextGenModel.SystemVRF CurrVRF = new NextGenModel.SystemVRF();
                if (IsValidDraw)
                {
                    CurrVRF = this.projectLegacy.CanvasODUList[0];
                    if (CurrVRF.IsAuto == true)
                    {
                        result = Global.DoSelectUniversalODUFirst(CurrVRF, this.projectLegacy.RoomIndoorList, this.projectLegacy, CurrVRF.Series, out ERRList, out MSGList);
                    }
                    else
                    {
                        // For Maunal We are Calling below Methods
                        JCHVRF.BLL.NextGen.OutdoorBLL bll = new JCHVRF.BLL.NextGen.OutdoorBLL(this.projectLegacy.SubRegionCode, this.projectLegacy.BrandCode, this.projectLegacy.RegionCode);
                        CurrVRF.OutdoorItem = bll.GetOutdoorItemBySeries(CurrVRF.OutdoorItem.ModelFull, this.projectLegacy.CanvasODUList[0].Series);
                        result = Global.DoSelectUniversalOduManual(CurrVRF, this.projectLegacy.RoomIndoorList, this.projectLegacy, CurrVRF.Series, out ERRList);
                    }
                    if (result == SelectOutdoorResult.OK)
                    {
                        btnGenrateReport.IsEnabled = true;
                        lnkBtnValidate.IsEnabled = true;
                        if (this.projectLegacy.SystemListNextGen.Count == 0)
                            this.projectLegacy.SystemListNextGen.Add(CurrVRF);
                        else
                            this.projectLegacy.SystemListNextGen[0] = CurrVRF;
                        UpdatePipingNodeStructure();
                        UpdateWiringNodeStructure();
                        brdMain.Child = null;
                        ErrorLog.RemoveErrorFromList(Model.ErrorType.Error, Model.ErrorCategory.PipingErrors);
                    }
                    else
                    {
                        try
                        {
                            this.projectLegacy.SystemListNextGen.FirstOrDefault().MyPipingNodeOut = null;
                            this.projectLegacy.SystemListNextGen.FirstOrDefault().MyPipingNodeOutTemp = null;
                            this.projectLegacy.SystemListNextGen.FirstOrDefault().MyPipingNodeOut = null;
                            this.projectLegacy.SystemListNextGen.FirstOrDefault().IsPipingOK = false;
                            btnGenrateReport.IsEnabled = false;
                            lnkBtnValidate.IsEnabled = false;
                        }
                        catch (Exception ex) { }
                        IsValidDraw = false;
                        if (ERRList != null && ERRList.Count > 0)
                            MessageBox.Show(ERRList[0]);
                        ErrorLog.LogError(Model.ErrorType.Error, Model.ErrorCategory.PipingErrors, ERRList);
                    }

                }
                else
                {
                    IsValidDraw = false;
                    MessageBox.Show(string.Format(ErrMsg));
                    ErrorLog.LogError(Model.ErrorType.Error, Model.ErrorCategory.PipingErrors, ErrMsg);
                }
            }
            catch (Exception ex)
            {
                IsValidDraw = false;
                if (MSGList != null && MSGList.Count > 0)
                {
                    MessageBox.Show(MSGList[0]);
                    ErrorLog.LogError(Model.ErrorType.Error, Model.ErrorCategory.PipingErrors, MSGList);
                    if (ERRList != null && ERRList.Count > 0)
                        ErrorLog.LogError(Model.ErrorType.Error, Model.ErrorCategory.PipingErrors, ERRList);
                }
            }
            return IsValidDraw;

        }


        private void UpdatePipingNodeStructure()
        {
            try
            {
                NextGenBLL.PipingBLL pipBll = GetPipingBLLInstance();
                pipBll.CreatePipingNodeStructure(this.projectLegacy.SystemListNextGen[0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occured : " + ex.Message);
            }
        }

        private void UpdateWiringNodeStructure()
        {
            try
            {
                string imageDir = @"/Image/TypeImages/";
                JCHVRF_New.Utility.WiringBLL bll = new JCHVRF_New.Utility.WiringBLL(this.projectLegacy, imageDir);
                bll.CreateWiringNodeStructure(this.projectLegacy.SystemListNextGen[0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occured : " + ex.Message);
            }
        }
        private NextGenBLL.PipingBLL GetPipingBLLInstance()
        {
            string ut_length = SystemSetting.UserSetting.unitsSetting.settingLENGTH;
            string ut_power = SystemSetting.UserSetting.unitsSetting.settingPOWER;
            string ut_temperature = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            string ut_airflow = SystemSetting.UserSetting.unitsSetting.settingAIRFLOW;
            string ut_weight = SystemSetting.UserSetting.unitsSetting.settingWEIGHT;
            string ut_dimension = SystemSetting.UserSetting.unitsSetting.settingDimension;

            bool isInch = CommonBLL.IsDimension_inch();
            NextGenBLL.UtilPiping utilPiping = new NextGenBLL.UtilPiping();
            return new NextGenBLL.PipingBLL(this.projectLegacy, utilPiping, null, isInch, ut_weight, ut_length, ut_power);
        }
        // end added for Side bar collapse Setting Panel 12-15-2018

        //US- 5.1.1.1 
        private void MenuItemSystemProperties_Click(object sender, RoutedEventArgs e)
        {
            MainWindow systemPropObject = new MainWindow();
            projectLegacy.RegionCode = "EU_W";
            projectLegacy.SubRegionCode = "GBR";
            systemPropObject.DisplaySystemProperties(projectLegacy);
            // var winMain = new MainWindow(projectLegacy);
            //winMain.ShowDialog();
            systemPropObject.Show();
        }

        private void lnkBtnPropertiesIcon_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsEnabled = true;
            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
        }
        private void lnkBtnValidate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.projectLegacy.SystemListNextGen == null || this.projectLegacy.SystemListNextGen.Count == 0 || this.projectLegacy.SystemListNextGen.FirstOrDefault().MyPipingNodeOut == null)
                    return;
                designerCanvas.Validate();
                btnGenerateReport.IsEnabled = true; //Todo
                //ValidateViewModel vm = new ValidateViewModel(this.projectLegacy);
            }
            catch (Exception ex)
            {

            }
        }

        private void lnkBtnSaveIcon_Click(object sender, RoutedEventArgs e)
        {
            DoSavePipingTempNodeOutStructure();
            ProjectInfoBLL objProjectInfoBll = new ProjectInfoBLL();
            bool status = objProjectInfoBll.UpdateProject(this.projectLegacy);
            Application.Current.Properties["ProjectId"] = this.projectLegacy.projectID;
            MessageBox.Show("Successfully saved!");
        }
        private void lnkBtnHomeIcon_Click(object sender, RoutedEventArgs e)
        {
            //To Do
            //this.Close(); //added to close the master designer            
            //Dashboard objDashboard = new Dashboard();
            //objDashboard.ShowDialog();


        }

        public void DoSavePipingTempNodeOutStructure()
        {
            try
            {
                NextGenBLL.PipingBLL pipBllNG = GetPipingBLLInstance();
                pipBllNG.SaveAllPipingStructure();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occured : " + ex.Message);
            }
        }


        public void ReDrawPipingNodeAndWiringNode()
        {
            try
            {
                if (this.projectLegacy.SystemListNextGen == null || this.projectLegacy.SystemListNextGen.Count == 0)
                    return;
                else if (this.projectLegacy.SystemListNextGen != null && this.projectLegacy.SystemListNextGen.Count == 0 && this.projectLegacy.SystemListNextGen[0].MyPipingNodeOutTemp != null)
                    return;
                else if (this.projectLegacy.CanvasODUList != null && this.projectLegacy.CanvasODUList.Count == 0)
                    return;
                else if (this.projectLegacy.CanvasODUList.Count > 1)
                    return;

                NextGenBLL.PipingBLL pipBllNG = GetPipingBLLInstance();
                if (this.projectLegacy.SystemListNextGen[0].MyPipingNodeOutTemp != null)
                {
                    pipBllNG.LoadPipingNodeStructure(this.projectLegacy.SystemListNextGen[0]);
                    designerCanvas.DoDrawingPiping(true);
                    ShowSystemProperties();
                    if (this.projectLegacy.SystemListNextGen[0].IsPipingOK == true)
                    {
                        btnGenerateReport.IsEnabled = true;//Todo
                        this.projectLegacy.SystemListNextGen[0].IsPipingOK = false;
                        designerCanvas.Validate();
                    }
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occured : " + ex.Message);
            }
        }

        #region Generate Report Visibility Bug 1645 issue no 9
        public void CheckReportVisibility()
        {
            if (this.projectLegacy.SystemListNextGen == null || this.projectLegacy.SystemListNextGen.Count == 0 || this.projectLegacy.SystemListNextGen.FirstOrDefault().MyPipingNodeOut == null)
            {
                btnGenerateReport.IsEnabled = false;//Todo
            }
            else
            {
                btnGenerateReport.IsEnabled = true;//Todo

            }

        }

        #endregion Generate Report Visibility Bug 1645 issue no 9

        //public void SelectNextNodeOnDeletion(NextGenModel.ImageData NodeDataNext, bool blnIsLastNode)
        //{
        //    if (blnIsLastNode)
        //    {
        //        brdMain.Child = null;
        //        IDUEquipmentPropertiesViewModel = null;
        //        iduEquipmentPropertiesViewModel = null;
        //        MasterDesigner md = new MasterDesigner(this.projectLegacy);
        //    }
        //    else
        //    {
        //        this.SelectedNode = NodeDataNext;
        //    }
        //}
    }



}
