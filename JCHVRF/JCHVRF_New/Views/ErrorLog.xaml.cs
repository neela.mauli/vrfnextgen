﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JCHVRF_New.ViewModels;
using JCHVRF_New.Model;

namespace JCHVRF_New.Views
{
    /// <summary>
    /// Interaction logic for ErrorLog.xaml
    /// </summary>
    public partial class ErrorLog : UserControl
    {
        public static ObservableCollection<ErrorLogViewModel> GetPipingError = new ObservableCollection<ErrorLogViewModel>();
        public ErrorLog()
        {
            InitializeComponent();
            ErrorGrid.Items.Refresh();
            ErrorGrid.ItemsSource = GetPipingError;
        }

        public static void LogError(ErrorType Err, ErrorCategory ErrCat, string ErrMsg)
        {
            if(ErrMsg.Trim()=="")
            {
                RemoveErrorFromList(Err, ErrCat);
                return;
            }
           
            if (!(GetPipingError.Any(x => x.Description ==ErrMsg)))
            {
                string imgpath = "";
                if (ErrorType.Error == Err)
                {
                    imgpath = "..\\..\\Image\\TypeImages\\Error.png";
                }
                else imgpath = "..\\..\\Image\\TypeImages\\Warning.png";

                ErrorLogViewModel evm = new ErrorLogViewModel();
                evm.ErrorType = Err.ToString();
                evm.ErrorTypeImg = imgpath;
                evm.Category = ErrCat.ToString();
                evm.Description = ErrMsg;
                GetPipingError.Add(evm);
            }
            else
            {
                return;
            }
        }

        public static void LogError(ErrorType Err, ErrorCategory ErrCat, List<string> ErrMsg)
        {
            try
            {
                string imgpath = "";
                ErrorLogViewModel evm;
                if (ErrorType.Error == Err)
                {
                    imgpath = "..\\..\\Image\\TypeImages\\Error.png";
                }
                else imgpath = "..\\..\\Image\\TypeImages\\Warning.png";
                // RemoveErrorFromList(Err, ErrCat);
                if (ErrMsg != null && ErrMsg.Count > 0)
                    foreach (var Msg in ErrMsg)
                    {
                        evm = new ErrorLogViewModel();
                        evm.ErrorType = Err.ToString();
                        evm.ErrorTypeImg = imgpath;
                        evm.Category = ErrCat.ToString();
                        evm.Description = Msg;
                        GetPipingError.Add(evm);
                    }
            }
            catch { }
        }

        public static void RemoveErrorFromList(ErrorType Err, ErrorCategory ErrCat)
        {
            try
            {
                //GetPipingError.Clear();
                foreach (var Item in GetPipingError.ToList())
                {
                    if (Item.ErrorType == Err.ToString() && Item.Category == ErrCat.ToString())
                    {
                        GetPipingError.RemoveAt(GetPipingError.Count - 1);

                    }
                }
            }
            catch { }
        }



        private void Error_Click(object sender, RoutedEventArgs e)
        {
            var result = GetPipingError.AsEnumerable().Where(myRow => myRow.ErrorType == ErrorType.Error.ToString());
            ErrorGrid.ItemsSource = result;
        }

        private void Warning_Click(object sender, RoutedEventArgs e)
        {
            var result = GetPipingError.AsEnumerable().Where(myRow => myRow.ErrorType == ErrorType.Warning.ToString());
            ErrorGrid.ItemsSource = result;
        }
    }
}

