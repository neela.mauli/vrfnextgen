﻿/****************************** File Header ******************************\
File Name:	Bootstrapper.cs
Date Created:	2/7/2019
Description:	
\*************************************************************************/

namespace JCHVRF_New
{
    using JCHVRF.BLL.New;
    using JCHVRF.DAL.New;
    using JCHVRF_New.Common.CustomRegionAdapters;
    using JCHVRF_New.Views;
    using Prism.Regions;
    using Prism.Unity;
    using System.Windows;
    using Unity;
    using Xceed.Wpf.AvalonDock;

    public class Bootstrapper : UnityBootstrapper
    {
        #region Methods

        /// <summary>
        /// The ConfigureContainer
        /// </summary>
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            RegisterViewsForNavigation();
            RegisterTypesForDependencies();
        }

        /// <summary>
        /// The ConfigureRegionAdapterMappings
        /// </summary>
        /// <returns>The <see cref="RegionAdapterMappings"/></returns>
        protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
        {
            var mappings = base.ConfigureRegionAdapterMappings();
            mappings.RegisterMapping(typeof(DockingManager), Container.Resolve<DockingManagerRegionAdapter>());
            return mappings;
        }

        /// <summary>
        /// The CreateShell
        /// </summary>
        /// <returns>The <see cref="DependencyObject"/></returns>
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<Shell>();
        }

        /// <summary>
        /// The InitializeShell
        /// </summary>
        protected override void InitializeShell()
        {
            Application.Current.MainWindow.Show();
        }

        /// <summary>
        /// The RegisterViewsForNavigation
        /// </summary>
        private void RegisterViewsForNavigation()
        {
            //Ideally this would have been done in the module file, 
            //but we are working on a single project and no different modules
            //If Plan changes, This code should be moved to Initialize() method of Module file later.
            Container.RegisterType<object, Dashboard>("Dashboard");
            Container.RegisterType<object, Views.Settings>("Settings");
            Container.RegisterType<object, Views.NotificationsBar>("NotificationsBar");
            Container.RegisterType<object, Views.MasterDesigner>("MasterDesigner");

            Container.RegisterType<object, Views.ProjectDetails>("ProjectDetails");
            Container.RegisterType<object, Views.SystemDetails>("SystemDetails");
            Container.RegisterType<object, Views.PipingInfoProperties>("PipingInfo");
            Container.RegisterType<object, Views.CanvasProperties>("CanvasProperties");
            //Container.RegisterType<object, Views.IDUProperties>("IDUProperties");
            Container.RegisterType<object, Views.ErrorLog>("ErrorLog");
            Container.RegisterType<object, Views.Navigator>("Navigator");
            Container.RegisterType<object, Views.MainApp>("MainApp");           
            Container.RegisterType<object, Views.Activation>("Activation");
            Container.RegisterType<object, Views.Tools>("Tools");
            Container.RegisterType<object, Views.Help>("Help");

        }   

        /// <summary>
        /// The RegisterTypesForDependencies
        /// </summary>
        void RegisterTypesForDependencies()
        {
            //Ideally this would have been done in the module file, 
            //but we are working on a single project and no different modules
            //If Plan changes, This code should be moved to Initialize() method of Module file later.
            Container.RegisterType<IProjectInfoDAL, ProjectInfoDAL>();
            Container.RegisterType<IProjectInfoBAL, ProjectInfoBLL>();
        }

        #endregion
    }
}
