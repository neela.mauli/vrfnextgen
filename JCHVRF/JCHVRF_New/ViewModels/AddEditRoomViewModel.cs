﻿using JCHVRF.BLL;
using JCHVRF.Model;
using JCHVRF_New.Common.Helpers;
using JCHVRF_New.Views;
using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace JCHVRF_New.ViewModels
{

    public class AddEditRoomViewModel : ViewModelBase
    {
        private string _TempUnit;
        JCHVRF.Model.Project CurrentProject;
        public DelegateCommand RemoveRoomCommand { get; set; }
        public DelegateCommand BulkRemoveRoomCommand { get; set; }
        public DelegateCommand BulkAddCommand { get; set; }
        public DelegateCommand RoomCheckChangedCommand { get; set; }
        private bool _isEnableRemove;

        public bool IsEnableRemove
        {
            get { return _isEnableRemove; }
            set { this.SetValue(ref _isEnableRemove, value); }
        }
        public DelegateCommand<Window> AddEditFloorCommand { get; set; }
        public string TemperatureUnit
        {
            get
            {
                return _TempUnit;
            }
            set
            {
                this.SetValue(ref _TempUnit, value);
            }
        }

        private string _areaUnit;

        public string AreaUnit
        {
            get
            {
                return _areaUnit;
            }
            set
            {
                this.SetValue(ref _areaUnit, value);
            }
        }

        private string _powerUnit;

        public string PowerUnit
        {
            get
            {
                return _powerUnit;
            }
            set
            {
                this.SetValue(ref _powerUnit, value);
            }
        }

        private string _airFlowRate;

        public string AirFlowRateUnit
        {
            get
            {
                return _airFlowRate;
            }
            set
            {
                this.SetValue(ref _airFlowRate, value);
            }
        }
        private string _pressureUnit;
        public string PressureUnit
        {
            get
            {
                return _pressureUnit;
            }
            set
            {
                this.SetValue(ref _pressureUnit, value);
            }
        }
        #region Local_Property
        private IEventAggregator _eventAggregator;
        #endregion
        string lblRoom = string.Empty;
        string sRoomName = SystemSetting.UserSetting.defaultSetting.RoomName;
        #region View_Model_Property
        private double _CoolingDryBulb;
        public double CoolingDryBulb
        {
            get
            {
                return SystemSetting.UserSetting.defaultSetting.IndoorCoolingDB;
            }
            set { this.SetValue(ref _CoolingDryBulb, value); }
        }
        private double _CoolingWetBulb;
        public double CoolingWetBulb
        {
            get
            {
                return SystemSetting.UserSetting.defaultSetting.IndoorCoolingWB;
            }
            set { this.SetValue(ref _CoolingWetBulb, value); }
        }
        private double _CoolingRelativeHumidity;
        public double CoolingRelativeHumidity
        {
            get
            {
                return SystemSetting.UserSetting.defaultSetting.IndoorCoolingRH;
            }
            set { this.SetValue(ref _CoolingRelativeHumidity, value); }
        }
        private double _HeatingDryBulb;
        public double HeatingDryBulb
        {
            get
            {
                return SystemSetting.UserSetting.defaultSetting.IndoorHeatingDB;
            }
            set { this.SetValue(ref _HeatingDryBulb, value); }
        }
        private double _RoomArea;
        public double RoomArea
        {
            get { return RoomArea; }
            set { this.SetValue(ref _RoomArea, value); }
        }

        private double _SensibleHeat = 0.0;

        public double SensibleHeat
        {
            get { return _SensibleHeat; }
            set { this.SetValue(ref _SensibleHeat, value); }
        }
        #endregion
        private double _RqCapacityCool = 0.0;

        public double RqCapacityCool
        {
            get { return _RqCapacityCool; }
            set { this.SetValue(ref _RqCapacityCool, value); }
        }
        private double _RqCapacityHeat = 0.0;

        public double RqCapacityHeat
        {
            get { return _RqCapacityHeat; }
            set { this.SetValue(ref _RqCapacityHeat, value); }
        }

        private double _airflow = 0.0;

        public double Airflow
        {
            get { return _airflow; }
            set { this.SetValue(ref _airflow, value); }
        }

        private double _staticPressure=0.0;

        public double StaticPressure
        {
            get { return _staticPressure; }
            set { this.SetValue(ref _staticPressure, value); }
        }

        private double _freshAir;

        public double FreshAir
        {
            get { return _freshAir; }
            set { _freshAir = value; }
        }


        public DelegateCommand AddRoomCommand { get; set; }

        public DelegateCommand<Window> CancelClickCommand { get; private set; }
        public DelegateCommand chkSelectAll_CheckedCommand { get; set; }

        private ObservableCollection<Room> _RoomList;
        public ObservableCollection<Room> RoomList
        {
            get
            {
                if (_RoomList == null)
                    _RoomList = new ObservableCollection<Room>();
                return _RoomList;
            }
        }

        public AddEditRoomViewModel(IEventAggregator eventAggregator)
        {
            CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            _eventAggregator = eventAggregator;
            TemperatureUnit = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            AreaUnit = SystemSetting.UserSetting.unitsSetting.settingAREA;
            PowerUnit = SystemSetting.UserSetting.unitsSetting.settingPOWER;
            AirFlowRateUnit = SystemSetting.UserSetting.unitsSetting.settingAIRFLOW;
            PressureUnit = SystemSetting.UserSetting.unitsSetting.settingPressure;
            AddEditFloorCommand = new DelegateCommand<Window>(this.AddEditFloorClick);
            AddRoomCommand = new DelegateCommand(AddRoomIconClick);
            CancelClickCommand = new DelegateCommand<Window>(this.CancelClick);
            RemoveRoomCommand = new DelegateCommand(RemoveRoomIconClick);
            BulkRemoveRoomCommand = new DelegateCommand(BulkRemoveRoom);
            BulkAddCommand = new DelegateCommand(BulkAddClick);
            _eventAggregator.GetEvent<PubSubEvent<int>>().Subscribe(OnMultiRoomValueBulkUpload);
            AddRoomDefoult();
            RoomCheckChangedCommand = new DelegateCommand(()=> { RaisePropertyChanged("AreAllRoomChecked"); });
        }

        private void AddRoomDefoult()
        {

            if (RoomList.Count > 0)
            {
                RoomList.Clear();
            }
            else
            {
                
                CurrentProject.FloorList[0].RoomList.ForEach((item) =>
                {
                    RoomList.Add(new Room
                    {
                        Id = item.Id,
                        Name = item.Name,
                        SensibleHeat = item.SensibleHeat,
                        RqCapacityCool = item.RqCapacityCool,
                        RqCapacityHeat = item.RqCapacityHeat,
                        HeatingDryBulb = item.HeatingDryBulb,
                        CoolingDryBulb = item.CoolingDryBulb,
                        CoolingWetBulb = item.CoolingWetBulb,
                        CoolingRelativeHumidity = item.CoolingRelativeHumidity
                    });
                });
                EnableDisable();
            }
            // RoomList.Add(new Room { });
        }
        private void RemoveRoomIconClick()
        {
            if (this.RoomList != null && this.RoomList.Count > 0)
            {
                Room room = this.RoomList.LastOrDefault();
                this.RoomList.Remove(room);
                EnableDisable();
            }
        }
        private void OnMultiRoomValueBulkUpload(int introomCount)
        {
            //int introomCount = int.Parse(RoomCount);
            if (introomCount>0)
            {
                int BulkAddCount = this.RoomList.Count + introomCount;
                for (int i =this.RoomList.Count; i < BulkAddCount; i++)
                {
                    RoomList.Add(new Room
                    {
                        Id = (i.ToString()),
                        Name = sRoomName + (i),
                        SensibleHeat = SensibleHeat,
                        RqCapacityCool = RqCapacityCool,
                        RqCapacityHeat = RqCapacityHeat,
                        //HeatingDryBulb = HeatingDryBulb,
                        //CoolingDryBulb = CoolingDryBulb,
                        //CoolingWetBulb = CoolingWetBulb,
                        //CoolingRelativeHumidity = CoolingRelativeHumidity
                        HeatingDryBulb = Convert.ToDouble(CurrentProject.DesignCondition.indoorCoolingHDB),
                        CoolingDryBulb = Convert.ToDouble(CurrentProject.DesignCondition.indoorCoolingDB),
                        CoolingWetBulb = Convert.ToDouble(CurrentProject.DesignCondition.indoorCoolingWB),
                        CoolingRelativeHumidity = Convert.ToDouble(CurrentProject.DesignCondition.indoorCoolingRH)
                    });
                }
                EnableDisable();
            }
        }
        private void BulkAddClick()
        {
            BulkRoomPopup BrpopModel = new BulkRoomPopup();
            BrpopModel.ShowDialog();
        }
        private void BulkRemoveRoom()
        {
            for (int i = RoomList.Count-1; i >= 0; i--)
                if (RoomList[i].IsRoomChecked)
                    RoomList.RemoveAt(i);
        }

        private void EnableDisable()
        {
            if (this.RoomList != null && this.RoomList.Count < 1)
            {
                IsEnableRemove = false;
            }
            else
            {
                IsEnableRemove = true;
            }

        }
        private void AddEditFloorClick(Window win)
        {
            CurrentProject.FloorList[0].RoomList = (RoomList.ToList());
            _eventAggregator.GetEvent<RoomListSaveSubscriber>().Publish();
            if (win != null)
            {
                win.Close();
            }

        }

        private void AddRoomIconClick()
        {

            
            RoomList.Add(new Room(this.RoomList.Count)
            {
                Id = (this.RoomList.Count + 1).ToString(),
                Name = sRoomName + (this.RoomList.Count + 1),
                SensibleHeat = SensibleHeat,
                RqCapacityCool = RqCapacityCool,
                RqCapacityHeat = RqCapacityHeat,
                HeatingDryBulb = Convert.ToDouble(CurrentProject.DesignCondition.indoorCoolingHDB),
                CoolingDryBulb = Convert.ToDouble(CurrentProject.DesignCondition.indoorCoolingDB),
                CoolingWetBulb = Convert.ToDouble(CurrentProject.DesignCondition.indoorCoolingWB),
                CoolingRelativeHumidity = Convert.ToDouble(CurrentProject.DesignCondition.indoorCoolingRH)
            });
            EnableDisable();
        }

        private void CancelClick(Window win)
        {
            if (win != null)
            {
                win.Close();
            }
        }

        public bool AreAllRoomChecked
        {
            get { return RoomList.All(a=>a.IsRoomChecked); }
            set { foreach (Room item in RoomList)
                {
                    item.IsRoomChecked = value;
                } }
        }

      

    }
}
