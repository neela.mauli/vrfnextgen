﻿/****************************** File Header ******************************\
File Name:	PipingInfoPropertiesViewModel.cs
Date Created:	2/20/2019
Description:	
\*************************************************************************/

namespace JCHVRF_New.ViewModels
{
    using JCHVRF_New.Common.Contracts;
    using JCHVRF_New.Common.Helpers;
    using JCHVRF_New.Model;
    using Prism.Events;
    using System.Collections.Generic;
    using JCHVRF.Model;
    using JCHVRF.VRFMessage;
    using JCBase.Utility;

    public class PipingInfoPropertiesViewModel : ViewModelBase, ILayoutItem
    {
        #region Fields

        private double _FloatingHeight;

        private double _FloatingLeft;

        private double _FloatingTop;

        private double _FloatingWidth;

        private List<PipingInfoModel> _ListPipingInfoHeight;

        private List<PipingInfoModel> _ListPipingInfoLength;

        private string _title;

        private string _LengthUnit;

        IEventAggregator _eventAggregator;

        #endregion

        #region Constructors

        public PipingInfoPropertiesViewModel(IEventAggregator EventAggregator)
        {
            _eventAggregator = EventAggregator;
            //Title = "Piping Info";
            LengthUnit = SystemSetting.UserSetting.unitsSetting.settingLENGTH;
            Title = "Piping Info";
            FloatingHeight = 500;
            FloatingWidth = 700;
            FloatingTop = 100;
            FloatingLeft = 100;
            BindPipingInfoLength();
            BindPipingInfoHeight();
            BindAdditionalPipingInfo();

        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the FloatingHeight
        /// </summary>
        public double FloatingHeight
        {
            get { return this._FloatingHeight; }
            set { this.SetValue(ref _FloatingHeight, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingLeft
        /// </summary>
        public double FloatingLeft
        {
            get { return this._FloatingLeft; }
            set { this.SetValue(ref _FloatingLeft, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingTop
        /// </summary>
        public double FloatingTop
        {
            get { return this._FloatingTop; }
            set { this.SetValue(ref _FloatingTop, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingWidth
        /// </summary>
        public double FloatingWidth
        {
            get { return this._FloatingWidth; }
            set { this.SetValue(ref _FloatingWidth, value); }
        }

        /// <summary>
        /// Gets or sets the ListPipingInfoHeight
        /// </summary>
        public List<PipingInfoModel> ListPipingInfoHeight
        {
            get
            {
                return _ListPipingInfoHeight;
            }
            set
            {
                this.SetValue(ref _ListPipingInfoHeight, value);
            }
        }

        ///// <summary>
        ///// Gets or sets the ListPipingInfoLength
        ///// </summary>
        public List<PipingInfoModel> ListPipingInfoLength
        {
            get
            {
                return _ListPipingInfoLength;
            }
            set
            {
                this.SetValue(ref _ListPipingInfoLength, value);
            }
        }

        /// <summary>
        /// Gets or sets the unit
        /// </summary>
        public string LengthUnit
        {
            get
            {
                return _LengthUnit;
            }
            set
            {
                this.SetValue(ref _LengthUnit, value);
            }
        }

        /// <summary>
        /// Gets or sets the Title
        /// </summary>
        public string Title
        {
            get { return this._title; }
            set { this.SetValue(ref _title, value); }
        }

        //Additional PipingInformation


        public string IUConnectable { get; set; }
        public string ConnectedCap { get; set; }
        public double IUConnectableProjectInfo { get; set; }
        public double IUConnectableMaxInfo { get; set; }
        public double ConnectedCapProjectInfo { get; set; }
        public double ConnectedCaplowerRangeMaxInfo { get; set; }
        public double ConnectedCapupperRangeMaxInfo { get; set; }
        #endregion

        #region Methods

        /// <summary>
        /// The BindPipingInfoHeight
        /// </summary>
        public void BindPipingInfoHeight()
        {
            ListPipingInfoHeight = new List<PipingInfoModel>();
            //Static Object Created for testing Purpose
            PipingInfoModel PipingObjectHeightDiffUpper = new PipingInfoModel();
            
            PipingObjectHeightDiffUpper.Description = "Ht diff. O.U. is upper";
            PipingObjectHeightDiffUpper.LongDescription = Msg.GetResourceString("PipingRules_HeightDiffH");
            PipingObjectHeightDiffUpper.Max = Unit.ConvertToControl(50, UnitType.LENGTH_M, LengthUnit);
            PipingObjectHeightDiffUpper.Value = Unit.ConvertToControl(0, UnitType.LENGTH_M, LengthUnit);
            ListPipingInfoHeight.Add(PipingObjectHeightDiffUpper);

            PipingInfoModel PipingObjectHeightDiffLower = new PipingInfoModel();
            
            PipingObjectHeightDiffLower.Description = "Ht diff. O.U. is lower";
            PipingObjectHeightDiffLower.LongDescription = "Height difference between(O.U. is lower)";// Msg.GetResourceString("PipingRules_HeightDiffL");
            PipingObjectHeightDiffLower.Max = Unit.ConvertToControl(40, UnitType.LENGTH_M, LengthUnit);
            PipingObjectHeightDiffLower.Value = Unit.ConvertToControl(60, UnitType.LENGTH_M, LengthUnit);
            ListPipingInfoHeight.Add(PipingObjectHeightDiffLower);

            PipingInfoModel PipingObjectHeightDiffIDU = new PipingInfoModel();
            
            PipingObjectHeightDiffIDU.Description = "Max Length Equival";
            PipingObjectHeightDiffIDU.LongDescription= "Height difference between indoor units"; // Msg.GetResourceString("PipingRules_DiffIndoorHeight");
            PipingObjectHeightDiffIDU.Max = Unit.ConvertToControl(30, UnitType.LENGTH_M, LengthUnit);
            PipingObjectHeightDiffIDU.Value = Unit.ConvertToControl(0, UnitType.LENGTH_M, LengthUnit);
            ListPipingInfoHeight.Add(PipingObjectHeightDiffIDU);
            
        }

        /// <summary>
        /// The BindPipingInfoLength
        /// </summary>
        public void BindPipingInfoLength()
        {

            ListPipingInfoLength = new List<PipingInfoModel>();
            //Static Object Created for testing Purpose
            PipingInfoModel PipingObjectLength = new PipingInfoModel();
            PipingObjectLength.Description = "Total Length";
            PipingObjectLength.LongDescription = Msg.GetResourceString("PipingRules_TotalPipeLength");
            PipingObjectLength.Max = Unit.ConvertToControl(1000, UnitType.LENGTH_M, LengthUnit); 
            PipingObjectLength.Value = Unit.ConvertToControl(234, UnitType.LENGTH_M, LengthUnit); 
            ListPipingInfoLength.Add(PipingObjectLength);

            PipingInfoModel PipingObjectMaxLengthActual = new PipingInfoModel();
           
            PipingObjectMaxLengthActual.Description = "Max Length Actuals";
            PipingObjectMaxLengthActual.LongDescription = Msg.GetResourceString("PipingRules_PipeActualLength");
            PipingObjectMaxLengthActual.Max = Unit.ConvertToControl(456, UnitType.LENGTH_M, LengthUnit); 
            PipingObjectMaxLengthActual.Value = Unit.ConvertToControl(345, UnitType.LENGTH_M, LengthUnit); 
            ListPipingInfoLength.Add(PipingObjectMaxLengthActual);

            PipingInfoModel PipingObjectMaxLengthEquiVal = new PipingInfoModel();
            
            PipingObjectMaxLengthEquiVal.Description = "Max Length Equival";
            PipingObjectMaxLengthEquiVal.LongDescription = Msg.GetResourceString("PipingRules_PipeEquivalentLength");
            PipingObjectMaxLengthEquiVal.Max = Unit.ConvertToControl(700, UnitType.LENGTH_M, LengthUnit); 
            PipingObjectMaxLengthEquiVal.Value = Unit.ConvertToControl(755, UnitType.LENGTH_M, LengthUnit); 
            ListPipingInfoLength.Add(PipingObjectMaxLengthEquiVal);

            PipingInfoModel PipingObjectMaxLengthFirstPipeLength = new PipingInfoModel();
            
            PipingObjectMaxLengthFirstPipeLength.Description = "Max Length Equival";
            PipingObjectMaxLengthFirstPipeLength.LongDescription = Msg.GetResourceString("PipingRules_FirstPipeLength");
            PipingObjectMaxLengthFirstPipeLength.Max = Unit.ConvertToControl(700, UnitType.LENGTH_M, LengthUnit); 
            PipingObjectMaxLengthFirstPipeLength.Value = Unit.ConvertToControl(755, UnitType.LENGTH_M, LengthUnit); 
            ListPipingInfoLength.Add(PipingObjectMaxLengthFirstPipeLength);

            PipingInfoModel PipingObjectMaxLengthActualMaxMKIndoorPipeLength = new PipingInfoModel();
           
            PipingObjectMaxLengthActualMaxMKIndoorPipeLength.Description = "Max Length Equival";
            PipingObjectMaxLengthActualMaxMKIndoorPipeLength.LongDescription = Msg.GetResourceString("PipingRules_ActualMaxMKIndoorPipeLength");
            PipingObjectMaxLengthActualMaxMKIndoorPipeLength.Max = Unit.ConvertToControl(700, UnitType.LENGTH_M, LengthUnit); 
            PipingObjectMaxLengthActualMaxMKIndoorPipeLength.Value = Unit.ConvertToControl(755, UnitType.LENGTH_M, LengthUnit); 
            ListPipingInfoLength.Add(PipingObjectMaxLengthActualMaxMKIndoorPipeLength);           
        }

        /// <summary>
        /// The BindAdditionalPipingInfo
        /// </summary>
        public void BindAdditionalPipingInfo()
        {

            ConnectedCap = Msg.GetResourceString("PipingRules_ConnectedCap");
            IUConnectable = Msg.GetResourceString("PipingRules_IUConnectable");
            IUConnectableProjectInfo = Unit.ConvertToControl(3, UnitType.LENGTH_M, LengthUnit); ;
            IUConnectableMaxInfo = Unit.ConvertToControl(5, UnitType.LENGTH_M, LengthUnit); ;
            ConnectedCapProjectInfo = Unit.ConvertToControl(56, UnitType.LENGTH_M, LengthUnit); ;
            ConnectedCaplowerRangeMaxInfo = 50; //this is a percentage value
            ConnectedCapupperRangeMaxInfo = 130; //this is a percentage value
        }

        #endregion
    }
}
