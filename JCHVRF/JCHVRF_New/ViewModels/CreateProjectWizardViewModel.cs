﻿/****************************** File Header ******************************\
File Name:	CreateProjectWizardViewModel.cs
Date Created:	2/8/2019
Description:	View Model for CreateProjectWizard View.
\*************************************************************************/

namespace JCHVRF_New.ViewModels
{
    using JCHVRF.BLL.New;
    using JCHVRF.Model;
    using JCHVRF_New.Common.Constants;
    using JCHVRF_New.Common.Helpers;
    using Prism.Commands;
    using Prism.Events;
    using Prism.Regions;
    using System.Windows;

    public class CreateProjectWizardViewModel : ViewModelBase
    {
        #region Fields
        private int _selectedTabIndex;
        private IEventAggregator _eventAggregator;
        private IProjectInfoBAL _projectBAL;
        #endregion Fields
        string systemTypeSelected = null;

        #region Properties
        public DelegateCommand<Window> CancelClickCommand { get; private set; }
        public DelegateCommand<Window> CreateClickCommand { get; private set; }
        public DelegateCommand NextClickCommand { get; set; }
        public DelegateCommand PreviousClickCommand { get; set; }

        /// <summary>
        /// Gets or sets the SelectedTabIndex
        /// </summary>
        private Visibility _floorvisibility;
        public Visibility FloorVisibility
        {
            get { return this._floorvisibility; }
            set { this.SetValue(ref _floorvisibility, value); }
        }

        private void GetSetFloorTabVisibility(Visibility floorvisibility)
        {
            FloorVisibility = floorvisibility;
        }

        //Accord - - Total Heat Exchange Unit Info Tab binding variable
        /// </summary>
        private Visibility _theuInfoVisibility;
        public Visibility TheuInfoVisibility
        {
            get { return this._theuInfoVisibility; }
            set { this.SetValue(ref _theuInfoVisibility, value); }
        }
        private void GetSetTHEUITabVisibility(Visibility theuInfoVisibility)
        {
            TheuInfoVisibility = theuInfoVisibility;
        }
        //Accord - End

        bool IsProjectInfovalidateTabinfo = false;
        private void SetProjectInfoValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsProjectInfovalidateTabinfo = IsvalidateTabinfoReturn;
        }
        bool IsDesignTabvalidateTabinfo = false;
        private void SetDesignTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsDesignTabvalidateTabinfo = IsvalidateTabinfoReturn;
        }
        bool IsTypeTabvalidateTabinfo = false;
        private void SetTypeTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsTypeTabvalidateTabinfo = IsvalidateTabinfoReturn;
        }
        bool IsFloorTabvalidateinfo = false;
        private void SetFloorTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsFloorTabvalidateinfo = IsvalidateTabinfoReturn;
        }

        bool IsOduTabvalidateinfo = false;
        private void SetOduTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsOduTabvalidateinfo = IsvalidateTabinfoReturn;
        }

        bool IsIDUTabvalidateinfo = false;
        private void SetIDUTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsIDUTabvalidateinfo = IsvalidateTabinfoReturn;
        }

        private Visibility _iduVisibility;
        public Visibility IduVisibility
        {
            get { return this._iduVisibility; }
            set { this.SetValue(ref _iduVisibility, value); }
        }

        private void GetSetIduTabVisibility(Visibility iduvisibility)
        {
            IduVisibility = iduvisibility;
        }

        /// <summary>
        /// Gets or sets the SelectedTabIndex
        /// </summary>
        public int SelectedTabIndex
        {
            get { return this._selectedTabIndex; }
            set { this.SetValue(ref _selectedTabIndex, value); }
        }

        public IRegionManager RegionManager { get; set; }


        #endregion Properties

        #region Constructors
        public CreateProjectWizardViewModel(IEventAggregator EventAggregator, IProjectInfoBAL projctInfoBll, IRegionManager _regionManager)
        {
            _eventAggregator = EventAggregator;
            _projectBAL = projctInfoBll;
            _eventAggregator.GetEvent<PubSubEvent<Visibility>>().Subscribe(GetSetFloorTabVisibility);
            _eventAggregator.GetEvent<OduIduVisibility>().Subscribe(GetSetIduTabVisibility);
            _eventAggregator.GetEvent<TheuInfoVisibility>().Subscribe(GetSetTHEUITabVisibility);  //ACC - RAG 
            //_eventAggregator.GetEvent<RefreshDashboard>().Subscribe(RefreshDashBoard);
            CancelClickCommand = new DelegateCommand<Window>(this.CancelClick);
            CreateClickCommand = new DelegateCommand<Window>(this.CreateClick);
            NextClickCommand = new DelegateCommand(NextClick);
            PreviousClickCommand = new DelegateCommand(PreviousClick);
            this.FloorVisibility = Visibility.Collapsed;
            this.TheuInfoVisibility = Visibility.Collapsed;  //ACC - RAG 
            _eventAggregator.GetEvent<ProjectInfoSubscriber>().Subscribe(SetProjectInfoValidateValue);
            _eventAggregator.GetEvent<DesignerTabSubscriber>().Subscribe(SetDesignTabValidateValue);
            _eventAggregator.GetEvent<TypeTabSubscriber>().Subscribe(SetTypeTabValidateValue);
            _eventAggregator.GetEvent<FloorTabSubscriber>().Subscribe(SetFloorTabValidateValue);
            _eventAggregator.GetEvent<NextButtonVisibility>().Subscribe(GetSetNextBtnVisibility); //ACC - RAG
            this.NextButtonVisibility = Visibility.Collapsed; //ACC - RAG
            _eventAggregator.GetEvent<OduTabSubscriber>().Subscribe(SetOduTabValidateValue);
            _eventAggregator.GetEvent<IduTabSubscriber>().Subscribe(SetIDUTabValidateValue);
            _eventAggregator.GetEvent<CreateButtonVisibility>().Subscribe(GetSetCreateBtnVisibility); //ACC - RAG
            this.CreateButtonVisibility = Visibility.Collapsed; //ACC - RAG
            RegionManager = _regionManager;
        }

        #endregion

        private void RefreshDashBoard()
        {
            _eventAggregator.GetEvent<RefreshDashboard>().Publish();
        }

        private void SelectWizardNextTab()
        {
            if (this.SelectedTabIndex == 2)
            {
                if (this.FloorVisibility == Visibility.Collapsed)
                {
                    //ACC - RAG START
                    //To view Total Heat Exchange Unit Info Tab Control
                    if (this.TheuInfoVisibility == Visibility.Collapsed)
                    {
                        this.SelectedTabIndex = this.SelectedTabIndex + 2;
                    }
                    else
                    {
                        this.SelectedTabIndex = this.SelectedTabIndex + 4;
                        CreateButtonVisibility = Visibility.Visible;
                        NextButtonVisibility = Visibility.Collapsed;
                    }
                    //ACC - RAG END
                }
                else if (this.FloorVisibility == Visibility.Visible)
                {
                    this.SelectedTabIndex = this.SelectedTabIndex + 1;
                    NextButtonVisibility = Visibility.Visible;
                }
            }
            else
            {
                if (this.TheuInfoVisibility == Visibility.Visible && this.SelectedTabIndex >=2) //Accord - Bug fix
                {
                    this.SelectedTabIndex = this.SelectedTabIndex + 3;
                    CreateButtonVisibility = Visibility.Visible;
                    NextButtonVisibility = Visibility.Collapsed;
                }
                else
                {
                    
                    this.SelectedTabIndex = this.SelectedTabIndex + 1;
                    if (this.SelectedTabIndex <= 2)
                    {
                        CreateButtonVisibility = Visibility.Collapsed;
                        NextButtonVisibility = Visibility.Visible;
                    }
                    else
                    {
                        if(this.SelectedTabIndex == 4)
                        {
                            CreateButtonVisibility = Visibility.Collapsed;
                            NextButtonVisibility = Visibility.Visible;
                        }
                        else
                        {
                            CreateButtonVisibility = Visibility.Visible;
                            NextButtonVisibility = Visibility.Collapsed;
                        }                       
                    }
                }
            }
        }

        private void SelectWizardPreviousTab()
        {
            //ACC - RAG 
            //Updated to correct the flow of tab page changes
            if (this.FloorVisibility == Visibility.Collapsed)
            {
                if (this.SelectedTabIndex == 4)
                {
                    this.SelectedTabIndex = this.SelectedTabIndex - 2;
                }
                else if (this.SelectedTabIndex == 6)
                {
                    this.SelectedTabIndex = this.SelectedTabIndex - 4;
                    CreateButtonVisibility = Visibility.Collapsed;
                    NextButtonVisibility = Visibility.Visible;
                }
                else
                {
                    this.SelectedTabIndex = this.SelectedTabIndex - 1;
                    CreateButtonVisibility = Visibility.Collapsed;
                    NextButtonVisibility = Visibility.Visible;
                }
            }
            else
            {
                if (this.SelectedTabIndex == 6)
                {
                    this.SelectedTabIndex = this.SelectedTabIndex - 3;
                    CreateButtonVisibility = Visibility.Collapsed;
                    NextButtonVisibility = Visibility.Visible;
                }
                else
                {
                    this.SelectedTabIndex = this.SelectedTabIndex - 1;
                    CreateButtonVisibility = Visibility.Collapsed;
                    NextButtonVisibility = Visibility.Visible;
                }
            }                
        }

        //ACC - RAG START
        //Create Button Visibility Property binding
        private Visibility _createButtonVisibility;
        public Visibility CreateButtonVisibility
        {
            get { return this._createButtonVisibility; }
            set { this.SetValue(ref _createButtonVisibility, value); }
        }
        private void GetSetCreateBtnVisibility(Visibility createButtonVisibility)
        {
            CreateButtonVisibility = createButtonVisibility;
        }
        //ACC - RAG END


        //ACC - RAG START
        //Next Button Visibility Property binding
        private Visibility _nextButtonVisibility;
        public Visibility NextButtonVisibility
        {
            get { return this._nextButtonVisibility; }
            set { this.SetValue(ref _nextButtonVisibility, value); }
        }
        private void GetSetNextBtnVisibility(Visibility nextButtonVisibility)
        {
            NextButtonVisibility = nextButtonVisibility;
        }
        //ACC - RAG END

        private void CancelClick(Window win)
        {
            if (win != null)
            {
                win.Close();
            }
        }


       //IRegionManager regionManager;
        private void CreateClick(Window win)
        {
            
            var proj = JCHVRF.Model.Project.GetProjectInstance;
            
            if (_projectBAL.CreateProject(proj))
            {
                //ACC - RAG START
                if(this.SelectedTabIndex == 2)
                {
                    //works when create is pressed for central controller
                    bool IsAdded = false;
                    if (JCHVRF.Model.Project.CurrentProject.SystemListNextGen.Count > 0)
                    {
                        foreach (JCHVRF.Model.NextGen.SystemVRF sys in JCHVRF.Model.Project.CurrentProject.SystemListNextGen)
                        {
                            if (JCHVRF.Model.Project.CurrentProject.SystemName != sys.Name)
                            {
                                sys.IsActiveSystem = false;
                            }
                            else
                            {
                                IsAdded = true;
                            }
                        }
                    }

                    if (IsAdded == false)
                    {
                        JCHVRF.Model.Project.CurrentProject.SystemListNextGen.Add(new JCHVRF.Model.NextGen.SystemVRF
                        {
                            Name = JCHVRF.Model.Project.CurrentProject.SystemName,
                            SystemTypeNexGen = "Central Controller",
                            IsActiveSystem = true,
                        });// = SelectedsystemName.Name;
                    }
                }

                systemTypeSelected = JCHVRF.Model.Project.GetProjectInstance.SystemListNextGen[0].SystemTypeNexGen.ToString(); //TODO : Not working for Central Controller                                                                            
                           
                if (systemTypeSelected == "Heat Exchanger" || systemTypeSelected == "Central Controller")
                {
                    MessageBox.Show(systemTypeSelected + " has no system available for connection");
                }
                else
                {
                    MessageBox.Show("Project Saved Successfully");
                }
                //ACC - RAG END
                if (win != null)
                {
                    var w = Application.Current.MainWindow;
                    w.Hide();
                    RefreshDashBoard();
                    var projectId = proj.projectID;
                    int projectid = (int)projectId;
                    Application.Current.Properties["ProjectId"] = projectId;
                    ProjectInfoBLL bll = new ProjectInfoBLL();
                    JCHVRF.Entity.ProjectInfo projectNextGen = JCHVRF.Entity.ProjectInfo.create();
                    projectNextGen = bll.GetProjectInfo(projectId);
                    projectNextGen.ProjectLegacy.RegionCode = JCHVRF.Model.Project.CurrentProject.RegionCode;
                    projectNextGen.ProjectLegacy.SubRegionCode = JCHVRF.Model.Project.CurrentProject.SubRegionCode;
                    projectNextGen.ProjectLegacy.projectID = projectid;
                    //Added by Ramya - 7/3/2019
                    //To get System type selected by user
                    
                    NavigationParameters param = new NavigationParameters();
                    param.Add("Project", projectNextGen.ProjectLegacy);

                    RegionManager.RequestNavigate(RegionNames.ContentRegion, "MasterDesigner",(a)=> { win.Close(); } ,param);
                    w.Show();

                }
                
            }
        }
        private void NextClick()
        {
            switch (this.SelectedTabIndex)
            {
                //ProjectInfo
                case 0:
                    _eventAggregator.GetEvent<ProjectTypeInfoTabNext>().Publish();
                    if (IsProjectInfovalidateTabinfo)
                        SelectWizardNextTab();// 
                    break;
                //Design Conditions Tab
                case 1:
                    _eventAggregator.GetEvent<DesignConditionTabNext>().Publish();
                    if (IsDesignTabvalidateTabinfo)                        
                        SelectWizardNextTab();
                    break;
                //Type Tab
                case 2:
                    _eventAggregator.GetEvent<TypeInfoTabNext>().Publish();
                    if (IsTypeTabvalidateTabinfo)
                        SelectWizardNextTab();
                    break;
                //Floor Tab
                case 3:
                    _eventAggregator.GetEvent<FloorTabNext>().Publish();
                    // if (IsFloorTabvalidateinfo)   //ACC - RAG //UnComment this
                    SelectWizardNextTab();
                    break;
                //ODU Tab
                case 4:
                    _eventAggregator.GetEvent<ODUTypeTabNext>().Publish();
                   if (IsOduTabvalidateinfo)
                        SelectWizardNextTab();
                    break;
                //IDU Tab
                case 5:
                    SelectWizardNextTab();
                    break;
/*
                case 6:
                    SelectWizardNextTab();
                    break;
                    */

                default:
                    break;
            }
        }
        private void PreviousClick()
        {
            //ACC - SHIV
            if(this.SelectedTabIndex <= 2)
            {
                CreateButtonVisibility = Visibility.Collapsed;
                NextButtonVisibility = Visibility.Visible;
            }
            SelectWizardPreviousTab();
        }

        #region Properties

        /// <summary>
        /// Gets or sets the SelectedTabIndex
        /// </summary>



        #endregion Properties
    }
}
