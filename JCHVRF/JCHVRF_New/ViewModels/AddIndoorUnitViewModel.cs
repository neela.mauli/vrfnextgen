﻿using JCBase.UI;
using JCBase.Utility;
using JCHVRF.BLL;
using JCHVRF.Const;
using JCHVRF.Model;
using JCHVRF.VRFMessage;
using JCHVRF_New.Common.Contracts;
using JCHVRF_New.Common.Helpers;
using JCHVRF_New.Model;
using JCHVRF_New.Utility;
using JCHVRF_New.Views;
using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace JCHVRF_New.ViewModels
{
    public class AddIndoorUnitViewModel : ViewModelBase
    {
        private IEventAggregator _eventAggregator;
        private bool _isLoading;
        private static bool _isAutoSelecting;

        #region Local_Property
        private double outdoorCoolingDB = 0;
        private double outdoorHeatingWB = 0;
        private double outdoorCoolingIW = 0;
        private double outdoorHeatingIW = 0;
        DataTable IDuModelTypeList = new DataTable();
        string _factory = "";
        string _type = "";
        string _productType = "";
        string ODUProductTypeUniversal = "Universal IDU";
        List<string> _typeList = new List<string>();
        List<Indoor> IDUIndoorList;
        string ODUSeries;// = "Commercial VRF HP, FSXNK"; // "Commercial VRF HP, FSXNK",  Residential VRF HP, FS(V/Y)N1Q/FSNMQ
        string ODUProductType;// = "Comm. Tier 2, HP"; // "Comm. Tier 2, HP"  Res. Tier 1, HP
        IndoorBLL bll;
        string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
        string navigateToFolder = "..\\..\\Image\\TypeImageProjectCreation";
        string utPower = SystemSetting.UserSetting.unitsSetting.settingPOWER;
        string utAirflow = SystemSetting.UserSetting.unitsSetting.settingAIRFLOW;

        DataRow datarow;
        #endregion


        #region View_Model_Property

        private List<RoomIndoor> _selectedRoomIndoors;

        public List<RoomIndoor> ListOfIndoorsToAdd
        {
            get { return _selectedRoomIndoors; }
            set { _selectedRoomIndoors = value; }
        }


        private FreshAirArea _currentFreshAirArea;

        public FreshAirArea CurrentFreshAirArea
        {
            get { return _currentFreshAirArea; }
            private set
            {
                SetValue<FreshAirArea>(ref _currentFreshAirArea, value);
            }
        }
      


        private JCHVRF.Model.Project _project;

        private string _autoSelectNotification;

        public string AutoSelectionMessage
        {
            get { return _autoSelectNotification; }
            private set {
                SetValue<string>(ref _autoSelectNotification, value);
            }
        }

        private RoomIndoor _selectedIndoor;

        public RoomIndoor SelectedIndoor
        {
            get { return _selectedIndoor; }
            set
            {
                SetValue<RoomIndoor>(ref _selectedIndoor, value);
            }
        }

       
        public string TempMasureUnit
        {
            get
            {
                return SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            }
        }

        private string _designConditionTempMasureUnite;
        public string DesignConditionTempMasureUnit
        {
            get { return _designConditionTempMasureUnite; }
            set { this.SetValue(ref _designConditionTempMasureUnite, value); }
        }


        private string _RelativeHumidityMasureUnit;
        public string RelativeHumidityMasureUnit
        {
            get { return _RelativeHumidityMasureUnit; }
            set { this.SetValue(ref _RelativeHumidityMasureUnit, value); }
        }

        public string CapacityMasureUnit
        {
            get
            {
                return SystemSetting.UserSetting.unitsSetting.settingPOWER;
            }
        }

        public string AreaMasureUnit
        {
            get
            {
                return SystemSetting.UserSetting.unitsSetting.settingAREA;
            }
        }

        public string AirFlowMasureUnit
        {
            get
            {
                return SystemSetting.UserSetting.unitsSetting.settingAIRFLOW;
            }
        }

        public string PressureMasureUnit
        {
            get
            {
                return SystemSetting.UserSetting.unitsSetting.settingPressure;
            }
        }

        private string _ChangeToFandC;
        public string ChangeToFandC
        {
            get { return _ChangeToFandC; }
            set { this.SetValue(ref _ChangeToFandC, value); }
        }

        private string _IndoorUnitName;
        [Required(ErrorMessage = "Please enter Indoor Unit Name")]
        public string IndoorUnitName
        {
            get { return _IndoorUnitName; }
            set { this.SetValue(ref _IndoorUnitName, value); }
        }



        private string _IndoorError;
        public string IndoorError
        {
            get { return _IndoorError==null ? string.Empty:_IndoorError; }
            set { this.SetValue(ref _IndoorError, value); }
        }


        private ObservableCollection<Room> _ListRoom;
        public ObservableCollection<Room> ListRoom
        {
            get
            {
                return _ListRoom;
            }
            set
            {
                this.SetValue(ref _ListRoom, value);
            }
        }

        private Room _SelectedRoom;
        public Room SelectedRoom
        {
            get { return _SelectedRoom; }
            set
            {

                this.SetValue(ref _SelectedRoom, value);
                CurrentFreshAirArea = _project.FreshAirAreaList.FirstOrDefault(i => i.Id == _SelectedRoom.FreshAirAreaId);
                BindCapacityRequirements();
            }
        }


        private ObservableCollection<JCHVRF.Model.Floor> _ListFloor;
        public ObservableCollection<JCHVRF.Model.Floor> ListFloor
        {
            get
            {
                return _ListFloor;
            }
            set
            {
                this.SetValue(ref _ListFloor, value);
            }
        }

        private string _SelectedFloor;
        public string SelectedFloor
        {
            get { return _SelectedFloor; }
            set { this.SetValue(ref _SelectedFloor, value); }
        }

        private string _IduImagePath;
        public string IduImagePath
        {
            get { return _IduImagePath; }
            set { this.SetValue(ref _IduImagePath, value); }
        }

        public List<string> IduPosition
        {
            get
            {
                return Enum.GetNames(typeof(PipingPositionType)).ToList();
            }
        }


        private string _SelectedIduPosition;
        public string SelectedIduPosition
        {
            get { return _SelectedIduPosition; }
            set { this.SetValue(ref _SelectedIduPosition, value); }
        }



        private double? _HeightDifference;
        public double? HeightDifference
        {
            get
            {
                return _HeightDifference;
            }
            set
            {
                this.SetValue(ref _HeightDifference, value);
            }
        }

        private string _HeightDifferenceError;
        public string HeightDifferenceError
        {
            get
            {
                return _HeightDifferenceError == null ? string.Empty : _HeightDifferenceError;
            }            
            set
            {
                this.SetValue(ref _HeightDifferenceError, value);
            }
        }



        public List<string> FanSpeed
        {
            get
            {
                return Enum.GetNames(typeof(FanSpeed)).ToList(); ;
            }

        }


        private string _SelectedFanSpeed;
        public string SelectedFanSpeed
        {
            get { return _SelectedFanSpeed; }
            set
            {
                this.SetValue(ref _SelectedFanSpeed, value); BatchCalculateAirFlow();
            }
        }


        private bool _ManualSelection;
        public bool ManualSelection
        {
            get { return _ManualSelection; }
            set
            {
                this.SetValue(ref _ManualSelection, value);
                SetCapacityRequirementVisibility();
                DoAutoSelect();
            }
        }

        private Visibility _CapacityRequirementsVisibility;
        public Visibility CapacityRequirementsVisibility
        {
            get { return _CapacityRequirementsVisibility; }
            set
            {
                this.SetValue(ref _CapacityRequirementsVisibility, value);

            }
        }


        private Visibility _ESPVisibility;
        public Visibility ESPVisibility
        {
            get { return _ESPVisibility; }
            set
            {
                this.SetValue(ref _ESPVisibility, value);

            }
        }

        private TemperatureUnit _ChangeToUnit;
        public TemperatureUnit ChangeToUnit
        {
            get { return _ChangeToUnit; }
            set { this.SetValue(ref _ChangeToUnit, value); }
        }



        private bool _UseRoomTemperature;
        public bool UseRoomTemperature
        {
            get { return _UseRoomTemperature; }
            set
            {
                this.SetValue(ref _UseRoomTemperature, value);
                BindInternalDesignConditions();
            }
        }

        private List<ComboBox> _ListUnitType;
        public List<ComboBox> ListUnitType
        {
            get
            {
                return _ListUnitType;
            }
            set
            {
                this.SetValue(ref _ListUnitType, value);
            }
        }

        public void GetRoomList()
        {
            CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            ListRoom = new ObservableCollection<Room>(JCHVRF.Model.Project.GetProjectInstance.FloorList[0].RoomList);
        }

        private string _SelectedUnitType;
        public string SelectedUnitType
        {
            get { return _SelectedUnitType; }
            set
            {
                this.SetValue(ref _SelectedUnitType, value);
                if (_SelectedUnitType != null)
                {
                    BindIDuModelType();
                    BindESPVisibility();
                    DoAutoSelect();

                }

            }
        }



        private string _UnitTypeError;
        public string UnitTypeError
        {
            get { return _UnitTypeError==null ? string.Empty : _UnitTypeError; }
            set
            {
                this.SetValue(ref _UnitTypeError, value);
            }
        }

        private ObservableCollection<ComboBox> _ListModel;
        public ObservableCollection<ComboBox> ListModel
        {
            get
            {
                return _ListModel;
            }
            set
            {
                this.SetValue(ref _ListModel, value);
            }
        }


        private string _ListModelError;
        public string ListModelError
        {
            get { return _ListModelError == null ? string.Empty : _ListModelError; }
            set
            {
                this.SetValue(ref _ListModelError, value);
            }
        }


        private string _SelectedModel;
        public string SelectedModel
        {
            get { return _SelectedModel; }
            set
            {
                this.SetValue(ref _SelectedModel, value);
                if (_SelectedModel != null)
                {
                    BindIndoorImageToUI();
                    BatchCalculateEstValue();
                    BatchCalculateAirFlow();
                }
            }
        }

        #region Internal_Design_Conditions_Property
        private double? _CoolingDryBulb;
        public double? CoolingDryBulb
        {
            get
            {
                return _CoolingDryBulb;
            }
            set
            {
                this.SetValue(ref _CoolingDryBulb, value);
                if (CoolingDryBulb != null)
                {
                    // DoCalculateByOption(UnitTemperature.DB.ToString());
                    BatchCalculateEstValue();
                }
            }
        }

        private string _CoolingDryBulbError;
        public string CoolingDryBulbError
        {
            get { return _CoolingDryBulbError == null ? string.Empty : _CoolingDryBulbError; }
            set
            {
                this.SetValue(ref _CoolingDryBulbError, value);
            }
        }


        private double? _CoolingWetBulb;
        public double? CoolingWetBulb
        {
            get
            {
                return _CoolingWetBulb;
            }
            set
            {
                this.SetValue(ref _CoolingWetBulb, value);
                if (CoolingWetBulb != null)
                {
                    BatchCalculateEstValue();
                }
            }
        }

        private string _CoolingWetBulbError;
        public string CoolingWetBulbError
        {
            get { return _CoolingWetBulbError == null ? string.Empty : _CoolingWetBulbError; }
            set
            {
                this.SetValue(ref _CoolingWetBulbError, value);
            }
        }

        private double? _HeatingDryBulb;
        public double? HeatingDryBulb
        {
            get
            {
                return _HeatingDryBulb;
            }
            set
            {
                this.SetValue(ref _HeatingDryBulb, value);
                if (HeatingDryBulb != null)
                {
                    BatchCalculateEstValue();
                }
            }
        }

        private string _HeatingDryBulbError;
        public string HeatingDryBulbError
        {
            get { return _HeatingDryBulbError == null ? string.Empty : _HeatingDryBulbError; }
            set
            {
                this.SetValue(ref _HeatingDryBulbError, value);
            }
        }

        private double? _RelativeHumidity;
        public double? RelativeHumidity
        {
            get
            {
                return _RelativeHumidity;
            }
            set
            {
                this.SetValue(ref _RelativeHumidity, value);
            }
        }

        private string _RelativeHumidityError;
        public string RelativeHumidityError
        {
            get { return _RelativeHumidityError == null ? string.Empty : _RelativeHumidityError; }
            set
            {
                this.SetValue(ref _RelativeHumidityError, value);
            }
        }

        #endregion

        #region Capacity_Requirements_Property
        private double? _CR_TotalCapacity;
        public double? CR_TotalCapacity
        {
            get
            {
                return _CR_TotalCapacity;
            }
            set
            {
                this.SetValue(ref _CR_TotalCapacity, value);
                //DoAutoSelect();
            }
        }


        private string _CR_TotalCapacityError;
        public string CR_TotalCapacityError
        {
            get { return _CR_TotalCapacityError== null ? string.Empty : _CR_TotalCapacityError; }
            set
            {
                this.SetValue(ref _CR_TotalCapacityError, value);
            }
        }


        private double? _CR_SensibleCapacity;
        public double? CR_SensibleCapacity
        {
            get
            {
                return _CR_SensibleCapacity;
            }
            set
            {
                this.SetValue(ref _CR_SensibleCapacity, value);
                //DoAutoSelect();
            }
        }


        private string _CR_SensibleCapacityError;
        public string CR_SensibleCapacityError
        {
            get { return _CR_SensibleCapacityError == null ? string.Empty : _CR_SensibleCapacityError; }
            set
            {
                this.SetValue(ref _CR_SensibleCapacityError, value);
            }
        }


        private double? _CR_HeatingCapacity;
        public double? CR_HeatingCapacity
        {
            get
            {
                return _CR_HeatingCapacity;
            }
            set
            {
                this.SetValue(ref _CR_HeatingCapacity, value);
            }
        }

        private string _CR_HeatingCapacityError;
        public string CR_HeatingCapacityError
        {
            get { return _CR_HeatingCapacityError == null ? string.Empty : _CR_HeatingCapacityError; }
            set
            {
                this.SetValue(ref _CR_HeatingCapacityError, value);
            }
        }

        private double? _CR_FreshAir;
        public double? CR_FreshAir
        {
            get
            {
                return _CR_FreshAir;
            }
            set
            {
                this.SetValue(ref _CR_FreshAir, value);
            }
        }

        private string _CR_FreshAirError;
        public string CR_FreshAirError
        {
            get { return _CR_FreshAirError == null ? string.Empty : _CR_FreshAirError; }
            set
            {
                this.SetValue(ref _CR_FreshAirError, value);
            }
        }


        private double? _CR_AirFlow;
        public double? CR_AirFlow
        {
            get
            {
                return _CR_AirFlow;
            }
            set
            {
                this.SetValue(ref _CR_AirFlow, value);
            }
        }

        private string _CR_AirFlowError;
        public string CR_AirFlowError
        {
            get { return _CR_AirFlowError == null ? string.Empty : _CR_AirFlowError; }
            set
            {
                this.SetValue(ref _CR_AirFlowError, value);
            }
        }

        private double? _CR_ESP;
        public double? CR_ESP
        {
            get
            {
                return _CR_ESP;
            }
            set
            {
                this.SetValue(ref _CR_ESP, value);
            }
        }


        private string _CR_ESPError;
        public string CR_ESPError
        {
            get { return _CR_ESPError== null ? string.Empty : _CR_ESPError; }
            set
            {
                this.SetValue(ref _CR_ESPError, value);
            }
        }
        #endregion



        #region Selected_Capacity_Property
        private string _SR_TotalCapacity;
        public string SR_TotalCapacity
        {
            get
            {
                return _SR_TotalCapacity;
            }
            set
            {
                this.SetValue(ref _SR_TotalCapacity, value);
            }
        }

        private string _SR_SensibleCapacity;
        public string SR_SensibleCapacity
        {
            get
            {
                return _SR_SensibleCapacity;
            }
            set
            {
                this.SetValue(ref _SR_SensibleCapacity, value);
            }
        }

        private string _SR_HeatingCapacity;
        public string SR_HeatingCapacity
        {
            get
            {
                return _SR_HeatingCapacity;
            }
            set
            {
                this.SetValue(ref _SR_HeatingCapacity, value);
            }
        }


        private string _SR_FreshAir;
        public string SR_FreshAir
        {
            get
            {
                return _SR_FreshAir;
            }
            set
            {
                this.SetValue(ref _SR_FreshAir, value);
            }
        }


        private string _SR_AirFlow;
        public string SR_AirFlow
        {
            get
            {
                return _SR_AirFlow;
            }
            set
            {
                this.SetValue(ref _SR_AirFlow, value);
            }
        }


        private string _SR_ESP;
        public string SR_ESP
        {
            get
            {
                return _SR_ESP;
            }
            set
            {
                this.SetValue(ref _SR_ESP, value);
            }
        }

        #endregion


        private ObservableCollection<RoomIndoor> _ListRoomIndoor;
        public ObservableCollection<RoomIndoor> ListRoomIndoor
        {
            get
            {
                return _ListRoomIndoor;
            }
            set
            {
                this.SetValue(ref _ListRoomIndoor, value);
            }
        }

        #endregion

        #region DelegateCommand
        public DelegateCommand OnTargetUpdate{ get; set; }
        public DelegateCommand OpenAddRoomWindowCommand { get; set; }
        public DelegateCommand OpenAddFloorWindowCommand { get; set; }
        public DelegateCommand AddAllCommand { get; set; }

        public DelegateCommand ChangeDesignTempCommand { get; set; }
        JCHVRF.Model.Project CurrentProject;

        public DelegateCommand AddToListCommand { get; set; }

        public DelegateCommand ResetCommand { get; set; }
        public DelegateCommand<IClosable> CancelCommand { get; set; }

        #endregion

        #region Ctor
        public AddIndoorUnitViewModel(IEventAggregator EventAggregator)
        {
            _isLoading = true;
            this.ESPVisibility = Visibility.Visible;
            AutoSelectionMessage = string.Empty;
            _eventAggregator = EventAggregator;
            BindDefaultFanSpeed();
            this.IndoorUnitName = SystemSetting.UserSetting.defaultSetting.IndoorName + GetIndoorCount();
            this.DesignConditionTempMasureUnit = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            this.RelativeHumidityMasureUnit = "%";
            this.ManualSelection = true;
            this.UseRoomTemperature = false;
            this.HeightDifference = 0;
            this.CR_TotalCapacity = 0;
            this.CR_SensibleCapacity = 0;
            this.CR_HeatingCapacity = 0;
            this.CR_AirFlow = 0;
            this.CR_FreshAir = 0;
            this.CR_ESP = 0;
            BindOutdoorProperty();
            BindFloorList();
            GetRoomList();
            this.CapacityRequirementsVisibility = Visibility.Visible;          
            if (this.DesignConditionTempMasureUnit == Unit.ut_Temperature_c)
            {
                this.ChangeToFandC = "Change To °F";
            }
            else
            {
                this.ChangeToFandC = "Change To °C";
            }
            OpenAddRoomWindowCommand = new DelegateCommand(OpenAddRoomWindowOnClick);
            OpenAddFloorWindowCommand = new DelegateCommand(OpenAddFloorWindowOnClick);
            OnTargetUpdate = new DelegateCommand(DoAutoSelect);
            AddAllCommand = new DelegateCommand(AddAllIndoorUnitOnClick);
            AddToListCommand = new DelegateCommand(AddIndoorToListOnClick);
            ChangeDesignTempCommand = new DelegateCommand(ChangeDesignTempOnClick);
            ResetCommand = new DelegateCommand(ResetCommandOnClick);
            CancelCommand = new DelegateCommand<IClosable>(CancelCommandOnClick);
            _eventAggregator.GetEvent<FloorListSaveSubscriber>().Subscribe(OpenGetFloorList);
            _eventAggregator.GetEvent<RoomListSaveSubscriber>().Subscribe(OpenGetRoomList);
            _project = JCHVRF.Model.Project.CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            bll = new IndoorBLL(JCHVRF.Model.Project.GetProjectInstance.SubRegionCode, JCHVRF.Model.Project.GetProjectInstance.BrandCode);

            BindIDUPosition();
            BindIDuUnitType();
            BindIndoorImageToUI();
            BindInternalDesignConditions();
            BatchCalculateEstValue();
            BatchCalculateAirFlow();
           

        }
        #endregion
        private void OpenGetFloorList()
        {
            BindFloorList();
        }
        private void OpenGetRoomList()
        {
            GetRoomList();
        }

        #region PropertyChangeEvent       
        private void SetCapacityRequirementVisibility()
        {
            if (this.ManualSelection == true)
            {
                this.CapacityRequirementsVisibility = Visibility.Collapsed;
            }
            else
            {
                this.CapacityRequirementsVisibility = Visibility.Visible;
            }
        }
        #endregion

        #region BindingData

        private void BindESPVisibility()
        {
            if(this.SelectedUnitType!=null)
            {
                if (this.SelectedUnitType.ToLower().Contains("ducted"))
                    this.ESPVisibility = Visibility.Visible;
                else
                    this.ESPVisibility = Visibility.Hidden;
            }

        }
        private void DoCalculateByOption(string Opt)
        {
            try
            {
                if (this.CoolingDryBulb != null && this.CoolingWetBulb != null && this.RelativeHumidity != null)
                {
                    double dbcool = Unit.ConvertToSource(Convert.ToDouble(this.CoolingDryBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                    double wbcool = Unit.ConvertToSource(Convert.ToDouble(this.CoolingWetBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                    double rhcool = Convert.ToDouble(this.RelativeHumidity);

                    FormulaCalculate fc = new FormulaCalculate();
                    decimal pressure = fc.GetPressure(Convert.ToDecimal(JCHVRF.Model.Project.GetProjectInstance.Altitude));
                    if (Opt == UnitTemperature.WB.ToString())
                    {
                        double rh = Convert.ToDouble(fc.GetRH(Convert.ToDecimal(dbcool), Convert.ToDecimal(wbcool), pressure));
                        this.RelativeHumidity = (rh * 100);
                    }
                    else if (Opt == UnitTemperature.DB.ToString())
                    {
                        double wb = Convert.ToDouble(fc.GetWTByDT(Convert.ToDecimal(dbcool), Convert.ToDecimal(rhcool / 100), pressure));
                        if (rhcool != 0)
                        {
                            this.CoolingWetBulb = wb;
                        }
                    }
                    else if (Opt == UnitTemperature.RH.ToString())
                    {
                        double wb = Convert.ToDouble(fc.GetWTByDT(Convert.ToDecimal(dbcool), Convert.ToDecimal(rhcool / 100), pressure));
                        if (rhcool != 0)
                        {
                            this.CoolingWetBulb = wb;
                        }
                    }
                }
            }
            catch { }
        }

        private void BindCapacityRequirements()
        {
            if (this.SelectedRoom != null)
            {
                this.CR_TotalCapacity = Unit.ConvertToControl(Convert.ToDouble(SelectedRoom.CoolingDryBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                this.CR_SensibleCapacity = Unit.ConvertToControl(Convert.ToDouble(SelectedRoom.CoolingWetBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                this.CR_HeatingCapacity = Unit.ConvertToControl(Convert.ToDouble(SelectedRoom.HeatingDryBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                this.CR_AirFlow = Unit.ConvertToControl(SelectedRoom.AirFlow, UnitType.AIRFLOW, utAirflow);
                this.CR_FreshAir = Unit.ConvertToControl(SelectedRoom.FreshAir, UnitType.AIRFLOW, utAirflow);
                this.CR_ESP = Convert.ToDouble(SelectedRoom.StaticPressure);
            }
        }

        private void BindInternalDesignConditions()
        {
            if (this.UseRoomTemperature == false)
            {
                var InternalDesignConditions = JCHVRF.Model.Project.GetProjectInstance.DesignCondition;
                this.CoolingDryBulb = Unit.ConvertToControl(Convert.ToDouble(InternalDesignConditions.indoorCoolingDB), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                this.CoolingWetBulb = Unit.ConvertToControl(Convert.ToDouble(InternalDesignConditions.indoorCoolingWB), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                this.HeatingDryBulb = Unit.ConvertToControl(Convert.ToDouble(InternalDesignConditions.outdoorHeatingDB), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                this.RelativeHumidity = Convert.ToDouble(InternalDesignConditions.indoorCoolingRH);
            }
            else
            {
                if (SelectedRoom != null)
                {
                    this.CoolingDryBulb = Unit.ConvertToControl(Convert.ToDouble(SelectedRoom.CoolingDryBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                    this.CoolingWetBulb = Unit.ConvertToControl(Convert.ToDouble(SelectedRoom.CoolingWetBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                    this.HeatingDryBulb = Unit.ConvertToControl(Convert.ToDouble(SelectedRoom.HeatingDryBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
                    this.RelativeHumidity = Convert.ToDouble(SelectedRoom.CoolingRelativeHumidity);
                }
            }
        }

        private void BindFloorList()
        {
            this.ListFloor = new ObservableCollection<JCHVRF.Model.Floor>(JCHVRF.Model.Project.GetProjectInstance.FloorList);
            if (this.SelectedFloor == null)
            {
                this.SelectedFloor = this.ListFloor.FirstOrDefault().Id;
            }
        }

        private void BindDefaultFanSpeed()
        {
            this.SelectedFanSpeed = JCHVRF_New.Model.FanSpeed.Max.ToString();
        }

        private void BindIDUPosition()
        {
            this.SelectedIduPosition = PipingPositionType.SameLevel.ToString();
        }

        private void BindIDuUnitType()
        {
            ListUnitType = new List<ComboBox>();
            if (ProjectBLL.IsSupportedUniversalSelection(JCHVRF.Model.Project.GetProjectInstance))
            {
                var IndoorTypeList = bll.GetIndoorDisplayName();
                foreach (DataRow rowView in IndoorTypeList.Rows)
                {
                    ListUnitType.Add(new ComboBox { DisplayName = Convert.ToString(rowView["Display_Name"]), Value = Convert.ToString(rowView["Display_Name"]) });
                }
                if (ListUnitType.Count > 0)
                    this.SelectedUnitType = ListUnitType.FirstOrDefault().DisplayName;
            }
            else
            {
                if (JCHVRF.Model.Project.GetProjectInstance == null || string.IsNullOrEmpty(JCHVRF.Model.Project.GetProjectInstance.SubRegionCode) || string.IsNullOrEmpty(ODUProductType))
                    return;
                string colName = "UnitType";
                DataTable dt = bll.GetIndoorFacCodeList(ODUProductType);
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToInt32(dr["FactoryCount"].ToString()) > 1)
                    {
                        switch (dr["FactoryCode"].ToString())
                        {
                            case "G":
                                dr[colName] += "-GZF";
                                break;
                            case "E":
                                dr[colName] += "-HAPE";
                                break;
                            case "Q":
                                dr[colName] += "-HAPQ";
                                break;
                            case "B":
                                dr[colName] += "-HAPB";
                                break;
                            case "I":
                                dr[colName] += "-HHLI";
                                break;
                            case "M":
                                dr[colName] += "-HAPM";
                                break;
                            case "S":
                                dr[colName] += "-SMZ";
                                break;
                        }
                    }
                }
                var dv = new DataView(dt);
                if (ODUProductType == "Comm. Tier 2, HP")
                {

                    if (ODUSeries == "Commercial VRF HP, FSN6Q" || ODUSeries == "Commercial VRF HP, JVOH-Q")
                    {
                        dv.RowFilter = "UnitType not in ('High Static Ducted-HAPE','Medium Static Ducted-HAPE','Low Static Ducted-HAPE','High Static Ducted-SMZ','Medium Static Ducted-SMZ','Four Way Cassette-SMZ')";
                    }
                    else
                    {
                        dv.RowFilter = "UnitType <>'Four Way Cassette-HAPQ'";
                    }
                }
                foreach (DataRowView rowView in dv)
                {
                    ListUnitType.Add(new ComboBox { DisplayName = Convert.ToString(rowView.Row["UnitType"]), Value = Convert.ToString(rowView.Row["FactoryCode"]) });
                }
                if (ListUnitType.Count > 0)
                    this.SelectedUnitType = ListUnitType.FirstOrDefault().DisplayName;
            }

        }
        private void UpdateUnitType()
        {
            int i = this._SelectedUnitType.IndexOf("-");
            if (i > 0)
            {
                _factory = this._SelectedUnitType.Substring(i + 1, this._SelectedUnitType.Length - i - 1);
                _type = this._SelectedUnitType.Substring(0, i);
            }
            else
            {
                _factory = "";
                _type = this._SelectedUnitType;
            }
        }
        private void UpdateUnitTypeUniversal()
        {
            _factory = bll.GetFCodeByDisUnitType(this._SelectedUnitType, out _typeList);
        }
        private async void BindIDuModelType()
        {
            if (ProjectBLL.IsSupportedUniversalSelection(JCHVRF.Model.Project.GetProjectInstance))
            {
                UpdateUnitTypeUniversal();
                IDuModelTypeList = bll.GetUniversalIndoorListStd(this._SelectedUnitType, _factory, _typeList);
                IDuModelTypeList.DefaultView.Sort = "CoolCapacity";
                await Task.Run(() => BindCompatibleIndoorUniversal());
            }
            else
            {
                UpdateUnitType();
                IDuModelTypeList = bll.GetIndoorListStd(_type, ODUProductType, _factory);
                IDuModelTypeList.DefaultView.Sort = "CoolCapacity";
                await Task.Run(() => BindCompatibleIndoorSimple());
            }
        }
        private void BindCompatibleIndoorSimple()
        {
            try
            {
                IDUIndoorList = new List<Indoor>();
                List<ComboBox> ListModelTemp = new List<ComboBox>();
                ListModel = new ObservableCollection<ComboBox>();
                foreach (DataRow drIduModel in IDuModelTypeList.Rows)
                {
                    var Indoor = bll.GetItem(drIduModel["ModelFull"].ToString(), _type, ODUProductType, ODUSeries);
                    IDUIndoorList.Add(Indoor);
                    if (JCHVRF.Model.Project.GetProjectInstance.BrandCode == "Y")
                        ListModelTemp.Add(new ComboBox { DisplayName = Convert.ToString(drIduModel["Model_York"]), Value = Convert.ToString(drIduModel["ModelFull"]) });
                    else
                        ListModelTemp.Add(new ComboBox { DisplayName = Convert.ToString(drIduModel["Model_Hitachi"]), Value = Convert.ToString(drIduModel["ModelFull"]) });

                }
                if (ListModelTemp != null && ListModelTemp.Count > 0)
                {
                    ListModel = new ObservableCollection<ComboBox>(ListModelTemp);
                    this.SelectedModel = ListModel.FirstOrDefault().Value;
                    DoAutoSelect();
                }
            }
            catch (Exception ex) {
                Debug.WriteLine(ex);
            }
        }
        private void BindCompatibleIndoorUniversal()
        {
            try
            {
                ListModel = new ObservableCollection<ComboBox>();
                List<ComboBox> ListModelTemp = new List<ComboBox>();
                // ODUSeries = "Commercial VRF HP, FSXNSE";
                IDUIndoorList = new List<Indoor>();
                MyProductTypeBLL productTypeBll = new MyProductTypeBLL();
                DataTable typeDt = productTypeBll.GetIduTypeBySeries(JCHVRF.Model.Project.GetProjectInstance.BrandCode, JCHVRF.Model.Project.GetProjectInstance.SubRegionCode, ODUSeries);
                foreach (DataRow drIduModel in IDuModelTypeList.Rows)
                {
                    var Indoor = bll.GetItem(drIduModel["ModelFull"].ToString(), _type, ODUProductTypeUniversal, ODUSeries);
                    IDUIndoorList.Add(Indoor);
                    if (typeDt != null)
                        foreach (DataRow dr in typeDt.Rows)
                        {
                            if (Indoor.Type == dr["IDU_UnitType"].ToString())
                            {
                                var modelHitachi = dr["IDU_Model_Hitachi"].ToString();
                                if (string.IsNullOrEmpty(modelHitachi) || modelHitachi.Contains(";" + Convert.ToString(drIduModel["Model_Hitachi"]) + ";"))
                                {
                                    if (JCHVRF.Model.Project.GetProjectInstance.BrandCode == "Y")
                                        ListModelTemp.Add(new ComboBox { DisplayName = Convert.ToString(drIduModel["Model_York"]), Value = Convert.ToString(drIduModel["ModelFull"]) });
                                    else
                                        ListModelTemp.Add(new ComboBox { DisplayName = Convert.ToString(drIduModel["Model_Hitachi"]), Value = Convert.ToString(drIduModel["ModelFull"]) });
                                }
                                break;
                            }
                        }
                }
                if (ListModelTemp != null && ListModelTemp.Count > 0)
                {
                    ListModel = new ObservableCollection<ComboBox>(ListModelTemp);
                    this.SelectedModel = ListModel.FirstOrDefault().Value;
                    DoAutoSelect();
                }
            }
            catch (Exception ex) {
                Debug.WriteLine(ex);
            }

        }

        private void BindIndoorImageToUI()
        {
            var sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            if (SelectedModel != null)
            {
                DataRow dr = IDuModelTypeList.AsEnumerable().FirstOrDefault(r => r.Field<string>("ModelFull") == SelectedModel);
                if (dr != null)
                {
                    this.IduImagePath = sourceDir + "\\" + Convert.ToString(dr["TypeImage"]);
                }
            }
        }

        private bool ValidateEstCapacity(double est, string partLoadTableName, IndoorType flag)
        {
            bool res = false;
            if (est == 0)
            {
                //if (flag != IndoorType.Exchanger)
                //    JCMsg.ShowWarningOK(Msg.DB_NOTABLE_CAP + "[" + partLoadTableName + "]\nRegion:" + JCHVRF.Model.Project.GetProjectInstance.SubRegionCode + ";ProductType:" + _productType + "");
            }
            else if (est == -1)
                JCMsg.ShowWarningOK(Msg.WARNING_DATA_EXCEED);
            else
                res = true;
            return res;
        }

        void BindOutdoorProperty()
        {
            var CurrentSys = JCHVRF.Model.Project.GetProjectInstance.SystemListNextGen.FirstOrDefault(sys => sys.IsActiveSystem == true);
            if (CurrentSys != null)
            {
                outdoorCoolingDB = CurrentSys.DBCooling;
                outdoorHeatingWB = CurrentSys.WBHeating;
                outdoorCoolingIW = CurrentSys.IWCooling;
                outdoorHeatingIW = CurrentSys.IWHeating;

                var outdooritem = CurrentSys.OutdoorItem;
                if (outdooritem != null)
                {
                    ODUSeries = outdooritem.Series;
                    ODUProductType = outdooritem.ProductType;
                }
            }

        }

        private void BatchCalculateEstValue()
        {
            double wb_c, db_h;
            var FanSpeed = (int)Enum.Parse(typeof(JCHVRF_New.Model.FanSpeed), this.SelectedFanSpeed);
            var InternalDesignConditions = JCHVRF.Model.Project.GetProjectInstance.DesignCondition;
            wb_c = Unit.ConvertToSource(Convert.ToDouble(this.CoolingWetBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
            db_h = Unit.ConvertToSource(Convert.ToDouble(this.HeatingDryBulb), UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit);
            if (this.SelectedModel != null)
            {
                datarow = IDuModelTypeList.AsEnumerable().FirstOrDefault(r => r.Field<string>("ModelFull") == this.SelectedModel);
                if (datarow != null)
                {
                    Indoor inItem = IDUIndoorList.FirstOrDefault(m => m.ModelFull == this.SelectedModel);
                    if (inItem != null)
                    {
                        double est_cool = 0;
                        double est_heat = 0;
                        double est_sh = 0;
                        string type = inItem.Type;
                        if (type.Contains("YDCF") || type.Contains("Fresh Air") || type.Contains("Ventilation"))
                        {
                            est_cool = inItem.CoolingCapacity;
                            est_heat = inItem.HeatingCapacity;
                            est_sh = inItem.SensibleHeat;
                        }
                        else if (type.Contains("Hydro Free") || type == "DX-Interface")
                        {
                            est_cool = inItem.CoolingCapacity;
                            est_heat = inItem.HeatingCapacity;
                            est_sh = 0d;
                        }
                        else
                        {

                            double db_c = inItem.ProductType.Contains("Water Source") ? outdoorCoolingIW : outdoorCoolingDB;
                            double wb_h = inItem.ProductType.Contains("Water Source") ? outdoorHeatingIW : outdoorHeatingWB;


                            est_cool = bll.CalIndoorEstCapacity(inItem, db_c, wb_c, false);
                            if (!ValidateEstCapacity(est_cool, inItem.PartLoadTableName, inItem.Flag))
                                return;

                            double shf = inItem.GetSHF(FanSpeed);
                            est_sh = est_cool * shf;

                            if (!inItem.ProductType.Contains(", CO"))
                            {

                                est_heat = bll.CalIndoorEstCapacity(inItem, wb_h, db_h, true);
                                if (!ValidateEstCapacity(est_heat, inItem.PartLoadTableName, inItem.Flag))
                                    return;
                            }
                        }

                        this.SR_TotalCapacity = Unit.ConvertToControl(est_cool, UnitType.POWER, utPower).ToString("n1");
                        this.SR_HeatingCapacity = Unit.ConvertToControl(est_heat, UnitType.POWER, utPower).ToString("n1");
                        this.SR_SensibleCapacity = Unit.ConvertToControl(est_sh, UnitType.POWER, utPower).ToString("n1");
                    }
                }
            }

        }

        private void BatchCalculateAirFlow()
        {
            Indoor inItem;
            var FanSpeed = (int)Enum.Parse(typeof(JCHVRF_New.Model.FanSpeed), this.SelectedFanSpeed);
            if (this.SelectedModel != null)
            {
                datarow = IDuModelTypeList.AsEnumerable().FirstOrDefault(r => r.Field<string>("ModelFull") == this.SelectedModel);
                if (datarow != null)
                {
                    inItem = IDUIndoorList.FirstOrDefault(m => m.ModelFull == this.SelectedModel);
                    if (inItem != null)
                    {
                        double fa = 0;
                        double airflow = 0;
                        double staticPressure = 0;
                        string type = inItem.Type;
                        if (IndoorBLL.IsFreshAirUnit(type))
                        {
                            fa = inItem.AirFlow;
                        }
                        else if (!type.Contains("Hydro Free") && type != "DX-Interface")
                        {
                            airflow = inItem.GetAirFlow(FanSpeed);
                            if (type.Contains("Ducted") || type.Contains("Total Heat Exchanger"))
                            {
                                staticPressure = inItem.GetStaticPressure();
                            }
                        }

                        this.SR_AirFlow = Unit.ConvertToControl(airflow, UnitType.AIRFLOW, utAirflow).ToString("n0");
                        this.SR_ESP = staticPressure.ToString("n0");
                        this.SR_FreshAir = Unit.ConvertToControl(fa, UnitType.AIRFLOW, utAirflow).ToString("n0");

                    }
                }
            }

        }

        private bool IsValid()
        {
            float DBCoolJCMinValue = float.Parse(Unit.ConvertToControl(16, UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit).ToString("n1"));
            float DBCoolJCMaxValue = float.Parse(Unit.ConvertToControl(30, UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit).ToString("n1"));
            float WBCoolJCMinValue = float.Parse(Unit.ConvertToControl(14, UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit).ToString("n1"));
            float WBCoolJCMaxValue = float.Parse(Unit.ConvertToControl(24, UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit).ToString("n1"));
            float DBHeatJCMinValue = float.Parse(Unit.ConvertToControl(16, UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit).ToString("n1"));
            float DBHeatJCMaxValue = float.Parse(Unit.ConvertToControl(24, UnitType.TEMPERATURE, this.DesignConditionTempMasureUnit).ToString("n1"));
            float RHJCMinValue = float.Parse("13");
            float RHJCMaxValue = float.Parse("100");
            bool result = true;
            if (String.IsNullOrEmpty(this.IndoorUnitName))
            {
                this.IndoorError = "Required *";
                result = false;
            }
            else this.IndoorError = string.Empty;

            if (this.HeightDifference == null)
            {
                this.HeightDifferenceError = "Required *";
                result = false;
            }
            else this.HeightDifferenceError = string.Empty;


            if (String.IsNullOrEmpty(this.SelectedUnitType))
            {
                this.UnitTypeError = "Required *";
                result = false;
            }
            else this.UnitTypeError = string.Empty;

            if (String.IsNullOrEmpty(this.SelectedModel))
            {
                this.ListModelError = "Required *";
                result = false;
            }
            else this.ListModelError = string.Empty;


            if (this.CR_TotalCapacity == null)
            {
                this.CR_TotalCapacityError = "Required *";
                result = false;
            }
            else this.CR_TotalCapacityError = string.Empty;

            if (this.CR_SensibleCapacity == null)
            {
                this.CR_SensibleCapacityError = "Required *";
                result = false;
            }
            else this.CR_SensibleCapacityError = string.Empty;

            if (this.CR_HeatingCapacity == null)
            {
                this.CR_HeatingCapacityError = "Required *";
                result = false;
            }
            else this.CR_HeatingCapacityError = string.Empty;

            if (this.CR_AirFlow == null)
            {
                this.CR_AirFlowError = "Required *";
                result = false;
            }
            else this.CR_AirFlowError = string.Empty;

            if (this.CR_FreshAir == null)
            {
                this.CR_FreshAirError = "Required *";
                result = false;
            }
            else this.CR_FreshAirError = string.Empty;

            if (this.CR_ESP == null)
            {
                this.CR_ESPError = "Required *";
                result = false;
            }
            else this.CR_ESPError = string.Empty;


            if (this.CoolingDryBulb == null)
            {
                this.CoolingDryBulbError = "Required * ";
                result = false;
            }
            else if (this.CoolingDryBulb < DBCoolJCMinValue || this.CoolingDryBulb > DBCoolJCMaxValue)
            {
                this.CoolingDryBulbError = "Range[" + Convert.ToString(DBCoolJCMinValue) + "," + Convert.ToString(DBCoolJCMaxValue) + "] *";
                result = false;
            }
            else this.CoolingDryBulbError = string.Empty;


            if (this.CoolingWetBulb == null)
            {
                this.CoolingWetBulbError = "Required * ";
                result = false;
            }
            else if (this.CoolingWetBulb < WBCoolJCMinValue || this.CoolingWetBulb > WBCoolJCMaxValue)
            {
                this.CoolingWetBulbError = "Range[" + Convert.ToString(WBCoolJCMinValue) + "," + Convert.ToString(WBCoolJCMaxValue) + "] *"; //"Range [14,24] *";
                result = false;
            }
            else this.CoolingWetBulbError = string.Empty;


            if (this.HeatingDryBulb == null)
            {
                this.HeatingDryBulbError = "Required *";
                result = false;
            }
            else if (this.HeatingDryBulb < DBHeatJCMinValue || this.HeatingDryBulb > DBHeatJCMaxValue)
            {
                this.HeatingDryBulbError = "Range[" + Convert.ToString(DBHeatJCMinValue) + ", " + Convert.ToString(DBHeatJCMaxValue) + "] * ";//"Range[16,24] *";
                result = false;
            }
            else this.HeatingDryBulbError = string.Empty;


            if (this.RelativeHumidity == null)
            {
                this.RelativeHumidityError = "Required *";
                result = false;
            }
            else if (this.RelativeHumidity < RHJCMinValue || this.RelativeHumidity > RHJCMaxValue)
            {
                this.RelativeHumidityError = "Range[" + Convert.ToString(RHJCMinValue) + "," + Convert.ToString(RHJCMaxValue) + "] *"; // "Range[13,100] *";
                result = false;
            }
            else this.RelativeHumidityError = string.Empty;



            return result;
        }

        #endregion

        #region ViewModelMethods
        private int GetIndoorCount()
        {
            int InNodeCount = 0;
            if (JCHVRF.Model.Project.GetProjectInstance.RoomIndoorList != null)
                foreach (RoomIndoor ri in JCHVRF.Model.Project.GetProjectInstance.RoomIndoorList)
                {
                    InNodeCount = InNodeCount > ri.IndoorNO ? InNodeCount : ri.IndoorNO;
                }
            if (ListRoomIndoor != null)
                foreach (RoomIndoor ri in ListRoomIndoor)
                {
                    InNodeCount = InNodeCount > ri.IndoorNO ? InNodeCount : ri.IndoorNO;
                }
            return InNodeCount + 1;
        }
        #endregion

        #region DelegateCommandEvents       

        private void ChangeDesignTempOnClick()
        {
            string currentTempUnit = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            if (this.DesignConditionTempMasureUnit == Unit.ut_Temperature_c)
            {
                this.CoolingDryBulb = Unit.ConvertToControl(this.CoolingDryBulb ?? 0, UnitType.TEMPERATURE, Unit.ut_Temperature_f);
                this.CoolingWetBulb = Unit.ConvertToControl(this.CoolingWetBulb ?? 0, UnitType.TEMPERATURE, Unit.ut_Temperature_f);
                this.HeatingDryBulb = Unit.ConvertToControl(this.HeatingDryBulb ?? 0, UnitType.TEMPERATURE, Unit.ut_Temperature_f);
                this.DesignConditionTempMasureUnit = Unit.ut_Temperature_f;
                this.ChangeToFandC = "Change To °C";
            }
            else
            {
                this.CoolingDryBulb = Unit.ConvertToSource(this.CoolingDryBulb ?? 0, UnitType.TEMPERATURE, Unit.ut_Temperature_f);
                this.CoolingWetBulb = Unit.ConvertToSource(this.CoolingWetBulb ?? 0, UnitType.TEMPERATURE, Unit.ut_Temperature_f);
                this.HeatingDryBulb = Unit.ConvertToSource(this.HeatingDryBulb ?? 0, UnitType.TEMPERATURE, Unit.ut_Temperature_f);
                this.DesignConditionTempMasureUnit = Unit.ut_Temperature_c;
                this.ChangeToFandC = "Change To °F";
            }
        }
        private void OpenAddRoomWindowOnClick()
        {
            AddEditRoom addEditRoomView = new AddEditRoom();
            addEditRoomView.ShowDialog();
        }

        private void CancelCommandOnClick(IClosable objClosable)
        {
            objClosable.RequestClose();
        }

        private void ResetCommandOnClick()
        {
            BindInternalDesignConditions();
        }
        private void OpenAddFloorWindowOnClick()
        {
            AddEditFloor win = new AddEditFloor();
            _eventAggregator.GetEvent<FloorButtonEnableSubscriber>().Publish(true);
            win.ShowDialog();
            _eventAggregator.GetEvent<FloorButtonEnableSubscriber>().Publish(false);
        }


        private void AddIndoorToListOnClick()
        {
            IsValid();
        }
        private void AddAllIndoorUnitOnClick()
        {
            try
            {
                ListRoomIndoor = new ObservableCollection<RoomIndoor>();
                RoomIndoor obj = new RoomIndoor();
                obj.IndoorName = IndoorUnitName;
                ListRoomIndoor.Add(obj);
                _eventAggregator.GetEvent<PubSubEvent<ObservableCollection<RoomIndoor>>>().Publish(ListRoomIndoor);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
        #endregion

        #region AutoSeletionMethods

        private void DoAutoSelect()
        {

            //if (_isLoading || _isAutoSelecting || IDUIndoorList == null) return;
            if (IDUIndoorList == null) return;

            _isAutoSelecting = true;

            if (!ManualSelection)
            {
                var isUniversal = ProjectBLL.IsSupportedUniversalSelection(JCHVRF.Model.Project.GetProjectInstance);
                // is universal 
                if (isUniversal)
                {
                    if (SelectedRoom != null)
                        DoAutoSelectUniversalWithRoom(IDUIndoorList, SelectedRoom, CurrentFreshAirArea, _SelectedUnitType);
                    else
                        DoAutoSelectUniversalWithoutRoom(IDUIndoorList, CR_TotalCapacity.GetValueOrDefault(), CR_HeatingCapacity.GetValueOrDefault(), CR_SensibleCapacity.GetValueOrDefault(), CR_AirFlow.GetValueOrDefault(), CR_ESP.GetValueOrDefault(), CR_FreshAir.GetValueOrDefault());
                }
                else
                {
                    if (SelectedRoom != null)
                        DoAutoSelectWithRoom(IDUIndoorList, SelectedRoom, CurrentFreshAirArea, ODUSeries);
                    else
                        DoAutoSelectWithoutRoom(_SelectedUnitType, ODUSeries, IDUIndoorList, CR_TotalCapacity.GetValueOrDefault(), CR_HeatingCapacity.GetValueOrDefault(), CR_SensibleCapacity.GetValueOrDefault(), CR_AirFlow.GetValueOrDefault(), CR_ESP.GetValueOrDefault(), CR_FreshAir.GetValueOrDefault());
                }
            }

            _isAutoSelecting = false;
        }

        /// <summary>
        /// Do auto select in case of univeral idu without room specification
        /// </summary>
        /// <param name="type"></param>
        /// <param name="productType"></param>
        /// <param name="srcIduList"></param>
        /// <param name="inputRqCapC"></param>
        /// <param name="intputRqCapH"></param>
        /// <param name="inputSensiCapC"></param>
        /// <param name="inputRqAirFlow"></param>
        /// <param name="inputRoomStaticPressure"></param>
        /// <param name="inputFA"></param>
        /// <param name="selectedIndoor"></param>
        private void DoAutoSelectUniversalWithoutRoom(List<Indoor> srcIduList, double? inputRqCapC, double? intputRqCapH,
            double? inputSensiCapC, double? inputRqAirFlow,
            double? inputRoomStaticPressure, double? inputFA, string productType = "Universal IDU")
        {
            AutoSelectionMessage = string.Empty;

            var rq_cool = Unit.ConvertToSource(Convert.ToDouble(inputRqCapC), UnitType.POWER, CapacityMasureUnit);
            var rq_heat = Unit.ConvertToSource(Convert.ToDouble(intputRqCapH), UnitType.POWER, CapacityMasureUnit);
            var rq_sensiable = Unit.ConvertToSource(Convert.ToDouble(inputSensiCapC), UnitType.POWER, CapacityMasureUnit);
            var rq_airflow = Unit.ConvertToSource(Convert.ToDouble(inputRqAirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);
            var rq_StaticPressure = Convert.ToDouble(inputRoomStaticPressure);
            var rq_fa = Unit.ConvertToSource(Convert.ToDouble(inputFA), UnitType.AIRFLOW, AirFlowMasureUnit);

            bool isPass = false;

            foreach (var r in srcIduList)
            {
                double std_cool = Unit.ConvertToSource(Convert.ToDouble(r.CoolingCapacity), UnitType.POWER, CapacityMasureUnit);
                double std_heat = Unit.ConvertToSource(Convert.ToDouble(r.HeatingCapacity), UnitType.POWER, CapacityMasureUnit);

                isPass = (rq_cool <= std_cool) && (rq_heat <= std_heat);

                if (!isPass || !AutoCompareUniversalWithoutRoom(r, rq_fa, rq_cool, rq_sensiable, rq_airflow, rq_StaticPressure, rq_heat))
                    continue;

                SelectedModel = r.ModelFull;

                return;
            }

        }

        /// <summary>
        ///  Do autoselect in case of universal idu with room specification
        /// </summary>
        /// <param name="srcIduList"></param>
        /// <param name="room"></param>
        /// <param name="freshAirArea"></param>
        /// <param name="curSelectType"></param>
        private void DoAutoSelectUniversalWithRoom(List<Indoor> srcIduList, Room room, FreshAirArea freshAirArea,
            string curSelectType)
        {
            bool isOK = false;
            foreach (var stdRow in srcIduList)
            {
                bool isFreshAir = IndoorBLL.IsFreshAirUnit(_type);
                bool isPass = true;

                // unit conversion              
                double std_cool = Unit.ConvertToSource(Convert.ToDouble(stdRow.CoolingCapacity), JCBase.Utility.UnitType.POWER, CapacityMasureUnit);
                double std_heat = Unit.ConvertToSource(Convert.ToDouble(stdRow.HeatingCapacity), JCBase.Utility.UnitType.POWER, CapacityMasureUnit);

                if (room != null)
                {
                    if (_project.IsCoolingModeEffective && std_cool < room.RqCapacityCool)
                        isPass = false;

                    if (curSelectType == "Hydro Free-High temp.") isPass = true;

                    if (_project.IsHeatingModeEffective && std_heat < room.RqCapacityHeat)
                        isPass = false;

                    if (!isPass || !AutoCompareUniversalWithRoom(isFreshAir, stdRow, room) && curSelectType != "Hydro Free-High temp.")
                        continue;
                    else
                        isOK = true;
                }
                else if (freshAirArea != null)
                {
                    if (!isPass || !AutoCompareFreshAirArea(stdRow, freshAirArea))
                        continue;
                    else
                        isOK = true;
                }

                SelectedModel = stdRow.ModelFull;
                break;
            }
        }

        /// <summary>
        /// Do auto select in case of non universal idu without room
        /// </summary>
        /// <param name="type"></param>
        /// <param name="oduSeries"></param>
        /// <param name="srcIduList"></param>
        /// <param name="inputRqCapC"></param>
        /// <param name="intputRqCapH"></param>
        /// <param name="inputSensiCapC"></param>
        /// <param name="inputRqAirFlow"></param>
        /// <param name="inputRoomStaticPressure"></param>
        /// <param name="inputFA"></param>
        private void DoAutoSelectWithoutRoom(string type, string oduSeries, List<Indoor> srcIduList, double? inputRqCapC,
            double? intputRqCapH, double? inputSensiCapC, double? inputRqAirFlow, double? inputRoomStaticPressure, double? inputFA)
        {
            if (string.IsNullOrEmpty(type))
                return;

            // do input conversion
            var rq_cool = Unit.ConvertToSource(Convert.ToDouble(inputRqCapC), UnitType.POWER, CapacityMasureUnit);
            var rq_heat = Unit.ConvertToSource(Convert.ToDouble(intputRqCapH), UnitType.POWER, CapacityMasureUnit);
            var rq_sensiable = Unit.ConvertToSource(Convert.ToDouble(inputSensiCapC), UnitType.POWER, CapacityMasureUnit);
            var rq_airflow = Unit.ConvertToSource(Convert.ToDouble(inputRqAirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);
            var rq_StaticPressure = Convert.ToDouble(inputRoomStaticPressure);
            var rq_fa = Unit.ConvertToSource(Convert.ToDouble(inputFA), UnitType.AIRFLOW, AirFlowMasureUnit);

            bool pass = false;


            foreach (var r in srcIduList)
            {
                double std_cool = Unit.ConvertToSource(Convert.ToDouble(r.CoolingCapacity), UnitType.POWER, CapacityMasureUnit);
                double std_heat = Unit.ConvertToSource(Convert.ToDouble(r.HeatingCapacity), UnitType.POWER, CapacityMasureUnit);

                pass = (rq_cool <= std_cool) && (rq_heat <= std_heat);

                if (!pass || !AutoCompareWithoutRoom(type, oduSeries, r, rq_fa, rq_cool, rq_sensiable, rq_airflow, rq_StaticPressure, rq_heat))
                    continue;

                SelectedModel = r.ModelFull;

                return;
            }

        }

        /// <summary>
        /// Do auto select as per room / fresh air area specific requirements and non univeral IDUs
        /// </summary>
        /// <param name="srcIduList"></param>
        /// <param name="room"></param>
        /// <param name="freshAirArea"></param>
        /// <param name="oduSeries"></param>
        private void DoAutoSelectWithRoom(List<Indoor> srcIduList, Room room, FreshAirArea freshAirArea, string oduSeries)
        {
            var isOK = false;
            var pass = true;

            if (srcIduList == null || srcIduList.Count == 0)
                return;

            foreach (var idu in srcIduList)
            {

                var isFreshAir = IndoorBLL.IsFreshAirUnit(idu.Type);
                double stdCool = Unit.ConvertToSource(Convert.ToDouble(idu.CoolingCapacity), JCBase.Utility.UnitType.POWER, CapacityMasureUnit);
                double stdHeat = Unit.ConvertToSource(Convert.ToDouble(idu.HeatingCapacity), JCBase.Utility.UnitType.POWER, CapacityMasureUnit);

                if (room != null)
                {
                    if (_project.IsCoolingModeEffective && stdCool < room.RqCapacityCool)
                    {
                        pass = false;
                    }

                    if (_project.IsHeatingModeEffective && !oduSeries.Contains(", CO") && stdHeat < room.RqCapacityHeat)
                        pass = false;

                    if (!pass || !AutoCompare(isFreshAir, idu, room, oduSeries))
                        continue;
                    else
                        isOK = true;
                }
                else if (freshAirArea != null)
                {
                    if (!pass || !AutoCompareFreshAirArea(idu, freshAirArea))
                        continue;
                    else
                        isOK = true;
                }

                if (isOK) SelectedModel = idu.ModelFull;

                break;
            }

            return;
        }

        /// <summary>
        /// Compare requirements and return true if finds a match
        /// </summary>
        /// <param name="isFreshAir"></param>
        /// <param name="stdRow"></param>
        /// <param name="room"></param>
        /// <param name="productType"></param>
        /// <returns></returns>
        private bool AutoCompare(bool isFreshAir, Indoor stdRow, Room room, string productType)
        {
            if (stdRow == null) return false;
            bool pass = true;

            double est_cool = Unit.ConvertToSource(Convert.ToDouble(stdRow.CoolingCapacity), UnitType.POWER, CapacityMasureUnit);
            double est_heat = Unit.ConvertToSource(Convert.ToDouble(stdRow.CoolingCapacity), UnitType.POWER, CapacityMasureUnit);
            double est_sh = Unit.ConvertToSource(Convert.ToDouble(stdRow.SensibleHeat), UnitType.POWER, CapacityMasureUnit);
            double airflow = Unit.ConvertToSource(Convert.ToDouble(stdRow.AirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);
            double staticPressure = Convert.ToDouble(stdRow.GetStaticPressure());

            if (isFreshAir)
            {
                airflow = Unit.ConvertToSource(Convert.ToDouble(stdRow.AirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);
                // Compare estimated capacity to current demand
                if (airflow < room.FreshAir)
                    pass = false;
            }
            else
            {
                if (_project.IsCoolingModeEffective)
                {
                    if (est_cool < room.RqCapacityCool || est_sh < room.SensibleHeat || airflow < room.AirFlow || staticPressure < room.StaticPressure)
                        pass = false;
                }

                if (_project.IsHeatingModeEffective && !productType.Contains(", CO"))
                {
                    if (est_heat < room.RqCapacityHeat)
                        pass = false;
                }
            }
            return pass;
        }

        /// <summary>
        /// Compare for fresh air requirement and return true in case finds a match
        /// </summary>
        /// <param name="stdRow"></param>
        /// <param name="area"></param>
        /// <param name="utAirflow"></param>
        /// <returns></returns>
        private bool AutoCompareFreshAirArea(Indoor stdRow, FreshAirArea area)
        {
            bool pass = true;

            //DoCalculateEstValue(stdRow);

            double airflow = Unit.ConvertToSource(Convert.ToDouble(stdRow.AirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);

            if (airflow < area.FreshAir)
                pass = false;

            return pass;
        }

        private bool AutoCompareWithoutRoom(string type, string productType, Indoor r,
            double rq_fa, double rq_cool, double rq_sensiable, double rq_airflow, double rq_StaticPressure, double rq_heat)
        {
            bool pass = true;

            bool isFreshAir = type.Contains("YDCF") || type.Contains("Fresh Air") || type.Contains("Ventilation");


            double est_cool = Unit.ConvertToSource(Convert.ToDouble(r.CoolingCapacity), UnitType.POWER, CapacityMasureUnit);
            double est_heat = Unit.ConvertToSource(Convert.ToDouble(r.HeatingCapacity), UnitType.POWER, CapacityMasureUnit);
            double est_sh = Unit.ConvertToSource(Convert.ToDouble(r.SensibleHeat), UnitType.POWER, CapacityMasureUnit);
            double airflow = Unit.ConvertToSource(Convert.ToDouble(r.AirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);
            double staticPressure = r.GetStaticPressure();

            if (isFreshAir)
            {
                airflow = Unit.ConvertToSource(Convert.ToDouble(r.AirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);
                if (airflow < rq_fa)
                    pass = false;
            }
            else
            {
                if (_project.IsCoolingModeEffective)
                {
                    if (est_cool < rq_cool || est_sh < rq_sensiable || airflow < rq_airflow || staticPressure < rq_StaticPressure)
                        pass = false;
                }

                if (_project.IsHeatingModeEffective && !productType.Contains(", CO"))
                {
                    if (est_heat < rq_heat)
                        pass = false;
                }
            }

            return pass;
        }

        /// <summary>
        /// Compare compatible indoor units univerally
        /// </summary>
        /// <param name="isFreshAir"></param>
        /// <param name="stdRow"></param>
        /// <param name="room"></param>
        /// <returns></returns>
        private bool AutoCompareUniversalWithRoom(bool isFreshAir, Indoor stdRow, Room room)
        {
            bool pass = true;

            double est_cool = Unit.ConvertToSource(Convert.ToDouble(stdRow.CoolingCapacity), UnitType.POWER, CapacityMasureUnit);
            double est_heat = Unit.ConvertToSource(Convert.ToDouble(stdRow.HeatingCapacity), UnitType.POWER, CapacityMasureUnit);
            double est_sh = Unit.ConvertToSource(Convert.ToDouble(stdRow.SensibleHeat), UnitType.POWER, CapacityMasureUnit);
            double airflow = Unit.ConvertToSource(Convert.ToDouble(stdRow.AirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);
            double staticPressure = Convert.ToDouble(stdRow.GetStaticPressure());

            if (isFreshAir)
            {
                airflow = Unit.ConvertToSource(Convert.ToDouble(stdRow.AirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);

                if (airflow < room.FreshAir)
                    pass = false;
            }
            else
            {
                if (_project.IsCoolingModeEffective)
                {
                    if (est_cool < room.RqCapacityCool || est_sh < room.SensibleHeat || airflow < room.AirFlow || staticPressure < room.StaticPressure)
                        pass = false;
                }

                if (_project.IsHeatingModeEffective)
                {
                    if (est_heat < room.RqCapacityHeat)
                        pass = false;
                }
            }
            return pass;
        }

        private bool AutoCompareUniversalWithoutRoom(Indoor r,
            double rq_fa, double rq_cool, double rq_sensiable, double rq_airflow, double rq_StaticPressure, double rq_heat,
            string productType = "Universal IDU")
        {
            bool pass = true;

            //var modelfull = r.ModelFull;
            //var inItem = r.IndoorItem as Indoor;
            //if (inItem == null) return false;

            var type = r.Type;

            bool isFreshAir = type.Contains("YDCF") || type.Contains("Fresh Air") || type.Contains("Ventilation");
            double est_cool = Unit.ConvertToSource(Convert.ToDouble(r.CoolingCapacity), UnitType.POWER, CapacityMasureUnit);
            double est_heat = Unit.ConvertToSource(Convert.ToDouble(r.HeatingCapacity), UnitType.POWER, CapacityMasureUnit);
            double est_sh = Unit.ConvertToSource(Convert.ToDouble(r.SensibleHeat), UnitType.POWER, CapacityMasureUnit);
            double airflow = Unit.ConvertToSource(Convert.ToDouble(r.AirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);
            double staticPressure = Convert.ToDouble(r.GetStaticPressure());

            if (isFreshAir)
            {
                airflow = Unit.ConvertToSource(Convert.ToDouble(r.AirFlow), UnitType.AIRFLOW, AirFlowMasureUnit);
                if (airflow < rq_fa)
                    pass = false;
            }
            else
            {
                if (_project.IsCoolingModeEffective)
                {
                    if (est_cool < rq_cool || est_sh < rq_sensiable || airflow < rq_airflow || staticPressure < rq_StaticPressure)
                        pass = false;
                }
                if (_project.IsHeatingModeEffective && !productType.Contains(", CO"))
                {
                    if (est_heat < rq_heat)
                        pass = false;
                }
            }

            return pass;
        }

        #endregion

        //private void AddSelectedIndoor()
        //{
        //    //has selected row
        //    //find if the selected indoor has matching producttype(ODU type)
        //    //add selected indoor to the selected list 'addToSelectedRow'
        //    //do calculateselectedsumcapacity(optional)
        //    //DoValidateCapacity (optional in case of autoselection)
        //}
    }
}
