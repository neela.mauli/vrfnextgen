﻿/****************************** File Header ******************************\
File Name:	CanvasPropertiesViewModel.cs
Date Created:	2/25/2019
Description:	
\*************************************************************************/

namespace JCHVRF_New.ViewModels
{
    using JCHVRF_New.Common.Contracts;
    using JCHVRF_New.Common.Helpers;
    using Prism.Events;

    class CanvasPropertiesViewModel : ViewModelBase, ILayoutItem
    {
        #region Fields

        private double _FloatingHeight;

        private double _FloatingLeft;

        private double _FloatingTop;

        private double _FloatingWidth;

        private string _title;

        IEventAggregator _eventAggregator;

        #endregion

        #region Constructors

        public CanvasPropertiesViewModel(IEventAggregator EventAggregator)
        {
            _eventAggregator = EventAggregator;
            Title = "Properties";
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the FloatingHeight
        /// </summary>
        public double FloatingHeight
        {
            get { return this._FloatingHeight; }
            set { this.SetValue(ref _FloatingHeight, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingLeft
        /// </summary>
        public double FloatingLeft
        {
            get { return this._FloatingLeft; }
            set { this.SetValue(ref _FloatingLeft, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingTop
        /// </summary>
        public double FloatingTop
        {
            get { return this._FloatingTop; }
            set { this.SetValue(ref _FloatingTop, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingWidth
        /// </summary>
        public double FloatingWidth
        {
            get { return this._FloatingWidth; }
            set { this.SetValue(ref _FloatingWidth, value); }
        }

        /// <summary>
        /// Gets or sets the Title
        /// </summary>
        public string Title
        {
            get { return this._title; }
            set { this.SetValue(ref _title, value); }
        }

        #endregion
    }
}
