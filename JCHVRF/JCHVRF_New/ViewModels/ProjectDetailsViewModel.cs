﻿/****************************** File Header ******************************\
File Name:	ProjectDetailsViewModel.cs
Date Created:	2/20/2019
Description:	
\*************************************************************************/

namespace JCHVRF_New.ViewModels
{
    using FontAwesome.WPF;
    using JCHVRF_New.Common.Contracts;
    using JCHVRF_New.Common.Helpers;
    using JCHVRF_New.Model;
    using System.Collections.ObjectModel;

    public class ProjectDetailsViewModel : ViewModelBase, ILayoutItem
    {
        #region Fields

        private double _FloatingHeight;

        private double _FloatingLeft;

        private double _FloatingTop;

        private double _FloatingWidth;

        private LeftSideBarItem _selectedLeftSideBarItem;

        private string _title;

        #endregion

        #region Constructors

        public ProjectDetailsViewModel()
        {
            Title = "Project Details";

            string systemName = JCHVRF.Model.Project.CurrentProject.SystemName;
            string sytemType = JCHVRF.Model.Project.CurrentProject.SystemListNextGen[0].SystemTypeNexGen.ToString();

            LeftSideBarItems = new ObservableCollection<LeftSideBarItem>()
            {

                new LeftSideBarItem(sytemType)
                {
                    Children = new ObservableCollection<LeftSideBarChild>()
                    {
                        new LeftSideBarChild(systemName,sytemType,FontAwesomeIcon.Circle),
                        new LeftSideBarChild(systemName,sytemType,FontAwesomeIcon.DotCircleOutline),
                        new LeftSideBarChild(systemName,sytemType,FontAwesomeIcon.CircleOutlineNotch),
                        new LeftSideBarChild(systemName,sytemType,FontAwesomeIcon.InfoCircle)
                    }
                },
               /* new LeftSideBarItem("Central Controller")
                {
                    Children=new ObservableCollection<LeftSideBarChild>()
                    {
                        new LeftSideBarChild("Controller System 01","Central Controller",FontAwesomeIcon.StopCircleOutline),
                        new LeftSideBarChild("Controller System 02","Central Controller",FontAwesomeIcon.UserCircle)

                    }
                },
                 new LeftSideBarItem("Heat Exchangers")
                {
                    Children=new ObservableCollection<LeftSideBarChild>()
                    {
                        new LeftSideBarChild("Heat Exchanger 01","Heat Exchangers",FontAwesomeIcon.StopCircleOutline),
                        new LeftSideBarChild("Heat Exchanger 02","Heat Exchangers",FontAwesomeIcon.UserCircle)

                    }
                },
                  new LeftSideBarItem("Air Handling Unit")
                {
                    Children=new ObservableCollection<LeftSideBarChild>()
                    {
                        new LeftSideBarChild("AHU System 01","Air Handling Unit",FontAwesomeIcon.StopCircleOutline),
                        new LeftSideBarChild("AHU System 02","Air Handling Unit",FontAwesomeIcon.UserCircle)

                    }
                },
                   new LeftSideBarItem("Package Air Conditioning")
                {
                    Children=new ObservableCollection<LeftSideBarChild>()
                    {
                        new LeftSideBarChild("PAC System 01","Package Air Conditioning",FontAwesomeIcon.StopCircleOutline),
                        new LeftSideBarChild("PAC System 02","Package Air Conditioning",FontAwesomeIcon.UserCircle)

                    }
                },
                    new LeftSideBarItem("Refridgeration Air Conditioning")
                {
                    Children=new ObservableCollection<LeftSideBarChild>()
                    {
                        new LeftSideBarChild("RAC System 01","Refridgeration Air Conditioning",FontAwesomeIcon.StopCircleOutline),
                        new LeftSideBarChild("RAC System 02","Refridgeration Air Conditioning",FontAwesomeIcon.UserCircle)

                    }
                }*/
            };
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the FloatingHeight
        /// </summary>
        public double FloatingHeight
        {
            get { return this._FloatingHeight; }
            set { this.SetValue(ref _FloatingHeight, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingLeft
        /// </summary>
        public double FloatingLeft
        {
            get { return this._FloatingLeft; }
            set { this.SetValue(ref _FloatingLeft, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingTop
        /// </summary>
        public double FloatingTop
        {
            get { return this._FloatingTop; }
            set { this.SetValue(ref _FloatingTop, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingWidth
        /// </summary>
        public double FloatingWidth
        {
            get { return this._FloatingWidth; }
            set { this.SetValue(ref _FloatingWidth, value); }
        }

        /// <summary>
        /// Gets the LeftSideBarItems
        /// </summary>
        public ObservableCollection<LeftSideBarItem> LeftSideBarItems { get; }

        /// <summary>
        /// Gets or sets the SelectedLeftSideBarItem
        /// </summary>
        public LeftSideBarItem SelectedLeftSideBarItem
        {
            get { return _selectedLeftSideBarItem; }
            set
            {
                this.SetValue(ref _selectedLeftSideBarItem, value);
            }
        }

        /// <summary>
        /// Gets or sets the Title
        /// </summary>
        public string Title
        {
            get { return this._title; }
            set { this.SetValue(ref _title, value); }
        }

        #endregion
    }
}
