﻿using JCHVRF.BLL;
using JCHVRF.BLL.New;
using JCHVRF.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using JCHVRF_New.Model;
using Prism.Commands;
using System.Windows;
using Prism.Regions;
using JCHVRF_New.Common.Constants;

namespace JCHVRF_New.ViewModels
{
    public class ProjectTemplateViewModel
    {
        Random rnd = new Random();
        private IRegionManager _regionManager;

        public ObservableCollection<Project> Projects { get; set; }
        public DelegateCommand<object> EditCommand { get; set; }
        public string SearchType { get; set; }

        public ProjectTemplateViewModel(IRegionManager regionManager)
        {
            //_regionManager = regionManager;
            //LoadProjects();
            // AddAccessoryViewModel obj= new AddAccessoryViewModel();

            EditCommand = new DelegateCommand<object>((projectID) =>
            {
                //Button senderButton = sender as Button;
                //var item = senderButton.DataContext;
                //var projectID = ((Project)item).ProjectID;
                int projectId = (int)projectID;
                Application.Current.Properties["ProjectId"] = projectId;
                ProjectInfoBLL bll = new ProjectInfoBLL();

                JCHVRF.Entity.ProjectInfo projectNextGen = bll.GetProjectInfo(projectId);                JCHVRF.Model.Project.CurrentProject = projectNextGen.ProjectLegacy;
                //projectNextGen.ProjectLegacy.RegionCode = "EU_W";
                //projectNextGen.ProjectLegacy.SubRegionCode = "GBR";
                //projectNextGen.ProjectLegacy.projectID = projectId;

                NavigationParameters param = new NavigationParameters();
                param.Add("Project", projectNextGen.ProjectLegacy);

                regionManager.RequestNavigate(RegionNames.ContentRegion, "MasterDesigner", param);
                //var winMain = new MasterDesigner(projectNextGen.ProjectLegacy);
                //winMain.ShowDialog();
            });

            //SearchType = "all";
           // LoadProjects("All");


        }


        public void LoadProjects(string searchType)
        {
            /// TODO : Check weather region is available or Not, need to notify to do location settings. 
            //if (string.IsNullOrWhiteSpace(JCHVRF.Model.SystemSetting.UserSetting.locationSetting.region))
            //{
            //    return;
            //}
            //else
            //{
                string region = JCHVRF.Model.SystemSetting.UserSetting.locationSetting.region;
                string subRegion = JCHVRF.Model.SystemSetting.UserSetting.locationSetting.subRegion;
                ObservableCollection<Project> projects = new ObservableCollection<Project> { };
                List<JCHVRF.Entity.ProjectInfo> prjList = new List<JCHVRF.Entity.ProjectInfo>();
                ProjectInfoBLL bll = new ProjectInfoBLL();
                prjList = bll.GetAllProjectsRegionWise(searchType, region, subRegion);
                foreach (var item in prjList)
                {
                    projects.Add(new Project
                    {
                        CreatedDate = DateTime.Today,
                        DeliveryDate = item.DeliveryDate,
                        LastModifiedBy = "UserName",
                        ModifiedDate = " : " + item.LastUpdateDate.Year.ToString() + "/" + item.LastUpdateDate.Month.ToString() + "/" + item.LastUpdateDate.Day.ToString(),
                        ProjectID = item.ProjectID,
                        RemainingDays = " days remaining until due date", // Added on 30-11-2018 for split days and remain text
                        RemainingDaysInNos = Convert.ToString((item.DeliveryDate - DateTime.UtcNow).Days),  //  Added on 30-11-2018 for split days and remain text
                        ProjectStatusPer = rnd.Next(1, 100), //Added on 30-11-2018 for perc
                        ProjectName = item.ProjectName
                        //RemainingDays = (item.DeliveryDate - DateTime.UtcNow).Days + " days remaining until due date"

                    });

                }
                Projects = projects;
           // }
        }
        
    }
}
