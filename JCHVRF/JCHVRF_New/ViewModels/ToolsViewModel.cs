﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JCHVRF_New.Common.Helpers;
using System.Threading.Tasks;
using JCHVRF_New.Model;
using System.Collections.ObjectModel;

namespace JCHVRF_New.ViewModels
{
   public class ToolsViewModel: ViewModelBase
    {
        private ObservableCollection<ToolsModel> _toolCollection;

        public ObservableCollection<ToolsModel> ToolCollection
        {
            get
            {
                if (_toolCollection == null)
                    _toolCollection = new ObservableCollection<ToolsModel>();
                return _toolCollection;
            }
            set { this.SetValue(ref _toolCollection, value); }
        }

        public ToolsViewModel()
        {
            ToolCollection.Add(new ToolsModel() {

                Icon = FontAwesome.WPF.FontAwesomeIcon.SnowflakeOutline,
                Header = "Heat Load Calculator",
                Content= "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."
            });

            ToolCollection.Add(new ToolsModel()
            {
                Icon = FontAwesome.WPF.FontAwesomeIcon.Bolt,
                Header = "Consumption Calculator",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."
            });

            ToolCollection.Add(new ToolsModel()
            {
                Icon = FontAwesome.WPF.FontAwesomeIcon.Th,
                Header = "Controller Simulator",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."
            });
            ToolCollection.Add(new ToolsModel()
            {
                Icon = FontAwesome.WPF.FontAwesomeIcon.LineChart,
                Header = "Peak Load Calculator",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."
            });

            ToolCollection.Add(new ToolsModel()
            {
                Icon = FontAwesome.WPF.FontAwesomeIcon.SnowflakeOutline,
                Header = "Temperature Simulator",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."
            });

            ToolCollection.Add(new ToolsModel()
            {
                Icon = FontAwesome.WPF.FontAwesomeIcon.Download,
                Header = "Download Templates",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."
            });
            ToolCollection.Add(new ToolsModel()
            {
                Icon = FontAwesome.WPF.FontAwesomeIcon.Globe,
                Header = "Emissions Calculator",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."

            });

            ToolCollection.Add(new ToolsModel()
            {
                Icon = FontAwesome.WPF.FontAwesomeIcon.FileText,
                Header = "Site Audit Report",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."
            });

            ToolCollection.Add(new ToolsModel()
            {
                Icon = FontAwesome.WPF.FontAwesomeIcon.FileZipOutline,
                Header = "Design Standards Library",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."
            });

            ToolCollection.Add(new ToolsModel()
            {
                Icon = FontAwesome.WPF.FontAwesomeIcon.FileText,
                Header = "Product Comparisions",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum reet."

            });

           
        }
    }
}
