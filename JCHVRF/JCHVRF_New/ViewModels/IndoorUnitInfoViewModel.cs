﻿using JCHVRF.Model;
using JCHVRF_New.Common.Helpers;
using JCHVRF_New.Model;
using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace JCHVRF_New.ViewModels
{

    public class IndoorUnitInfoViewModel : ViewModelBase
    {
        #region Privates
        private JCHVRF.Model.Project _project;
        private bool _isCheckAll;
        private IEventAggregator _eventAggregator;
        #endregion

        public bool IsCheckAll
        {
            get { return _isCheckAll; }
            set
            {
                _isCheckAll = value;
                RaisePropertyChanged("IsCheckAll");
            }
        }

        private bool _canRemove;

        public bool CanRemove
        {
            get { return (bool)SelectedIDU?.Any(i => i.IsChecked); }
        }

        public string Settings { get; set; }

        public DelegateCommand RemoveCommand { get; set; }
        public DelegateCommand<IDU> SelectCommand { get; set; }
        public DelegateCommand SelectAllCommand { get; set; }

        private ObservableCollection<IDU> _selectedIDU;
        public ObservableCollection<IDU> SelectedIDU
        {
            get
            {
                return _selectedIDU;
            }
            set
            {
                _selectedIDU = value;
                RaisePropertyChanged("SelectedIDU");
            }
        }

        public IndoorUnitInfoViewModel(IEventAggregator EventAggregator)
        {
            _project = JCHVRF.Model.Project.GetProjectInstance;

            _eventAggregator = EventAggregator;

            _eventAggregator.GetEvent<PubSubEvent<ObservableCollection<RoomIndoor>>>().Subscribe(BindAllTheList);

            SelectedIDU = new ObservableCollection<IDU>();

            RemoveCommand = new DelegateCommand(OnRemove,
                () => { return CanRemove; });

            SelectCommand = new DelegateCommand<IDU>(OnSelect,
                i => SelectedIDU != null && SelectedIDU.Count > 0);

            SelectAllCommand = new DelegateCommand(OnSelectAll,
               () =>
               {
                   return _project != null && _project.RoomIndoorList?.Count > 0;
               });


        }

        private void BindAllTheList(ObservableCollection<RoomIndoor> item)
        {
            UpdateProject(item);

            Refresh();
           
        }

        /// <summary>
        /// Add all IDU to the legacy project object and do the validation
        /// </summary>
        private void UpdateProject(ObservableCollection<RoomIndoor> item)
        {
            Debug.WriteLine("Update project called");

            if (_project == null)
                _project = JCHVRF.Model.Project.GetProjectInstance;

            // TODO : confirm if this logic needs to be there, remove if not
            //foreach (var ri in item)
            //{
            //    foreach (RoomIndoor d in _project.RoomIndoorList)
            //    {
            //        if (ri.IndoorName == d.IndoorName)
            //        {
            //            if (ri.IndoorNO != d.IndoorNO)
            //            {
            //                _project.RoomIndoorList.Remove(ri);
            //            }
            //        }
            //    }
            //}

            _project.RoomIndoorList.AddRange(item.ToList());

        }

        private void RemoveIdu(IEnumerable<RoomIndoor> rmList)
        {
            if (rmList == null) return;
            Debug.WriteLine("RemoveIdu called");

            if (_project.RoomIndoorList != null)
            {
                foreach (var rind in rmList)
                {
                    var rInd = _project.RoomIndoorList.FirstOrDefault(i => i.Equals(rmList));
                    if (rInd != null)
                        rInd.IsDelete = true;
                }
            }
            IsCheckAll = false;
            Refresh();
        }

        private void OnRemove()
        {
            var toRemove = SelectedIDU.Where(i => i.IsChecked).Select(i => i.RoomIndoor);
            RemoveIdu(toRemove);
            RemoveCommand.RaiseCanExecuteChanged();
        }

        private void OnSelect(IDU obj)
        {
            IsCheckAll = SelectedIDU
                .Count(i => i.IsChecked) == SelectedIDU.Count;

            RemoveCommand.RaiseCanExecuteChanged();
        }

        private void OnSelectAll()
        {
            foreach (var ri in _project.RoomIndoorList) {

                ri.IsDelete = IsCheckAll;
            }
            Refresh();
            RemoveCommand.RaiseCanExecuteChanged();
        }

        private void Refresh()
        {
            SelectedIDU = new ObservableCollection<IDU>(
                    JCHVRF.Model.Project.GetProjectInstance.RoomIndoorList.Where(r => !r.IsDelete)
                        .Select(i => new IDU(i))
                );

            SelectAllCommand.RaiseCanExecuteChanged();

        }
    }
}
