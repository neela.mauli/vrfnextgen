﻿/****************************** File Header ******************************\
File Name:	MasterDesignerViewModel.cs
Date Created:	2/15/2019
Description:	View Model for the MasterDesigner.
\*************************************************************************/

namespace JCHVRF_New.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using JCHVRF_New.Common.Constants;
    using JCHVRF_New.Common.Helpers;
    using Prism.Commands;
    using Prism.Regions;

    public class MasterDesignerViewModel : ViewModelBase
    {
        #region Fields

        private JCHVRF.Model.Project _project;
        IRegionManager regionManager;
        public readonly string projectPath = "C:\\JCH_VSTS";
        #endregion

        #region Constructors

        public MasterDesignerViewModel(IRegionManager regionManager)
        {
            this.regionManager = regionManager;
            LoadedCommand = new DelegateCommand(
               () =>
               {

                   regionManager.RequestNavigate(RegionNames.MasterDesignerLeftArea, "ProjectDetails");
                   regionManager.RequestNavigate(RegionNames.MasterDesignerLeftArea, "SystemDetails");
                   regionManager.RequestNavigate(RegionNames.MasterDesignerRightTopArea, "PipingInfo");
                   regionManager.RequestNavigate(RegionNames.MasterDesignerRightTopArea, "CanvasProperties");
                  
                   regionManager.RequestNavigate(RegionNames.MasterDesignerRightBottomArea, "ErrorLog");
                   regionManager.RequestNavigate(RegionNames.MasterDesignerRightBottomArea, "Navigator");
               }
               );

            NewClickCommand = new DelegateCommand(OnNewProjectClicked);
            CloseClickCommand = new DelegateCommand(OnCloseProjectClicked);
            OpenRecentClickCommand = new DelegateCommand(OnOpenRecentClicked); 

            //system menu submenu commands
            NewSystemCommand = new DelegateCommand(OnNewSystemClicked);

            //Property change commands
            //Added by Isteyak -7/3/2019
            PropertyCommand = new DelegateCommand(OnPropertyClicked);

            RecentProjectFilesData.Add("1");
            RecentProjectFilesData.Add("2");
            RecentProjectFilesData.Add("3");
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the LoadedCommand
        /// </summary>
        public DelegateCommand LoadedCommand { get; set; }

        /// <summary>
        /// Gets or sets the Project
        /// </summary>
        public JCHVRF.Model.Project Project
        {
            get { return _project; }
            set { this.SetValue(ref _project, value); }
        }

        public DelegateCommand CloseClickCommand { get; set; }

        public DelegateCommand NewClickCommand { get; set; }

        public DelegateCommand NewSystemCommand { get; set; }

        public DelegateCommand PropertyCommand { get; set; }

        public DelegateCommand OpenRecentClickCommand { get; set; }

        public string recentProjectPaths { get; set; }

        private ObservableCollection<string> _recentProjectFiles;

        public ObservableCollection<string> RecentProjectFilesData
        {
            get
            {
                if (_recentProjectFiles == null)
                    _recentProjectFiles = new ObservableCollection<string>();
                return _recentProjectFiles;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The OnNavigatedTo
        /// </summary>
        /// <param name="navigationContext">The navigationContext<see cref="NavigationContext"/></param>
        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            if (navigationContext.Parameters.ContainsKey("Project"))
                Project = navigationContext.Parameters["Project"] as JCHVRF.Model.Project;
        }

        /// <summary>
        /// The OnNewProjectClicked
        /// </summary>

        private void OnNewProjectClicked()
        {

            //MessageBox.Show("Would you like to save changes in the current project?", "Alert", MessageBoxButton.OKCancel);
            //if (MessageBox.Result == DialogResult.Ok)
            //    //we have to use the Save functionlity to save the current project details in db. Development in-progress;
            //else if (MessageBox.Result == DialogResult.Cancel)
            //    close the current project on that tab;

            var createProjectWizard = new Views.CreateProjectWizard();
            createProjectWizard.ShowDialog();

        }


        private void OnNewSystemClicked()
        {

            //MessageBox.Show("Would you like to save changes in the current project?", "Alert", MessageBoxButton.OKCancel);
            //if (MessageBox.Result == DialogResult.Ok)
            //    //we have to use the Save functionlity to save the current project details in db. Development in-progress;
            //else if (MessageBox.Result == DialogResult.Cancel)
            //    close the current project on that tab;

            var createSystemWizard = new Views.CreateSystemWizard();
            createSystemWizard.ShowDialog();

        }

        private void OnPropertyClicked()
        {
            var SystemPropWiz = new Views.SystemPropertyWizard();
            SystemPropWiz.ShowDialog();

        }

        /// <summary>
        /// The OnCloseProjectClicked
        /// </summary>

        private void OnCloseProjectClicked()
        {
            regionManager.RequestNavigate(RegionNames.ContentRegion, "Dashboard");

        }

        private void OnOpenRecentClicked()
        {
            // string fileExtension = ".vrf";
            //projectPath

        }
        #endregion
    }
}
