﻿using JCHVRF_New.Common.Helpers;
using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel.DataAnnotations;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace JCHVRF_New.ViewModels
{ 
    public class BulkFloorPopupViewModel : ViewModelBase
    {

        #region Deleget Commands
        public DelegateCommand PlushClickCommand {get;set;}
        public DelegateCommand MinusClickCommand { get; set; }
        public DelegateCommand<Window> CancelBulkPopupClickedCommand { get; set; }
        public DelegateCommand<Window> AddBulkPopupClickedCommand { get; set; }
        public DelegateCommand<TextCompositionEventArgs> BulkTextChangedCommand { get; set; }
        #endregion
        #region Property Member
        private string floorCount = string.Empty;
        private IEventAggregator _eventAggregator;
        private string txtBulkAdd;
        public string TxtBulkAdd
        {
            get { return txtBulkAdd; }
            set {
                this.SetValue(ref txtBulkAdd, value);
            }
        }
        #endregion

        #region constructor
        public BulkFloorPopupViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            PlushClickCommand = new DelegateCommand(PlushClick);
            MinusClickCommand = new DelegateCommand(MinusClick);
            CancelBulkPopupClickedCommand = new DelegateCommand<Window>(OnCancelBulkPopupClickedCommand);
            AddBulkPopupClickedCommand = new DelegateCommand<Window>(OnAddBulkPopupClickedCommand);
            BulkTextChangedCommand = new DelegateCommand<TextCompositionEventArgs>(OnBulkTextChangedCommand);
        }

        private void OnBulkTextChangedCommand(TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        #endregion

        #region command methods
        private void OnAddBulkPopupClickedCommand(Window objBulkAddPopupWindow)
        {
            _eventAggregator.GetEvent<PubSubEvent<string>>().Publish(TxtBulkAdd);
            objBulkAddPopupWindow.Close();

        }
        private void OnCancelBulkPopupClickedCommand(Window objBulkAddPopupWindow)
        {
            _eventAggregator.GetEvent<PubSubEvent>().Publish();
            objBulkAddPopupWindow.Close();
        }
        private void MinusClick()
        {
            if (Convert.ToInt32(floorCount) > 1)
            {
                floorCount = string.IsNullOrEmpty(TxtBulkAdd) ? "0" : TxtBulkAdd;
                if (floorCount != "0")
                    TxtBulkAdd = Convert.ToString(Convert.ToInt32(floorCount) - 1);
            }
        }

        private void PlushClick()
        {
            floorCount = string.IsNullOrEmpty(TxtBulkAdd) ? "0" : TxtBulkAdd;
            TxtBulkAdd = Convert.ToString(Convert.ToInt32(floorCount) + 1);
        }
       #endregion
    }
}
