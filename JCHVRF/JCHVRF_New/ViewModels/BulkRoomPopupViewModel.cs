﻿using JCHVRF_New.Common.Helpers;
using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace JCHVRF_New.ViewModels
{
    class BulkRoomPopupViewModel: ViewModelBase
    {

        #region Deleget Commands
        public DelegateCommand PlushRoomClickCommand { get; set; }
        public DelegateCommand MinusClickCommand { get; set; }
        public DelegateCommand<Window> CancelBulkPopupClickedCommand { get; set; }
        public DelegateCommand<Window> AddBulkPopupClickedCommand { get; set; }
        public DelegateCommand<TextCompositionEventArgs> BulkTextChangedCommand { get; set; }
        #endregion
        #region Property Member
        private string roomCount = string.Empty;
        private IEventAggregator _eventAggregator;
        private string txtBulkAdd;
        public string TxtBulkAdd
        {
            get { return txtBulkAdd; }
            set
            {
                this.SetValue(ref txtBulkAdd, value);
            }
        }
        #endregion

        #region constructor
        public BulkRoomPopupViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            PlushRoomClickCommand = new DelegateCommand(PlushClick);
            MinusClickCommand = new DelegateCommand(MinusClick);
            CancelBulkPopupClickedCommand = new DelegateCommand<Window>(OnCancelBulkPopupClickedCommand);
            AddBulkPopupClickedCommand = new DelegateCommand<Window>(OnAddBulkPopupClickedCommand);
            BulkTextChangedCommand = new DelegateCommand<TextCompositionEventArgs>(OnBulkTextChangedCommand);
        }

        private void OnBulkTextChangedCommand(TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        #endregion
        #region command methods
        private void OnAddBulkPopupClickedCommand(Window objBulkAddPopupWindow)
        {
            int introomCount = int.Parse(TxtBulkAdd);
            _eventAggregator.GetEvent<PubSubEvent<int>>().Publish(introomCount);
            objBulkAddPopupWindow.Close();

        }
        private void OnCancelBulkPopupClickedCommand(Window objBulkAddPopupWindow)
        {
            _eventAggregator.GetEvent<PubSubEvent>().Publish();
            objBulkAddPopupWindow.Close();
        }
        private void MinusClick()
        {
            if (Convert.ToInt32(roomCount) > 1)
            {
                roomCount = string.IsNullOrEmpty(TxtBulkAdd) ? "0" : TxtBulkAdd;
                if (roomCount != "0")
                    TxtBulkAdd = Convert.ToString(Convert.ToInt32(roomCount) - 1);
            }
        }

        private void PlushClick()
        {
            roomCount = string.IsNullOrEmpty(TxtBulkAdd) ? "0" : TxtBulkAdd;
            TxtBulkAdd = Convert.ToString(Convert.ToInt32(roomCount) + 1);
        }
        #endregion
    }
}
