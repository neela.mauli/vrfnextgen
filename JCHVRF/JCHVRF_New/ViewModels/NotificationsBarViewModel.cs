﻿using JCHVRF_New.Common.Helpers;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCHVRF_New.ViewModels
{
    public class NotificationsBarViewModel : ViewModelBase
    {
        string GetUserName()
        {
            string name = Environment.UserName;
            StringBuilder actualName = new StringBuilder();
            if (name != null && name.Length > 0)
            {
                actualName.Append(char.ToUpper(name[0]));
                for (int i = 1; i < name.Length; i++)
                {
                    if (char.IsUpper(name[i]))
                    {
                        actualName.Append(" ");
                    }
                    actualName.Append(name[i]);

                }
            }
            return actualName.ToString();
        }

        public string UserName
        {
            get
            {
                return GetUserName();
            }
        }

        public NotificationsBarViewModel()
        {

        }
    }
}
