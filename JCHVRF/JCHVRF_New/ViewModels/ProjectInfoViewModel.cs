﻿using JCHVRF.BLL;
using JCHVRF.BLL.New;
using JCHVRF.DAL.New;
using JCHVRF_New.Common.Helpers;
using JCHVRF_New.ViewModels;
using JCHVRF_New.Views;
using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace JCHVRF_New.ViewModels
{
    public class ProjectInfoViewModel : ViewModelBase
    {


        private IProjectInfoBAL _projectInfoBll;
        public DelegateCommand NewClientClickedCommand { get; set; }
        public DelegateCommand ClientNameDBCommand { get; set; }

        public DelegateCommand CoolingCheckedCommand { get; set; }
        public DelegateCommand CoolingUnCheckedCommand { get; set; }
        public DelegateCommand HeatingCheckedCommand { get; set; }
        public DelegateCommand HeatingUnCheckedCommand { get; set; }
        public DelegateCommand BothCheckedCommand { get; set; }
        public DelegateCommand BothUnCheckedCommand { get; set; }
        public DelegateCommand HitachiCheckedCommand { get; set; }
        public DelegateCommand HitachiUnCheckedCommand { get; set; }
        public DelegateCommand YorkCheckedCommand { get; set; }
        public DelegateCommand YorkUnCheckedCommand { get; set; }
        public DelegateCommand SelectedDateChanged { get; set; }

        private string _lookupProjectName;
        JCHVRF.Model.Project CurrentProject;

        public string LookupProjectName
        {
            get { return _lookupProjectName; }
            set { this.SetValue(ref _lookupProjectName, value); }
        }

        //private string _lookupClientName;
        //public string LookupClientName
        //{
        //    get { return _lookupClientName; }
        //    set { this.SetValue(ref _lookupClientName, value); }
        //}

        private string _txtClientName;
        public string TxtClientName
        {
            get { return _txtClientName; }
            set { this.SetValue(ref _txtClientName, value); }
        }
        //private string _txtCreatorName;
        //public string TxtCreatorName
        //{
        //    get { return _txtCreatorName; }
        //    set { this.SetValue(ref _txtCreatorName, value); }
        //}


        //public List<ListBox> _listClient;
        //public List<ListBox> ListClient
        //{
        //    get
        //    {
        //        return _listClient;
        //    }
        //    set
        //    {
        //        this.SetValue(ref _listClient, value);
        //    }
        //}

      

        //public ObservableCollection<ListBox> _listCreatedBy;
        //public ObservableCollection<ListBox> ListCreatedBy
        //{
        //    get
        //    {
        //        return _listCreatedBy;
        //    }
        //    set
        //    {
        //        this.SetValue(ref _listCreatedBy, value);
        //    }
        //}



        public ObservableCollection<ListBox> _listBindClient;
        public ObservableCollection<ListBox>ListBindClient
        {
            get
            {
                return _listBindClient;
            }

            set
            {
                this.SetValue(ref _listBindClient, value);
            }
        }

        private List<ComboBox> _listCreator;
        public List<ComboBox> ListCreator
        {
            get
            {
                return _listCreator;
            }
            set
            {
                this.SetValue(ref _listCreator, value);
            }
        }

        private ObservableCollection<ComboBox> _listClient;
        public ObservableCollection<ComboBox> ListClient
        {
            get
            {
                return _listClient;
            }
            set
            {
                this.SetValue(ref _listClient, value);
            }
        }

        private string _selectedClient;

        public string SelectedClient
        {
            get { return _selectedClient; }
            set {this.SetValue(ref _selectedClient, value); }
        }

        private string _selectedCreator;

        public string SelectedCreator
        {
            get { return _selectedCreator; }
            set {  this.SetValue(ref _selectedCreator, value);}
        }


        //private ListBox _selectedListviewCreator;

        //public ListBox SelectedListviewCreator
        //{
        //    get { return _selectedListviewCreator; }
        //    set { this.SetValue(ref _selectedListviewCreator, value);
        //        if(_selectedListviewCreator!=null)
        //        this.TxtCreatorName = _selectedListviewCreator.DisplayName;
        //    }
        //}



        private ListBox _selectedListviewClient;

        public ListBox SelectedListviewClient
        {
            get { return _selectedListviewClient; }
            set
            {
                this.SetValue(ref _selectedListviewClient, value);
                if (_selectedListviewClient != null)
                    this.TxtClientName = _selectedListviewClient.DisplayName;
            }
        }


        private bool _isCoolingChecked;

        public bool IsCoolingChecked
        {
            get { return _isCoolingChecked; }
            set { this.SetValue(ref _isCoolingChecked, value); }
        }


        private bool _isHeatingChecked;

        public bool IsHeatingChecked
        {
            get { return _isHeatingChecked; }
            set { this.SetValue(ref _isHeatingChecked, value); }
        }

      
        private bool _isBothChecked;

        public bool IsBothChecked
        {
            get { return _isBothChecked; }
            set { this.SetValue(ref _isBothChecked, value); }
        }

        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { this.SetValue(ref _notes, value); }
        }

        private bool _isHitachi;
        public bool IsHitachi
        {
            get { return _isHitachi; }
            set { this.SetValue(ref _isHitachi, value); }
        }

        private bool _isYork;
        public bool IsYork
        {
            get { return _isYork; }
            set { this.SetValue(ref _isYork, value); }
        }

        private bool _isEnableRemove;

        public bool IsEnableRemove
        {
            get { return _isEnableRemove; }
            set { this.SetValue(ref _isEnableRemove, value); }
        }
        private DateTime _deliveryDate;
        public DateTime DeliveryDate
        {
            get { return _deliveryDate; }
            set { this.SetValue(ref _deliveryDate, value); }
        }

       
        private IEventAggregator _eventAggregator;
        public ProjectInfoViewModel(IProjectInfoBAL projctInfoBll, IEventAggregator eventAggregator)
        {
            Initialization();
            _eventAggregator = eventAggregator;
            CoolingCheckedCommand = new DelegateCommand(CoolingCheckedEvent);
            CoolingUnCheckedCommand = new DelegateCommand(CoolingUncheckedEvent);
            HeatingCheckedCommand = new DelegateCommand(HeatingCheckedEvent);
            HeatingUnCheckedCommand = new DelegateCommand(HeatingUncheckedEvent);
            BothCheckedCommand = new DelegateCommand(BothCheckedEvent);
            BothUnCheckedCommand = new DelegateCommand(BothUncheckedEvent);
            HitachiCheckedCommand = new DelegateCommand(HitachiCheckedEvent);
            HitachiUnCheckedCommand = new DelegateCommand(HitachiUncheckedEvent);
            YorkCheckedCommand = new DelegateCommand(YorkCheckedEvent);
            YorkUnCheckedCommand = new DelegateCommand(YorkUncheckedEvent);
            SelectedDateChanged = new DelegateCommand(DeliveryDateChangedEvent);
            ProjectInitialisation();
            _projectInfoBll = projctInfoBll;
            LookupProjectName = GetDefaultProjectName();

            NewClientClickedCommand = new DelegateCommand(OnNewClientClicked);
            ClientNameDBCommand = new DelegateCommand(OnClientNameKeyUpCommand);
           
            GetClientList();
            GetCreatorList();
            _eventAggregator.GetEvent<ProjectTypeInfoTabNext>().Subscribe(ProjectTypeNextClick);


            _eventAggregator.GetEvent<AddCreatorPayLoad>().Subscribe(RebindCreatorList);
            
        }

       

        private void OnClientNameKeyUpCommand()
     {
            string typedString = TxtClientName;
            ListBindClient = GetClientList(typedString);

            if (ListBindClient.Count > 0)
            {
                IsEnableRemove = true;
            }

            else
            {
                IsEnableRemove = false;

            }

        }
        private void RebindCreatorList()
        {
            GetCreatorList();
        }

        private string _isHitachiRdbVisible;

        public string IsHitachiRdbVisible
        {
            get { return _isHitachiRdbVisible; }
            set { this.SetValue(ref _isHitachiRdbVisible, value); }
        }

        private string _isHitachiImgVisible;

        public string IsHitachiImgVisible
        {
            get { return _isHitachiImgVisible; }
            set { this.SetValue(ref _isHitachiImgVisible, value); }
        }

        private string _isYorkRdbVisible;

        public string IsYorkRdbVisible
        {
            get { return _isYorkRdbVisible; }
            set { this.SetValue(ref _isYorkRdbVisible, value); }
        }

        private string _isYorkImgVisible;

        public string IsYorkImgVisible
        {
            get { return _isYorkImgVisible; }
            set { this.SetValue(ref _isYorkImgVisible, value); }
        }

        private void BindBrandCode()
        {
            MyProductTypeBLL bll = new MyProductTypeBLL();
            List<string> brandList = bll.GetBrandCodeList(CurrentProject.SubRegionCode.ToString());
            brandList.ForEach((item) =>
            {
                if (item.Equals("Y", StringComparison.OrdinalIgnoreCase))
                {
                    IsYorkImgVisible = "Visible";
                    IsYorkRdbVisible = "Visible";
                }
                else if (item.Equals("H", StringComparison.OrdinalIgnoreCase))
                {
                    IsHitachiRdbVisible = "Visible";
                    IsHitachiImgVisible = "Visible";
                }
            });

        }

        private void ProjectTypeNextClick()
        {
            if (ProjectInfoToProjectLegacy())
            {
                _eventAggregator.GetEvent<ProjectInfoSubscriber>().Publish(true);
            }
            else
            {
                _eventAggregator.GetEvent<ProjectInfoSubscriber>().Publish(false);
            }

        }
        private void DeliveryDateChangedEvent()
        {
            // Do Validation for DeliveryDate
            // DeliveryDate = this.DeliveryDate;
        }

        private void Initialization()
        {
            IsCoolingChecked = true;
            IsHitachi = true;
            DeliveryDate = DateTime.Now;
        }
        private void CoolingUncheckedEvent()
        {
            IsCoolingChecked = false;
        }

        private void CoolingCheckedEvent()
        {
            IsCoolingChecked = true;
            IsHeatingChecked = false;
            IsBothChecked = false;
        }
        private void HeatingUncheckedEvent()
        {
            IsHeatingChecked = false;
        }

        private void HeatingCheckedEvent()
        {
            IsHeatingChecked = true;
            IsCoolingChecked = false;
            IsBothChecked = false;
        }
        private void BothUncheckedEvent()
        {
            IsBothChecked = false;
        }

        private void BothCheckedEvent()
        {
            IsBothChecked = true;

        }

        private void HitachiUncheckedEvent()
        {
            IsHitachi = false;
        }

        private void HitachiCheckedEvent()
        {
            IsHitachi = true;
        }

        private void YorkUncheckedEvent()
        {
            IsYork = false;
        }

        private void YorkCheckedEvent()
        {
            IsYork = true;
        }

        private void OnNewClientClicked()
        {
            AddNewClient newClientWindow = new AddNewClient();
            newClientWindow.ShowDialog();
           GetClientName();

        }

       
        public string GetDefaultProjectName()
        {
            return _projectInfoBll.GetDefaultProjectName();
        }

        public void GetClientName()
        {
            if (Application.Current.Properties["TxtContactName"] != null)
                TxtClientName = Application.Current.Properties["TxtContactName"].ToString();
            GetClientList();
          
            //return LookupClientName;
        }

        

        public ObservableCollection<ComboBox> GetClientList()
        {
            List<Tuple<string, string>> getClientList = null;
            ListClient = new ObservableCollection<ComboBox>();
            getClientList = _projectInfoBll.GetClientInfo();
            getClientList.ForEach((item) =>
            {
                ListClient.Add(new ComboBox { DisplayName = item.Item2, Value = item.Item1 });
            });
            return ListClient;
        }


        public List<ComboBox> GetCreatorList()
        {
            List<Tuple<string, string>> getCreatorList = null;
            ListCreator = new List<ComboBox>();
            getCreatorList = _projectInfoBll.GetCreatorInfo();
            getCreatorList.ForEach((item) =>
            {
                ListCreator.Add(new ComboBox { DisplayName = item.Item2, Value = item.Item1 });
            });
            return ListCreator;
        }
       

        public ObservableCollection<ListBox> GetClientList(string typeText)
        {
            List<Tuple<string, string>> getClientList = null;
            ListBindClient = new ObservableCollection<ListBox>();
            getClientList = _projectInfoBll.GetClientInfoList(typeText);
            if (getClientList != null)
            {
                getClientList.ForEach((item) =>
                {
                    //ListClient.Add(new ListBox { DisplayName = item });
                    ListBindClient.Add(new ListBox { Value = item.Item1, DisplayName = item.Item2, });

                });

            }

            return ListBindClient;
        }

        void ProjectInitialisation()
        {
            CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            IsHitachiImgVisible = "Hidden";
            IsHitachiRdbVisible = "Hidden";
            IsYorkImgVisible = "Hidden";
            IsYorkRdbVisible = "Hidden";
            BindBrandCode();
        }
        bool ProjectInfoToProjectLegacy()
        {
            string errorMessage = null;
            bool IsValid = ValidateProjectDataValid(out errorMessage);
            if (IsValid)
            {
                CurrentProject.Name = LookupProjectName;
                //CurrentProject.clientName = SelectedClient;
                CurrentProject.clientName = this.TxtClientName;
                CurrentProject.CreatorName = SelectedCreator;
                //  projectLegacy.Location =  ;
                CurrentProject.DeliveryRequiredDate = Convert.ToDateTime(DeliveryDate);
                CurrentProject.Remarks = Notes;
                if (IsBothChecked)
                {
                    CurrentProject.IsCoolingModeEffective = true;
                    CurrentProject.IsHeatingModeEffective = true;
                }
                else if (IsHeatingChecked)
                {
                    CurrentProject.IsHeatingModeEffective = true;
                    CurrentProject.IsCoolingModeEffective = false;
                }
                else if (IsCoolingChecked)
                {
                    CurrentProject.IsCoolingModeEffective = true;
                    CurrentProject.IsHeatingModeEffective = false;
                }
                if (IsHitachi)
                    CurrentProject.BrandCode = "H";
                if (IsYork)
                    CurrentProject.BrandCode = "Y";
                CurrentProject.FactoryCode = "S";
                //projectLegacy.IsProjectTypeTabValid = true;
            }
            else
            {
                MessageBox.Show(errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                //projectLegacy.IsProjectTypeTabValid = true;
            }
            return IsValid;

            //  projectLegacy.FactoryCode = "S";
        }



        public bool ValidateProjectDataValid(out string errormessage)
        {

            bool IsValid = true;
            errormessage = "";
            string specialCharacters = @"\*?:'$%#!^&*|" + "\"";
            char[] specialCharactersArray = specialCharacters.ToCharArray();
            if (string.IsNullOrEmpty(LookupProjectName.Trim()))
            {
                errormessage = "Project Name cannot be blank";
                IsValid = false;

            }
            else if (LookupProjectName.IndexOfAny(specialCharactersArray) > -1)
            {
                errormessage = "Project Name cannot contain any of the following characters: " + specialCharacters.ToString();
                IsValid = false;
            }

            //else if (string.IsNullOrEmpty(SelectedClient))
            //{
            //    errormessage = "Client name cannot be blank";
            //    IsValid = false;

            //}
            //else if (string.IsNullOrEmpty(SelectedCreator))
            //{
            //    errormessage = "Creator Name cannot be blank";
            //    IsValid = false;

            //}
            else if (string.IsNullOrEmpty(Convert.ToString(DeliveryDate)))
            {
                errormessage = "Delivery Date cannot be blank";
                IsValid = false;
            }
            else
            {
                DateTime dateValue;
                if (DateTime.TryParse(Convert.ToString(DeliveryDate), out dateValue))
                {
                    if (DeliveryDate < DateTime.Now.Date)
                    {
                        errormessage = "Delivery Date cannot be less than Current date";
                        DeliveryDate = DateTime.Now;
                        IsValid = false;
                    }
                }
                else
                {
                    errormessage = "Please enter valid date";
                    IsValid = false;
                }
            }
            return IsValid;
        }
    }
}
