﻿/****************************** File Header ******************************\
File Name:	CreateProjectWizardViewModel.cs
Date Created:	2/8/2019
Description:	View Model for CreateProjectWizard View.
\*************************************************************************/

namespace JCHVRF_New.ViewModels
{
    using JCHVRF.BLL.New;
    using JCHVRF.Model;
    using JCHVRF_New.Common.Constants;
    using JCHVRF_New.Common.Helpers;
    using Prism.Commands;
    using Prism.Events;
    using Prism.Regions;
    using System.Windows;

    public class SystemPropertyWizardViewModel : ViewModelBase
    {
        #region Fields
        private int _selectedTabIndex;
        private IEventAggregator _eventAggregator;
        private IProjectInfoBAL _projectBAL;
        #endregion Fields

        #region Properties
        public DelegateCommand<Window> CancelClickCommand { get; private set; }
        public DelegateCommand<Window> CreateClickCommand { get; private set; }
        public DelegateCommand NextClickCommand { get; set; }
        public DelegateCommand PreviousClickCommand { get; set; }

        /// <summary>
        /// Gets or sets the SelectedTabIndex
        /// </summary>
        private Visibility _floorvisibility;
        public Visibility FloorVisibility
        {
            get { return this._floorvisibility; }
            set { this.SetValue(ref _floorvisibility, value); }
        }

        private void GetSetFloorTabVisibility(Visibility floorvisibility)
        {
            FloorVisibility = floorvisibility;
        }
        bool IsProjectInfovalidateTabinfo = false;
        private void SetProjectInfoValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsProjectInfovalidateTabinfo = IsvalidateTabinfoReturn;
        }
        bool IsDesignTabvalidateTabinfo = false;
        private void SetDesignTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsDesignTabvalidateTabinfo = IsvalidateTabinfoReturn;
        }
        bool IsTypeTabvalidateTabinfo = false;
        private void SetTypeTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsTypeTabvalidateTabinfo = IsvalidateTabinfoReturn;
        }
        bool IsFloorTabvalidateinfo = false;
        private void SetFloorTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsFloorTabvalidateinfo = IsvalidateTabinfoReturn;
        }

        bool IsOduTabvalidateinfo = false;
        private void SetOduTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsOduTabvalidateinfo = IsvalidateTabinfoReturn;
        }

        bool IsIDUTabvalidateinfo = false;
        private void SetIDUTabValidateValue(bool IsvalidateTabinfoReturn)
        {
            IsIDUTabvalidateinfo = IsvalidateTabinfoReturn;
        }

        private Visibility _iduVisibility;
        public Visibility IduVisibility
        {
            get { return this._iduVisibility; }
            set { this.SetValue(ref _iduVisibility, value); }
        }

        private void GetSetIduTabVisibility(Visibility iduvisibility)
        {
            IduVisibility = iduvisibility;
        }

        //Accord - - Total Heat Exchange Unit Info Tab binding variable
        /// </summary>
        private Visibility _theuInfoVisibility;
        public Visibility TheuInfoVisibility
        {
            get { return this._theuInfoVisibility; }
            set { this.SetValue(ref _theuInfoVisibility, value); }
        }
        private void GetSetTHEUITabVisibility(Visibility theuInfoVisibility)
        {
            TheuInfoVisibility = theuInfoVisibility;
        }

        /// <summary>
        /// Gets or sets the SelectedTabIndex
        /// </summary>
        public int SelectedTabIndex
        {
            get { return this._selectedTabIndex; }
            set { this.SetValue(ref _selectedTabIndex, value); }
        }

        public IRegionManager RegionManager { get; set; }


        #endregion Properties

        #region Constructors
        public SystemPropertyWizardViewModel(IEventAggregator EventAggregator, IProjectInfoBAL projctInfoBll, IRegionManager _regionManager)
        {
            _eventAggregator = EventAggregator;
            _projectBAL = projctInfoBll;
            _eventAggregator.GetEvent<PubSubEvent<Visibility>>().Subscribe(GetSetFloorTabVisibility);
            _eventAggregator.GetEvent<OduIduVisibility>().Subscribe(GetSetIduTabVisibility);
            //_eventAggregator.GetEvent<RefreshDashboard>().Subscribe(RefreshDashBoard);
            _eventAggregator.GetEvent<TheuInfoVisibility>().Subscribe(GetSetTHEUITabVisibility);
            CancelClickCommand = new DelegateCommand<Window>(this.CancelClick);
            CreateClickCommand = new DelegateCommand<Window>(this.CreateClick);
            NextClickCommand = new DelegateCommand(NextClick);
            PreviousClickCommand = new DelegateCommand(PreviousClick);
            this.FloorVisibility = Visibility.Collapsed;
            this.TheuInfoVisibility = Visibility.Collapsed;
            _eventAggregator.GetEvent<TypeTabSubscriber>().Subscribe(SetTypeTabValidateValue);
            _eventAggregator.GetEvent<FloorTabSubscriber>().Subscribe(SetFloorTabValidateValue);
            _eventAggregator.GetEvent<OduTabSubscriber>().Subscribe(SetOduTabValidateValue);
            _eventAggregator.GetEvent<IduTabSubscriber>().Subscribe(SetIDUTabValidateValue);
            RegionManager = _regionManager;
        }

        #endregion

        //TODO to get rid of this selectedTabIndex
        private void SelectWizardNextTab()
        {

            int systemId = System.Convert.ToInt32(WorkFlowContext.Systemid);

            if (this.SelectedTabIndex != 0)
            {
                if (systemId == 2)
                {
                    this.SelectedTabIndex = 4;
                    return;
                }
                else
                {
                    this.SelectedTabIndex = SelectedTabIndex + 1;
                    return;
                }
            }

            if (this.FloorVisibility == Visibility.Visible)
            {
                this.SelectedTabIndex = this.SelectedTabIndex + 1;
                return;
            }

            else if (this.FloorVisibility == Visibility.Collapsed)
            {
                switch (systemId)
                {
                    case 1:
                        this.SelectedTabIndex = 2;
                        break;
                    case 2://HeatExchanger Tab
                        this.SelectedTabIndex = 4;
                        break;

                }
            }
        }


        private void SelectWizardPreviousTab()
        {
            if (this.FloorVisibility == Visibility.Collapsed)
            {
                if (this.SelectedTabIndex == 3)
                {
                    this.SelectedTabIndex = this.SelectedTabIndex - 1;
                }
                else if (this.SelectedTabIndex == 2 || this.SelectedTabIndex == 4)
                {
                    this.SelectedTabIndex = 0;
                }
            }
            else
            {
                if (this.SelectedTabIndex == 4)
                {
                    this.SelectedTabIndex = this.SelectedTabIndex - 3;
                }
                else
                {
                    this.SelectedTabIndex = this.SelectedTabIndex - 1;
                }
            }
        }


        private void CancelClick(Window win)
        {
            if (win != null)
            {
                win.Close();
            }
        }
       //IRegionManager regionManager;
        private void CreateClick(Window win)
        {
            
            var proj = Project.GetProjectInstance;
            
            if (_projectBAL.UpdateProject(proj))
            {
                
                MessageBox.Show("System Saved Successfully");
                if (win != null)
                {
                    var w = Application.Current.MainWindow;
                    w.Hide();
                    //RefreshDashBoard();
                    var projectId = proj.projectID;
                    Application.Current.Properties["ProjectId"] = projectId;
                    ProjectInfoBLL bll = new ProjectInfoBLL();
                    JCHVRF.Entity.ProjectInfo projectNextGen = bll.GetProjectInfo(projectId);
                    projectNextGen.ProjectLegacy.RegionCode = JCHVRF.Model.Project.CurrentProject.RegionCode;
                    projectNextGen.ProjectLegacy.SubRegionCode = JCHVRF.Model.Project.CurrentProject.SubRegionCode;
                    projectNextGen.ProjectLegacy.projectID = projectId;

                    NavigationParameters param = new NavigationParameters();
                    param.Add("Project", projectNextGen.ProjectLegacy);

                    RegionManager.RequestNavigate(RegionNames.ContentRegion, "MasterDesigner",(a)=> { win.Close(); } ,param);
                    w.Show();

                }
                
            }
        }
        private void NextClick()
        {
            switch (this.SelectedTabIndex)
            {             
                //Type Tab
                case 0:
                    _eventAggregator.GetEvent<TypeInfoTabNext>().Publish();
                    if (IsTypeTabvalidateTabinfo)
                        SelectWizardNextTab();
                    break;
                //Floor Tab
                case 1:
                    _eventAggregator.GetEvent<FloorTabNext>().Publish();
                    if (IsFloorTabvalidateinfo)
                        SelectWizardNextTab();
                    break;
                //ODU Tab
                case 2:
                    _eventAggregator.GetEvent<ODUTypeTabNext>().Publish();
                   if (IsOduTabvalidateinfo)
                        SelectWizardNextTab();
                    break;
                //IDU Tab
                case 3:
                    SelectWizardNextTab();
                    break;
                default:
                    break;
            }
        }
        private void PreviousClick()
        {
            SelectWizardPreviousTab();
        }
    }
}
