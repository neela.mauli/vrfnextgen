﻿using JCHVRF.Model.New;
using JCHVRF_New.Common.Helpers;
using Prism.Commands;
using Prism.Events;
using JCHVRF_New.Views;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using JCHVRF_New.Common.Contracts;
using System.Windows.Input;

namespace JCHVRF_New.ViewModels
{
    public class FloorTabViewModel : ViewModelBase
    {
        #region  Variable and constructions 
        private ObservableCollection<Floor> _listFloor;

        private string _isCancelEnable;

        public string IsCancelEnable
        {
            get { return _isCancelEnable; }
            set { this.SetValue(ref _isCancelEnable, value); }
        }

        private string _isSaveEnable;

        public string IsSaveEnable
        {
            get { return _isSaveEnable; }
            set { this.SetValue(ref _isSaveEnable, value); }
        }
        private string _isEnableTitle;

        public string IsEnableTitle
        {
            get { return _isEnableTitle; }
            set { this.SetValue(ref _isEnableTitle, value); }
        }
        private IEventAggregator _eventAggregator;
        public DelegateCommand chkSelectAll_UnCheckedCommand { get; set; }
        public DelegateCommand chkSelectAll_CheckedCommand { get; set; }
        public DelegateCommand BulkRemoveFloorCommand { get; set; }
        public DelegateCommand BulkAddCommand { get; set; }
        public DelegateCommand AddFloorCommand { get; set; }
        public DelegateCommand RemoveFloorCommand { get; set; }
        public DelegateCommand<IClosable> AddEditFloorCommand { get; set; }
        JCHVRF.Model.Project CurrentProject;
        public FloorTabViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            ListFloor = new ObservableCollection<Floor>();
            CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            chkSelectAll_UnCheckedCommand = new DelegateCommand(chkSelectAll_UnChecked);
            chkSelectAll_CheckedCommand = new DelegateCommand(chkSelectAll_Checked);
            BulkRemoveFloorCommand = new DelegateCommand(BulkRemoveFloor);
            AddFloorCommand = new DelegateCommand(AddFloorIconClick);
            AddEditFloorCommand = new DelegateCommand<IClosable>(AddEditFloorClick);
            RemoveFloorCommand = new DelegateCommand(RemoveFloorIconClick);
            _eventAggregator.GetEvent<PubSubEvent<int?>>().Subscribe(OnMultiFloorValueTypeTab);
            _eventAggregator.GetEvent<PubSubEvent>().Subscribe(OnAddFloorEdit);
            _eventAggregator.GetEvent<PubSubEvent<string>>().Subscribe(OnMultiFloorValueBulkUpload);
            _eventAggregator.GetEvent<FloorButtonEnableSubscriber>().Subscribe(EnableFLoorSavebutton);
            _eventAggregator.GetEvent<FloorListAddSubscriber>().Subscribe(AddFloor);
            BulkAddCommand = new DelegateCommand(BulkAddClick);
            ProjectInitialisation();
            _eventAggregator.GetEvent<FloorTabNext>().Subscribe(FloorNextClick);
            AddDefoultFloorlist();
        }
        void ProjectInitialisation()
        {
            CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            IsSaveEnable = "Hidden";
            IsCancelEnable = "Hidden";
            IsEnableTitle = "Hidden";
        }

        private void EnableFLoorSavebutton(bool IsValid)
        {
            if (IsValid == true)
            {
                IsSaveEnable = "Visible";
                IsCancelEnable = "Visible";
                IsEnableTitle = "Visible";
            }
            else if (IsValid == false)
            {
                IsEnableTitle = "Hidden";
                IsSaveEnable = "Hidden";
                IsCancelEnable = "Hidden";
            }
        }
        private void FloorNextClick()
        {
            if (FloorTabInfoToProjectLegacy())
            {
                _eventAggregator.GetEvent<FloorTabSubscriber>().Publish(true);
            }
            else
            {
                _eventAggregator.GetEvent<FloorTabSubscriber>().Publish(false);
            }
        }

        private void AddFloorIconClick()
        {
            if (this.ListFloor != null)
            {
                string lblFloor = string.Empty;
                lblFloor = JCHVRF.Model.SystemSetting.UserSetting.defaultSetting.FloorName + " " + (this.ListFloor.Count + 1);
                this.ListFloor.Add(new Floor { floorName = lblFloor, elevationFromGround = 0, IsFloorChecked = false });

                WorkFlowContext.FloorNames.Add(lblFloor);                                           //sharad-add floor on TotalHeatEx -03/07/19
            }
            else
            {
                this.ListFloor = new ObservableCollection<Floor>();
                string lblFloor = string.Empty;
                lblFloor = JCHVRF.Model.SystemSetting.UserSetting.defaultSetting.FloorName + " " + (this.ListFloor.Count + 1);
                this.ListFloor.Add(new Floor { floorName = lblFloor, elevationFromGround = 0, IsFloorChecked = false });

                WorkFlowContext.FloorNames.Add(lblFloor);                                             //sharad-add floor on TotalHeatEx -03/07/19
            }
            EnableDisable();
        }

        private void RemoveFloorIconClick()
        {
            if (this.ListFloor != null && this.ListFloor.Count > 0)
            {
                Floor floor = this.ListFloor.LastOrDefault();
                this.ListFloor.Remove(floor);
                EnableDisable();
            }
        }

        private void BulkRemoveFloor()
        {
            ObservableCollection<Floor> newListFloor = null;
            if (ListFloor != null && ListFloor.Count > 0)
            {
                newListFloor = new ObservableCollection<Floor>();
                foreach (var item in ListFloor)
                {
                    if (!item.IsFloorChecked)
                    {
                        newListFloor.Add(item);
                    }
                }
            }
            ListFloor.Clear();
            ListFloor = newListFloor;
        }

        private void chkSelectAll_UnChecked()
        {
            if (ListFloor != null && ListFloor.Count > 0)
            {
                foreach (var item in ListFloor)
                {
                    item.IsFloorChecked = false;
                }
            }
        }

        private void AddDefoultFloorlist()
        {
            if (CurrentProject.FloorList.Count > 0)
            {
                IsEnableRemove = true;
                for (int i = 0; i < CurrentProject.FloorList.Count; i++)
                {
                    this.ListFloor.Add(new Floor { floorName = CurrentProject.FloorList[i].Name.ToString(), elevationFromGround = 0, IsFloorChecked = false });
                }
            }
        }
        private bool FloorTabInfoToProjectLegacy()
        {
            List<JCHVRF.Model.Floor> floorLegacy = new List<JCHVRF.Model.Floor>();
            int floorID = 0;
            if (ListFloor.Count > 1)
            {
                foreach (var floor in ListFloor)
                {
                    floorLegacy.Add(new JCHVRF.Model.Floor
                    {
                        Name = floor.floorName,
                        Height = floor.elevationFromGround,
                        Id = Convert.ToString(floorID)
                    });
                }
                CurrentProject.FloorList = floorLegacy;
                return true;
            }
            else
            {
                return false;
            }
        }
        private void BulkAddClick()
        {
            BulkFloorPopup objAssBulkFloor = new BulkFloorPopup();
            objAssBulkFloor.ShowDialog();
        }

        private bool isChecked;
        public bool IsChecked
        {
            get { return isChecked; }
            set { isChecked = value; }
        }

        private bool _isEnableRemove;

        public bool IsEnableRemove
        {
            get { return _isEnableRemove; }
            set { this.SetValue(ref _isEnableRemove, value); }
        }

        private void chkSelectAll_Checked()
        {
            if (ListFloor != null && ListFloor.Count > 0)
            {
                foreach (var item in ListFloor)
                {
                    item.IsFloorChecked = true;
                }
            }
        }
        #endregion

        #region View model properties
        public ObservableCollection<Floor> ListFloor
        {
            get { return _listFloor; }
            set
            {
                this.SetValue(ref _listFloor, value);
            }
        }

        #endregion

        private void EnableDisable()
        {
            if (this.ListFloor != null && this.ListFloor.Count < 1)
            {
                IsEnableRemove = false;
            }
            else
            {
                IsEnableRemove = true;
            }

        }

        #region Methods and Command methods
        private void OnAddFloorEdit()
        {
            if (CurrentProject.FloorList != null)
            {
                this.ListFloor.Clear();
                if (CurrentProject.FloorList.Count > 0)
                {
                    for (int i = 0; i < CurrentProject.FloorList.Count; i++)
                    {
                        this.ListFloor.Add(new Floor { floorName = CurrentProject.FloorList[i].Name.ToString(), elevationFromGround = 0, IsFloorChecked = false });
                    }
                }
            }
        }
        private void OnMultiFloorValueTypeTab(int? floorCount)
        {
            if (floorCount != null)
            {
                this.ListFloor.Clear();
                if (this.ListFloor.Count == 0)
                {
                    for (int i = 0; i < floorCount; i++)
                    {
                        string lblFloor = string.Empty;
                        lblFloor = JCHVRF.Model.SystemSetting.UserSetting.defaultSetting.FloorName + " " + (i);
                        this.ListFloor.Add(new Floor { floorName = lblFloor, elevationFromGround = 0, IsFloorChecked = false });
                        EnableDisable();
                    }
                }
                //else
                //{
                //    for (int i = this.ListFloor.Count; i < floorCount; i++)
                //    {
                //        string lblFloor = string.Empty;
                //        lblFloor = "Floor " + (i);
                //        this.ListFloor.Add(new Floor { floorName = lblFloor, elevationFromGround = 0, IsFloorChecked = false });
                //    }

                //}
            }
        }
        private void OnMultiFloorValueBulkUpload(string floorCount)
        {
            int intfloorCount = int.Parse(floorCount);
            if (floorCount != null)
            {
                if (this.ListFloor.Count == 0)
                {
                    for (int i = 0; i < intfloorCount; i++)
                    {
                        string lblFloor = string.Empty;
                        lblFloor = JCHVRF.Model.SystemSetting.UserSetting.defaultSetting.FloorName + " " + (i);
                        this.ListFloor.Add(new Floor { floorName = lblFloor, elevationFromGround = 0, IsFloorChecked = false });
                    }
                }
                else
                {
                    //if (this.ListFloor.Count < intfloorCount)
                    //{
                    int BulkAddCount = this.ListFloor.Count + intfloorCount;
                    for (int i = this.ListFloor.Count; i < BulkAddCount; i++)
                    {
                        string lblFloor = string.Empty;
                        lblFloor = JCHVRF.Model.SystemSetting.UserSetting.defaultSetting.FloorName + " " + (i);
                        this.ListFloor.Add(new Floor { floorName = lblFloor, elevationFromGround = 0, IsFloorChecked = false });
                    }
                }
            }
        }

        private void AddFloor()
        {
            List<JCHVRF.Model.Floor> floorLegacy = new List<JCHVRF.Model.Floor>();
            int floorID = 0;
            if (this.ListFloor.Count > 1)
            {
                foreach (var floor in this.ListFloor)
                {
                    floorLegacy.Add(new JCHVRF.Model.Floor
                    {
                        Name = floor.floorName,
                        Height = floor.elevationFromGround,
                        Id = Convert.ToString(floorID)
                    });
                }
                CurrentProject.FloorList = floorLegacy;
                _eventAggregator.GetEvent<FloorListSaveSubscriber>().Publish();
                _eventAggregator.GetEvent<PubSubEvent>().Publish();
            }
        }
        private void AddEditFloorClick(IClosable objClosable)
        {
            List<JCHVRF.Model.Floor> floorLegacy = new List<JCHVRF.Model.Floor>();
            int floorID = 0;
            if (this.ListFloor.Count > 1)
            {
                foreach (var floor in this.ListFloor)
                {
                    floorLegacy.Add(new JCHVRF.Model.Floor
                    {
                        Name = floor.floorName,
                        Height = floor.elevationFromGround,
                        Id = Convert.ToString(floorID)
                    });
                }
                CurrentProject.FloorList = floorLegacy;
                _eventAggregator.GetEvent<FloorListSaveSubscriber>().Publish();
                _eventAggregator.GetEvent<PubSubEvent>().Publish();
                objClosable.RequestClose();
            }

        }
        #endregion  Methods and Command methods
    }
}
