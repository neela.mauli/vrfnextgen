﻿using JCHVRF_New.Common.Contracts;
using JCHVRF_New.Common.Helpers;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCHVRF_New.ViewModels
{
   public class ErrorLogViewModel : ViewModelBase, ILayoutItem
    {
        #region Fields

        IEventAggregator _eventAggregator;

        private string _errorType;
        public  string ErrorType
        {
            get { return _errorType; }
            set
            {
                SetValue(ref _errorType, value);
            }
        }
        public string ErrorTypeImg { get; set; }

        private string _category;
       public string Category
        {
            get { return _category; }
            set
            {
                SetValue(ref _category, value);
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                SetValue(ref _description, value);
            }
        }

        #endregion
        #region Constructors

        public ErrorLogViewModel()
        {          
            Title = "Error Log";
        }
        #endregion

        private double _FloatingHeight;

        public double FloatingHeight
        {
            get { return this._FloatingHeight; }
            set { this.SetValue(ref _FloatingHeight, value); }
        }

        private double _FloatingLeft;

        public double FloatingLeft
        {
            get { return this._FloatingLeft; }
            set { this.SetValue(ref _FloatingLeft, value); }
        }

        private double _FloatingTop;

        public double FloatingTop
        {
            get { return this._FloatingTop; }
            set { this.SetValue(ref _FloatingTop, value); }
        }

        private double _FloatingWidth;

        public double FloatingWidth
        {
            get { return this._FloatingWidth; }
            set { this.SetValue(ref _FloatingWidth, value); }
        }

        private string _title;

        /// <summary>
        /// Gets or sets the Title
        /// </summary>
        public string Title
        {
            get { return this._title; }
            set { this.SetValue(ref _title, value); }
        }
    }
}
