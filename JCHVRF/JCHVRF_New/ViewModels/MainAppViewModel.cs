﻿using JCHVRF_New.Common.Helpers;
using FontAwesome.WPF;
using JCHVRF_New.Common.Constants;
using JCHVRF_New.Model;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Unity;
using System.Windows.Input;

namespace JCHVRF_New.ViewModels
{
    public class MainAppViewModel : ViewModelBase
    {
        #region Fields

        private IEventAggregator _eventAggregator;

        private ScrollBarVisibility _horizontalScrollBarVisibilityRequirement;

        private bool _isLeftDrawerOpen;

        private Thickness _marginRequirement;

        private SideBarItem _selectedSideBarItem;

        private ScrollBarVisibility _verticalScrollBarVisibilityRequirement;

        IUnityContainer _container;

        IRegionManager _regionManager;

        #endregion

        #region Constructors

        public MainAppViewModel(IRegionManager regionManager, IUnityContainer container, IEventAggregator eventAggregator)
        {
            _regionManager = regionManager;
            _container = container;
            _eventAggregator = eventAggregator;

            SideBarItems = new ObservableCollection<SideBarItem>()
            {
                new SideBarItem("Home", FontAwesomeIcon.Home),
                new SideBarItem("Settings", FontAwesomeIcon.Gear)
                {
                    Children = new ObservableCollection<SideBarChild>(){
                        new SideBarChild("General", "Settings"),
                        new SideBarChild("My Account", "Settings"),
                        new SideBarChild("Location", "Settings"),
                        new SideBarChild("Page View", "Settings"),
                        new SideBarChild("Tools", "Settings"),
                        new SideBarChild("Users", "Settings"),
                        new SideBarChild("Report Layout", "Settings"),
                        new SideBarChild("Notifications", "Settings"),
                        new SideBarChild("Measurement Unit", "Settings"),
                        new SideBarChild("Name Prefixes", "Settings")
                    }
                },
                new SideBarItem("Tools", FontAwesomeIcon.Wrench)
                {
                    Children = new ObservableCollection<SideBarChild>(){
                        new SideBarChild("Heat Load Calculator", "Tools"),
                        new SideBarChild("Controller Simulator", "Tools"),
                        new SideBarChild("Consumption Calculator", "Tools"),
                        new SideBarChild("Peak Load Calculator", "Tools"),
                        new SideBarChild("Temperature Simulator", "Tools"),
                        new SideBarChild("Download Templates", "Tools"),
                        new SideBarChild("Emmissions Calculator", "Tools"),
                        new SideBarChild("Standards Library", "Tools"),
                        new SideBarChild("Browse Catalogue", "Tools"),
                        new SideBarChild("Product Comparisons", "Tools")
                    }
                },
                new SideBarItem("Help", FontAwesomeIcon.QuestionCircleOutline)
            };
            MainAppLoadedCommand = new DelegateCommand(
                () =>
                {
                    _regionManager.RequestNavigate(RegionNames.ToolbarRegion, "NotificationsBar");
                    SelectedSideBarItem = SideBarItems.First();
                    Mouse.OverrideCursor = null;
                }
                );
            DrawerItemSelectionChangedCommand = new DelegateCommand<RoutedPropertyChangedEventArgs<object>>(OnDrawerItemSelected);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the DrawerItemSelectionChangedCommand
        /// </summary>
        public DelegateCommand<RoutedPropertyChangedEventArgs<object>> DrawerItemSelectionChangedCommand { get; set; }

        /// <summary>
        /// Gets or sets the HorizontalScrollBarVisibilityRequirement
        /// </summary>
        public ScrollBarVisibility HorizontalScrollBarVisibilityRequirement
        {
            get { return _horizontalScrollBarVisibilityRequirement; }
            set { this.SetValue(ref _horizontalScrollBarVisibilityRequirement, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsLeftDrawerOpen
        /// </summary>
        public bool IsLeftDrawerOpen
        {
            get { return _isLeftDrawerOpen; }
            set { this.SetValue(ref _isLeftDrawerOpen, value); }
        }

        /// <summary>
        /// Gets or sets the MarginRequirement
        /// </summary>
        public Thickness MarginRequirement
        {
            get { return _marginRequirement; }
            set { this.SetValue(ref _marginRequirement, value); }
        }

        /// <summary>
        /// Gets or sets the SelectedSideBarItem
        /// </summary>
        public SideBarItem SelectedSideBarItem
        {
            get { return _selectedSideBarItem; }
            set
            {
                this.SetValue(ref _selectedSideBarItem, value);
                SetContentFromSideBar();
            }
        }

        /// <summary>
        /// Gets or sets the MainAppLoadedCommand
        /// </summary>
        public DelegateCommand MainAppLoadedCommand { get; set; }

        /// <summary>
        /// Gets the SideBarItems
        /// </summary>
        public ObservableCollection<SideBarItem> SideBarItems { get; }

        /// <summary>
        /// Gets or sets the VerticalScrollBarVisibilityRequirement
        /// </summary>
        public ScrollBarVisibility VerticalScrollBarVisibilityRequirement
        {
            get { return _verticalScrollBarVisibilityRequirement; }
            set { this.SetValue(ref _verticalScrollBarVisibilityRequirement, value); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The OnDrawerItemSelected
        /// </summary>
        /// <param name="e">The e<see cref="RoutedPropertyChangedEventArgs{object}"/></param>
        private void OnDrawerItemSelected(RoutedPropertyChangedEventArgs<object> e)
        {
            ISideBarItem selectedItem = e.NewValue as ISideBarItem;
            SelectedSideBarItem = null;
            SelectedSideBarItem = SideBarItems.FirstOrDefault(a => a.Header == selectedItem.MenuHeader);

            _eventAggregator.GetEvent<PubSubEvent<ISideBarItem>>().Publish(selectedItem);

            IsLeftDrawerOpen = false;
        }

        /// <summary>
        /// The SetContentFromSideBar
        /// </summary>
        private void SetContentFromSideBar()
        {
            string navigateTo = "";
            switch (SelectedSideBarItem?.Header)
            {
                case "Home":
                    navigateTo = "Dashboard";
                    break;
                case "Settings":
                    navigateTo = "Settings";
                    break;
                case "Tools":
                    navigateTo = "Tools";
                    break;
                case "Help":
                    navigateTo = "Help";
                    break;
            }
            if (!string.IsNullOrEmpty(navigateTo))
                _regionManager.RequestNavigate(RegionNames.ContentRegion, navigateTo);
        }

        #endregion
    }
}
