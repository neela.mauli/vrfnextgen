﻿using JCHVRF.BLL;
using JCHVRF.BLL.New;
using JCHVRF.Entity;
using JCHVRF.Model;
using JCHVRF_New.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using JCHVRF.VRFMessage;
using System.Windows.Forms;
using JCBase.UI;

namespace JCHVRF_New.ViewModels
{
    public class AddAccessoryViewModel : INotifyPropertyChanged
    {
        public string BrandCode { get; set; }
        public string Region { get; set; }
        public ObservableCollection<AccessoryModel> listAccessory
        {
            get; set;
        }

        public ObservableCollection<AccessoryModel> Accessories
        {

            get
            {
                if (_accessories == null)
                {
                    _accessories = new ObservableCollection<AccessoryModel>();
                    GetSelectedIDUAccessories();
                }
                return _accessories;
            }
            set
            {
                _accessories = value;
                OnPropertyChanged("Accessories");

            }
        }
        private ObservableCollection<AccessoryModel> _accessories;

        public RoomIndoor SelectedRoomIndoor { get; set; }

        public JCHVRF.Model.Project ProjectLegacy { get; set; }
        public AddAccessoryViewModel(JCHVRF.Model.Project projectLegacy,RoomIndoor selectedRoomIndoor)
        {
            this.ProjectLegacy = projectLegacy;
            this.Cancelcommand = new RelayCommand(this.Cancel);
            this.AddAccessoriesCommand = new RelayCommand(this.AddAccessories, CanAddExecute);
            this.RemoveFromAllCommand = new RelayCommand(this.RemoveFromAllAccessories, CanRemoveAllExecute);
            this.SelectedRoomIndoor = selectedRoomIndoor;
            GetAccessory();
        }

        public event PropertyChangedEventHandler PropertyChanged;


        #region RelayCommand 
        public RelayCommand Cancelcommand { get; set; }

        public RelayCommand AddAccessoriesCommand { get; set; }

        public RelayCommand RemoveFromAllCommand { get; set; }

        List<AccessoryModel> SelectedAccessories { get; set; }
        #endregion


        #region priavate methods
        private void Cancel(object parameter)
        {
            Window window = parameter as Window;
            if (window != null)
                window.Close();
        }

        private bool CanCancelExecute(object parameter)
        {
            return true;
        }

        private bool CanAddExecute(object parameter)
        {

            var accessoriesItems = parameter as ObservableCollection<AccessoryModel>;
            if (accessoriesItems != null)
            {
                var selectedItems = (from s in accessoriesItems where s.IsSelect == true select s).ToList();
                var applyToSimilar =
                    (from a in accessoriesItems where a.IsApplyToSimilarUnit == true select a).ToList();
                if (selectedItems.Count > 0 || applyToSimilar.Count > 0)
                    return true;
            }

            return false;
        }

        private bool CanRemoveAllExecute(object parameter)
        {

            var accessoriesItems = parameter as ObservableCollection<AccessoryModel>;
            if (accessoriesItems != null)
            {
                var selectedItems = (from s in accessoriesItems where s.IsSelect == true select s).ToList();
                if (selectedItems.Count > 0)
                    return true;
            }

            return false;
        }

        private void AddAccessories(object parameter)
        {

            Indoor indoor = ProjectLegacy.RoomIndoorList.Find(x => x.IndoorNO == SelectedRoomIndoor.IndoorNO).IndoorItem;
            indoor.ListAccessory = new List<Accessory>();
            var accessoriesItems = parameter as ObservableCollection<AccessoryModel>;
            if (accessoriesItems != null)
            {
                var selectedItems = (from s in accessoriesItems where s.IsSelect == true select s).ToList();
                var applyToSimilar = (from a in accessoriesItems where a.IsApplyToSimilarUnit == true select a).ToList();
                var applyToSimilarOnly = (from a in accessoriesItems where a.IsApplyToSimilarUnit == true && a.IsSelect != true select a);
                foreach (AccessoryModel accessory in selectedItems)
                {

                    Accessory acc = new Accessory()
                    {
                        Type = accessory.Type,
                        BrandCode = ProjectLegacy.BrandCode,
                        FactoryCode = ProjectLegacy.FactoryCode,
                        IsDefault = false, //? todo 
                        IsShared = false,//todo
                        MaxCapacity = 10,//todo
                        Model_Hitachi = accessory.Model,
                        MaxNumber = accessory.Count,
                        MinCapacity = 5,
                        Model_York = "",//todo
                        UnitType = ""//todo
                    };


                    //select and apply to similar
                    List<RoomIndoor> roomIndoor = new List<RoomIndoor>();
                    if (applyToSimilar.Contains(accessory))
                    {
                        var indoorlist =
                            (from inlist in ProjectLegacy.RoomIndoorList where inlist.IndoorItem.Type == accessory.Type select inlist.IndoorItem).ToList();
                        foreach (Indoor indoorUnit in indoorlist)
                        {                           
                            indoorUnit.ListAccessory.Add(acc);                            
                        }
                        
                    }
                    
                    indoor.ListAccessory.Add(acc);
                    GetSelectedIDUAccessories();
                }


                //apply to similar only
                foreach (AccessoryModel accessory in applyToSimilarOnly)
                {

                    Accessory acc = new Accessory()
                    {
                        Type = accessory.Type,
                        BrandCode = ProjectLegacy.BrandCode,
                        FactoryCode = ProjectLegacy.FactoryCode,
                        IsDefault = false, //? todo 
                        IsShared = false,//todo
                        MaxCapacity = 10,//todo
                        Model_Hitachi = accessory.Model,
                        MaxNumber = accessory.Count,
                        MinCapacity = 5,
                        Model_York = "",//todo
                        UnitType = ""//todo
                    };



                    //apply to similar only
                    
                        List<RoomIndoor> roomIndoor = new List<RoomIndoor>();
                        if (ProjectLegacy.RoomIndoorList != null && ProjectLegacy.RoomIndoorList.Count() > 0)
                        {
                            var selectedIDU = ProjectLegacy.RoomIndoorList.Find(x => x.IndoorNO == SelectedRoomIndoor.IndoorNO);
                            var indoorlist =
                           (from inlist in ProjectLegacy.RoomIndoorList where inlist.IndoorItem.Type == accessory.Type select inlist).ToList();
                            foreach (RoomIndoor indoorUnit in indoorlist)
                            {
                                RoomIndoor ri = new RoomIndoor();

                                //if apply to similar checked  but not select box checked
                                if (indoorUnit.IndoorNO == selectedIDU.IndoorNO)
                                    continue;
                               if(indoorUnit.IndoorItem!=null)
                                    indoorUnit.IndoorItem.ListAccessory.Add(acc);                               
                            }
                        }
                        //ProjectLegacy.RoomIndoorList = roomIndoor;
                        GetSelectedIDUAccessories();                    

                }

            }
            OnPropertyChanged("Accessories");
            IDUEquipmentPropertiesViewModel vm = new ViewModels.IDUEquipmentPropertiesViewModel(this.ProjectLegacy, SelectedRoomIndoor);

        }

        private void RemoveFromAllAccessories(object parameter)
        {
            RoomIndoor ri = ProjectLegacy.RoomIndoorList.Find(x => x.IndoorNO == SelectedRoomIndoor.IndoorNO);
            if (JCMsg.ShowConfirmYesNoCancel("Are you sure want to remove Accessories", true) == DialogResult.Yes)
            {
                var accessoriesItems = parameter as ObservableCollection<AccessoryModel>;
                if (accessoriesItems != null)
                {
                    var selectedItems = (from s in accessoriesItems where s.IsSelect == true select s).ToList();

                    foreach (AccessoryModel accessory in selectedItems)
                    {

                        if (ri != null && ri.IndoorItem != null && ri.IndoorItem.ListAccessory.Count > 0)
                        {
                            if(ri.ListAccessory!=null && ri.ListAccessory.Count>0)
                            ri.ListAccessory.RemoveAll(c => c.Type == accessory.Type);
                            if (ri.IndoorItem.ListAccessory != null && ri.IndoorItem.ListAccessory.Count > 0)
                                ri.IndoorItem.ListAccessory.RemoveAll(a => a.Type == accessory.Type);
                            accessory.IsSelect = false;
                        }

                    }
                }
                //ProjectLegacy.RoomIndoorList = new List<RoomIndoor>();
                //ProjectLegacy.RoomIndoorList.Add(ri);
                GetSelectedIDUAccessories();
                OnPropertyChanged("Accessories");
                IDUEquipmentPropertiesViewModel vm = new ViewModels.IDUEquipmentPropertiesViewModel(this.ProjectLegacy, ri);
            }

        }
        private RoomIndoor GetRoomIndoorDetial(RoomIndoor IndoorNo)
        {
            RoomIndoor ind = new RoomIndoor();
            //if (isExc)
            //    ind = projectLegacy.ExchangerList.Find(p => p.IndoorNO == IndoorNo);
            //else
            //ind = projectLegacy.RoomIndoorList.Find(p => p.IndoorNO == IndoorNo);
            return ind;
        }

        private void GetAccessory()
        {
            List<AccessoryModel> objListAccessory = new List<AccessoryModel>();
            AccessoryBLL accessoryBill = new AccessoryBLL();
            //ProjectLegacy.RoomIndoorList.Where(x => x.IndoorName.Equals("selectedIndoorUnitName")).FirstOrDefault();
            var ri = ProjectLegacy.RoomIndoorList.Find(x => x.IndoorNO == SelectedRoomIndoor.IndoorNO);
            //var appliedAccesories = indoor.ListAccessory;
            //var allCompatibleAccessoriesForTypeAndSeries = "";

            //RoomIndoor objRoomIndore = new RoomIndoor();
            //objRoomIndore.IndoorItem = new Indoor
            //{
            //    //ToRemove : 
            //    Series = "Commercial VRF HP, HNCQ",
            //    Model_Hitachi = "PCC-1A"
            //};
            //BrandCode = "H";
            // Need to Pass Indoor Item object this is dependend with Project class object
            //"H", "S", "Two Way Cassette", 4, "EU_W", objRoomIndore, "GBR"
            DataTable dtAccessory = new DataTable();
            if (ri != null && ri.IndoorItem != null)
            {
                ri.IndoorItem.Series = "Commercial VRF HP, HNCQ";
                ri.IndoorItem.Model_Hitachi = "PCC-1A";

                //dtAccessory = accessoryBill.GetInDoorAccessoryItemList(ProjectLegacy.BrandCode,
                //      ProjectLegacy.FactoryCode, ri.IndoorItem.Type, ri.IndoorItem.CoolingCapacity,
                //      ProjectLegacy.RegionCode, ri, ProjectLegacy.SubRegionCode);


                dtAccessory = accessoryBill.GetInDoorAccessoryItemList("H",
                    "S", "Two Way Cassette", 4,
                    "EU_W", ri, "GBR");

            }

            if (ProjectLegacy.BrandCode == "H")
            {
                objListAccessory = (from DataRow row in dtAccessory.Rows
                                    select new AccessoryModel
                                    {
                                        Type = row["Type"].ToString(),
                                        Model = row["Model_Hitachi"].ToString(),
                                        Description = string.Empty, // To Do need to more annalysis
                                        Count = 1,
                                        MaxCount = Convert.ToInt32(row["MaxNumber"]),
                                        IsSelect = false,
                                        IsApplyToSimilarUnit = false

                                    }).ToList();
            }
            else
            {
                objListAccessory = (from DataRow row in dtAccessory.Rows
                                    select new AccessoryModel
                                    {
                                        Type = row["Type"].ToString(),
                                        Model = row["Model_York"].ToString(),
                                        Description = string.Empty, // To Do need to more annalysis
                                        Count = 1,
                                        MaxCount = Convert.ToInt32(row["MaxNumber"]),
                                        IsSelect = false,
                                        IsApplyToSimilarUnit = false

                                    }).ToList();
            }
            List<AccessoryModel> finalListAccessory = new List<AccessoryModel>();
            List<string> distictType = new List<string>();
            var result = objListAccessory.Select(x => x.Type).Distinct();
            foreach (var type in result)
            {
                distictType.Add(type);
            }
            foreach (var item in distictType)
            {
                finalListAccessory.Add(new AccessoryModel
                {
                    Type = item,
                    ModelName = objListAccessory.Where(x => x.Type.Equals(item)).Select(y => y.Model).ToList(),
                    Model = objListAccessory.Where(x => x.Type.Equals(item)).Select(y => y.Model).FirstOrDefault(),
                    IsSelect = false,
                    IsApplyToSimilarUnit = false,
                    Count = 1,
                    MaxCount = Convert.ToInt32(objListAccessory.Where(x => x.Type.Equals(item)).Select(y => y.MaxCount).FirstOrDefault())

                });

            }
            listAccessory = new ObservableCollection<AccessoryModel>(finalListAccessory);
        }


        private void GetSelectedIDUAccessories()
        {

            var accessories = new ObservableCollection<AccessoryModel>();
            if (ProjectLegacy.RoomIndoorList != null)
            {

                var roomIndoor = ProjectLegacy.RoomIndoorList.Find(x => x.IndoorNO == SelectedRoomIndoor.IndoorNO);
                if (roomIndoor != null && roomIndoor.IndoorItem != null)
                {

                    if (roomIndoor.IndoorItem.ListAccessory != null && roomIndoor.IndoorItem.ListAccessory.Count > 0)
                    {
                        foreach (Accessory accessory in roomIndoor.IndoorItem.ListAccessory)
                        {
                            accessories.Add(new AccessoryModel() { Type = accessory.Type, Count = accessory.MaxNumber });
                        }
                    }
                    else if (roomIndoor.ListAccessory != null && roomIndoor.ListAccessory.Count > 0)
                    {
                        foreach (Accessory accessory in roomIndoor.ListAccessory)
                        {
                            accessories.Add(new AccessoryModel() { Type = accessory.Type, Count = accessory.MaxNumber });
                        }
                    }
                }
                _accessories = accessories;
            }

        }
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

