﻿/****************************** File Header ******************************\
File Name:	SystemDetailsViewModel.cs
Date Created:	2/20/2019
Description:	
\*************************************************************************/

namespace JCHVRF_New.ViewModels
{
    using System.Collections.Generic;
    using JCBase.Utility;
    using JCHVRF.Model;
    using JCHVRF_New.Common.Contracts;
    using JCHVRF_New.Common.Helpers;
    using JCHVRF_New.Model;

    public class SystemDetailsViewModel : ViewModelBase, ILayoutItem
    {
        #region Fields

        private double _FloatingHeight;

        private double _FloatingLeft;

        private double _FloatingTop;

        private double _FloatingWidth;

        private string _title;

        public List<IDUODUCapacityDetails> ListIndoorCapacitydetails;
        public List<IDUODUCapacityDetails> ListOutdoorCapacitydetails;
        public List<IDUODUCapacityDetails> ListAdditionalCapacitydetails;

        public string powerUnit;
        public string weightUnit;

        #endregion

        #region Constructors

        public SystemDetailsViewModel()
        {
            Title = "System Details";
            powerUnit = SystemSetting.UserSetting.unitsSetting.settingPOWER;
            weightUnit = SystemSetting.UserSetting.unitsSetting.settingWEIGHT;
            BindIndoorCapacityDetails();
            BindOutdoorCapacityDetails();
            BindAdditionalCapacityDetails();

            IDUODUCapacityDetails objIDUODUCapacityDetails = Instance;
            objIDUODUCapacityDetails.ActualRatio = 100;
            objIDUODUCapacityDetails.minConnections = 10;
            objIDUODUCapacityDetails.maxConnections = 16;
            objIDUODUCapacityDetails.AdditionalRefrigerantQty = Unit.ConvertToControl(100, UnitType.WEIGHT, weightUnit);

        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the FloatingHeight
        /// </summary>
        public double FloatingHeight
        {
            get { return this._FloatingHeight; }
            set { this.SetValue(ref _FloatingHeight, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingLeft
        /// </summary>
        public double FloatingLeft
        {
            get { return this._FloatingLeft; }
            set { this.SetValue(ref _FloatingLeft, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingTop
        /// </summary>
        public double FloatingTop
        {
            get { return this._FloatingTop; }
            set { this.SetValue(ref _FloatingTop, value); }
        }

        /// <summary>
        /// Gets or sets the FloatingWidth
        /// </summary>
        public double FloatingWidth
        {
            get { return this._FloatingWidth; }
            set { this.SetValue(ref _FloatingWidth, value); }
        }

        /// <summary>
        /// Gets or sets the Title
        /// </summary>
        public string Title
        {
            get { return this._title; }
            set { this.SetValue(ref _title, value); }
        }

        /// <summary>
        /// Gets the Model Instance
        /// </summary>
        public IDUODUCapacityDetails Instance
        {
            get { return new IDUODUCapacityDetails(); }
           
        }

        public string SystemSettings { get; private set; }
        #endregion

        #region Methods
        public void BindIndoorCapacityDetails()
        {
            ListIndoorCapacitydetails = new List<IDUODUCapacityDetails>();
            IDUODUCapacityDetails obj1 = Instance;
            obj1.RatedCapacity = Unit.ConvertToControl(50.00, UnitType.POWER, powerUnit);
            obj1.CorrectedCapacity = Unit.ConvertToControl(51.04, UnitType.POWER, powerUnit);

            ListIndoorCapacitydetails.Add(obj1);

            IDUODUCapacityDetails obj2 = Instance;
            obj2.RatedCapacity = Unit.ConvertToControl(56.00, UnitType.POWER, powerUnit);
            obj2.CorrectedCapacity = Unit.ConvertToControl(58.40, UnitType.POWER, powerUnit);

            ListIndoorCapacitydetails.Add(obj2);
        }

        public void BindOutdoorCapacityDetails()
        {
            ListOutdoorCapacitydetails = new List<IDUODUCapacityDetails>();
            IDUODUCapacityDetails obj1 = Instance;
            obj1.RatedCapacity = Unit.ConvertToControl(50.00, UnitType.POWER, powerUnit);
            obj1.CorrectedCapacity = Unit.ConvertToControl(51.04, UnitType.POWER, powerUnit);

            ListOutdoorCapacitydetails.Add(obj1);

            IDUODUCapacityDetails obj2 = Instance;
            obj2.RatedCapacity = Unit.ConvertToControl(56.00, UnitType.POWER, powerUnit);
            obj2.CorrectedCapacity = Unit.ConvertToControl(58.40, UnitType.POWER, powerUnit);

            ListOutdoorCapacitydetails.Add(obj2);

            IDUODUCapacityDetails obj3 = Instance;
            obj3.RatedCapacity = Unit.ConvertToControl(56.00, UnitType.POWER, powerUnit);
            obj3.CorrectedCapacity = Unit.ConvertToControl(58.40, UnitType.POWER, powerUnit);

            ListOutdoorCapacitydetails.Add(obj3);
        }

        public void BindAdditionalCapacityDetails()
        {
            ListAdditionalCapacitydetails = new List<IDUODUCapacityDetails>();
            IDUODUCapacityDetails obj1 = Instance;
            obj1.RatedCapacity = 0.0;
            obj1.CorrectedCapacity = 0.0;
            ListOutdoorCapacitydetails.Add(obj1);


        }
        #endregion
    }
}
