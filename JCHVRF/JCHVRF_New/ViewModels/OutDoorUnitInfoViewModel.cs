﻿using JCHVRF.Model;
using JCHVRF.Model.NextGen;
using JCHVRF_New.Common.Helpers;
using JCHVRF_New.Model;
using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace JCHVRF_New.ViewModels
{
    class OutDoorUnitInfoViewModel : ViewModelBase
    {
        #region Fields
        JCHVRF.BLL.OutdoorBLL bll;
        string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
        string navigateToFolder = "..\\..\\Image\\TypeImageProjectCreation";
        #endregion Fields

        #region View_Model_Property

        private ObservableCollection<string> listProductType;
        public ObservableCollection<string> ListProductType
        {
            get { return listProductType; }
            set
            {

                value = listProductType;

            }
        }


        private string _selectedProductType;
        public string SelectedProductType
        {
            get
            {
                return _selectedProductType;
            }
            set
            {
                _selectedProductType = value;
                OnPropertyChanged("SelectedProductType");
                GetSeries(_selectedProductType);


            }
        }


        private ObservableCollection<int> listMaxRatio;
        public ObservableCollection<int> ListMaxRatio
        {
            get { return listMaxRatio; }
            set { listMaxRatio = value; }
        }


        private ObservableCollection<SeriesModel> listSeries;
        public ObservableCollection<SeriesModel> ListSeries
        {
            get { return listSeries; }
            set { this.SetValue(ref listSeries, value); }

        }


        private string _selectedSeries;
        public string SelectedSeries
        {
            get
            {
              
                return _selectedSeries;
            }
            set
            {
               
                this.SetValue(ref _selectedSeries, value);
                if (_selectedSeries != null)
                {
                   
                    BindOutDoorImageUI(SelectedSeries);
                }

               
            }
        }

        private int _selectedIndex;
        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                _selectedIndex = value;
                OnPropertyChanged("SelectedIndex");

            }
        }

        private int _selectedMaxRatio;
        public int SelectedMaxRatio
        {
            get
            {
                return _selectedMaxRatio;
            }
            set
            {
                _selectedMaxRatio = value;
                OnPropertyChanged("SelectedMaxRatio");

            }
        }

        private ObservableCollection<Outdoor> _listOutdoor;
        public ObservableCollection<Outdoor> _ListOutdoor
        {
            get
            {
                return _listOutdoor;
            }
            set
            {
                this.SetValue(ref _listOutdoor, value);
            }
        }

        private string _OduImagePath=string.Empty;
        public string OduImagePath
        {
            get { return _OduImagePath; }
            set { this.SetValue(ref _OduImagePath, value); }
        }
        private IEventAggregator _eventAggregator;
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion  View_Model_Property
        public OutDoorUnitInfoViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            JCHVRF.Model.Project.CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            bll = new JCHVRF.BLL.OutdoorBLL(JCHVRF.Model.Project.CurrentProject.SubRegionCode, JCHVRF.Model.Project.CurrentProject.BrandCode);
            GetProductType();
            BindRatio();
            _eventAggregator.GetEvent<ODUTypeTabNext>().Subscribe(OduTypeNextClick);

        }

        #region Methods

        private void BindOutDoorImageUI(string series)
        {
            var sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            if (ListSeries.Where(MM => MM.DisplayName == series).FirstOrDefault() != null)
                OduImagePath = ListSeries.Where(MM => MM.DisplayName == series).FirstOrDefault().OduImagePath;
            this. OduImagePath = OduImagePath;

        }
        private void OduTypeNextClick()
        {
            if (OutDoorInfoProjectLegacy())
            {
                if(JCHVRF.Model.Project.CurrentProject.SystemListNextGen!=null)
                {
                    int SysIndex = JCHVRF.Model.Project.CurrentProject.SystemListNextGen.FindIndex(sys => sys.IsActiveSystem == true);
                    if(SysIndex==0)
                    {
                        JCHVRF.Model.Project.CurrentProject.SystemListNextGen[SysIndex].OutdoorItem = new Outdoor
                        {
                            ProductType = SelectedProductType,
                            Series = SelectedSeries,
                            MaxRatio = SelectedMaxRatio
                        };
                    }
                   

                }
               
                _eventAggregator.GetEvent<OduTabSubscriber>().Publish(true);
            }

            else
            {
                _eventAggregator.GetEvent<OduTabSubscriber>().Publish(false);
            }



        }

        bool OutDoorInfoProjectLegacy()
        {

            string errorMessage = null;
            bool IsValid = ValidateOutdoorDataValid(out errorMessage);
            if (IsValid)
            {
            }
            else
            {
                MessageBox.Show(errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            return IsValid;
        }

        public bool ValidateOutdoorDataValid(out string errormessage)
        {

            bool IsValid = true;
            errormessage = "";

            if (string.IsNullOrEmpty(SelectedProductType))
            {
                errormessage = "Product Type cannot be blank";
                IsValid = false;

            }
            else if (string.IsNullOrEmpty(SelectedSeries))
            {
                errormessage = "Series cannot be blank";
                IsValid = false;
            }


            return IsValid;
        }

        #endregion Methods

        #region Binding Data
        private void GetProductType()
        {
            listProductType = new ObservableCollection<string>();
            DataTable dtProductType = bll.GetOutdoorListStd();
            foreach (DataRow producttypeRow in dtProductType.Rows)
            {
                if (this.ListProductType.Contains((string)producttypeRow["ProductType"]))
                {
                    continue;
                }
                else
                {
                    this.ListProductType.Add((string)producttypeRow["ProductType"]);
                }

            }

        }

        private void GetSeries(string selectedProductType)
        {
            var sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            ListSeries = new ObservableCollection<SeriesModel>();

            DataTable dtSeries = bll.GetOutdoorListStd(selectedProductType);
            foreach (DataRow dtSeriesTypeRow in dtSeries.Rows)
            {
                if (ListSeries.Count == 0)
                {
                    ListSeries.Add(new SeriesModel()
                    {
                        DisplayName = dtSeriesTypeRow.ItemArray[0].ToString(),
                        SelectedValues = dtSeriesTypeRow.ItemArray[0].ToString(),
                        OduImagePath = sourceDir + "\\" + Convert.ToString(dtSeriesTypeRow.ItemArray[1].ToString())
                    });
                }
                else
                {
                    if (ListSeries.Any(MM => MM.DisplayName == dtSeriesTypeRow.ItemArray[0].ToString()))
                    {
                        continue;
                    }
                    else
                    {
                        ListSeries.Add(new SeriesModel()
                        {
                            DisplayName = dtSeriesTypeRow.ItemArray[0].ToString(),
                            SelectedValues = dtSeriesTypeRow.ItemArray[0].ToString(),
                            OduImagePath = sourceDir + "\\" + Convert.ToString(dtSeriesTypeRow.ItemArray[1].ToString())
                        });
                    }

                }

            }
        }

        private void BindRatio()
        {
            listMaxRatio = new ObservableCollection<int>();
            double max = 1.3;
            double min = 0.5;
            double defaultMaxRatio = 1;
            defaultMaxRatio = 0;
            for (int i = (int)(max * 100); i >= min * 100; i -= 10)
            {
                listMaxRatio.Add(i);
            }
            this.ListMaxRatio = listMaxRatio;
            this.SelectedIndex = (int)(Math.Round(max - defaultMaxRatio, 2) * 10);
        }

        #endregion Binding Data



    }


}


