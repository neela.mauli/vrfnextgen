﻿using JCHVRF_New.Common.Helpers;
using JCHVRF_New.Model;
using Prism.Commands;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using JCBase.UI;
using JCBase.Utility;
using JCHVRF.Model;
using System.Collections.Generic;
using Prism.Events;
using System.Data;
using System.Linq;

namespace JCHVRF_New.ViewModels
{
    class TotalHeatExUnitInfoViewModel : ViewModelBase
    {
        #region Fields
        JCHVRF.BLL.OutdoorBLL bll;
        string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
        string navigateToFolder = "..\\..\\Image\\TypeImageProjectCreation";
        JCHVRF.Model.Project thisProject;
        private IEventAggregator _eventAggregator;
        string ut_length = string.Empty;
        JCHVRF.Model.DefaultSettingModel designConditionsLegacy = SystemSetting.UserSetting.defaultSetting;
        const string Celcisous = "°C";
        const string Farienhiet = "°F";
        const string ChangeToF = "Change To °F";
        const string ChangeToC = "Change To °C";
        JCHVRF.DAL.IndoorDAL _dal;

        // JCHVRF.Model.Project CurrentProject;
        private JCHVRF.Model.Project _project;
        #endregion Fields
        public TotalHeatExUnitInfoViewModel(IEventAggregator EventAggregator, JCHVRF_New.Model.Project thisProj)
        {
            //  ChangeRoom =new DelegateCommand(RoomSelectionChanged);
            ChangeTempCommand = new DelegateCommand(btnChangeTempUClicked);
            AddFloorCommand = new DelegateCommand(OnAddFloorClicked);
            AddEditRoomCommand = new DelegateCommand(OnAddEditRoomClicked);
            _eventAggregator = EventAggregator;
            _eventAggregator.GetEvent<RoomListSaveSubscriber>().Subscribe(GetRoomList);
            this.UseRoomTemperature = false;
            _project = JCHVRF.Model.Project.CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            outdoorHeatingRH = Convert.ToDecimal(Unit.ConvertToControl(Convert.ToDouble(designConditionsLegacy.OutdoorHeatingRH), UnitType.TEMPERATURE, SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE));
            Initialisationvalues();
            bll = new JCHVRF.BLL.OutdoorBLL(JCHVRF.Model.Project.CurrentProject.SubRegionCode, JCHVRF.Model.Project.CurrentProject.BrandCode);
            _dal = new JCHVRF.DAL.IndoorDAL(JCHVRF.Model.Project.CurrentProject.RegionCode, JCHVRF.Model.Project.CurrentProject.BrandCode);
            //BindInternalDesignConditions();
            BindFanSpeed();
            BindFloor();
            BindSeries();

            // BindPeople();
            //   SystemName = SystemSetting.UserSetting.defaultSetting.FreshAirAreaName + " " + JCHVRF.Model.Project.GetProjectInstance.SystemList.Count() + 1;


            WorkFlowContext.FloorNames = new List<string>();

        }

        private void BindPeople()
        {
            if (JCHVRF.Model.Project.GetProjectInstance.FloorList[0].RoomList.Count > 0)
            {
                JCHVRF.Model.Project.GetProjectInstance.FloorList[0].RoomList[0].PeopleNumber = this.NoOfPeople;
            }
        }

        private void BindFreshAir()
        {


            if (JCHVRF.Model.Project.GetProjectInstance.FloorList[0].RoomList.Count > 0)
            {
                JCHVRF.Model.Project.GetProjectInstance.FloorList[0].RoomList[0].FreshAir = this.FreshAir;
            }
        }
        private void GetRoomList()
        {

            if (JCHVRF.Model.Project.GetProjectInstance.FloorList.Count > 0)
                if (JCHVRF.Model.Project.GetProjectInstance.FloorList[0].RoomList.Count > 0)
                {
                    //CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
                    _project = JCHVRF.Model.Project.GetProjectInstance;
                    RoomName = new ObservableCollection<Room>(JCHVRF.Model.Project.GetProjectInstance.FloorList[0].RoomList);


                }

        }

        //getting series from database
        private void GetSeriesTHeatEx()
        {

            //string strregion=   JCHVRF.Model.Project.GetProjectInstance.RegionCode + "/" + JCHVRF.Model.Project.GetProjectInstance.SubRegionCode + "," + JCHVRF.Model.Project.GetProjectInstance.BrandCode;
            var sourceDir = System.IO.Path.Combine(defaultFolder, navigateToFolder);
            ListSeries = new ObservableCollection<SeriesModel>();

            DataTable dtSeries = bll.GetOutdoorListStdSeries(JCHVRF.Model.Project.GetProjectInstance.SubRegionCode);
            foreach (DataRow dtSeriesTypeRow in dtSeries.Rows)
            {
                if (ListSeries.Count == 0)
                {
                    ListSeries.Add(new SeriesModel()
                    {
                        DisplayName = dtSeriesTypeRow.ItemArray[0].ToString(),
                        SelectedValues = dtSeriesTypeRow.ItemArray[0].ToString(),

                    });
                }
                else
                {
                    if (ListSeries.Any(MM => MM.DisplayName == dtSeriesTypeRow.ItemArray[0].ToString()))
                    {
                        continue;
                    }
                    else
                    {
                        ListSeries.Add(new SeriesModel()
                        {
                            DisplayName = dtSeriesTypeRow.ItemArray[0].ToString(),
                            SelectedValues = dtSeriesTypeRow.ItemArray[0].ToString(),

                        });
                    }

                }


            }



        }
        //private void BindPowerMode()
        //{

        // . IndoorBLL indoor = new JCHVRF.BLL.IndoorBLL(thisProject.SubRegionCode, thisProject.BrandCode);

        //    DataTable dtPower = indoor.GetPowerSupplyList();
        //    DataTable dtList = Global.InitExchangerTypeList(this.thisProject);
        //    if (dtList.Rows.Count > 0)
        //    {





        //    }
        //}



        private void BindSeries()
        {

        }

        void Initialisationvalues()
        {

            TemperatureTypeOCDB = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;//updated as part of default measurement units

            TemperatureTypeOHIW = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            TemperatureTypeOCWB = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            TemperatureTypeOHDB = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            TemperatureTypeICDB = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;


            if (string.IsNullOrEmpty(UnitName))
                UnitName = "Exc Unit 01";

        }

        private void OnAddFloorClicked()
        {
            WorkFlowContext.FloorNames = new List<string>();
            Views.AddEditFloor newFloorWindow = new Views.AddEditFloor();
            newFloorWindow.ShowDialog();

            BindFloor();

        }
        private void OnAddEditRoomClicked()
        {

            Views.AddEditRoom addEditRoomView = new Views.AddEditRoom();
            addEditRoomView.ShowDialog();

        }



        void btnChangeTempUClicked()
        {

            CurrentTempUnit = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE;
            CurrentTempUnit = SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE = CurrentTempUnit == Unit.ut_Temperature_c ? Unit.ut_Temperature_f : Unit.ut_Temperature_c;




            if (CurrentTempUnit == Unit.ut_Temperature_f)
            {

                outdoorCoolingDB = Convert.ToDecimal(Unit.ConvertToControl(Convert.ToDouble(outdoorCoolingDB), UnitType.TEMPERATURE, CurrentTempUnit));
                outdoorHeatingDB = Convert.ToDecimal(Unit.ConvertToControl(Convert.ToDouble(outdoorHeatingDB), UnitType.TEMPERATURE, CurrentTempUnit));
                //outdoorHeatingWB = Convert.ToDecimal(Unit.ConvertToControl(Convert.ToDouble(outdoorHeatingWB), UnitType.TEMPERATURE, CurrentTempUnit));
                outdoorCoolingIW = Convert.ToDecimal(Unit.ConvertToControl(Convert.ToDouble(outdoorCoolingIW), UnitType.TEMPERATURE, CurrentTempUnit));
                // outdoorHeatingIW = Convert.ToDecimal(Unit.ConvertToControl(Convert.ToDouble(outdoorHeatingIW), UnitType.TEMPERATURE, CurrentTempUnit));
            }
            else
            {
                //indoorCoolingDB = Convert.ToDecimal(Unit.ConvertToSource(Convert.ToDouble(indoorCoolingDB), UnitType.TEMPERATURE, Unit.ut_Temperature_f));
                //indoorCoolingWB = Convert.ToDecimal(Unit.ConvertToSource(Convert.ToDouble(indoorCoolingWB), UnitType.TEMPERATURE, Unit.ut_Temperature_f));
                //indoorHeatingDB = Convert.ToDecimal(Unit.ConvertToSource(Convert.ToDouble(indoorHeatingDB), UnitType.TEMPERATURE, Unit.ut_Temperature_f));
                outdoorCoolingDB = Convert.ToDecimal(Unit.ConvertToSource(Convert.ToDouble(outdoorCoolingDB), UnitType.TEMPERATURE, Unit.ut_Temperature_f));
                outdoorHeatingDB = Convert.ToDecimal(Unit.ConvertToSource(Convert.ToDouble(outdoorHeatingDB), UnitType.TEMPERATURE, Unit.ut_Temperature_f));
                // outdoorHeatingWB = Convert.ToDecimal(Unit.ConvertToSource(Convert.ToDouble(outdoorHeatingWB), UnitType.TEMPERATURE, Unit.ut_Temperature_f));
                // outdoorCoolingIW = Convert.ToDecimal(Unit.ConvertToSource(Convert.ToDouble(outdoorCoolingIW), UnitType.TEMPERATURE, Unit.ut_Temperature_f));
                // outdoorHeatingIW = Convert.ToDecimal(Unit.ConvertToSource(Convert.ToDouble(outdoorHeatingIW), UnitType.TEMPERATURE, Unit.ut_Temperature_f));
            }


            // TemperatureTypeOCDB = TemperatureTypeOCWB = TemperatureTypeOHDB = TemperatureTypeICDB = TemperatureTypeOHIW = CurrentTempUnit;

        }


        #region BindFanSpeed

        private void BindFanSpeed()
        {
            FanSpeeds = new ObservableCollection<TotalHeatExUnitInfoModel>() {
            new TotalHeatExUnitInfoModel(){FanSpeeds="Max"}
            ,new TotalHeatExUnitInfoModel(){FanSpeeds="High2"}
            ,new TotalHeatExUnitInfoModel(){FanSpeeds="High"}
            ,new TotalHeatExUnitInfoModel(){FanSpeeds="Medium"}
            ,new TotalHeatExUnitInfoModel() {FanSpeeds="Low" }
            };
        }


        private void BindFloor()
        {
            if (WorkFlowContext.FloorNames != null)
            {
                if (FloorList == null)
                    FloorList = new ObservableCollection<TotalHeatExUnitInfoModel>();

                for (int i = 0; i < WorkFlowContext.FloorNames.Count; i++)
                {
                    FloorList.Add(new TotalHeatExUnitInfoModel()
                    {
                        Floor = WorkFlowContext.FloorNames[i]
                    });

                }

            }
            else
            {
                FloorList = new ObservableCollection<TotalHeatExUnitInfoModel>();
                FloorList.Add(new TotalHeatExUnitInfoModel()
                {
                    Floor = "Floor 0"
                });
            }

            GetSeriesTHeatEx();


        }
        private bool _UseRoomTemperature;
        public bool UseRoomTemperature
        {
            get { return _UseRoomTemperature; }
            set
            {
                this.SetValue(ref _UseRoomTemperature, value);
                // BindInternalDesignConditions();
            }
        }
        private ObservableCollection<TotalHeatExUnitInfoModel> _fanSpeeds;

        public ObservableCollection<TotalHeatExUnitInfoModel> FanSpeeds
        {
            get { return _fanSpeeds; }
            set { this.SetValue(ref _fanSpeeds, value); }
        }


        private ObservableCollection<TotalHeatExUnitInfoModel> _floor;

        public ObservableCollection<TotalHeatExUnitInfoModel> FloorList
        {
            get { return _floor; }
            set { this.SetValue(ref _floor, value); }
        }
        //private ObservableCollection<TotalHeatExUnitInfoModel> _seriesL;

        //public ObservableCollection<TotalHeatExUnitInfoModel> SeriesList
        //{
        //    get { return _seriesL; }
        //    set { this.SetValue(ref _seriesL, value); }
        //}


        private ObservableCollection<SeriesModel> listSeries;

        public ObservableCollection<SeriesModel> ListSeries
        {
            get { return listSeries; }
            set
            {
                this.SetValue(ref listSeries, value);
                //  BindPowerMode();
            }

        }
        private int _selectedSeries;
        public int SelectedValue
        {
            get { return _selectedSeries; }
            set
            {
                this.SetValue(ref _selectedSeries, value);
            }
        }
        private ObservableCollection<PowerModel> _power;

        public ObservableCollection<PowerModel> Power
        {
            get { return _power; }
            set
            {
                this.SetValue(ref _power, value);
                //  BindPowerlist();
            }

        }


        private double _eSPVal;
        public double ESPVal
        {
            get { return _eSPVal; }
            set { this.SetValue(ref _eSPVal, value); }
        }
        private double _area;
        public double Area
        {
            get { return _area; }
            set { this.SetValue(ref _area, value); }
        }
        private string _unitName;
        public string UnitName
        {
            get { return _unitName; }
            set { this.SetValue(ref _unitName, value); }
        }
        //public ListBox SelectedListRoom
        //{
        //    get { return _selectedListRoom; }
        //    set
        //    {
        //        this.SetValue(ref _selectedListRoom, value);
        //        if (_selectedListRoom != null)
        //            this.TxtRoom = _selectedListRoom.DisplayName;
        //    }
        //}

        private ObservableCollection<Room> _listRoom;
        public ObservableCollection<Room> RoomName
        {
            get
            {
                return _listRoom;
            }
            set
            {
                this.SetValue(ref _listRoom, value);

            }
        }
        private ObservableCollection<TotalHeatExUnitInfoModel> _series;
        public ObservableCollection<TotalHeatExUnitInfoModel> _Series
        {
            get
            {
                return _series;
            }
            set
            {
                this.SetValue(ref _series, value);

            }
        }


        private double _freshAir;
        public double FreshAir
        {
            get
            {
                return _freshAir;
            }
            set
            {
                this.SetValue(ref _freshAir, value);
                BindFreshAir();
            }
        }
        private string _freshAirError;
        public string FreshAirError
        {
            get { return _freshAirError == null ? string.Empty : _freshAirError; }
            set
            {
                this.SetValue(ref _freshAirError, value);
            }
        }
        private int _noOfPeople;
        public int NoOfPeople
        {
            get
            {
                return _noOfPeople;
            }
            set
            {
                this.SetValue(ref _noOfPeople, value);
                BindPeople();
            }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                this.SetValue(ref _name, value);

            }
        }


        private string _sfanspeed;

        public string SFanSpeed
        {
            get { return _sfanspeed; }
            set
            {
                this.SetValue(ref _sfanspeed, value);
            }
        }
        private string _sfloor;

        public string SFloor
        {
            get { return _sfloor; }
            set
            {
                this.SetValue(ref _sfloor, value);
            }
        }

        #endregion BindFanSpeed

        //private event PropertyChangedEventHandler PropertyChanged;
        //protected void OnPropertyChanged(string propertyName)
        //{
        //    if (PropertyChanged != null)
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //}
        private string _sR_Name;
        public string SRName
        {
            get { return _sR_Name; }
            set
            {
                this.SetValue(ref _sR_Name, value);
            }

        }

        private string _currentTempUnit;
        public string CurrentTempUnit
        {
            get { return SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE; }
            set
            {
                SystemSetting.UserSetting.unitsSetting.settingTEMPERATURE = value;
                string display = value == Unit.ut_Temperature_c ? Unit.ut_Temperature_f : Unit.ut_Temperature_c;
                this.SetValue(ref _currentTempUnit, value);
                DisplayCurrentTempUnit = display;
            }
        }


        private string _displayCurrentTempUnit;
        public string DisplayCurrentTempUnit
        {
            get { return CurrentTempUnit == Unit.ut_Temperature_c ? Unit.ut_Temperature_f : Unit.ut_Temperature_c; ; }
            set
            {
                this.SetValue(ref _displayCurrentTempUnit, value);
            }
        }
        private decimal _indoorCoolingDB;
        public decimal indoorCoolingDB
        {
            get
            {
                return _indoorCoolingDB;
            }
            set
            {

                this.SetValue(ref _indoorCoolingDB, value);

            }
        }

        private decimal _indoorCoolingWB;
        public decimal indoorCoolingWB
        {
            get
            {
                return _indoorCoolingWB;
            }
            set
            {
                this.SetValue(ref _indoorCoolingWB, value);

            }
        }
        private decimal _indoorCoolingRH;
        public decimal indoorCoolingRH
        {
            get
            {
                return _indoorCoolingRH;
            }
            set
            {
                this.SetValue(ref _indoorCoolingRH, value);

            }
        }
        private decimal _indoorHeatingDB;
        public decimal indoorHeatingDB
        {
            get
            {
                return _indoorHeatingDB;
            }
            set
            {
                this.SetValue(ref _indoorHeatingDB, value);

            }
        }
        private decimal _outdoorCoolingDB;
        public decimal outdoorCoolingDB
        {
            get
            {
                return _outdoorCoolingDB;
            }
            set
            {
                this.SetValue(ref _outdoorCoolingDB, value);

            }
        }// = 35.0m;
        private decimal _outdoorHeatingDB;
        public decimal outdoorHeatingDB
        {
            get
            {
                return _outdoorHeatingDB;
            }
            set
            {
                this.SetValue(ref _outdoorHeatingDB, value);

            }
        }//= 7.0m;
        private decimal _outdoorHeatingWB;
        public decimal outdoorHeatingWB
        {
            get
            {
                return _outdoorHeatingWB;
            }
            set
            {
                this.SetValue(ref _outdoorHeatingWB, value);

            }
        }//= 3.1m;
        private decimal _outdoorCoolingIW;
        public decimal outdoorCoolingIW
        {
            get
            {
                return _outdoorCoolingIW;
            }
            set
            {
                this.SetValue(ref _outdoorCoolingIW, value);

            }
        }
        private decimal _outdoorHeatingRH;
        public decimal outdoorHeatingRH
        {
            get
            {
                return _outdoorHeatingRH;
            }
            set
            {
                this.SetValue(ref _outdoorHeatingRH, value);

            }
        } //= 87.00m;
        private decimal _outdoorHeatingIW;
        public decimal outdoorHeatingIW
        {
            get
            {
                return _outdoorHeatingIW;
            }
            set
            {
                this.SetValue(ref _outdoorHeatingIW, value);

            }
        }
        private string _TemperatureTypeOCDB;
        public string TemperatureTypeOCDB
        {
            get { return _TemperatureTypeOCDB; }
            set { this.SetValue(ref _TemperatureTypeOCDB, value); }
        }

        private string _TemperatureTypeOHIW;
        public string TemperatureTypeOHIW
        {
            get { return _TemperatureTypeOHIW; }
            set { this.SetValue(ref _TemperatureTypeOHIW, value); }
        }
        private string _TemperatureTypeOCWB;
        public string TemperatureTypeOCWB
        {
            get { return _TemperatureTypeOCWB; }
            set { this.SetValue(ref _TemperatureTypeOCWB, value); }
        }
        private string _TemperatureTypeOHDB;
        public string TemperatureTypeOHDB
        {
            get { return _TemperatureTypeOHDB; }
            set { this.SetValue(ref _TemperatureTypeOHDB, value); }
        }
        private string _TemperatureTypeICDB;
        public string TemperatureTypeICDB
        {
            get { return _TemperatureTypeICDB; }
            set { this.SetValue(ref _TemperatureTypeICDB, value); }
        }


        private string _txtAltitudeUnit;

        public string txtAltitudeUnit
        {
            get { return _txtAltitudeUnit; }
            set { this.SetValue(ref _txtAltitudeUnit, value); }
        }
        #region Delegate Commands

        public DelegateCommand ChangeTempCommand { get; set; }

        // public DelegateCommand ChangeRoom { get; set; }

        public DelegateCommand AddFloorCommand { get; set; }
        public DelegateCommand AddEditRoomCommand { get; set; }

        #endregion
    }
    public class CommandImplement : ICommand
    {
        #region Command Implementation
        public CommandImplement(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("execute");
            }

            if (canExecute == null)
            {
                throw new ArgumentNullException("canExecute");
            }

            this.execute = execute;
            this.canExecute = canExecute;
        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
                this.CanExecuteChangedInternal += value;
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
                this.CanExecuteChangedInternal -= value;
            }
        }
        private event EventHandler CanExecuteChangedInternal;
        private Action<object> execute;

        private Predicate<object> canExecute;
        public bool CanExecute(object parameter)
        {
            return this.canExecute != null && this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
        public void Destroy()
        {
            this.canExecute = _ => false;
            this.execute = _ => { return; };
        }
        #endregion


    }

}
