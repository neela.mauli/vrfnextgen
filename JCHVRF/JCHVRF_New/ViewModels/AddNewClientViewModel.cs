﻿using JCHVRF_New.Common.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using System.Text.RegularExpressions;
using JCHVRF_New.Common.Contracts;
using JCHVRF.BLL.New;
using Prism.Events;
//using JCHVRF_New.Model;
using JCHVRF.Model.New;
using System.Windows;
using JCHVRF_New.Views;
using System.Windows.Input;

namespace JCHVRF_New.ViewModels
{
    public class AddNewClientViewModel : ViewModelBase
    {
        private IEventAggregator _eventAggregator;
        private IProjectInfoBAL _projectInfoBll;
        #region Delegate Commands
        public DelegateCommand EmailValidationDBCommand { get; set; }
        public DelegateCommand<IClosable> CancelNewClientClickedCommand { get; set; }
        public DelegateCommand<TextCompositionEventArgs> PhoneValidationDBCommand { get; set; }
        public DelegateCommand<IClosable> AddNewClientClickedCommand { get; set; }

      

        #endregion Delegate Commands
        public AddNewClientViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            EmailValidationDBCommand = new DelegateCommand(EmailId_LostFocus);
            CancelNewClientClickedCommand = new DelegateCommand<IClosable>(OnCancelClickedCommand);
            PhoneValidationDBCommand = new DelegateCommand<TextCompositionEventArgs>(PhoneNumber_LostFocus);
            AddNewClientClickedCommand = new DelegateCommand<IClosable>(OnNewClientClickedCommand);
          
            //CancelNewCreatorClickedCommand = new DelegateCommand<IClosable>(OnCancelClickedCommand);
        }

        #region Properties

        private string _txtCompanyName;
        public string TxtCompanyName
        {
            get { return _txtCompanyName; }
            set { this.SetValue(ref _txtCompanyName, value); }
        }

        private string _txtStreetAddress;
        public string TxtStreetAddress
        {
            get { return _txtStreetAddress; }
            set { this.SetValue(ref _txtStreetAddress, value); }
        }

        private string _txtSuburb;
        public string TxtSuburb
        {
            get { return _txtSuburb; }
            set { this.SetValue(ref _txtSuburb, value); }
        }


        private string _txtTownCity;
        public string TxtTownCity
        {
            get { return _txtTownCity; }
            set { this.SetValue(ref _txtTownCity, value); }
        }

        private string _txtCountry;
        public string TxtCountry
        {
            get { return _txtCountry; }
            set { this.SetValue(ref _txtCountry, value); }
        }

        private string _txtGpsPosition;
        public string TxtGpsPosition
        {
            get { return _txtGpsPosition; }
            set { this.SetValue(ref _txtGpsPosition, value); }
        }


        private string _txtExistingClient;
        public string TxtExistingClient
        {
            get { return _txtExistingClient; }
            set { this.SetValue(ref _txtExistingClient, value); }
        }

        private string _txtPhone;
        [Phone]
        [Required(ErrorMessage = "Filling this is Mandatory")]
        public string TxtPhone
        {
            get { return string.IsNullOrEmpty(_txtPhone)?"":_txtPhone; }
            set { this.SetValue(ref _txtPhone, value); }
        }

        private string _txtContactEmail;

        public string TxtContactEmail
        {
            get { return string.IsNullOrEmpty(_txtContactEmail)?"":_txtContactEmail; }
            set { this.SetValue(ref _txtContactEmail, value); }
        }

      
        private string _lblContactEmail;
        public string lblContactEmail
        {
            get
            {
                return _lblContactEmail;
            }
            set
            {
                this.SetValue(ref _lblContactEmail, value);
            }
        }

        private string _lblPhoneNumber;
        public string lblPhoneNumber
        {
            get
            {
                return _lblPhoneNumber;
            }
            set
            {
                this.SetValue(ref _lblPhoneNumber, value);
            }
        }
        private string _txtContactName;
        [Required(ErrorMessage = "Contact Name is Mandatory")]
        public string TxtContactName
        {
            get { return _txtContactName; }
            set { this.SetValue(ref _txtContactName, value); }
        }

        private string _txtIdNumber;
        // private IEventAggregator _eventAggregator;

        //[RegularExpression(@"!|@|#|\$|%|\?|\>|\<|\*", ErrorMessage = "Special characters not allowed.")]
        public string TxtIdNumber
        {
            get { return _txtIdNumber; }
            set { this.SetValue(ref _txtIdNumber, value); }
        }

        #endregion Properties



        #region Methods

        private void OnNewClientClickedCommand(IClosable objClientWindow)
        {
            if (ValidateViewModel()==true && ValidateEmailId()==true)
            {
               Client objClient = new Client();
                _projectInfoBll = new ProjectInfoBLL();
                objClient.CompanyName= TxtCompanyName;
                objClient.ContactName = TxtContactName;
                objClient.StreetAddress= TxtStreetAddress;
                objClient.Suburb = TxtSuburb;
                objClient.TownCity = TxtTownCity;
                objClient.Country = TxtCountry;
                objClient.GpsPosition = TxtGpsPosition;
                objClient.Phone = TxtPhone;
                objClient.ContactEmail = TxtContactEmail;
                objClient.IdNumber= TxtIdNumber;
                var result = _projectInfoBll.InsertClientInfo(objClient);
                if (result == 1)
                {
                    _eventAggregator.GetEvent<AddCreatorPayLoad>().Publish();
                    MessageBox.Show("Save Successfully");
                    Application.Current.Properties["TxtContactName"] = objClient.ContactName;
                    ResetValue();
                    objClientWindow.RequestClose();
                }
            }
        }

        private void ResetValue()
        {
            this.TxtCompanyName = "";
            this.TxtStreetAddress = "";
            this.TxtSuburb = "";
            this.TxtTownCity = "";
            this.TxtCountry = "";
            this.TxtGpsPosition = "";
            this.TxtContactName = "";
            this.TxtPhone = "";
            this.TxtContactEmail = "";
            this.TxtIdNumber = "";
        }
        private void OnCancelClickedCommand(IClosable objClosable)
        {

            objClosable.RequestClose();

        }
        private void NewClientOpenWindowCommand()
        {
            AddNewClient objClient = new AddNewClient();
            objClient.ShowDialog();
        }
        private bool ValidateEmailId()
        {
            
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(TxtContactEmail);

            if(!string.IsNullOrEmpty(TxtContactEmail))
            {
                if (match.Success)
                {
                    lblContactEmail = string.Empty;
                    return true;
                }
                else
                {
                    lblContactEmail = "Please enter valid mail id.";
                    return false;
                }

            }
            return true;

        }
        private bool ValidatePhoneNumber(TextCompositionEventArgs e)
        {
           
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
           
            return false;

        }
        private void EmailId_LostFocus()
        {
           
            if (ValidateEmailId() == false)
            {
               
            }
        }
        
        private void PhoneNumber_LostFocus(TextCompositionEventArgs e)
        {
            if (ValidatePhoneNumber(e) == false)
            {

            }
        }
        private bool ValidateViewModel()
        {

            if (this["TxtContactName"].Length >0)
            {
                return false;
            }
            return true;
        }
    }
   
}
        #endregion Methods
    
