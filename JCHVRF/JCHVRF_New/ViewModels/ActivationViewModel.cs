﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JCBase.Utility;
using JCHVRF_New.Model;
using JCHVRF_New.Common.Helpers;
using Prism.Commands;
using Prism.Events;
using Registr;
using Prism.Regions;
using JCHVRF_New.Common.Constants;
using System.Windows.Input;

namespace JCHVRF_New.ViewModels
{
    public class ActivationViewModel : ViewModelBase
    {
        
        private IRegionManager _regionManager;
        public DelegateCommand OkClickCommand { get; set; }
        public DelegateCommand CancelClickCommand { get; set; }
        public DelegateCommand LoadedCommand { get; set; }

        public bool ISVALID_Region = false;
        public bool ISVALID_Date = false;
        private string _password;
        private string _statusText;

        public string StatusText
        {
            get { return this._statusText; }
            set { this.SetValue(ref _statusText, value); }
        }

        public string Password
        {
            get { return this._password; }
            set { this.SetValue(ref _password, value); }
        }
        public ActivationViewModel(IRegionManager regionManager)
        {

            _regionManager = regionManager;
            OkClickCommand = new DelegateCommand(OnActivationClicked);
            CancelClickCommand = new DelegateCommand(OnCancelClicked);
            StatusText = "Activation Password";
                        
            LoadedCommand = new DelegateCommand(() =>
            {
                if (checkRegistration())
                {
                    _regionManager.RequestNavigate(RegionNames.MainAppRegion, "MainApp");
                }
            });
        }        
        private void OnActivationClicked()
        {            
            Registration.DoValidation(Password);

            if (checkRegistration())
            {
                StatusText = "Please wait. Customizing it for you...";
                Mouse.OverrideCursor = Cursors.Wait;                
                _regionManager.RequestNavigate(RegionNames.MainAppRegion, "MainApp");                
            }
            //else
            //{
            //    this.txtActivation.Focus();
            //}
        }
        private void OnCancelClicked()
        {
            Environment.Exit(0);
        }
        private bool checkRegistration()
        {
            bool validRegistration = false;
            
            if (Registration.IsValid())
            {
                ISVALID_Region = true;
                ISVALID_Date = true;
                validRegistration = true;
                
                //this.DialogResult = DialogResult.OK;
                //Close();
            }
            return validRegistration;
        }
    }
}
