﻿using JCHVRF.BLL;
using JCHVRF.Model;
using JCHVRF_New.Common.Contracts;
using JCHVRF_New.Common.Helpers;
using JCHVRF_New.Model;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCHVRF_New.ViewModels
{
    public class IDUPropertiesViewModel: ViewModelBase, ILayoutItem
    {
        private IEventAggregator _eventAggregator;

        #region Local_Property
        JCHVRF.Model.Project CurrentProject;
        private double outdoorCoolingDB = 0;
        private double outdoorHeatingWB = 0;
        private double outdoorCoolingIW = 0;
        private double outdoorHeatingIW = 0;
        DataTable IDuModelTypeList = new DataTable();
        string _factory = "";
        string _type = "";
        string ODUProductTypeUniversal = "Universal IDU";
        List<string> _typeList = new List<string>();
        List<Indoor> IDUIndoorList;
        string ODUSeries;// = "Commercial VRF HP, FSXNK"; // "Commercial VRF HP, FSXNK",  Residential VRF HP, FS(V/Y)N1Q/FSNMQ
        string ODUProductType;// = "Comm. Tier 2, HP"; // "Comm. Tier 2, HP"  Res. Tier 1, HP
        IndoorBLL bll;
        string defaultFolder = AppDomain.CurrentDomain.BaseDirectory;
        string navigateToFolder = "..\\..\\Image\\TypeImages";
        string utPower = SystemSetting.UserSetting.unitsSetting.settingPOWER;
        string utAirflow = SystemSetting.UserSetting.unitsSetting.settingAIRFLOW;


        DataRow datarow;
        #endregion

        public IDUPropertiesViewModel(IEventAggregator EventAggregator)
        {
             Title = "Properties";
            _eventAggregator = EventAggregator;
            JCHVRF.Model.Project.CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            bll = new IndoorBLL(JCHVRF.Model.Project.GetProjectInstance.SubRegionCode, JCHVRF.Model.Project.GetProjectInstance.BrandCode);
            this.IndoorUnitName = SystemSetting.UserSetting.defaultSetting.IndoorName + GetIndoorCount();
            BindFloorList();
            BindDefaultFanSpeed();
            BindIDUPosition();
            BindIDuUnitType();
        }

        private void OpenGetRoomList()
        {
            GetRoomList();
        }


        public double FloatingHeight { get; set; }
        public double FloatingLeft { get; set; }
        public double FloatingTop { get; set; }
        public double FloatingWidth { get; set; }
        public string Title { get; set; }
        
        #region View_Model_Property

        private string _indoorUnitName;
        public string IndoorUnitName
        {
            get { return _indoorUnitName; }
            set { this.SetValue(ref _indoorUnitName, value); }
        }

        #region Indoor Prorperties

        private List<ComboBox> _listIndoorType;
        public List<ComboBox> ListIndoorType
        {
            get
            {
                return _listIndoorType;
            }
            set
            {
                this.SetValue(ref _listIndoorType, value);
            }
        }

        private string _selectedIndoorType;
        public string SelectedIndoorType
        {
            get { return _selectedIndoorType; }
            set
            {
                this.SetValue(ref _selectedIndoorType, value);
                if (SelectedIndoorType != null)
                {
                    BindIDuModelType();
                }

            }
        }


        private string _indoorError;
        public string IndoorError
        {
            get { return _indoorError; }
            set { this.SetValue(ref _indoorError, value); }
        }

        private ObservableCollection<ComboBox> _ListRoom;
        public ObservableCollection<ComboBox> ListRoom
        {
            get
            {
                return _ListRoom;
            }
            set
            {
                this.SetValue(ref _ListRoom, value);
            }
        }

        private string _selectedRoom;
        public string SelectedRoom
        {
            get { return _selectedRoom; }
            set { this.SetValue(ref _selectedRoom, value); }
        }

        private ObservableCollection<JCHVRF.Model.Floor> _listFloor;
        public ObservableCollection<JCHVRF.Model.Floor> ListFloor
        {
            get
            {
                return _listFloor;
            }
            set
            {
                this.SetValue(ref _listFloor, value);
            }
        }

        private string _selectedFloor;
        public string SelectedFloor
        {
            get { return _selectedFloor; }
            set { this.SetValue(ref _selectedFloor, value); }
        }

        private string _iduImagePath;
        public string IduImagePath
        {
            get { return _iduImagePath; }
            set { this.SetValue(ref _iduImagePath, value); }
        }

        public List<string> IduPosition
        {
            get
            {
                return Enum.GetNames(typeof(PipingPositionType)).ToList();
            }
        }

        private string _selectedIduPosition;
        public string SelectedIduPosition
        {
            get { return _selectedIduPosition; }
            set { this.SetValue(ref _selectedIduPosition, value); }
        }

        private double? _heightDifference;
        public double? HeightDifference
        {
            get
            {
                return _heightDifference;
            }
            set
            {
                this.SetValue(ref _heightDifference, value);
            }
        }

        private string _heightDifferenceError;
        public string HeightDifferenceError
        {
            get
            {
                return _heightDifferenceError;
            }
            set
            {
                this.SetValue(ref _heightDifferenceError, value);
            }
        }

        public List<string> FanSpeed
        {
            get
            {
                return Enum.GetNames(typeof(FanSpeed)).ToList(); ;
            }

        }

        private string _selectedFanSpeed;
        public string SelectedFanSpeed
        {
            get { return _selectedFanSpeed; }
            set { this.SetValue(ref _selectedFanSpeed, value); }
        }


        private bool _manualSelection;
        public bool ManualSelection
        {
            get { return _manualSelection; }
            set
            {
                this.SetValue(ref _manualSelection, value);
            }
        }

        private string _unitTypeError;
        public string UnitTypeError
        {
            get { return _unitTypeError; }
            set
            {
                this.SetValue(ref _unitTypeError, value);
            }
        }

        private ObservableCollection<ComboBox> _listModel;
        public ObservableCollection<ComboBox> ListModel
        {
            get
            {
                return _listModel;
            }
            set
            {
                this.SetValue(ref _listModel, value);
            }
        }

        private string _listModelError;
        public string ListModelError
        {
            get { return _listModelError; }
            set
            {
                this.SetValue(ref _listModelError, value);
            }
        }

        private string _selectedModel;
        public string SelectedModel
        {
            get { return _selectedModel; }
            set
            {
                this.SetValue(ref _selectedModel, value);
                
            }
        }

        #endregion

        #region Internal_Design_Conditions_Property

        private string _changeToFandC;
        public string ChangeToFandC
        {
            get { return _changeToFandC; }
            set { this.SetValue(ref _changeToFandC, value); }
        }

       
        private bool _UseRoomTemperature;
        public bool UseRoomTemperature
        {
            get { return _UseRoomTemperature; }
            set { this.SetValue(ref _UseRoomTemperature, value); }
        }

        private double? _coolingDryBulb;
        public double? CoolingDryBulb
        {
            get
            {
                return _coolingDryBulb;
            }
            set
            {
                this.SetValue(ref _coolingDryBulb, value);
            }
        }

        private string _coolingDryBulbError;
        public string CoolingDryBulbError
        {
            get { return _coolingDryBulbError; }
            set
            {
                this.SetValue(ref _coolingDryBulbError, value);
            }
        }


        private double? _coolingWetBulb;
        public double? CoolingWetBulb
        {
            get
            {
                return _coolingWetBulb;
            }
            set
            {
                this.SetValue(ref _coolingWetBulb, value);
            }
        }

        private string _coolingWetBulbError;
        public string CoolingWetBulbError
        {
            get { return _coolingWetBulbError; }
            set
            {
                this.SetValue(ref _coolingWetBulbError, value);
            }
        }

        private double? _heatingDryBulb;
        public double? HeatingDryBulb
        {
            get
            {
                return _heatingDryBulb;
            }
            set
            {
                this.SetValue(ref _heatingDryBulb, value);
            }
        }

        private string _heatingDryBulbError;
        public string HeatingDryBulbError
        {
            get { return _heatingDryBulbError; }
            set
            {
                this.SetValue(ref _heatingDryBulbError, value);
            }
        }

        private double? _relativeHumidity;
        public double? RelativeHumidity
        {
            get
            {
                return _relativeHumidity;
            }
            set
            {
                this.SetValue(ref _relativeHumidity, value);
            }
        }

        private string _relativeHumidityError;
        public string RelativeHumidityError
        {
            get { return _relativeHumidityError; }
            set
            {
                this.SetValue(ref _relativeHumidityError, value);
            }
        }

        #endregion

        #region Model Specification

        #endregion

        #endregion
        #region BindingData
        private int GetIndoorCount()
        {
            int InNodeCount = 0;
            if (JCHVRF.Model.Project.GetProjectInstance.RoomIndoorList != null)
                foreach (RoomIndoor ri in JCHVRF.Model.Project.GetProjectInstance.RoomIndoorList)
                {
                    InNodeCount = InNodeCount > ri.IndoorNO ? InNodeCount : ri.IndoorNO;
                }
           
            return InNodeCount + 1;
        }

        public ObservableCollection<ComboBox> GetRoomList()
        {
            CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            ListRoom = new ObservableCollection<ComboBox>();
            CurrentProject.FloorList[0].RoomList.ForEach((item) =>
            {
                ListRoom.Add(new ComboBox { DisplayName = item.Name, Value = item.Id });
            });
            return ListRoom;
        }

        private void BindFloorList()
        {
            this.ListFloor = new ObservableCollection<JCHVRF.Model.Floor>(JCHVRF.Model.Project.GetProjectInstance.FloorList);
            if (this.SelectedFloor == null)
            {
                this.SelectedFloor = this.ListFloor.FirstOrDefault().Id;
            }
        }

        private void BindDefaultFanSpeed()
        {
            this.SelectedFanSpeed = JCHVRF_New.Model.FanSpeed.Max.ToString();
        }

        private void BindIDUPosition()
        {
            this.SelectedIduPosition = PipingPositionType.SameLevel.ToString();
        }

        private void BindIDuUnitType()
        {
            ListIndoorType = new List<ComboBox>();
            if (ProjectBLL.IsSupportedUniversalSelection(JCHVRF.Model.Project.GetProjectInstance))
            {
                var IndoorTypeList = bll.GetIndoorDisplayName();
                foreach (DataRow rowView in IndoorTypeList.Rows)
                {
                    ListIndoorType.Add(new ComboBox { DisplayName = Convert.ToString(rowView["Display_Name"]), Value = Convert.ToString(rowView["Display_Name"]) });
                }
                if (ListIndoorType.Count > 0)
                    this.SelectedIndoorType = ListIndoorType.FirstOrDefault().DisplayName;
            }
            else
            {
                if (JCHVRF.Model.Project.GetProjectInstance == null || string.IsNullOrEmpty(JCHVRF.Model.Project.GetProjectInstance.SubRegionCode) || string.IsNullOrEmpty(ODUProductType))
                    return;
                string colName = "UnitType";
                DataTable dt = bll.GetIndoorFacCodeList(ODUProductType);
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToInt32(dr["FactoryCount"].ToString()) > 1)
                    {
                        switch (dr["FactoryCode"].ToString())
                        {
                            case "G":
                                dr[colName] += "-GZF";
                                break;
                            case "E":
                                dr[colName] += "-HAPE";
                                break;
                            case "Q":
                                dr[colName] += "-HAPQ";
                                break;
                            case "B":
                                dr[colName] += "-HAPB";
                                break;
                            case "I":
                                dr[colName] += "-HHLI";
                                break;
                            case "M":
                                dr[colName] += "-HAPM";
                                break;
                            case "S":
                                dr[colName] += "-SMZ";
                                break;
                        }
                    }
                }
                var dv = new DataView(dt);
                if (ODUProductType == "Comm. Tier 2, HP")
                {

                    if (ODUSeries == "Commercial VRF HP, FSN6Q" || ODUSeries == "Commercial VRF HP, JVOH-Q")
                    {
                        dv.RowFilter = "UnitType not in ('High Static Ducted-HAPE','Medium Static Ducted-HAPE','Low Static Ducted-HAPE','High Static Ducted-SMZ','Medium Static Ducted-SMZ','Four Way Cassette-SMZ')";
                    }
                    else
                    {
                        dv.RowFilter = "UnitType <>'Four Way Cassette-HAPQ'";
                    }
                }
                foreach (DataRowView rowView in dv)
                {
                    ListIndoorType.Add(new ComboBox { DisplayName = Convert.ToString(rowView.Row["UnitType"]), Value = Convert.ToString(rowView.Row["FactoryCode"]) });
                }
                if (ListIndoorType.Count > 0)
                    this.SelectedIndoorType = ListIndoorType.FirstOrDefault().DisplayName;
            }

        }
        private void UpdateUnitType()
        {
            int i = this.SelectedIndoorType.IndexOf("-");
            if (i > 0)
            {
                _factory = this.SelectedIndoorType.Substring(i + 1, this.SelectedIndoorType.Length - i - 1);
                _type = this.SelectedIndoorType.Substring(0, i);
            }
            else
            {
                _factory = "";
                _type = this.SelectedIndoorType;
            }
        }
        private void UpdateUnitTypeUniversal()
        {
            _factory = bll.GetFCodeByDisUnitType(this.SelectedIndoorType, out _typeList);
        }
        private async void BindIDuModelType()
        {
            if (ProjectBLL.IsSupportedUniversalSelection(JCHVRF.Model.Project.GetProjectInstance))
            {
                UpdateUnitTypeUniversal();
                IDuModelTypeList = bll.GetUniversalIndoorListStd(this.SelectedIndoorType, _factory, _typeList);
                IDuModelTypeList.DefaultView.Sort = "CoolCapacity";
                await Task.Run(() => BindCompatibleIndoorUniversal());
            }
            else
            {
                UpdateUnitType();
                IDuModelTypeList = bll.GetIndoorListStd(_type, ODUProductType, _factory);
                IDuModelTypeList.DefaultView.Sort = "CoolCapacity";
                await Task.Run(() => BindCompatibleIndoorSimple());
            }

        }

        private void BindCompatibleIndoorUniversal()
        {
            try
            {
                ListModel = new ObservableCollection<ComboBox>();
                List<ComboBox> ListModelTemp = new List<ComboBox>();
                ODUSeries = "Commercial VRF HP, FSXNSE";
                IDUIndoorList = new List<Indoor>();
                MyProductTypeBLL productTypeBll = new MyProductTypeBLL();
                DataTable typeDt = productTypeBll.GetIduTypeBySeries(JCHVRF.Model.Project.GetProjectInstance.BrandCode, JCHVRF.Model.Project.GetProjectInstance.SubRegionCode, ODUSeries);
                foreach (DataRow drIduModel in IDuModelTypeList.Rows)
                {
                    var Indoor = bll.GetItem(drIduModel["ModelFull"].ToString(), _type, ODUProductTypeUniversal, ODUSeries);
                    IDUIndoorList.Add(Indoor);
                    if (typeDt != null)
                        foreach (DataRow dr in typeDt.Rows)
                        {
                            if (Indoor.Type == dr["IDU_UnitType"].ToString())
                            {
                                var modelHitachi = dr["IDU_Model_Hitachi"].ToString();
                                if (string.IsNullOrEmpty(modelHitachi) || modelHitachi.Contains(";" + Convert.ToString(drIduModel["Model_Hitachi"]) + ";"))
                                {
                                    if (JCHVRF.Model.Project.GetProjectInstance.BrandCode == "Y")
                                        ListModelTemp.Add(new ComboBox { DisplayName = Convert.ToString(drIduModel["Model_York"]), Value = Convert.ToString(drIduModel["ModelFull"]) });
                                    else
                                        ListModelTemp.Add(new ComboBox { DisplayName = Convert.ToString(drIduModel["Model_Hitachi"]), Value = Convert.ToString(drIduModel["ModelFull"]) });
                                }
                                break;
                            }
                        }
                }
                if (ListModelTemp != null && ListModelTemp.Count > 0)
                {
                    ListModel = new ObservableCollection<ComboBox>(ListModelTemp);
                    this.SelectedModel = ListModel.FirstOrDefault().Value;
                }
            }
            catch (Exception ex) { }

        }

        private void BindCompatibleIndoorSimple()
        {
            try
            {
                IDUIndoorList = new List<Indoor>();
                List<ComboBox> ListModelTemp = new List<ComboBox>();
                ListModel = new ObservableCollection<ComboBox>();
                foreach (DataRow drIduModel in IDuModelTypeList.Rows)
                {
                    var Indoor = bll.GetItem(drIduModel["ModelFull"].ToString(), _type, ODUProductType, ODUSeries);
                    IDUIndoorList.Add(Indoor);
                    if (JCHVRF.Model.Project.GetProjectInstance.BrandCode == "Y")
                        ListModelTemp.Add(new ComboBox { DisplayName = Convert.ToString(drIduModel["Model_York"]), Value = Convert.ToString(drIduModel["ModelFull"]) });
                    else
                        ListModelTemp.Add(new ComboBox { DisplayName = Convert.ToString(drIduModel["Model_Hitachi"]), Value = Convert.ToString(drIduModel["ModelFull"]) });

                }
                if (ListModelTemp != null && ListModelTemp.Count > 0)
                {
                    ListModel = new ObservableCollection<ComboBox>(ListModelTemp);
                    this.SelectedModel = ListModel.FirstOrDefault().Value;
                }
            }
            catch (Exception ex) { }
        }

        #endregion

    }
}