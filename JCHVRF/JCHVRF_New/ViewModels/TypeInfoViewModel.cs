﻿using JCHVRF.Model;
using JCHVRF_New.Common.Helpers;
using JCHVRF.Model.New;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Events;
using System.Windows;
using JCHVRF.Model.NextGen;
using JCHVRF_New.Model;
using JCHVRF.BLL.New;

namespace JCHVRF_New.ViewModels
{
    public class TypeInfoViewModel : ViewModelBase
    {

        #region Delegate Commands
        JCHVRF.Model.Project CurrentProject;
        
        public DelegateCommand<int?> LoadFloor { get; set; }
        private IProjectInfoBAL _projectInfoBll;
        private ObservableCollection<SystemTypeItem> _systemTypeCollection;
        public ICommand ShowCommand { get; private set; }
        public DelegateCommand SingleFloorCheckedCommand { get; set; }
        public DelegateCommand SingleFloorUnCheckedCommand { get; set; }
        public DelegateCommand MultipleFloorCheckedCommand { get; set; }
        public DelegateCommand MultipleFloorUnCheckedCommand { get; set; }
        public DelegateCommand RegularCheckedCommnd { get; set; }
        public ICommand SystemTypeCommad { get; private set; }
        private IEventAggregator _eventAggregator;
        public ICommand HideCommand { get; private set; }
        #endregion
        #region Viewmodel properties
        public ObservableCollection<SystemTypeItem> SystemTypeCollection
        {
            get
            {
                if (_systemTypeCollection == null)
                    _systemTypeCollection = new ObservableCollection<SystemTypeItem>();
                return _systemTypeCollection;
            }
            set { this.SetValue(ref _systemTypeCollection, value); }
        }

        private JCHVRF.Model.Project _listFloor;
        public JCHVRF.Model.Project ListFloor
        {
            get { return _listFloor; }
            set
            {
                this.SetValue(ref _listFloor, value);

            }
        }
        private SystemTypeItem _selectedsystemName;
        public SystemTypeItem SelectedsystemName
        {
            get { return _selectedsystemName; }
            set
            {
                this.SetValue(ref _selectedsystemName, value);
                ShowOduAndIduSystem();
                HideControllerTypeText();               
                ShowTheuInfoSystem(); //ACC - RAG
                ShowCreateBtnOnClick(); //ACC - RAG
                ShowNextBtnOnClick(); //ACC - RAG
                WorkFlowContext.Systemid = SelectedsystemName.SystemID;
            }
        }

        private string _systemName;
        public string SystemName
        {
            get { return _systemName; }
            set { this.SetValue(ref _systemName, value); }
        }

        private bool _isSingleFoorChecked;
        public bool IsSingleFoorChecked
        {
            get { return _isSingleFoorChecked; }
            set
            {
                this.SetValue(ref _isSingleFoorChecked, value);
            }
        }

        private bool _ismultipleFloorChecked;
        public bool IsMultipleFloorChecked
        {
            get { return _ismultipleFloorChecked; }
            set { this.SetValue(ref _ismultipleFloorChecked, value); }
        }
        private int _floorcounts;

        public int FloorCount
        {
            get { return _floorcounts = IsSingleFoorChecked == false ? _floorcounts : 1; }
            set
            {
                this.SetValue(ref _floorcounts, value);

            }
        }
        private bool _isRegular;
        public bool IsRegular
        {
            get { return _isRegular; }
            set { this.SetValue(ref _isRegular, value); }
        }
        private bool _isCAD;
        public bool IsCAD
        {
            get { return _isCAD; }
            set { this.SetValue(ref _isCAD, value); }
        }

        private List<JCHVRF.Model.Floor> _floorList;
        public List<JCHVRF.Model.Floor> FloorList
        {
            get { return _floorList; }
            set
            {
                this.SetValue(ref _floorList, value);
               
            }
        }
        #endregion
        #region constructor and Initisation
        public TypeInfoViewModel(IProjectInfoBAL projctInfoBll, IEventAggregator EventAggregator)
        {
            Initializations();
            _eventAggregator = EventAggregator;
            this.ButtonVisibility = "Hidden";
            SingleFloorCheckedCommand = new DelegateCommand(SingleFloorCheckedEvent);
            SingleFloorUnCheckedCommand = new DelegateCommand(SingleFloorUnCheckedEvent);
            MultipleFloorCheckedCommand = new DelegateCommand(MultipleFloorCheckedEvent);
            MultipleFloorUnCheckedCommand = new DelegateCommand(MultipleFloorUnCheckedEvent);
            _eventAggregator.GetEvent<cntrlTexthiding>().Subscribe(GetTypeTabVisibility);
            LoadFloor = new DelegateCommand<int?>(OnMultiFloorLostFocus);
            RegularCheckedCommnd = new DelegateCommand(RegularCheckedEvent);
            _eventAggregator.GetEvent<TypeInfoTabNext>().Subscribe(TypeTabNextClick);
            _projectInfoBll = projctInfoBll;           
                      
            GetSystemTypeList();            
            if (SystemTypeCollection != null && SystemTypeCollection.Count > 0)
                SelectedsystemName = SystemTypeCollection.FirstOrDefault();
            // ACC - SHIV START
            SystemName = SystemSetting.UserSetting.defaultSetting.FreshAirAreaName + " " + JCHVRF.Model.Project.CurrentProject.SystemListNextGen.Count() + 1;
            CurrentProject.SystemName = SystemName; //takes name onto the system Name Texbox

            ShowCommand = new DelegateCommand(ShowMethod);
            HideCommand = new DelegateCommand(HideMethod);
        }
        #endregion
        #region Delegate Command Events
        private void OnMultiFloorLostFocus(int? value)
        {
            _eventAggregator.GetEvent<PubSubEvent<int?>>().Publish(value);
        }

        //Accord - CC

        private Visibility _cntrlvisibility;
        public Visibility Cntrlvisibility
        {
            get { return this._cntrlvisibility; }
            set { this.SetValue(ref _cntrlvisibility, value); }
        }

        private void GetTypeTabVisibility(Visibility cntrlvisibility)
        {
            Cntrlvisibility = cntrlvisibility;
        }

        public void ShowMethod()
        {
            this.ButtonVisibility = "Visible";
            SetFloorVisibility(false);
        }
        public void ShowOduAndIduSystem()
        {
            Visibility IduOduVisibility = Visibility.Visible;
            if (SelectedsystemName != null)
            {
                if (SelectedsystemName.SystemID == "1")
                    IduOduVisibility = Visibility.Visible;
                else
                    IduOduVisibility = Visibility.Collapsed;

                _eventAggregator.GetEvent<OduIduVisibility>().Publish(IduOduVisibility);
            }

        }
        private void SetOutdoorindoorVisibility(bool IsTrue)
        {
            Visibility FloorVisibility;
            if (IsTrue == true)
                FloorVisibility = Visibility.Collapsed;
            else
                FloorVisibility = Visibility.Visible;
            _eventAggregator.GetEvent<PubSubEvent<Visibility>>().Publish(FloorVisibility);
        }

        public Visibility FloorVisibility_bck = new Visibility();
        private void SetFloorVisibility(bool IsTrue)
        {
            Visibility FloorVisibility;
            if (IsTrue == true)
                FloorVisibility = Visibility.Collapsed;
            else
                FloorVisibility = Visibility.Visible;

            FloorVisibility_bck = FloorVisibility;
            _eventAggregator.GetEvent<PubSubEvent<Visibility>>().Publish(FloorVisibility);
        }
        public void HideMethod()
        {
            this.ButtonVisibility = "Hidden";
            SetFloorVisibility(true);

        }
        private string btnVisibility;
        public string ButtonVisibility
        {
            get
            {
                return btnVisibility;
            }
            set
            {
                this.SetValue(ref btnVisibility, value);
            }

        }

        //ACC - RAG START
        //This will show/hide Create button when user selects different System types
        public void ShowCreateBtnOnClick()
        {
            Visibility CreateBtnVisibility = Visibility.Collapsed;
            if (SelectedsystemName != null)
            {
                if (SelectedsystemName.SystemID == "6")
                    CreateBtnVisibility = Visibility.Visible;
                else
                    CreateBtnVisibility = Visibility.Collapsed;

                _eventAggregator.GetEvent<CreateButtonVisibility>().Publish(CreateBtnVisibility);
            }
        }
        //ACC - RAG END


        //ACC - RAG START
        //This will show/hide next button when user selects different System types
        public void ShowNextBtnOnClick()
        {
            Visibility NextBtnVisibility = Visibility.Visible;
            if (SelectedsystemName != null)
            {                              
                if (SelectedsystemName.SystemID == "6")
                    NextBtnVisibility = Visibility.Collapsed;
                else
                    NextBtnVisibility = Visibility.Visible;
                

                _eventAggregator.GetEvent<NextButtonVisibility>().Publish(NextBtnVisibility);
            }
        }
        //ACC - RAG END

        private void SingleFloorCheckedEvent()
        {
            IsSingleFoorChecked = true;
            IsMultipleFloorChecked = false;

        }
        private void SingleFloorUnCheckedEvent()
        {
            IsSingleFoorChecked = false;

        }
        private void Initializations()
        {
            CurrentProject = JCHVRF.Model.Project.GetProjectInstance;
            IsSingleFoorChecked = true;
            IsRegular = true;
        }
        private void RegularCheckedEvent()
        {
            IsRegular = true;
        }
        private void MultipleFloorCheckedEvent()
        {
            IsMultipleFloorChecked = true;
            IsSingleFoorChecked = false;
        }
        private void MultipleFloorUnCheckedEvent()
        {
            IsMultipleFloorChecked = false;
        }
        private void TypeTabNextClick()
        {
            if (SystemType())
            {
                _eventAggregator.GetEvent<TypeTabSubscriber>().Publish(true);
            }
            else
            {
                _eventAggregator.GetEvent<TypeTabSubscriber>().Publish(false);
            }
        }
        bool SystemType()
        {

            string errorMessage = null;
            bool IsAdded = false;
            bool IsValid = ValidateSystemTypeValid(out errorMessage);
            if (IsValid)
            {
                if (CurrentProject.SystemListNextGen.Count > 0)
                {
                    foreach (JCHVRF.Model.NextGen.SystemVRF sys in CurrentProject.SystemListNextGen)
                    {
                        if (SystemName != sys.Name)
                        {
                            sys.IsActiveSystem = false;
                        }
                        else
                        {
                            IsAdded = true;
                        }
                    }
                }
                if (IsAdded == false)
                {
                    CurrentProject.SystemListNextGen.Add(new JCHVRF.Model.NextGen.SystemVRF
                    {
                        Name = SystemName,
                        SystemTypeNexGen = SelectedsystemName.Name,
                        IsActiveSystem = true,
                        FloorCount = FloorCount,
                        IsRegular = IsRegular,
                        DBCooling = Convert.ToDouble(CurrentProject.DesignCondition.indoorCoolingDB),
                        DBHeating = Convert.ToDouble(CurrentProject.DesignCondition.outdoorHeatingDB),
                        WBHeating = Convert.ToDouble(CurrentProject.DesignCondition.outdoorHeatingWB),
                        IWCooling = Convert.ToDouble(CurrentProject.DesignCondition.outdoorCoolingIW),
                        IWHeating = Convert.ToDouble(CurrentProject.DesignCondition.outdoorHeatingIW),
                        RHHeating = Convert.ToDouble(CurrentProject.DesignCondition.outdoorHeatingRH),

                    });// = SelectedsystemName.Name;
                }

                //CurrentProject.SystemName = SystemName;
                //CurrentProject.FloorCount = FloorCount;
                //CurrentProject.IsRegular = IsRegular;
                if (IsSingleFoorChecked && CurrentProject.FloorList.Count < 1)
                {
                    CurrentProject.FloorList.Add(new JCHVRF.Model.Floor() { Name = "Floor 0", Id = "0" });
                }
                else if (IsMultipleFloorChecked)
                {
                    CurrentProject.FloorList.Clear();
                }
            }
            else
            {
                MessageBox.Show(errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return IsValid;
        }
        #endregion
        #region helping Methods
        private ObservableCollection<SystemTypeItem> GetSystemTypeList()
        {
            List<Tuple<string, string, string>> getSystemTypeList = new List<Tuple<string, string, string>>();
            getSystemTypeList = _projectInfoBll.GetAllSystemType();
            getSystemTypeList.ForEach((item) =>
            {
                SystemTypeCollection.Add(new SystemTypeItem { SystemID = item.Item1, Name = item.Item2, Path = item.Item3 });
            });
            return SystemTypeCollection;
        }
        #endregion       
        #region validation methods
        private bool ValidateSystemTypeValid(out string errormessage)
        {

            bool IsValid = true;
            errormessage = "";
            if (string.IsNullOrEmpty(SystemName))
            {
                errormessage = "System Name cannot be blank";
                IsValid = false;

            }
            if (string.IsNullOrEmpty(SelectedsystemName.Name))
            {
                errormessage = "Select system type ";
                IsValid = false;

            }
            else if (IsMultipleFloorChecked)
            {
                if (FloorCount <= 0)
                {
                    errormessage = "Please add atleast one floor";
                    IsValid = false;
                }
            }

            return IsValid;
        }
        #endregion

        //ACC - RAG START
        public void ShowTheuInfoSystem()
        {
            Visibility HeuVisibility = Visibility.Visible;
            if (SelectedsystemName != null)
            {
                if (SelectedsystemName.SystemID == "2")
                    HeuVisibility = Visibility.Visible;
                else
                    HeuVisibility = Visibility.Collapsed;

                _eventAggregator.GetEvent<TheuInfoVisibility>().Publish(HeuVisibility);
            }
        }
        //ACC - RAG END

        //Accord - CC
        public void HideControllerTypeText()
        {
            Visibility Textboxhiding = Visibility.Visible;
            if (SelectedsystemName != null)
            {
                if (SelectedsystemName.SystemID == "6")
                {
                    SetFloorVisibility(true);
                    Textboxhiding = Visibility.Collapsed;                   
                }
            }
            _eventAggregator.GetEvent<cntrlTexthiding>().Publish(Textboxhiding);
        }


    }
}
