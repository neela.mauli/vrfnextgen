﻿using JCHVRF.BLL.New;
using JCHVRF.Model.New;
using JCHVRF_New.Common.Helpers;
using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel.DataAnnotations;
using JCHVRF_New.Common.Contracts;

namespace JCHVRF_New.ViewModels
{
    public class NewCreatorInformationViewModel : ViewModelBase
    {
        private IProjectInfoBAL _projectInfoBll;

        private string _companyName;
        public string CompanyName
        {
            get { return _companyName; }
            set { this.SetValue(ref _companyName, value); }
        }

        private string _streetAddress;
        public string StreetAddress
        {
            get { return _streetAddress; }
            set { this.SetValue(ref _streetAddress, value); }
        }

        private string _suburb;
        public string Suburb
        {
            get { return _suburb; }
            set { this.SetValue(ref _suburb, value); }
        }


        private string _townCity;
        public string TownCity
        {
            get { return _townCity; }
            set { this.SetValue(ref _townCity, value); }
        }

        private string _country;
        public string Country
        {
            get { return _country; }
            set { this.SetValue(ref _country, value); }
        }

        private string _gpsPosition;
        public string GpsPosition
        {
            get { return _gpsPosition; }
            set { this.SetValue(ref _gpsPosition, value); }
        }


        private string _contactName;
        [Required(ErrorMessage = "Contact Name is Mandatory")]
        public string ContactName
        {
            get { return _contactName; }
            set { this.SetValue(ref _contactName, value); }
        }

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { this.SetValue(ref _phone, value); }
        }

        private string _contactEmail;

        [EmailAddress(ErrorMessage = "Please Enter Valid Email")]
        public string ContactEmail
        {
            get { return _contactEmail; }
            set { this.SetValue(ref _contactEmail, value); }
        }

        private string _idNumber;
        private IEventAggregator _eventAggregator;

        [RegularExpression(@"!|@|#|\$|%|\?|\>|\<|\*", ErrorMessage = "Special characters not allowed.")]
        public string IdNumber
        {
            get { return _idNumber; }
            set { this.SetValue(ref _idNumber, value); }
        }

        public DelegateCommand<IClosable> AddNewCreatorClickedCommand { get; set; }

        public DelegateCommand<IClosable> CancelNewCreatorClickedCommand { get; set; }
        public NewCreatorInformationViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;   
            AddNewCreatorClickedCommand = new DelegateCommand<IClosable>(OnNewCreatorClickedCommand);
            CancelNewCreatorClickedCommand = new DelegateCommand<IClosable>(OnCancelClickedCommand);
        }

        private void OnCancelClickedCommand(IClosable objClosable)
        {


            objClosable.RequestClose();

        }

        private void OnNewCreatorClickedCommand(IClosable objCreatorWindow)
        {
            if (ValidateViewModel())
            {
                Creator objCreator = new Creator();
                _projectInfoBll = new ProjectInfoBLL();
                objCreator.CompanyName = CompanyName;
                objCreator.StreetAddress = StreetAddress;
                objCreator.Suburb = Suburb;
                objCreator.TownCity = TownCity;
                objCreator.Country = Country;
                objCreator.GpsPosition = GpsPosition;
                objCreator.ContactName = ContactName;
                objCreator.Phone = Phone;
                objCreator.ContactEmail = ContactEmail;
                objCreator.IdNumber = IdNumber;
                var result = _projectInfoBll.InsertCreatorInfo(objCreator);
                if (result == 1)
                {
                    _eventAggregator.GetEvent<AddCreatorPayLoad>().Publish();
                    MessageBox.Show("Save Successfully");
                    ResetValue();
                    // objCreatorWindow.RequestClose();
                }
            }
        }

        private void ResetValue()
        {
            this.CompanyName = "";
            this.StreetAddress = "";
            this.Suburb = "";
            this.TownCity = "";
            this.Country = "";
            this.GpsPosition = "";
            this.ContactName = "";
            this.Phone = "";
            this.ContactEmail = "";
            this.IdNumber = "";
        }


        private bool ValidateViewModel()
        {          
           
            if (this["ContactName"].Length > 0 || this["ContactEmail"].Length > 0)
            {
                return false;
            }
            return true;
        }
        
    }
}
