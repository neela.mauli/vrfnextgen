﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCHVRF_New.Utility
{
    public static class Validation
    {
        public static bool IsValidatedSystemVRF(JCHVRF.Model.Project project,out string ErrMsg)
        {
            bool Result = true;
            ErrMsg = "";
            if (project.RoomIndoorList == null || project.RoomIndoorList.Count == 0)
            {
                Result = false;
                ErrMsg = string.Format(JCHVRF.Const.ValidationMessage.AtleastOneIDU);
            }           
            else if (project.CanvasODUList == null || project.CanvasODUList.Count == 0)
            {
                Result = false;
                ErrMsg = string.Format(JCHVRF.Const.ValidationMessage.AtleastOneODU);
            }
            else if (project.CanvasODUList.Count > 1)
            {
                Result = false;
                ErrMsg = string.Format(JCHVRF.Const.ValidationMessage.MorethanOneODU);
            }
            else if (project.CanvasODUList.Count == 1)
            {
                var SysVRF = project.CanvasODUList.FirstOrDefault();
                if (SysVRF.IsAuto == false && SysVRF.OutdoorItem==null)
                {
                    Result = false;
                    ErrMsg = string.Format("Please select outdoor item for : " + SysVRF.Name);
                }
                else if (SysVRF.SelOutdoorType == null)
                {
                    Result = false;
                    ErrMsg = string.Format("Please configure type property for : " + SysVRF.Name);
                }
                else if (SysVRF.Power == null || SysVRF.Power.Trim() == "")
                {
                    Result = false;
                    ErrMsg = string.Format("Please configure power property for : " + SysVRF.Name);
                }
                else if (SysVRF.MaxRatio == 0 )
                {
                    Result = false;
                    ErrMsg = string.Format("Please configure max ratio property for : " + SysVRF.Name);
                }
            }
            foreach (var indoor in project.RoomIndoorList)
            {
                if(indoor.IndoorItem == null || indoor.IndoorItem.Type == null || indoor.IndoorItem.ModelFull == null)
                {
                    Result = false;
                    ErrMsg = string.Format("Please configure property for Indoor : " + indoor.IndoorName);
                    break;
                }
            }
           
            return Result;

        }

    }
}
