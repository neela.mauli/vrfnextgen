﻿using JCHVRF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace JCHVRF_New.Model
{
    public class IDU : INotifyPropertyChanged
    {
        #region Private Members

        #endregion

        #region Properties
        public bool _isChecked;
        
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                RaisePropertyChanged("IsChecked");
            }
        }


        private RoomIndoor _roomIndoor;

        public RoomIndoor RoomIndoor
        {
            get { return _roomIndoor; }
            set {
                _roomIndoor = value;
            }
        }

        #endregion

        #region INotifyPropertyChanged
        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this._roomIndoor, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Ctor
        public IDU()
        {
            _roomIndoor = new RoomIndoor();
        }
        public IDU(RoomIndoor obj)
        {
            this._roomIndoor = obj;
            this.IsChecked = _roomIndoor.IsDelete;

        } 
        #endregion
    }
}
