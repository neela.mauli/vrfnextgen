﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FontAwesome.WPF;
using JCHVRF_New.Common.Helpers;

namespace JCHVRF_New.Model
{
    public class PipingInfoModel : ModelBase
    {
        #region Fields
        private string _description;

        private double _value;

        private double _min;

        private double _max;

        private string _longDescription;

        #endregion
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                this.SetValue(ref _description, value);
            }
        }
        public bool IsValid
        {
            get
            {
                return _value >= _min && _value<=_max;
            }
        }
        public double Value
        {
            get
            {
                return _value;
            }
            set
            {
                this.SetValue(ref _value, value);
                RaisePropertyChanged("IsValid");
            }
        }


        public double Min
        {
            get
            {
                return _min;
            }
            set
            {
                this.SetValue(ref _min, value);
                RaisePropertyChanged("IsValid");
            }
        }

        public double Max
        {
            get
            {
                return _max;
            }
            set
            {
                this.SetValue(ref _max, value);
                RaisePropertyChanged("IsValid");
            }
        }
        public string LongDescription
        {
            get
            {
                return _longDescription;
            }
            set
            {
                this.SetValue(ref _longDescription, value);
            }
        }
        
    }
}
