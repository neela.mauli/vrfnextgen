﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JCHVRF_New.Common.Helpers;

namespace JCHVRF_New.Model
{
    public class IDUODUCapacityDetails:ModelBase
    {
        #region Fields
        private double _ratedCapacity;
        private double _correctedCapacity;
        private double _actualRatio;
        private double _maxConnections;
        private double _minConnections;
        private double _additionalRefrigerantQty;
        #endregion


        #region Properties
        /// <summary>
        /// Gets or sets the RatedCapacity
        /// </summary>
        public double RatedCapacity
        {
            get { return _ratedCapacity; }
            set { this.SetValue(ref _ratedCapacity, value); }
        }

        /// <summary>
        /// Gets or sets the CorrectedCapacity
        /// </summary>
        public double CorrectedCapacity
        {
            get { return _correctedCapacity; }
            set { this.SetValue(ref _correctedCapacity, value); }
        }

        /// <summary>
        /// Gets or sets the ActualRatio
        /// </summary>
        public double ActualRatio
        {
            get { return _actualRatio; }
            set { this.SetValue(ref _actualRatio, value); }
        }

        /// <summary>
        /// Gets or sets the minConnections
        /// </summary>
        public double minConnections
        {
            get { return _minConnections; }
            set { this.SetValue(ref _minConnections, value); }
        }

        /// <summary>
        /// Gets or sets the maxConnections
        /// </summary>
        public double maxConnections
        {
            get { return _maxConnections; }
            set { this.SetValue(ref _maxConnections, value); }
        }

        /// <summary>
        /// Gets or sets the AdditionalRefrigerantQty
        /// </summary>
        public double AdditionalRefrigerantQty
        {
            get { return _additionalRefrigerantQty; }
            set { this.SetValue(ref _additionalRefrigerantQty, value); }
        }
        #endregion

    }

}
