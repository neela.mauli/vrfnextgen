﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCHVRF_New.Model
{
    public class AccessoryModel: INotifyPropertyChanged
    {
        public string SelectedType { get; set; }
        public string SelectedModelName { get; set; }        
        public string Type { get; set; }
        public string Model { get; set; }
        public List<string> ModelName { get; set; }
        public string Description { get; set; }
        public int MaxCount { get; set; }
        public int Count { get; set; }
        public bool IsSelect { get; set; }
        public bool IsApplyToSimilarUnit { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
