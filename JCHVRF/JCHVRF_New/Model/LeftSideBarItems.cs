﻿

namespace JCHVRF_New.Model
{
    using FontAwesome.WPF;
    using JCHVRF_New.Common.Helpers;
    using System.Collections.ObjectModel;

    #region Interfaces

    public interface ILeftSideBarItem
    {
        #region Properties

        /// <summary>
        /// Gets the Header
        /// </summary>
        string Header { get; }

        /// <summary>
        /// Gets the MenuHeader
        /// </summary>
        string MenuHeader { get; }

        #endregion
    }

    #endregion
    public class LeftSideBarChild : ModelBase, ILeftSideBarItem
    {
        #region Fields
        private ObservableCollection<LeftSideBarChild> _children;

        private string _header;

        private string _menuHeader;

        private FontAwesomeIcon _icon;

        #endregion

        #region Constructors
        public LeftSideBarChild(string header,string menuheader, FontAwesomeIcon icon)
        {
            _header = header;
            _menuHeader = menuheader;
            _icon = icon;
        }

        public string Header
        {
            get { return _header; }
            set
            {
                this.SetValue(ref _header, value);
                RaisePropertyChanged("MenuHeader");
            }
        }

        /// <summary>
        /// Gets or sets the Icon
        /// </summary>
        public FontAwesomeIcon Icon
        {
            get { return _icon; }
            set
            {
                this.SetValue(ref _icon, value);
            }
        }
        public string MenuHeader
        {
            get { return _header; }
        }

        #endregion

    }

    public class LeftSideBarItem: ModelBase, ILeftSideBarItem
    {
        #region Fields
        private ObservableCollection<LeftSideBarChild> _children;

        private string _header;

        //private FontAwesomeIcon _icon;

        #endregion

        #region Constructors
        public LeftSideBarItem(string header)
        {
            _header = header;
           // _icon = icon;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Children
        /// </summary>
        public ObservableCollection<LeftSideBarChild> Children
        {
            get { return _children; }
            set { this.SetValue(ref _children, value); }
        }

        /// <summary>
        /// Gets or sets the Header
        /// </summary>
        public string Header
        {
            get { return _header; }
            set
            {
                this.SetValue(ref _header, value);
                RaisePropertyChanged("MenuHeader");
            }
        }

        /// <summary>
        /// Gets or sets the Icon
        /// </summary>
        //public FontAwesomeIcon Icon
        //{
        //    get { return _icon; }
        //    set
        //    {
        //        this.SetValue(ref _icon, value);
        //    }
        //}

        /// <summary>
        /// Gets the MenuHeader
        /// </summary>
        public string MenuHeader
        {
            get { return _header; }
        }

        #endregion
    }
}
