﻿/****************************** File Header ******************************\
File Name:	SideBarItems.cs
Date Created:	2/7/2019
Description:	
\*************************************************************************/

namespace JCHVRF_New.Model
{
    using FontAwesome.WPF;
    using JCHVRF_New.Common.Helpers;
    using System.Collections.ObjectModel;

    #region Interfaces

    public interface ISideBarItem
    {
        #region Properties

        /// <summary>
        /// Gets the Header
        /// </summary>
        string Header { get; }

        /// <summary>
        /// Gets the MenuHeader
        /// </summary>
        string MenuHeader { get; }

        #endregion
    }

    #endregion

    public class SideBarChild : ModelBase, ISideBarItem
    {
        #region Fields

        private ObservableCollection<SideBarChild> _children;

        private string _header;

        private string _menuHeader;

        #endregion

        #region Constructors

        public SideBarChild(string header, string menuHeader)
        {
            _header = header;
            _menuHeader = menuHeader;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Children
        /// </summary>
        public ObservableCollection<SideBarChild> Children
        {
            get { return _children; }
            set { this.SetValue(ref _children, value); }
        }

        /// <summary>
        /// Gets or sets the Header
        /// </summary>
        public string Header
        {
            get { return _header; }
            set { this.SetValue(ref _header, value); }
        }

        /// <summary>
        /// Gets or sets the MenuHeader
        /// </summary>
        public string MenuHeader
        {
            get { return _menuHeader; }
            set { this.SetValue(ref _header, value); }
        }

        #endregion
    }

    public class SideBarItem : ModelBase, ISideBarItem
    {
        #region Fields

        private ObservableCollection<SideBarChild> _children;

        private string _header;

        private FontAwesomeIcon _icon;

        #endregion

        #region Constructors

        public SideBarItem(string header, FontAwesomeIcon icon)
        {
            _header = header;
            _icon = icon;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Children
        /// </summary>
        public ObservableCollection<SideBarChild> Children
        {
            get { return _children; }
            set { this.SetValue(ref _children, value); }
        }

        /// <summary>
        /// Gets or sets the Header
        /// </summary>
        public string Header
        {
            get { return _header; }
            set
            {
                this.SetValue(ref _header, value);
                RaisePropertyChanged("MenuHeader");
            }
        }

        /// <summary>
        /// Gets or sets the Icon
        /// </summary>
        public FontAwesomeIcon Icon
        {
            get { return _icon; }
            set
            {
                this.SetValue(ref _icon, value);
            }
        }

        /// <summary>
        /// Gets the MenuHeader
        /// </summary>
        public string MenuHeader
        {
            get { return _header; }
        }

        #endregion
    }
}
