﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.VisualStyles;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JCHVRF_New
{
    /// <summary>
    /// Interaction logic for AddBulkFloor.xaml
    /// </summary>
    public partial class AddBulkFloor : Window
    {
        private string floorCount = string.Empty;
        public bool bAddBulk;
        public AddBulkFloor()
        {
            InitializeComponent();
            Application curApp = Application.Current;
            Window mainWindow = curApp.MainWindow;
            this.Left = mainWindow.Left + (mainWindow.Width - this.ActualWidth) / 2;
            this.Top = mainWindow.Top + (mainWindow.Height - this.ActualHeight) / 2;
            bAddBulk = true;

        }
        private MainWindow _objMainWindow;
        public MainWindow ObMainWindow
        {
            get { return _objMainWindow; }
            set { _objMainWindow = value; }
        }

        private void btnBulkAddFloor_Click(object sender, RoutedEventArgs e)
        {
            MainWindow objMainWnd = new MainWindow();

            try
            {
                int iFloorValue = Int32.Parse(txtBulkAdd.Text);// + Int32.Parse(objMainWnd.FloorName);

                if (iFloorValue > 999)
                {
                    txtBulkAdd.Focus();
                    MessageBox.Show("Floor limit is 1000 !");
                }
                else
                {
                    floorCount = string.IsNullOrEmpty(txtBulkAdd.Text) ? "0" : txtBulkAdd.Text;
                    ObMainWindow.AddFloor(Convert.ToInt32(floorCount));
                    ObMainWindow.Tab.SelectedIndex = 3;
                    this.bAddBulk = false;
                    this.Close();
                }
            }
            catch (System.Exception)
            {
                MessageBox.Show("Floor number ranges between 1 and 1000!");
                txtBulkAdd.Focus();
            }

        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void BtnAddPopup_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {

                floorCount = string.IsNullOrEmpty(txtBulkAdd.Text) ? "0" : txtBulkAdd.Text;
                txtBulkAdd.Text = Convert.ToString(Convert.ToInt32(floorCount) + 1);
            }
            catch (System.Exception)
            {
                MessageBox.Show("Floor number ranges between 1 and 1000!");
            }
        }

        private void BtnMinusPopup_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                floorCount = string.IsNullOrEmpty(txtBulkAdd.Text) ? "0" : txtBulkAdd.Text;
                if (floorCount != "0")
                    txtBulkAdd.Text = Convert.ToString(Convert.ToInt32(floorCount) - 1);
            }
            catch (System.Exception)
            {
                MessageBox.Show("Floor number ranges between 1 and 1000!");
            }
        }

        private void btnBulkAddFloorCancel_Click(object sender, RoutedEventArgs e)
        {
            MainWindow objMainWnd = new MainWindow();
            ObMainWindow.AddFloor(0);
            this.Close();
        }
    }
}
